<?php
$tdataarm_po = array();
$tdataarm_po[".searchableFields"] = array();
$tdataarm_po[".ShortName"] = "arm_po";
$tdataarm_po[".OwnerID"] = "";
$tdataarm_po[".OriginalTable"] = "public.arm_po";


$tdataarm_po[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataarm_po[".originalPagesByType"] = $tdataarm_po[".pagesByType"];
$tdataarm_po[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataarm_po[".originalPages"] = $tdataarm_po[".pages"];
$tdataarm_po[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataarm_po[".originalDefaultPages"] = $tdataarm_po[".defaultPages"];

//	field labels
$fieldLabelsarm_po = array();
$fieldToolTipsarm_po = array();
$pageTitlesarm_po = array();
$placeHoldersarm_po = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsarm_po["Russian"] = array();
	$fieldToolTipsarm_po["Russian"] = array();
	$placeHoldersarm_po["Russian"] = array();
	$pageTitlesarm_po["Russian"] = array();
	$fieldLabelsarm_po["Russian"]["armpo_id"] = "ID ПО";
	$fieldToolTipsarm_po["Russian"]["armpo_id"] = "";
	$placeHoldersarm_po["Russian"]["armpo_id"] = "";
	$fieldLabelsarm_po["Russian"]["boxid"] = "Имя компьютера";
	$fieldToolTipsarm_po["Russian"]["boxid"] = "";
	$placeHoldersarm_po["Russian"]["boxid"] = "";
	$fieldLabelsarm_po["Russian"]["po_name"] = "Наименование";
	$fieldToolTipsarm_po["Russian"]["po_name"] = "";
	$placeHoldersarm_po["Russian"]["po_name"] = "";
	$fieldLabelsarm_po["Russian"]["po_version"] = "Уст. версия";
	$fieldToolTipsarm_po["Russian"]["po_version"] = "";
	$placeHoldersarm_po["Russian"]["po_version"] = "";
	$fieldLabelsarm_po["Russian"]["po_arch"] = "Арх.";
	$fieldToolTipsarm_po["Russian"]["po_arch"] = "";
	$placeHoldersarm_po["Russian"]["po_arch"] = "";
	$fieldLabelsarm_po["Russian"]["po_key"] = "Ключ";
	$fieldToolTipsarm_po["Russian"]["po_key"] = "";
	$placeHoldersarm_po["Russian"]["po_key"] = "";
	$fieldLabelsarm_po["Russian"]["po_desc"] = "Описание";
	$fieldToolTipsarm_po["Russian"]["po_desc"] = "";
	$placeHoldersarm_po["Russian"]["po_desc"] = "";
	$fieldLabelsarm_po["Russian"]["box_id"] = "Box Id";
	$fieldToolTipsarm_po["Russian"]["box_id"] = "";
	$placeHoldersarm_po["Russian"]["box_id"] = "";
	if (count($fieldToolTipsarm_po["Russian"]))
		$tdataarm_po[".isUseToolTips"] = true;
}


	$tdataarm_po[".NCSearch"] = true;



$tdataarm_po[".shortTableName"] = "arm_po";
$tdataarm_po[".nSecOptions"] = 0;

$tdataarm_po[".mainTableOwnerID"] = "";
$tdataarm_po[".entityType"] = 0;
$tdataarm_po[".connId"] = "itbase3_at_192_168_1_15";


$tdataarm_po[".strOriginalTableName"] = "public.arm_po";

	



$tdataarm_po[".showAddInPopup"] = false;

$tdataarm_po[".showEditInPopup"] = false;

$tdataarm_po[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataarm_po[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataarm_po[".listAjax"] = true;
//	temporary
$tdataarm_po[".listAjax"] = false;

	$tdataarm_po[".audit"] = true;

	$tdataarm_po[".locking"] = true;


$pages = $tdataarm_po[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataarm_po[".edit"] = true;
	$tdataarm_po[".afterEditAction"] = 1;
	$tdataarm_po[".closePopupAfterEdit"] = 1;
	$tdataarm_po[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataarm_po[".add"] = true;
$tdataarm_po[".afterAddAction"] = 1;
$tdataarm_po[".closePopupAfterAdd"] = 1;
$tdataarm_po[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataarm_po[".list"] = true;
}



$tdataarm_po[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataarm_po[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataarm_po[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataarm_po[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataarm_po[".printFriendly"] = true;
}



$tdataarm_po[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataarm_po[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataarm_po[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataarm_po[".isUseAjaxSuggest"] = true;

$tdataarm_po[".rowHighlite"] = true;





$tdataarm_po[".ajaxCodeSnippetAdded"] = false;

$tdataarm_po[".buttonsAdded"] = false;

$tdataarm_po[".addPageEvents"] = false;

// use timepicker for search panel
$tdataarm_po[".isUseTimeForSearch"] = false;


$tdataarm_po[".badgeColor"] = "db7093";


$tdataarm_po[".allSearchFields"] = array();
$tdataarm_po[".filterFields"] = array();
$tdataarm_po[".requiredSearchFields"] = array();

$tdataarm_po[".googleLikeFields"] = array();
$tdataarm_po[".googleLikeFields"][] = "armpo_id";
$tdataarm_po[".googleLikeFields"][] = "boxid";
$tdataarm_po[".googleLikeFields"][] = "po_name";
$tdataarm_po[".googleLikeFields"][] = "po_version";
$tdataarm_po[".googleLikeFields"][] = "po_arch";
$tdataarm_po[".googleLikeFields"][] = "po_key";
$tdataarm_po[".googleLikeFields"][] = "po_desc";
$tdataarm_po[".googleLikeFields"][] = "box_id";



$tdataarm_po[".tableType"] = "list";

$tdataarm_po[".printerPageOrientation"] = 0;
$tdataarm_po[".nPrinterPageScale"] = 100;

$tdataarm_po[".nPrinterSplitRecords"] = 40;

$tdataarm_po[".geocodingEnabled"] = false;




$tdataarm_po[".isDisplayLoading"] = true;

$tdataarm_po[".isResizeColumns"] = true;





$tdataarm_po[".pageSize"] = 20;

$tdataarm_po[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataarm_po[".strOrderBy"] = $tstrOrderBy;

$tdataarm_po[".orderindexes"] = array();


$tdataarm_po[".sqlHead"] = "SELECT armpo_id,  	boxid,  	po_name,  	po_version,  	po_arch,  	po_key,  	po_desc,  	box_id";
$tdataarm_po[".sqlFrom"] = "FROM \"public\".arm_po";
$tdataarm_po[".sqlWhereExpr"] = "";
$tdataarm_po[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",
	'showRowCount' => 0,
	'hideEmpty' => 0,
);
$tdataarm_po[".arrGridTabs"] = $arrGridTabs;









//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataarm_po[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataarm_po[".arrGroupsPerPage"] = $arrGPP;

$tdataarm_po[".highlightSearchResults"] = true;

$tableKeysarm_po = array();
$tableKeysarm_po[] = "armpo_id";
$tdataarm_po[".Keys"] = $tableKeysarm_po;


$tdataarm_po[".hideMobileList"] = array();




//	armpo_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "armpo_id";
	$fdata["GoodName"] = "armpo_id";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","armpo_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "armpo_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "armpo_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["armpo_id"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "armpo_id";
//	boxid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "boxid";
	$fdata["GoodName"] = "boxid";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","boxid");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "boxid";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "boxid";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_box";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "box_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "box_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["boxid"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "boxid";
//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","po_name");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_po";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "po_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "po_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "po_key";

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["po_name"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "po_name";
//	po_version
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "po_version";
	$fdata["GoodName"] = "po_version";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","po_version");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_version";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_version";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["po_version"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "po_version";
//	po_arch
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "po_arch";
	$fdata["GoodName"] = "po_arch";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","po_arch");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_arch";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_arch";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["po_arch"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "po_arch";
//	po_key
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "po_key";
	$fdata["GoodName"] = "po_key";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","po_key");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "po_key";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_key";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_license";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "lic_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "lic_key";

	

	
	$edata["LookupOrderBy"] = "";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "po_name", "lookup" => "lic_poname" );

	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["po_key"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "po_key";
//	po_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "po_desc";
	$fdata["GoodName"] = "po_desc";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","po_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["po_desc"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "po_desc";
//	box_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "box_id";
	$fdata["GoodName"] = "box_id";
	$fdata["ownerTable"] = "public.arm_po";
	$fdata["Label"] = GetFieldLabel("public_arm_po","box_id");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=1024";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm_po["box_id"] = $fdata;
		$tdataarm_po[".searchableFields"][] = "box_id";


$tables_data["public.arm_po"]=&$tdataarm_po;
$field_labels["public_arm_po"] = &$fieldLabelsarm_po;
$fieldToolTips["public_arm_po"] = &$fieldToolTipsarm_po;
$placeHolders["public_arm_po"] = &$placeHoldersarm_po;
$page_titles["public_arm_po"] = &$pageTitlesarm_po;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.arm_po"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.arm_po"] = array();



	
				$strOriginalDetailsTable="public.hw_box";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.hw_box";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "hw_box";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.arm_po"][0] = $masterParams;
				$masterTablesData["public.arm_po"][0]["masterKeys"] = array();
	$masterTablesData["public.arm_po"][0]["masterKeys"][]="box_id";
				$masterTablesData["public.arm_po"][0]["detailKeys"] = array();
	$masterTablesData["public.arm_po"][0]["detailKeys"][]="boxid";
		
	
				$strOriginalDetailsTable="public.spr_po";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_po";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_po";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.arm_po"][1] = $masterParams;
				$masterTablesData["public.arm_po"][1]["masterKeys"] = array();
	$masterTablesData["public.arm_po"][1]["masterKeys"][]="po_id";
				$masterTablesData["public.arm_po"][1]["detailKeys"] = array();
	$masterTablesData["public.arm_po"][1]["detailKeys"][]="po_name";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_arm_po()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "armpo_id,  	boxid,  	po_name,  	po_version,  	po_arch,  	po_key,  	po_desc,  	box_id";
$proto0["m_strFrom"] = "FROM \"public\".arm_po";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "armpo_id",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto6["m_sql"] = "armpo_id";
$proto6["m_srcTableName"] = "public.arm_po";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "boxid",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto8["m_sql"] = "boxid";
$proto8["m_srcTableName"] = "public.arm_po";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto10["m_sql"] = "po_name";
$proto10["m_srcTableName"] = "public.arm_po";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "po_version",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto12["m_sql"] = "po_version";
$proto12["m_srcTableName"] = "public.arm_po";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "po_arch",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto14["m_sql"] = "po_arch";
$proto14["m_srcTableName"] = "public.arm_po";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "po_key",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto16["m_sql"] = "po_key";
$proto16["m_srcTableName"] = "public.arm_po";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "po_desc",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto18["m_sql"] = "po_desc";
$proto18["m_srcTableName"] = "public.arm_po";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "box_id",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "public.arm_po"
));

$proto20["m_sql"] = "box_id";
$proto20["m_srcTableName"] = "public.arm_po";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "public.arm_po";
$proto23["m_srcTableName"] = "public.arm_po";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "armpo_id";
$proto23["m_columns"][] = "boxid";
$proto23["m_columns"][] = "po_name";
$proto23["m_columns"][] = "po_version";
$proto23["m_columns"][] = "po_arch";
$proto23["m_columns"][] = "po_key";
$proto23["m_columns"][] = "po_desc";
$proto23["m_columns"][] = "box_id";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "\"public\".arm_po";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "public.arm_po";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.arm_po";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_arm_po = createSqlQuery_arm_po();


	
		;

								

$tdataarm_po[".sqlquery"] = $queryData_arm_po;



$tableEvents["public.arm_po"] = new eventsBase;
$tdataarm_po[".hasEvents"] = false;

?>