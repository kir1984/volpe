<?php
$tdataarm = array();
$tdataarm[".searchableFields"] = array();
$tdataarm[".ShortName"] = "arm";
$tdataarm[".OwnerID"] = "";
$tdataarm[".OriginalTable"] = "public.arm";


$tdataarm[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataarm[".originalPagesByType"] = $tdataarm[".pagesByType"];
$tdataarm[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataarm[".originalPages"] = $tdataarm[".pages"];
$tdataarm[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataarm[".originalDefaultPages"] = $tdataarm[".defaultPages"];

//	field labels
$fieldLabelsarm = array();
$fieldToolTipsarm = array();
$pageTitlesarm = array();
$placeHoldersarm = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsarm["Russian"] = array();
	$fieldToolTipsarm["Russian"] = array();
	$placeHoldersarm["Russian"] = array();
	$pageTitlesarm["Russian"] = array();
	$fieldLabelsarm["Russian"]["arm_id"] = "Arm Id";
	$fieldToolTipsarm["Russian"]["arm_id"] = "";
	$placeHoldersarm["Russian"]["arm_id"] = "";
	$fieldLabelsarm["Russian"]["box_id"] = "Имя компьютера";
	$fieldToolTipsarm["Russian"]["box_id"] = "Все поля используют ajax-поиск";
	$placeHoldersarm["Russian"]["box_id"] = "";
	$fieldLabelsarm["Russian"]["tel_id"] = "Телефон";
	$fieldToolTipsarm["Russian"]["tel_id"] = "";
	$placeHoldersarm["Russian"]["tel_id"] = "";
	$fieldLabelsarm["Russian"]["printer_id"] = "Принтер";
	$fieldToolTipsarm["Russian"]["printer_id"] = "";
	$placeHoldersarm["Russian"]["printer_id"] = "";
	$fieldLabelsarm["Russian"]["sotrudnik_id"] = "Сотрудник";
	$fieldToolTipsarm["Russian"]["sotrudnik_id"] = "";
	$placeHoldersarm["Russian"]["sotrudnik_id"] = "";
	$fieldLabelsarm["Russian"]["loc_id"] = "Кабинет";
	$fieldToolTipsarm["Russian"]["loc_id"] = "";
	$placeHoldersarm["Russian"]["loc_id"] = "";
	$fieldLabelsarm["Russian"]["socket_id"] = "Розетка";
	$fieldToolTipsarm["Russian"]["socket_id"] = "";
	$placeHoldersarm["Russian"]["socket_id"] = "";
	if (count($fieldToolTipsarm["Russian"]))
		$tdataarm[".isUseToolTips"] = true;
}


	$tdataarm[".NCSearch"] = true;



$tdataarm[".shortTableName"] = "arm";
$tdataarm[".nSecOptions"] = 0;

$tdataarm[".mainTableOwnerID"] = "";
$tdataarm[".entityType"] = 0;
$tdataarm[".connId"] = "itbase3_at_192_168_1_15";


$tdataarm[".strOriginalTableName"] = "public.arm";

	



$tdataarm[".showAddInPopup"] = false;

$tdataarm[".showEditInPopup"] = false;

$tdataarm[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataarm[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataarm[".listAjax"] = true;
//	temporary
$tdataarm[".listAjax"] = false;

	$tdataarm[".audit"] = true;

	$tdataarm[".locking"] = true;


$pages = $tdataarm[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataarm[".edit"] = true;
	$tdataarm[".afterEditAction"] = 1;
	$tdataarm[".closePopupAfterEdit"] = 1;
	$tdataarm[".afterEditActionDetTable"] = "public.hw_box";
}

if( $pages[PAGE_ADD] ) {
$tdataarm[".add"] = true;
$tdataarm[".afterAddAction"] = 1;
$tdataarm[".closePopupAfterAdd"] = 1;
$tdataarm[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataarm[".list"] = true;
}



$tdataarm[".strSortControlSettingsJSON"] = "[]";

$tdataarm[".strClickActionJSON"] = "{\"fields\":{\"arm_id\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":\"public.hw_box\"},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":\"public.hw_box\",\"url\":\"\"}},\"box_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"loc_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"printer_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"socket_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"sotrudnik_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"tel_id\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"noaction\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":\"public.hw_box\"},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":\"public.hw_box\",\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdataarm[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataarm[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataarm[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataarm[".printFriendly"] = true;
}



$tdataarm[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataarm[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataarm[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataarm[".isUseAjaxSuggest"] = true;

$tdataarm[".rowHighlite"] = true;





$tdataarm[".ajaxCodeSnippetAdded"] = false;

$tdataarm[".buttonsAdded"] = false;

$tdataarm[".addPageEvents"] = false;

// use timepicker for search panel
$tdataarm[".isUseTimeForSearch"] = false;


$tdataarm[".badgeColor"] = "bc8f8f";


$tdataarm[".allSearchFields"] = array();
$tdataarm[".filterFields"] = array();
$tdataarm[".requiredSearchFields"] = array();

$tdataarm[".googleLikeFields"] = array();
$tdataarm[".googleLikeFields"][] = "arm_id";
$tdataarm[".googleLikeFields"][] = "box_id";
$tdataarm[".googleLikeFields"][] = "tel_id";
$tdataarm[".googleLikeFields"][] = "printer_id";
$tdataarm[".googleLikeFields"][] = "sotrudnik_id";
$tdataarm[".googleLikeFields"][] = "loc_id";
$tdataarm[".googleLikeFields"][] = "socket_id";



$tdataarm[".tableType"] = "list";

$tdataarm[".printerPageOrientation"] = 0;
$tdataarm[".nPrinterPageScale"] = 100;

$tdataarm[".nPrinterSplitRecords"] = 40;

$tdataarm[".geocodingEnabled"] = false;




$tdataarm[".isDisplayLoading"] = true;

$tdataarm[".isResizeColumns"] = true;





$tdataarm[".pageSize"] = 30;

$tdataarm[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataarm[".strOrderBy"] = $tstrOrderBy;

$tdataarm[".orderindexes"] = array();


$tdataarm[".sqlHead"] = "SELECT arm_id,  	box_id,  	tel_id,  	printer_id,  	sotrudnik_id,  	loc_id,  	socket_id";
$tdataarm[".sqlFrom"] = "FROM \"public\".arm";
$tdataarm[".sqlWhereExpr"] = "";
$tdataarm[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",
	'showRowCount' => 0,
	'hideEmpty' => 0,
);
$tdataarm[".arrGridTabs"] = $arrGridTabs;









//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataarm[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataarm[".arrGroupsPerPage"] = $arrGPP;

$tdataarm[".highlightSearchResults"] = true;

$tableKeysarm = array();
$tableKeysarm[] = "arm_id";
$tdataarm[".Keys"] = $tableKeysarm;


$tdataarm[".hideMobileList"] = array();




//	arm_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "arm_id";
	$fdata["GoodName"] = "arm_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","arm_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "arm_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arm_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["arm_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "arm_id";
//	box_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "box_id";
	$fdata["GoodName"] = "box_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","box_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "box_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_box";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "box_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "box_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["box_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "box_id";
//	tel_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "tel_id";
	$fdata["GoodName"] = "tel_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","tel_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "tel_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_tel";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "tel_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tel_number";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["tel_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "tel_id";
//	printer_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "printer_id";
	$fdata["GoodName"] = "printer_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","printer_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "printer_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_printer";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "printer_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "printer_name || ' ' || printer_inv";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["printer_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "printer_id";
//	sotrudnik_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "sotrudnik_id";
	$fdata["GoodName"] = "sotrudnik_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","sotrudnik_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "sotrudnik_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["sotrudnik_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "sotrudnik_id";
//	loc_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "loc_id";
	$fdata["GoodName"] = "loc_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","loc_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "loc_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "loc_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_location";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "location_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "location_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["loc_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "loc_id";
//	socket_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "socket_id";
	$fdata["GoodName"] = "socket_id";
	$fdata["ownerTable"] = "public.arm";
	$fdata["Label"] = GetFieldLabel("public_arm","socket_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "socket_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "socket_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_socket";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "socket_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "socket_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataarm["socket_id"] = $fdata;
		$tdataarm[".searchableFields"][] = "socket_id";


$tables_data["public.arm"]=&$tdataarm;
$field_labels["public_arm"] = &$fieldLabelsarm;
$fieldToolTips["public_arm"] = &$fieldToolTipsarm;
$placeHolders["public_arm"] = &$placeHoldersarm;
$page_titles["public_arm"] = &$pageTitlesarm;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.arm"] = array();
//	public.hw_box
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_box";
		$detailsParam["dOriginalTable"] = "public.hw_box";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_box";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_box");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.arm"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.arm"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["masterKeys"][]="box_id";

				$detailsTablesData["public.arm"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["detailKeys"][]="box_id";
//	public.sotrudnik
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.sotrudnik";
		$detailsParam["dOriginalTable"] = "public.sotrudnik";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "sotrudnik";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_sotrudnik");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.arm"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.arm"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.arm"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["detailKeys"][]="sotrudnik_id";
//	public.hw_printer
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_printer";
		$detailsParam["dOriginalTable"] = "public.hw_printer";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_printer";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_printer");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.arm"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.arm"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["masterKeys"][]="printer_id";

				$detailsTablesData["public.arm"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["detailKeys"][]="printer_id";
//	public.hw_tel
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_tel";
		$detailsParam["dOriginalTable"] = "public.hw_tel";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_tel";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_tel");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.arm"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.arm"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["masterKeys"][]="tel_id";

				$detailsTablesData["public.arm"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.arm"][$dIndex]["detailKeys"][]="tel_id";

// tables which are master tables for current table (detail)
$masterTablesData["public.arm"] = array();



	
				$strOriginalDetailsTable="public.spr_location";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_location";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_location";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.arm"][0] = $masterParams;
				$masterTablesData["public.arm"][0]["masterKeys"] = array();
	$masterTablesData["public.arm"][0]["masterKeys"][]="location_id";
				$masterTablesData["public.arm"][0]["detailKeys"] = array();
	$masterTablesData["public.arm"][0]["detailKeys"][]="loc_id";
		
	
				$strOriginalDetailsTable="public.spr_socket";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_socket";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_socket";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.arm"][1] = $masterParams;
				$masterTablesData["public.arm"][1]["masterKeys"] = array();
	$masterTablesData["public.arm"][1]["masterKeys"][]="socket_id";
				$masterTablesData["public.arm"][1]["detailKeys"] = array();
	$masterTablesData["public.arm"][1]["detailKeys"][]="socket_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_arm()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "arm_id,  	box_id,  	tel_id,  	printer_id,  	sotrudnik_id,  	loc_id,  	socket_id";
$proto0["m_strFrom"] = "FROM \"public\".arm";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "arm_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto6["m_sql"] = "arm_id";
$proto6["m_srcTableName"] = "public.arm";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "box_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto8["m_sql"] = "box_id";
$proto8["m_srcTableName"] = "public.arm";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto10["m_sql"] = "tel_id";
$proto10["m_srcTableName"] = "public.arm";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto12["m_sql"] = "printer_id";
$proto12["m_srcTableName"] = "public.arm";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto14["m_sql"] = "sotrudnik_id";
$proto14["m_srcTableName"] = "public.arm";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "loc_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto16["m_sql"] = "loc_id";
$proto16["m_srcTableName"] = "public.arm";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "socket_id",
	"m_strTable" => "public.arm",
	"m_srcTableName" => "public.arm"
));

$proto18["m_sql"] = "socket_id";
$proto18["m_srcTableName"] = "public.arm";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "public.arm";
$proto21["m_srcTableName"] = "public.arm";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "arm_id";
$proto21["m_columns"][] = "box_id";
$proto21["m_columns"][] = "tel_id";
$proto21["m_columns"][] = "printer_id";
$proto21["m_columns"][] = "sotrudnik_id";
$proto21["m_columns"][] = "loc_id";
$proto21["m_columns"][] = "socket_id";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "\"public\".arm";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.arm";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.arm";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_arm = createSqlQuery_arm();


	
		;

							

$tdataarm[".sqlquery"] = $queryData_arm;



$tableEvents["public.arm"] = new eventsBase;
$tdataarm[".hasEvents"] = false;

?>