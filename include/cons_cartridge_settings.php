<?php
$tdatacons_cartridge = array();
$tdatacons_cartridge[".searchableFields"] = array();
$tdatacons_cartridge[".ShortName"] = "cons_cartridge";
$tdatacons_cartridge[".OwnerID"] = "";
$tdatacons_cartridge[".OriginalTable"] = "public.cons_cartridge";


$tdatacons_cartridge[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatacons_cartridge[".originalPagesByType"] = $tdatacons_cartridge[".pagesByType"];
$tdatacons_cartridge[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatacons_cartridge[".originalPages"] = $tdatacons_cartridge[".pages"];
$tdatacons_cartridge[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacons_cartridge[".originalDefaultPages"] = $tdatacons_cartridge[".defaultPages"];

//	field labels
$fieldLabelscons_cartridge = array();
$fieldToolTipscons_cartridge = array();
$pageTitlescons_cartridge = array();
$placeHolderscons_cartridge = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelscons_cartridge["Russian"] = array();
	$fieldToolTipscons_cartridge["Russian"] = array();
	$placeHolderscons_cartridge["Russian"] = array();
	$pageTitlescons_cartridge["Russian"] = array();
	$fieldLabelscons_cartridge["Russian"]["cartridge_name"] = "Наименование";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_name"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_name"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_desc"] = "Описание или комментарии";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_desc"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_desc"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_inv"] = "Инв.номер";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_inv"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_inv"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_inprinterid"] = "В принтере:";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_inprinterid"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_inprinterid"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_storage"] = "Местонахождение";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_storage"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_storage"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_status"] = "Статус";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_status"] = "При добавлении новой записи это поле не заполняется. Присваивается значение по умолчанию \"Заправлен\"";
	$placeHolderscons_cartridge["Russian"]["cartridge_status"] = "";
	$fieldLabelscons_cartridge["Russian"]["cartridge_type"] = "Тип";
	$fieldToolTipscons_cartridge["Russian"]["cartridge_type"] = "";
	$placeHolderscons_cartridge["Russian"]["cartridge_type"] = "";
	$fieldLabelscons_cartridge["Russian"]["conscart_id"] = "Conscart Id";
	$fieldToolTipscons_cartridge["Russian"]["conscart_id"] = "";
	$placeHolderscons_cartridge["Russian"]["conscart_id"] = "";
	if (count($fieldToolTipscons_cartridge["Russian"]))
		$tdatacons_cartridge[".isUseToolTips"] = true;
}


	$tdatacons_cartridge[".NCSearch"] = true;



$tdatacons_cartridge[".shortTableName"] = "cons_cartridge";
$tdatacons_cartridge[".nSecOptions"] = 0;

$tdatacons_cartridge[".mainTableOwnerID"] = "";
$tdatacons_cartridge[".entityType"] = 0;
$tdatacons_cartridge[".connId"] = "itbase3_at_192_168_1_15";


$tdatacons_cartridge[".strOriginalTableName"] = "public.cons_cartridge";

	



$tdatacons_cartridge[".showAddInPopup"] = false;

$tdatacons_cartridge[".showEditInPopup"] = false;

$tdatacons_cartridge[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacons_cartridge[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatacons_cartridge[".listAjax"] = true;
//	temporary
$tdatacons_cartridge[".listAjax"] = false;

	$tdatacons_cartridge[".audit"] = false;

	$tdatacons_cartridge[".locking"] = false;


$pages = $tdatacons_cartridge[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacons_cartridge[".edit"] = true;
	$tdatacons_cartridge[".afterEditAction"] = 1;
	$tdatacons_cartridge[".closePopupAfterEdit"] = 1;
	$tdatacons_cartridge[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacons_cartridge[".add"] = true;
$tdatacons_cartridge[".afterAddAction"] = 1;
$tdatacons_cartridge[".closePopupAfterAdd"] = 1;
$tdatacons_cartridge[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacons_cartridge[".list"] = true;
}



$tdatacons_cartridge[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatacons_cartridge[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacons_cartridge[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacons_cartridge[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacons_cartridge[".printFriendly"] = true;
}



$tdatacons_cartridge[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacons_cartridge[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacons_cartridge[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacons_cartridge[".isUseAjaxSuggest"] = true;

$tdatacons_cartridge[".rowHighlite"] = true;





$tdatacons_cartridge[".ajaxCodeSnippetAdded"] = false;

$tdatacons_cartridge[".buttonsAdded"] = false;

$tdatacons_cartridge[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacons_cartridge[".isUseTimeForSearch"] = false;


$tdatacons_cartridge[".badgeColor"] = "7B68EE";


$tdatacons_cartridge[".allSearchFields"] = array();
$tdatacons_cartridge[".filterFields"] = array();
$tdatacons_cartridge[".requiredSearchFields"] = array();

$tdatacons_cartridge[".googleLikeFields"] = array();
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_type";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_name";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_desc";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_inv";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_inprinterid";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_storage";
$tdatacons_cartridge[".googleLikeFields"][] = "cartridge_status";
$tdatacons_cartridge[".googleLikeFields"][] = "conscart_id";



$tdatacons_cartridge[".tableType"] = "list";

$tdatacons_cartridge[".printerPageOrientation"] = 0;
$tdatacons_cartridge[".nPrinterPageScale"] = 100;

$tdatacons_cartridge[".nPrinterSplitRecords"] = 40;

$tdatacons_cartridge[".geocodingEnabled"] = false;




$tdatacons_cartridge[".isDisplayLoading"] = true;

$tdatacons_cartridge[".isResizeColumns"] = true;





$tdatacons_cartridge[".pageSize"] = 20;

$tdatacons_cartridge[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacons_cartridge[".strOrderBy"] = $tstrOrderBy;

$tdatacons_cartridge[".orderindexes"] = array();


$tdatacons_cartridge[".sqlHead"] = "SELECT cartridge_type,  cartridge_name,  cartridge_desc,  cartridge_inv,  cartridge_inprinterid,  cartridge_storage,  cartridge_status,  conscart_id";
$tdatacons_cartridge[".sqlFrom"] = "FROM \"public\".cons_cartridge";
$tdatacons_cartridge[".sqlWhereExpr"] = "";
$tdatacons_cartridge[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacons_cartridge[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacons_cartridge[".arrGroupsPerPage"] = $arrGPP;

$tdatacons_cartridge[".highlightSearchResults"] = true;

$tableKeyscons_cartridge = array();
$tableKeyscons_cartridge[] = "conscart_id";
$tdatacons_cartridge[".Keys"] = $tableKeyscons_cartridge;


$tdatacons_cartridge[".hideMobileList"] = array();




//	cartridge_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "cartridge_type";
	$fdata["GoodName"] = "cartridge_type";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_type");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "cartridge_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_type"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_type";
//	cartridge_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "cartridge_name";
	$fdata["GoodName"] = "cartridge_name";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "cartridge_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_name"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_name";
//	cartridge_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "cartridge_desc";
	$fdata["GoodName"] = "cartridge_desc";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "cartridge_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=1024";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_desc"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_desc";
//	cartridge_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cartridge_inv";
	$fdata["GoodName"] = "cartridge_inv";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_inv");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "cartridge_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_inv"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_inv";
//	cartridge_inprinterid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "cartridge_inprinterid";
	$fdata["GoodName"] = "cartridge_inprinterid";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_inprinterid");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "cartridge_inprinterid";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_inprinterid";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_printer";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "printer_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "printer_inv || ' ' || printer_name";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_inprinterid"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_inprinterid";
//	cartridge_storage
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "cartridge_storage";
	$fdata["GoodName"] = "cartridge_storage";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_storage");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "cartridge_storage";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_storage";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_location";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "location_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "location_name";

				$edata["LookupWhere"] = "location_storagestatus=true";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_storage"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_storage";
//	cartridge_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "cartridge_status";
	$fdata["GoodName"] = "cartridge_status";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","cartridge_status");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "cartridge_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cartridge_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Используется";
	$edata["LookupValues"][] = "Пустой";
	$edata["LookupValues"][] = "Требуется ремонт";
	$edata["LookupValues"][] = "Заправлен";
	$edata["LookupValues"][] = "Восстановлен";
	$edata["LookupValues"][] = "Неремонтопригоден";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
		$fdata["filterTotalFields"] = "cartridge_inprinterid";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["cartridge_status"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "cartridge_status";
//	conscart_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "conscart_id";
	$fdata["GoodName"] = "conscart_id";
	$fdata["ownerTable"] = "public.cons_cartridge";
	$fdata["Label"] = GetFieldLabel("public_cons_cartridge","conscart_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "conscart_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "conscart_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacons_cartridge["conscart_id"] = $fdata;
		$tdatacons_cartridge[".searchableFields"][] = "conscart_id";


$tables_data["public.cons_cartridge"]=&$tdatacons_cartridge;
$field_labels["public_cons_cartridge"] = &$fieldLabelscons_cartridge;
$fieldToolTips["public_cons_cartridge"] = &$fieldToolTipscons_cartridge;
$placeHolders["public_cons_cartridge"] = &$placeHolderscons_cartridge;
$page_titles["public_cons_cartridge"] = &$pageTitlescons_cartridge;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.cons_cartridge"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.cons_cartridge"] = array();



	
				$strOriginalDetailsTable="public.hw_printer";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.hw_printer";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "hw_printer";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.cons_cartridge"][0] = $masterParams;
				$masterTablesData["public.cons_cartridge"][0]["masterKeys"] = array();
	$masterTablesData["public.cons_cartridge"][0]["masterKeys"][]="printer_id";
				$masterTablesData["public.cons_cartridge"][0]["detailKeys"] = array();
	$masterTablesData["public.cons_cartridge"][0]["detailKeys"][]="cartridge_inprinterid";
		
	
				$strOriginalDetailsTable="public.spr_location";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_location";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_location";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.cons_cartridge"][1] = $masterParams;
				$masterTablesData["public.cons_cartridge"][1]["masterKeys"] = array();
	$masterTablesData["public.cons_cartridge"][1]["masterKeys"][]="location_id";
				$masterTablesData["public.cons_cartridge"][1]["detailKeys"] = array();
	$masterTablesData["public.cons_cartridge"][1]["detailKeys"][]="cartridge_storage";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_cons_cartridge()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "cartridge_type,  cartridge_name,  cartridge_desc,  cartridge_inv,  cartridge_inprinterid,  cartridge_storage,  cartridge_status,  conscart_id";
$proto0["m_strFrom"] = "FROM \"public\".cons_cartridge";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_type",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto6["m_sql"] = "cartridge_type";
$proto6["m_srcTableName"] = "public.cons_cartridge";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_name",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto8["m_sql"] = "cartridge_name";
$proto8["m_srcTableName"] = "public.cons_cartridge";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_desc",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto10["m_sql"] = "cartridge_desc";
$proto10["m_srcTableName"] = "public.cons_cartridge";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_inv",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto12["m_sql"] = "cartridge_inv";
$proto12["m_srcTableName"] = "public.cons_cartridge";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_inprinterid",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto14["m_sql"] = "cartridge_inprinterid";
$proto14["m_srcTableName"] = "public.cons_cartridge";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_storage",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto16["m_sql"] = "cartridge_storage";
$proto16["m_srcTableName"] = "public.cons_cartridge";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "cartridge_status",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto18["m_sql"] = "cartridge_status";
$proto18["m_srcTableName"] = "public.cons_cartridge";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "conscart_id",
	"m_strTable" => "public.cons_cartridge",
	"m_srcTableName" => "public.cons_cartridge"
));

$proto20["m_sql"] = "conscart_id";
$proto20["m_srcTableName"] = "public.cons_cartridge";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "public.cons_cartridge";
$proto23["m_srcTableName"] = "public.cons_cartridge";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "conscart_id";
$proto23["m_columns"][] = "cartridge_type";
$proto23["m_columns"][] = "cartridge_name";
$proto23["m_columns"][] = "cartridge_desc";
$proto23["m_columns"][] = "cartridge_inv";
$proto23["m_columns"][] = "cartridge_inprinterid";
$proto23["m_columns"][] = "cartridge_storage";
$proto23["m_columns"][] = "cartridge_status";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "\"public\".cons_cartridge";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "public.cons_cartridge";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.cons_cartridge";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cons_cartridge = createSqlQuery_cons_cartridge();


	
		;

								

$tdatacons_cartridge[".sqlquery"] = $queryData_cons_cartridge;



$tableEvents["public.cons_cartridge"] = new eventsBase;
$tdatacons_cartridge[".hasEvents"] = false;

?>