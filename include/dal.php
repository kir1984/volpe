<?php

$dal_info = array();

/**
 * User API function
 * @param String sql
 */
function UsersTableName()
{
	global $cman;
	$connection = $cman->getForLogin();
	return $connection->addTableWrappers("public.sys_users");
}

/**
 * User API function
 * It uses the default db connection
 * @param String dalSQL
 */
function CustomQuery($dalSQL)
{
	$connection = getDefaultConnection();
	$result = $connection->query( $dalSQL );
	if($result)
		return $result;
}

/**
 * User API function
 * It uses the default db connection 
 * @param String sql
 */
function DBLookup($sql)
{
	$connection = getDefaultConnection();
	$data = $connection->query( $sql )->fetchAssoc();

	if( $data )
		return reset($data);
	  
	return null;
}

/**
  * Data Access Layer.
  */
class tDAL
{
	var $tblitbase3_at_192_168_1_15_public_arm;
	var $tblitbase3_at_192_168_1_15_public_arm_po;
	var $tblitbase3_at_192_168_1_15_public_cons_cartridge;
	var $tblitbase3_at_192_168_1_15_public_doc_in;
	var $tblitbase3_at_192_168_1_15_public_doc_int;
	var $tblitbase3_at_192_168_1_15_public_doc_out;
	var $tblitbase3_at_192_168_1_15_public_ecp;
	var $tblitbase3_at_192_168_1_15_public_hw_box;
	var $tblitbase3_at_192_168_1_15_public_hw_ibp;
	var $tblitbase3_at_192_168_1_15_public_hw_monitor1;
	var $tblitbase3_at_192_168_1_15_public_hw_printer;
	var $tblitbase3_at_192_168_1_15_public_hw_switch;
	var $tblitbase3_at_192_168_1_15_public_hw_tel;
	var $tblitbase3_at_192_168_1_15_public_license_used;
	var $tblitbase3_at_192_168_1_15_public_sec_uggroups;
	var $tblitbase3_at_192_168_1_15_public_sec_ugmembers;
	var $tblitbase3_at_192_168_1_15_public_sec_ugrights;
	var $tblitbase3_at_192_168_1_15_public_service_refilling;
	var $tblitbase3_at_192_168_1_15_public_sotrudnik;
	var $tblitbase3_at_192_168_1_15_public_spr_department;
	var $tblitbase3_at_192_168_1_15_public_spr_doc_type;
	var $tblitbase3_at_192_168_1_15_public_spr_docint_type;
	var $tblitbase3_at_192_168_1_15_public_spr_ecp;
	var $tblitbase3_at_192_168_1_15_public_spr_license;
	var $tblitbase3_at_192_168_1_15_public_spr_license_dogovor;
	var $tblitbase3_at_192_168_1_15_public_spr_location;
	var $tblitbase3_at_192_168_1_15_public_spr_partners;
	var $tblitbase3_at_192_168_1_15_public_spr_po;
	var $tblitbase3_at_192_168_1_15_public_spr_socket;
	var $tblitbase3_at_192_168_1_15_public_sys_audit;
	var $tblitbase3_at_192_168_1_15_public_sys_locking;
	var $tblitbase3_at_192_168_1_15_public_sys_settings;
	var $tblitbase3_at_192_168_1_15_public_sys_users;
	var $lstTables;
	var $Table = array();

	function FillTablesList()
	{
		if($this->lstTables)
			return;
		$this->lstTables[] = array("name" => "arm", "varname" => "itbase3_at_192_168_1_15_public_arm", "altvarname" => "arm", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "arm_po", "varname" => "itbase3_at_192_168_1_15_public_arm_po", "altvarname" => "arm_po", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "cons_cartridge", "varname" => "itbase3_at_192_168_1_15_public_cons_cartridge", "altvarname" => "cons_cartridge", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "doc_in", "varname" => "itbase3_at_192_168_1_15_public_doc_in", "altvarname" => "doc_in", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "doc_int", "varname" => "itbase3_at_192_168_1_15_public_doc_int", "altvarname" => "doc_int", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "doc_out", "varname" => "itbase3_at_192_168_1_15_public_doc_out", "altvarname" => "doc_out", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "ecp", "varname" => "itbase3_at_192_168_1_15_public_ecp", "altvarname" => "ecp", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_box", "varname" => "itbase3_at_192_168_1_15_public_hw_box", "altvarname" => "hw_box", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_ibp", "varname" => "itbase3_at_192_168_1_15_public_hw_ibp", "altvarname" => "hw_ibp", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_monitor1", "varname" => "itbase3_at_192_168_1_15_public_hw_monitor1", "altvarname" => "hw_monitor1", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_printer", "varname" => "itbase3_at_192_168_1_15_public_hw_printer", "altvarname" => "hw_printer", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_switch", "varname" => "itbase3_at_192_168_1_15_public_hw_switch", "altvarname" => "hw_switch", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "hw_tel", "varname" => "itbase3_at_192_168_1_15_public_hw_tel", "altvarname" => "hw_tel", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "license_used", "varname" => "itbase3_at_192_168_1_15_public_license_used", "altvarname" => "license_used", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sec_uggroups", "varname" => "itbase3_at_192_168_1_15_public_sec_uggroups", "altvarname" => "sec_uggroups", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sec_ugmembers", "varname" => "itbase3_at_192_168_1_15_public_sec_ugmembers", "altvarname" => "sec_ugmembers", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sec_ugrights", "varname" => "itbase3_at_192_168_1_15_public_sec_ugrights", "altvarname" => "sec_ugrights", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "service_refilling", "varname" => "itbase3_at_192_168_1_15_public_service_refilling", "altvarname" => "service_refilling", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sotrudnik", "varname" => "itbase3_at_192_168_1_15_public_sotrudnik", "altvarname" => "sotrudnik", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_department", "varname" => "itbase3_at_192_168_1_15_public_spr_department", "altvarname" => "spr_department", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_doc_type", "varname" => "itbase3_at_192_168_1_15_public_spr_doc_type", "altvarname" => "spr_doc_type", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_docint_type", "varname" => "itbase3_at_192_168_1_15_public_spr_docint_type", "altvarname" => "spr_docint_type", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_ecp", "varname" => "itbase3_at_192_168_1_15_public_spr_ecp", "altvarname" => "spr_ecp", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_license", "varname" => "itbase3_at_192_168_1_15_public_spr_license", "altvarname" => "spr_license", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_license_dogovor", "varname" => "itbase3_at_192_168_1_15_public_spr_license_dogovor", "altvarname" => "spr_license_dogovor", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_location", "varname" => "itbase3_at_192_168_1_15_public_spr_location", "altvarname" => "spr_location", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_partners", "varname" => "itbase3_at_192_168_1_15_public_spr_partners", "altvarname" => "spr_partners", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_po", "varname" => "itbase3_at_192_168_1_15_public_spr_po", "altvarname" => "spr_po", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "spr_socket", "varname" => "itbase3_at_192_168_1_15_public_spr_socket", "altvarname" => "spr_socket", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sys_audit", "varname" => "itbase3_at_192_168_1_15_public_sys_audit", "altvarname" => "sys_audit", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sys_locking", "varname" => "itbase3_at_192_168_1_15_public_sys_locking", "altvarname" => "sys_locking", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sys_settings", "varname" => "itbase3_at_192_168_1_15_public_sys_settings", "altvarname" => "sys_settings", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
		$this->lstTables[] = array("name" => "sys_users", "varname" => "itbase3_at_192_168_1_15_public_sys_users", "altvarname" => "sys_users", "connId" => "itbase3_at_192_168_1_15", "schema" => "public", "connName" => "itbase3 at 192.168.1.15");
	}

	/**
      * Returns table object by table name.
      * @intellisense
      */
	function & Table($strTable, $schema = "", $connName = "")
	{
		$this->FillTablesList();
		foreach($this->lstTables as $tbl)
		{
			if(strtoupper($strTable)==strtoupper($tbl["name"]) &&
				( $schema == "" || strtoupper($schema) == strtoupper($tbl["schema"]) ) &&
				($connName == "" || strtoupper($connName) == strtoupper($tbl["connName"]) ) )
			{
				$this->CreateClass($tbl);
				return $this->{"tbl".$tbl["varname"]};
			}
		}
//	check table names without dbo. and other prefixes
		foreach($this->lstTables as $tbl)
		{
			if(strtoupper(cutprefix($strTable))==strtoupper(cutprefix($tbl["name"])))
			{
				$this->CreateClass($tbl);
				return $this->{"tbl".$tbl["varname"]};
			}
		}
		$dummy=null;
		return $dummy;
	}
	
	function CreateClass(&$tbl)
	{
		if($this->{"tbl".$tbl["varname"]})
			return;
//	load table info
		global $dal_info;
		include(getabspath("include/dal/".($tbl["varname"]).".php"));
//	create class and object

		$classname="class_".$tbl["varname"];
		$str = "class ".$classname." extends tDALTable  {";
		foreach($dal_info[$tbl["varname"]] as $fld)
		{
			$str.=' var $'.$fld["varname"].'; ';
		}
		
		$tableName = $tbl["name"];
		if( $tbl["schema"] )
			$tableName = $tbl["schema"] . "." . $tbl["name"];
		$str.=' function __construct()
			{
				$this->m_TableName = \''.escapesq( $tableName ).'\';
				$this->infoKey = \'' . $tbl["varname"] . '\';
				$this->setConnection(\''. $tbl["connId"] .'\');
			}
		};';
		eval($str);
		$this->{"tbl".$tbl["varname"]} = new $classname;
		$this->{$tbl["altvarname"]} = $this->{"tbl".$tbl["varname"]};
		$this->Table[$tbl["name"]]=&$this->{"tbl".$tbl["varname"]};
	}
	
	/**
      * Returns list of table names.
      * @intellisense
      */
	function GetTablesList()
	{
		$this->FillTablesList();
		$res=array();
		foreach($this->lstTables as $tbl)
			$res[]=$tbl["name"];
		return $res;
	}
	
	/**
      * Get list of table fields by table name.
      * @intellisense
      */
	function GetFieldsList($table)
	{
		$tbl = $this->Table($table);
		return $tbl->GetFieldsList();
	}
	
	/**
      * Get field type by table name and field name.
      * @intellisense
      */
	function GetFieldType($table,$field)
	{
		$tbl = $this->Table($table);
		return $tbl->GetFieldType($field);
	}

	/**
      * Get table key fields by table name.
      * @intellisense
      */
	function GetDBTableKeys($table)
	{
		$tbl = $this->Table($table);
		return $tbl->GetDBTableKeys();
	}
}

$dal = new tDAL;

/**
 * Data Access Layer table class.
 */ 
class tDALTable
{
	var $m_TableName;
	var $infoKey;
	var $Param = array();
	var $Value = array();
	/**
	 * @type Connection
	 */
	var $_connection;
	
	/**
	 * Set the connection property
	 * @param String connId
	 */
	function setConnection( $connId )
	{
		global $cman;
		$this->_connection = $cman->byId( $connId );
	}
	
	/**
      * Get table key fields.
      * @intellisense
      */
	function GetDBTableKeys()
	{
		global $dal_info;
		if( !array_key_exists($this->infoKey, $dal_info) || !is_array($dal_info[ $this->infoKey ]) )
			return array();
		
		$ret = array();
		foreach($dal_info[ $this->infoKey ] as $fname=>$f)
		{
			if( @$f["key"] )
				$ret[] = $fname;
		}
		return $ret;
	}
	
	/**
      * Get list of table fields.
      * @intellisense
      */
	function GetFieldsList()
	{
		global $dal_info;
		return array_keys( $dal_info[ $this->infoKey ] );
	}

	/**
      * Get field type.
      * @intellisense
      */
	function GetFieldType($field)
	{
		global $dal_info;
		
		if( !array_key_exists( $field, $dal_info[ $this->infoKey ]) )
			return 200;
			
		return $dal_info[ $this->infoKey ][ $field ]["type"];
	}
	
	/**
	 *
	 */
	function PrepareValue($value, $type)
	{
		if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
		{
			if( IsBinaryType($type) )
			{
				if( $this->_connection->dbType == nDATABASE_Oracle )
					return "EMPTY_BLOB()";
					
				return "?";
			}
			
			if( $this->_connection->dbType == nDATABASE_Informix  && IsTextType($type) )
				return "?";
		}
	
		if( IsDateFieldType($type) )
		{
			if( !$value )
				return "null";
			else
				$this->_connection->addDateQuotes( $value );
		}
		
		if( NeedQuotes($type) )
			return $this->_connection->prepareString( $value );

		return 0 + $value;
	}
	
	/**
      * Get table name.
      * @intellisense
      */
	function TableName()
	{
		return $this->_connection->addTableWrappers( $this->m_TableName );
	} 

	/**
	 * @param Array blobs
	 * @param String dalSQL
	 * @param Array tableinfo
	 */
	protected function Execute_Query($blobs, $dalSQL, $tableinfo)
	{		
		$blobTypes = array();
		if( $this->_connection->dbType == nDATABASE_Informix )
		{		
			foreach( $blobs as $fname => $fvalue )
			{
				$blobTypes[ $fname ] = $tableinfo[ $fname ]["type"];
			}
		}

		$this->_connection->execWithBlobProcessing( $dalSQL, $blobs, $blobTypes );
	}

	/**
      * Add new record to the table.
      * @intellisense
      */
	function Add() 
	{
		global $dal_info;
		
		$insertFields = "";
		$insertValues = "";
		$tableinfo = &$dal_info[ $this->infoKey ];
		$blobs = array();
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			if( isset($this->{$fld['varname']}) )
			{
				$this->Value[ $fieldname ] = $this->{$fld['varname']};
			}
			
			foreach($this->Value as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$insertFields.= $this->_connection->addFieldWrappers( $fieldname ).",";
				$insertValues.= $this->PrepareValue($value,$fld["type"]) . ",";
				
				if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
				{
					if( IsBinaryType( $fld["type"] ) )
						$blobs[ $fieldname ] = $value;
						
					if( $this->_connection->dbType == nDATABASE_Informix && IsTextType( $fld["type"] ) )
						$blobs[ $fieldname ] = $value;
				}
				break;
			}
		}
		//	prepare and exec SQL
		if( $insertFields != "" && $insertValues != "" )		
		{
			$insertFields = substr($insertFields, 0, -1);
			$insertValues = substr($insertValues, 0, -1);
			$dalSQL = "insert into ".$this->_connection->addTableWrappers( $this->m_TableName )." (".$insertFields.") values (".$insertValues.")";
			$this->Execute_Query($blobs, $dalSQL, $tableinfo);
		}
		//	cleanup		
	    $this->Reset();
	}

	/**
      * Query all records from the table.
      * @intellisense
      */
	function QueryAll()
	{
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName );
		return $this->_connection->query( $dalSQL );
	}

	/**
      * Do a custom query on the table.
      * @intellisense
      */
	function Query($swhere = "", $orderby = "")
	{
		if ($swhere)
			$swhere = " where ".$swhere;
			
		if ($orderby)
			$orderby = " order by ".$orderby;
			
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName ).$swhere.$orderby;
		return $this->_connection->query( $dalSQL );
	}

	/**
      * Delete a record from the table.
      * @intellisense
      */
	function Delete()
	{
		global $dal_info;
		
		$deleteFields = "";
		$tableinfo = &$dal_info[ $this->infoKey ];
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			if( isset($this->{$fld['varname']}) )
			{
				$this->Param[ $fieldname ] = $this->{$fld['varname']};
			}
			
			foreach($this->Param as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$deleteFields.= $this->_connection->addFieldWrappers( $fieldname )."=". $this->PrepareValue($value, $fld["type"]) . " and ";
				break;
			}
		}
		//	do delete
		if ($deleteFields)
		{
			$deleteFields = substr($deleteFields, 0, -5);
			$dalSQL = "delete from ".$this->_connection->addTableWrappers( $this->m_TableName )." where ".$deleteFields;
			$this->_connection->exec( $dalSQL );
		}
	
		//	cleanup
	    $this->Reset();
	}

	/**
      * Reset table object.
      * @intellisense
      */
	function Reset()
	{
		global $dal_info;
		
		$this->Value = array();
		$this->Param = array();
		
		$tableinfo = &$dal_info[ $this->infoKey ];
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			unset($this->{$fld["varname"]});
		}
	}	

	/**
      * Update record in the table.
      * @intellisense
      */
	function Update()
	{
		global $dal_info;
		
		$tableinfo = &$dal_info[ $this->infoKey ];
		$updateParam = "";
		$updateValue = "";
		$blobs = array();

		foreach($tableinfo as $fieldname => $fld)
		{
			$command = 'if(isset($this->'.$fld['varname'].')) { ';
			if( $fld["key"] )
				$command.= '$this->Param[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			else
				$command.= '$this->Value[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			$command.= ' }';
			
			eval($command);
			
			if( !$fld["key"] && !array_key_exists( strtoupper($fieldname), array_change_key_case($this->Param, CASE_UPPER) ) )
			{
				foreach($this->Value as $field => $value)
				{
					if( strtoupper($field) != strtoupper($fieldname) )
						continue;
						
					$updateValue.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . ", ";
					
					if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
					{
						if( IsBinaryType( $fld["type"] ) )
							$blobs[ $fieldname ] = $value;
							
						if( $this->_connection->dbType == nDATABASE_Informix && IsTextType( $fld["type"] ) )	
							$blobs[ $fieldname ] = $value;		
					}
					break;
				}
			}
			else
			{
				foreach($this->Param as $field=>$value)
				{
					if( strtoupper($field) != strtoupper($fieldname) )
						continue;
						
					$updateParam.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . " and ";
					break;
				}
			}
		}

		//	construct SQL and do update	
		if ($updateParam)
			$updateParam = substr($updateParam, 0, -5);
		if ($updateValue)
			$updateValue = substr($updateValue, 0, -2);
			
		if ($updateValue && $updateParam)
		{
			$dalSQL = "update ".$this->_connection->addTableWrappers( $this->m_TableName )." set ".$updateValue." where ".$updateParam;
			$this->Execute_Query($blobs, $dalSQL, $tableinfo);
		}

		//	cleanup
		$this->Reset();
	}
	
	/**
	 * Select one or more records matching the condition
	 */
	function FetchByID()
	{
		global $dal_info;
		$tableinfo = &$dal_info[ $this->infoKey ];

		$dal_where = "";
		foreach($tableinfo as $fieldname => $fld)
		{
			$command = 'if(isset($this->'.$fld['varname'].')) { ';
			$command.= '$this->Value[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			$command.= ' }';
			
			eval($command);
			
			foreach($this->Param as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$dal_where.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . " and ";
				break;
			}
		}
		// cleanup
		$this->Reset();
		// construct and run SQL
		if ($dal_where)
			$dal_where = " where ".substr($dal_where, 0, -5);
			
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName ).$dal_where;
		return $this->_connection->query( $dalSQL );
	}
}

function cutprefix($table)
{
	$pos = strpos($table,".");
	if( $pos === false )
		return $table;
		
	return substr($table, $pos + 1);
}

function escapesq($str)
{
	return str_replace(array("\\","'"),array("\\\\","\\'"),$str);
}

?>