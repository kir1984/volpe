<?php
$tdatadoc_in = array();
$tdatadoc_in[".searchableFields"] = array();
$tdatadoc_in[".ShortName"] = "doc_in";
$tdatadoc_in[".OwnerID"] = "";
$tdatadoc_in[".OriginalTable"] = "public.doc_in";


$tdatadoc_in[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatadoc_in[".originalPagesByType"] = $tdatadoc_in[".pagesByType"];
$tdatadoc_in[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatadoc_in[".originalPages"] = $tdatadoc_in[".pages"];
$tdatadoc_in[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatadoc_in[".originalDefaultPages"] = $tdatadoc_in[".defaultPages"];

//	field labels
$fieldLabelsdoc_in = array();
$fieldToolTipsdoc_in = array();
$pageTitlesdoc_in = array();
$placeHoldersdoc_in = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsdoc_in["Russian"] = array();
	$fieldToolTipsdoc_in["Russian"] = array();
	$placeHoldersdoc_in["Russian"] = array();
	$pageTitlesdoc_in["Russian"] = array();
	$fieldLabelsdoc_in["Russian"]["docin_id"] = "Docin Id";
	$fieldToolTipsdoc_in["Russian"]["docin_id"] = "";
	$placeHoldersdoc_in["Russian"]["docin_id"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_num"] = "№ вх.";
	$fieldToolTipsdoc_in["Russian"]["doc_num"] = "";
	$placeHoldersdoc_in["Russian"]["doc_num"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_date"] = "Дата вх.";
	$fieldToolTipsdoc_in["Russian"]["doc_date"] = "";
	$placeHoldersdoc_in["Russian"]["doc_date"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_type"] = "Тип";
	$fieldToolTipsdoc_in["Russian"]["doc_type"] = "";
	$placeHoldersdoc_in["Russian"]["doc_type"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_source"] = "Отправитель";
	$fieldToolTipsdoc_in["Russian"]["doc_source"] = "";
	$placeHoldersdoc_in["Russian"]["doc_source"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_outnum"] = "№ исх. отправителя";
	$fieldToolTipsdoc_in["Russian"]["doc_outnum"] = "";
	$placeHoldersdoc_in["Russian"]["doc_outnum"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_outdate"] = "Дата исх. отправителя";
	$fieldToolTipsdoc_in["Russian"]["doc_outdate"] = "";
	$placeHoldersdoc_in["Russian"]["doc_outdate"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_destination"] = "Получатель";
	$fieldToolTipsdoc_in["Russian"]["doc_destination"] = "";
	$placeHoldersdoc_in["Russian"]["doc_destination"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_name"] = "Тема";
	$fieldToolTipsdoc_in["Russian"]["doc_name"] = "";
	$placeHoldersdoc_in["Russian"]["doc_name"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_desc"] = "Описание";
	$fieldToolTipsdoc_in["Russian"]["doc_desc"] = "";
	$placeHoldersdoc_in["Russian"]["doc_desc"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_resolution"] = "Резолюция";
	$fieldToolTipsdoc_in["Russian"]["doc_resolution"] = "";
	$placeHoldersdoc_in["Russian"]["doc_resolution"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_answer"] = "Наш ответ";
	$fieldToolTipsdoc_in["Russian"]["doc_answer"] = "";
	$placeHoldersdoc_in["Russian"]["doc_answer"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_date"] = "Дата контроля";
	$fieldToolTipsdoc_in["Russian"]["doc_control_date"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_date"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_status"] = "На контроль";
	$fieldToolTipsdoc_in["Russian"]["doc_control_status"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_status"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_executiondate"] = "Дата исполнения";
	$fieldToolTipsdoc_in["Russian"]["doc_control_executiondate"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_executiondate"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_executionmark"] = "Отметка об исполнении";
	$fieldToolTipsdoc_in["Russian"]["doc_control_executionmark"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_executionmark"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_executor"] = "Исполнитель";
	$fieldToolTipsdoc_in["Russian"]["doc_control_executor"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_executor"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_executordate"] = "Получено исполнителем";
	$fieldToolTipsdoc_in["Russian"]["doc_control_executordate"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_executordate"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_file"] = "Вложение";
	$fieldToolTipsdoc_in["Russian"]["doc_file"] = "";
	$placeHoldersdoc_in["Russian"]["doc_file"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_destination2"] = "Получатель 2";
	$fieldToolTipsdoc_in["Russian"]["doc_destination2"] = "";
	$placeHoldersdoc_in["Russian"]["doc_destination2"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_destination3"] = "Получатель 3";
	$fieldToolTipsdoc_in["Russian"]["doc_destination3"] = "";
	$placeHoldersdoc_in["Russian"]["doc_destination3"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_destination4"] = "Получатель 4";
	$fieldToolTipsdoc_in["Russian"]["doc_destination4"] = "";
	$placeHoldersdoc_in["Russian"]["doc_destination4"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_dayleft"] = "Doc Control Dayleft";
	$fieldToolTipsdoc_in["Russian"]["doc_control_dayleft"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_dayleft"] = "";
	$fieldLabelsdoc_in["Russian"]["doc_control_expiredstatus"] = "Статус контроль";
	$fieldToolTipsdoc_in["Russian"]["doc_control_expiredstatus"] = "";
	$placeHoldersdoc_in["Russian"]["doc_control_expiredstatus"] = "";
	if (count($fieldToolTipsdoc_in["Russian"]))
		$tdatadoc_in[".isUseToolTips"] = true;
}


	$tdatadoc_in[".NCSearch"] = true;



$tdatadoc_in[".shortTableName"] = "doc_in";
$tdatadoc_in[".nSecOptions"] = 0;

$tdatadoc_in[".mainTableOwnerID"] = "";
$tdatadoc_in[".entityType"] = 0;
$tdatadoc_in[".connId"] = "itbase3_at_192_168_1_15";


$tdatadoc_in[".strOriginalTableName"] = "public.doc_in";

	



$tdatadoc_in[".showAddInPopup"] = false;

$tdatadoc_in[".showEditInPopup"] = false;

$tdatadoc_in[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatadoc_in[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatadoc_in[".listAjax"] = true;
//	temporary
$tdatadoc_in[".listAjax"] = false;

	$tdatadoc_in[".audit"] = true;

	$tdatadoc_in[".locking"] = true;


$pages = $tdatadoc_in[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatadoc_in[".edit"] = true;
	$tdatadoc_in[".afterEditAction"] = 1;
	$tdatadoc_in[".closePopupAfterEdit"] = 1;
	$tdatadoc_in[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatadoc_in[".add"] = true;
$tdatadoc_in[".afterAddAction"] = 1;
$tdatadoc_in[".closePopupAfterAdd"] = 1;
$tdatadoc_in[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatadoc_in[".list"] = true;
}



$tdatadoc_in[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatadoc_in[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatadoc_in[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatadoc_in[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatadoc_in[".printFriendly"] = true;
}



$tdatadoc_in[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatadoc_in[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatadoc_in[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatadoc_in[".isUseAjaxSuggest"] = true;

$tdatadoc_in[".rowHighlite"] = true;





$tdatadoc_in[".ajaxCodeSnippetAdded"] = false;

$tdatadoc_in[".buttonsAdded"] = false;

$tdatadoc_in[".addPageEvents"] = false;

// use timepicker for search panel
$tdatadoc_in[".isUseTimeForSearch"] = false;


$tdatadoc_in[".badgeColor"] = "9ACD32";


$tdatadoc_in[".allSearchFields"] = array();
$tdatadoc_in[".filterFields"] = array();
$tdatadoc_in[".requiredSearchFields"] = array();

$tdatadoc_in[".googleLikeFields"] = array();
$tdatadoc_in[".googleLikeFields"][] = "docin_id";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_dayleft";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_expiredstatus";
$tdatadoc_in[".googleLikeFields"][] = "doc_num";
$tdatadoc_in[".googleLikeFields"][] = "doc_date";
$tdatadoc_in[".googleLikeFields"][] = "doc_type";
$tdatadoc_in[".googleLikeFields"][] = "doc_source";
$tdatadoc_in[".googleLikeFields"][] = "doc_outnum";
$tdatadoc_in[".googleLikeFields"][] = "doc_outdate";
$tdatadoc_in[".googleLikeFields"][] = "doc_destination";
$tdatadoc_in[".googleLikeFields"][] = "doc_name";
$tdatadoc_in[".googleLikeFields"][] = "doc_desc";
$tdatadoc_in[".googleLikeFields"][] = "doc_resolution";
$tdatadoc_in[".googleLikeFields"][] = "doc_answer";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_date";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_status";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_executiondate";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_executionmark";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_executor";
$tdatadoc_in[".googleLikeFields"][] = "doc_control_executordate";
$tdatadoc_in[".googleLikeFields"][] = "doc_file";
$tdatadoc_in[".googleLikeFields"][] = "doc_destination2";
$tdatadoc_in[".googleLikeFields"][] = "doc_destination3";
$tdatadoc_in[".googleLikeFields"][] = "doc_destination4";



$tdatadoc_in[".tableType"] = "list";

$tdatadoc_in[".printerPageOrientation"] = 0;
$tdatadoc_in[".nPrinterPageScale"] = 100;

$tdatadoc_in[".nPrinterSplitRecords"] = 40;

$tdatadoc_in[".geocodingEnabled"] = false;





$tdatadoc_in[".isResizeColumns"] = true;





$tdatadoc_in[".pageSize"] = 20;

$tdatadoc_in[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatadoc_in[".strOrderBy"] = $tstrOrderBy;

$tdatadoc_in[".orderindexes"] = array();


$tdatadoc_in[".sqlHead"] = "SELECT docin_id,  	(SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) doc_control_dayleft,  		CASE  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) > 7 THEN ' В работе'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) BETWEEN 1 AND 7 THEN 'На ответ менее недели'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) < 0 OR (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) = 0 THEN 'Просрочен'  		END doc_control_expiredstatus,  doc_num,  doc_date,  doc_type,  doc_source,  doc_outnum,  doc_outdate,  doc_destination,  doc_name,  doc_desc,  doc_resolution,  doc_answer,  doc_control_date,  doc_control_status,  doc_control_executiondate,  doc_control_executionmark,  doc_control_executor,  doc_control_executordate,  doc_file,  doc_destination2,  doc_destination3,  doc_destination4";
$tdatadoc_in[".sqlFrom"] = "FROM \"public\".doc_in";
$tdatadoc_in[".sqlWhereExpr"] = "";
$tdatadoc_in[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",
	'showRowCount' => 0,
	'hideEmpty' => 0,
);
$tdatadoc_in[".arrGridTabs"] = $arrGridTabs;









//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatadoc_in[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatadoc_in[".arrGroupsPerPage"] = $arrGPP;

$tdatadoc_in[".highlightSearchResults"] = true;

$tableKeysdoc_in = array();
$tableKeysdoc_in[] = "docin_id";
$tdatadoc_in[".Keys"] = $tableKeysdoc_in;


$tdatadoc_in[".hideMobileList"] = array();




//	docin_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "docin_id";
	$fdata["GoodName"] = "docin_id";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","docin_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "docin_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "docin_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["docin_id"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "docin_id";
//	doc_control_dayleft
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "doc_control_dayleft";
	$fdata["GoodName"] = "doc_control_dayleft";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_dayleft");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_control_dayleft";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "(SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_dayleft"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_dayleft";
//	doc_control_expiredstatus
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "doc_control_expiredstatus";
	$fdata["GoodName"] = "doc_control_expiredstatus";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_expiredstatus");
	$fdata["FieldType"] = 201;

	
	
	
			

		$fdata["strField"] = "doc_control_expiredstatus";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CASE  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) > 7 THEN ' В работе'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) BETWEEN 1 AND 7 THEN 'На ответ менее недели'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) < 0 OR (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) = 0 THEN 'Просрочен'  		END";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_expiredstatus"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_expiredstatus";
//	doc_num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "doc_num";
	$fdata["GoodName"] = "doc_num";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_num");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_num";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_num";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_num"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_num";
//	doc_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "doc_date";
	$fdata["GoodName"] = "doc_date";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_date");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "doc_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_date"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_date";
//	doc_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "doc_type";
	$fdata["GoodName"] = "doc_type";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_type");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_doc_type";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "doctype_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "type_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_type"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_type";
//	doc_source
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "doc_source";
	$fdata["GoodName"] = "doc_source";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_source");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_source";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_source";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_partners";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "partner_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "partner_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_source"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_source";
//	doc_outnum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "doc_outnum";
	$fdata["GoodName"] = "doc_outnum";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_outnum");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_outnum";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_outnum";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_outnum"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_outnum";
//	doc_outdate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "doc_outdate";
	$fdata["GoodName"] = "doc_outdate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_outdate");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "doc_outdate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_outdate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_outdate"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_outdate";
//	doc_destination
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "doc_destination";
	$fdata["GoodName"] = "doc_destination";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_destination");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_destination"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_destination";
//	doc_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "doc_name";
	$fdata["GoodName"] = "doc_name";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_name"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_name";
//	doc_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "doc_desc";
	$fdata["GoodName"] = "doc_desc";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_desc"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_desc";
//	doc_resolution
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "doc_resolution";
	$fdata["GoodName"] = "doc_resolution";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_resolution");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_resolution";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_resolution";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "В работу";
	$edata["LookupValues"][] = "Без исполнения";
	$edata["LookupValues"][] = "Для информации";
	$edata["LookupValues"][] = "Для сведения";
	$edata["LookupValues"][] = "К исполнению";
	$edata["LookupValues"][] = "К исполнению (срочно!)";
	$edata["LookupValues"][] = "На доработку";
	$edata["LookupValues"][] = "На контроль";
	$edata["LookupValues"][] = "Прошу подготовить ответ";
	$edata["LookupValues"][] = "Принять участие";
	$edata["LookupValues"][] = "Принять участие (обязательно)";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_resolution"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_resolution";
//	doc_answer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "doc_answer";
	$fdata["GoodName"] = "doc_answer";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_answer");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_answer";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_answer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.doc_out";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "docout_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "doc_num || ' от ' || doc_date  || ' : ' || doc_name";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_answer"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_answer";
//	doc_control_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "doc_control_date";
	$fdata["GoodName"] = "doc_control_date";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_date"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_date";
//	doc_control_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "doc_control_status";
	$fdata["GoodName"] = "doc_control_status";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_status");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "doc_control_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatadoc_in["doc_control_status"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_status";
//	doc_control_executiondate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "doc_control_executiondate";
	$fdata["GoodName"] = "doc_control_executiondate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_executiondate");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_executiondate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executiondate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_executiondate"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_executiondate";
//	doc_control_executionmark
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "doc_control_executionmark";
	$fdata["GoodName"] = "doc_control_executionmark";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_executionmark");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_control_executionmark";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executionmark";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Без исполнения в документы";
	$edata["LookupValues"][] = "В работе";
	$edata["LookupValues"][] = "Для сведения";
	$edata["LookupValues"][] = "Исполнено";
	$edata["LookupValues"][] = "Исполнено по электронной почте";
	$edata["LookupValues"][] = "Исполнено через Бюджетирование";
	$edata["LookupValues"][] = "Исполнено через ЕИСМС";
	$edata["LookupValues"][] = "Исполнено через личный кабинет";
	$edata["LookupValues"][] = "Исполнено через Росстат";
	$edata["LookupValues"][] = "Исполнено через ЭБ";
	$edata["LookupValues"][] = "Отправлено экспресс-почтой";
	$edata["LookupValues"][] = "Снято с контроля";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_executionmark"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_executionmark";
//	doc_control_executor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "doc_control_executor";
	$fdata["GoodName"] = "doc_control_executor";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_executor");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_control_executor";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executor";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_executor"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_executor";
//	doc_control_executordate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "doc_control_executordate";
	$fdata["GoodName"] = "doc_control_executordate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_control_executordate");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_executordate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executordate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_control_executordate"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_control_executordate";
//	doc_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "doc_file";
	$fdata["GoodName"] = "doc_file";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_file");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_file";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_file";

	
	
				$fdata["UploadFolder"] = "files/docin";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 20;

	
		$edata["maxTotalFilesSize"] = 80000;

	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_file"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_file";
//	doc_destination2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "doc_destination2";
	$fdata["GoodName"] = "doc_destination2";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_destination2");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination2";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination2";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_destination2"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_destination2";
//	doc_destination3
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "doc_destination3";
	$fdata["GoodName"] = "doc_destination3";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_destination3");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination3";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination3";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_destination3"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_destination3";
//	doc_destination4
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "doc_destination4";
	$fdata["GoodName"] = "doc_destination4";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("public_doc_in","doc_destination4");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination4";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination4";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_in["doc_destination4"] = $fdata;
		$tdatadoc_in[".searchableFields"][] = "doc_destination4";


$tables_data["public.doc_in"]=&$tdatadoc_in;
$field_labels["public_doc_in"] = &$fieldLabelsdoc_in;
$fieldToolTips["public_doc_in"] = &$fieldToolTipsdoc_in;
$placeHolders["public_doc_in"] = &$placeHoldersdoc_in;
$page_titles["public_doc_in"] = &$pageTitlesdoc_in;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.doc_in"] = array();
//	public.doc_out
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_out";
		$detailsParam["dOriginalTable"] = "public.doc_out";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_out";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_out");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.doc_in"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.doc_in"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.doc_in"][$dIndex]["masterKeys"][]="docin_id";

				$detailsTablesData["public.doc_in"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.doc_in"][$dIndex]["detailKeys"][]="doc_answer";
//	report_docout
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="report_docout";
		$detailsParam["dOriginalTable"] = "public.doc_out";



		$detailsParam["dType"]=PAGE_REPORT;
	
		$detailsParam["dShortTable"] = "report_docout";
	$detailsParam["dCaptionTable"] = GetTableCaption("report_docout");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.doc_in"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.doc_in"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.doc_in"][$dIndex]["masterKeys"][]="docin_id";

				$detailsTablesData["public.doc_in"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.doc_in"][$dIndex]["detailKeys"][]="doc_answer";

// tables which are master tables for current table (detail)
$masterTablesData["public.doc_in"] = array();



	
				$strOriginalDetailsTable="public.sotrudnik";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.sotrudnik";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "sotrudnik";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_in"][0] = $masterParams;
				$masterTablesData["public.doc_in"][0]["masterKeys"] = array();
	$masterTablesData["public.doc_in"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.doc_in"][0]["detailKeys"] = array();
	$masterTablesData["public.doc_in"][0]["detailKeys"][]="doc_control_executor";
		
	
				$strOriginalDetailsTable="public.doc_out";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.doc_out";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "doc_out";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_in"][1] = $masterParams;
				$masterTablesData["public.doc_in"][1]["masterKeys"] = array();
	$masterTablesData["public.doc_in"][1]["masterKeys"][]="docout_id";
				$masterTablesData["public.doc_in"][1]["detailKeys"] = array();
	$masterTablesData["public.doc_in"][1]["detailKeys"][]="doc_answer";
		
	
				$strOriginalDetailsTable="public.spr_doc_type";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_doc_type";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_doc_type";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_in"][2] = $masterParams;
				$masterTablesData["public.doc_in"][2]["masterKeys"] = array();
	$masterTablesData["public.doc_in"][2]["masterKeys"][]="doctype_id";
				$masterTablesData["public.doc_in"][2]["detailKeys"] = array();
	$masterTablesData["public.doc_in"][2]["detailKeys"][]="doc_type";
		
	
				$strOriginalDetailsTable="public.spr_partners";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_partners";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_partners";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_in"][3] = $masterParams;
				$masterTablesData["public.doc_in"][3]["masterKeys"] = array();
	$masterTablesData["public.doc_in"][3]["masterKeys"][]="partner_id";
				$masterTablesData["public.doc_in"][3]["masterKeys"][]="partner_id";
				$masterTablesData["public.doc_in"][3]["detailKeys"] = array();
	$masterTablesData["public.doc_in"][3]["detailKeys"][]="doc_source";
				$masterTablesData["public.doc_in"][3]["detailKeys"][]="doc_destination";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_doc_in()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "docin_id,  	(SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) doc_control_dayleft,  		CASE  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) > 7 THEN ' В работе'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) BETWEEN 1 AND 7 THEN 'На ответ менее недели'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) < 0 OR (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) = 0 THEN 'Просрочен'  		END doc_control_expiredstatus,  doc_num,  doc_date,  doc_type,  doc_source,  doc_outnum,  doc_outdate,  doc_destination,  doc_name,  doc_desc,  doc_resolution,  doc_answer,  doc_control_date,  doc_control_status,  doc_control_executiondate,  doc_control_executionmark,  doc_control_executor,  doc_control_executordate,  doc_file,  doc_destination2,  doc_destination3,  doc_destination4";
$proto0["m_strFrom"] = "FROM \"public\".doc_in";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "docin_id",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto6["m_sql"] = "docin_id";
$proto6["m_srcTableName"] = "public.doc_in";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$proto9=array();
$proto9["m_strHead"] = "SELECT";
$proto9["m_strFieldList"] = "CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null";
$proto9["m_strFrom"] = "";
$proto9["m_strWhere"] = "";
$proto9["m_strOrderBy"] = "";
	
		;
			$proto9["cipherer"] = null;
$proto11=array();
$proto11["m_sql"] = "";
$proto11["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_where"] = $obj;
$proto13=array();
$proto13["m_sql"] = "";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

$proto9["m_having"] = $obj;
$proto9["m_fieldlist"] = array();
						$proto15=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is"
));

$proto15["m_sql"] = "CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is";
$proto15["m_srcTableName"] = "public.doc_in";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "null";
$obj = new SQLFieldListItem($proto15);

$proto9["m_fieldlist"][]=$obj;
$proto9["m_fromlist"] = array();
$proto9["m_groupby"] = array();
$proto9["m_orderby"] = array();
$proto9["m_srcTableName"]="public.doc_in";		
$obj = new SQLQuery($proto9);

$proto8["m_sql"] = "(SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null)";
$proto8["m_srcTableName"] = "public.doc_in";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "doc_control_dayleft";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "CASE  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) > 7 THEN ' В работе'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) BETWEEN 1 AND 7 THEN 'На ответ менее недели'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) < 0 OR (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) = 0 THEN 'Просрочен'  		END"
));

$proto17["m_sql"] = "CASE  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) > 7 THEN ' В работе'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) BETWEEN 1 AND 7 THEN 'На ответ менее недели'  			WHEN (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) < 0 OR (SELECT CAST(doc_control_date AS date) - CAST( now() AS date) where doc_control_status is true and doc_control_executiondate is null) = 0 THEN 'Просрочен'  		END";
$proto17["m_srcTableName"] = "public.doc_in";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "doc_control_expiredstatus";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_num",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto19["m_sql"] = "doc_num";
$proto19["m_srcTableName"] = "public.doc_in";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_date",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto21["m_sql"] = "doc_date";
$proto21["m_srcTableName"] = "public.doc_in";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_type",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto23["m_sql"] = "doc_type";
$proto23["m_srcTableName"] = "public.doc_in";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_source",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto25["m_sql"] = "doc_source";
$proto25["m_srcTableName"] = "public.doc_in";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_outnum",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto27["m_sql"] = "doc_outnum";
$proto27["m_srcTableName"] = "public.doc_in";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
						$proto29=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_outdate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto29["m_sql"] = "doc_outdate";
$proto29["m_srcTableName"] = "public.doc_in";
$proto29["m_expr"]=$obj;
$proto29["m_alias"] = "";
$obj = new SQLFieldListItem($proto29);

$proto0["m_fieldlist"][]=$obj;
						$proto31=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto31["m_sql"] = "doc_destination";
$proto31["m_srcTableName"] = "public.doc_in";
$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "";
$obj = new SQLFieldListItem($proto31);

$proto0["m_fieldlist"][]=$obj;
						$proto33=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_name",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto33["m_sql"] = "doc_name";
$proto33["m_srcTableName"] = "public.doc_in";
$proto33["m_expr"]=$obj;
$proto33["m_alias"] = "";
$obj = new SQLFieldListItem($proto33);

$proto0["m_fieldlist"][]=$obj;
						$proto35=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_desc",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto35["m_sql"] = "doc_desc";
$proto35["m_srcTableName"] = "public.doc_in";
$proto35["m_expr"]=$obj;
$proto35["m_alias"] = "";
$obj = new SQLFieldListItem($proto35);

$proto0["m_fieldlist"][]=$obj;
						$proto37=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_resolution",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto37["m_sql"] = "doc_resolution";
$proto37["m_srcTableName"] = "public.doc_in";
$proto37["m_expr"]=$obj;
$proto37["m_alias"] = "";
$obj = new SQLFieldListItem($proto37);

$proto0["m_fieldlist"][]=$obj;
						$proto39=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_answer",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto39["m_sql"] = "doc_answer";
$proto39["m_srcTableName"] = "public.doc_in";
$proto39["m_expr"]=$obj;
$proto39["m_alias"] = "";
$obj = new SQLFieldListItem($proto39);

$proto0["m_fieldlist"][]=$obj;
						$proto41=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_date",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto41["m_sql"] = "doc_control_date";
$proto41["m_srcTableName"] = "public.doc_in";
$proto41["m_expr"]=$obj;
$proto41["m_alias"] = "";
$obj = new SQLFieldListItem($proto41);

$proto0["m_fieldlist"][]=$obj;
						$proto43=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto43["m_sql"] = "doc_control_status";
$proto43["m_srcTableName"] = "public.doc_in";
$proto43["m_expr"]=$obj;
$proto43["m_alias"] = "";
$obj = new SQLFieldListItem($proto43);

$proto0["m_fieldlist"][]=$obj;
						$proto45=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto45["m_sql"] = "doc_control_executiondate";
$proto45["m_srcTableName"] = "public.doc_in";
$proto45["m_expr"]=$obj;
$proto45["m_alias"] = "";
$obj = new SQLFieldListItem($proto45);

$proto0["m_fieldlist"][]=$obj;
						$proto47=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executionmark",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto47["m_sql"] = "doc_control_executionmark";
$proto47["m_srcTableName"] = "public.doc_in";
$proto47["m_expr"]=$obj;
$proto47["m_alias"] = "";
$obj = new SQLFieldListItem($proto47);

$proto0["m_fieldlist"][]=$obj;
						$proto49=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executor",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto49["m_sql"] = "doc_control_executor";
$proto49["m_srcTableName"] = "public.doc_in";
$proto49["m_expr"]=$obj;
$proto49["m_alias"] = "";
$obj = new SQLFieldListItem($proto49);

$proto0["m_fieldlist"][]=$obj;
						$proto51=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executordate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto51["m_sql"] = "doc_control_executordate";
$proto51["m_srcTableName"] = "public.doc_in";
$proto51["m_expr"]=$obj;
$proto51["m_alias"] = "";
$obj = new SQLFieldListItem($proto51);

$proto0["m_fieldlist"][]=$obj;
						$proto53=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_file",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto53["m_sql"] = "doc_file";
$proto53["m_srcTableName"] = "public.doc_in";
$proto53["m_expr"]=$obj;
$proto53["m_alias"] = "";
$obj = new SQLFieldListItem($proto53);

$proto0["m_fieldlist"][]=$obj;
						$proto55=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination2",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto55["m_sql"] = "doc_destination2";
$proto55["m_srcTableName"] = "public.doc_in";
$proto55["m_expr"]=$obj;
$proto55["m_alias"] = "";
$obj = new SQLFieldListItem($proto55);

$proto0["m_fieldlist"][]=$obj;
						$proto57=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination3",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto57["m_sql"] = "doc_destination3";
$proto57["m_srcTableName"] = "public.doc_in";
$proto57["m_expr"]=$obj;
$proto57["m_alias"] = "";
$obj = new SQLFieldListItem($proto57);

$proto0["m_fieldlist"][]=$obj;
						$proto59=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination4",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "public.doc_in"
));

$proto59["m_sql"] = "doc_destination4";
$proto59["m_srcTableName"] = "public.doc_in";
$proto59["m_expr"]=$obj;
$proto59["m_alias"] = "";
$obj = new SQLFieldListItem($proto59);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto61=array();
$proto61["m_link"] = "SQLL_MAIN";
			$proto62=array();
$proto62["m_strName"] = "public.doc_in";
$proto62["m_srcTableName"] = "public.doc_in";
$proto62["m_columns"] = array();
$proto62["m_columns"][] = "docin_id";
$proto62["m_columns"][] = "doc_num";
$proto62["m_columns"][] = "doc_date";
$proto62["m_columns"][] = "doc_type";
$proto62["m_columns"][] = "doc_source";
$proto62["m_columns"][] = "doc_outnum";
$proto62["m_columns"][] = "doc_outdate";
$proto62["m_columns"][] = "doc_destination";
$proto62["m_columns"][] = "doc_name";
$proto62["m_columns"][] = "doc_desc";
$proto62["m_columns"][] = "doc_resolution";
$proto62["m_columns"][] = "doc_answer";
$proto62["m_columns"][] = "doc_control_date";
$proto62["m_columns"][] = "doc_control_status";
$proto62["m_columns"][] = "doc_control_executiondate";
$proto62["m_columns"][] = "doc_control_executionmark";
$proto62["m_columns"][] = "doc_control_executor";
$proto62["m_columns"][] = "doc_control_executordate";
$proto62["m_columns"][] = "doc_file";
$proto62["m_columns"][] = "doc_destination2";
$proto62["m_columns"][] = "doc_destination3";
$proto62["m_columns"][] = "doc_destination4";
$obj = new SQLTable($proto62);

$proto61["m_table"] = $obj;
$proto61["m_sql"] = "\"public\".doc_in";
$proto61["m_alias"] = "";
$proto61["m_srcTableName"] = "public.doc_in";
$proto63=array();
$proto63["m_sql"] = "";
$proto63["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto63["m_column"]=$obj;
$proto63["m_contained"] = array();
$proto63["m_strCase"] = "";
$proto63["m_havingmode"] = false;
$proto63["m_inBrackets"] = false;
$proto63["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto63);

$proto61["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto61);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.doc_in";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_doc_in = createSqlQuery_doc_in();


	
		;

																								

$tdatadoc_in[".sqlquery"] = $queryData_doc_in;



$tableEvents["public.doc_in"] = new eventsBase;
$tdatadoc_in[".hasEvents"] = false;

?>