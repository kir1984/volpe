<?php
$tdatadoc_int = array();
$tdatadoc_int[".searchableFields"] = array();
$tdatadoc_int[".ShortName"] = "doc_int";
$tdatadoc_int[".OwnerID"] = "";
$tdatadoc_int[".OriginalTable"] = "public.doc_int";


$tdatadoc_int[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatadoc_int[".originalPagesByType"] = $tdatadoc_int[".pagesByType"];
$tdatadoc_int[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatadoc_int[".originalPages"] = $tdatadoc_int[".pages"];
$tdatadoc_int[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatadoc_int[".originalDefaultPages"] = $tdatadoc_int[".defaultPages"];

//	field labels
$fieldLabelsdoc_int = array();
$fieldToolTipsdoc_int = array();
$pageTitlesdoc_int = array();
$placeHoldersdoc_int = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsdoc_int["Russian"] = array();
	$fieldToolTipsdoc_int["Russian"] = array();
	$placeHoldersdoc_int["Russian"] = array();
	$pageTitlesdoc_int["Russian"] = array();
	$fieldLabelsdoc_int["Russian"]["docint_id"] = "Docint Id";
	$fieldToolTipsdoc_int["Russian"]["docint_id"] = "";
	$placeHoldersdoc_int["Russian"]["docint_id"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_num"] = "№";
	$fieldToolTipsdoc_int["Russian"]["doc_num"] = "";
	$placeHoldersdoc_int["Russian"]["doc_num"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_date"] = "Дата";
	$fieldToolTipsdoc_int["Russian"]["doc_date"] = "";
	$placeHoldersdoc_int["Russian"]["doc_date"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_type"] = "Тип";
	$fieldToolTipsdoc_int["Russian"]["doc_type"] = "";
	$placeHoldersdoc_int["Russian"]["doc_type"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_destination"] = "Получатель(орг)";
	$fieldToolTipsdoc_int["Russian"]["doc_destination"] = "";
	$placeHoldersdoc_int["Russian"]["doc_destination"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_initiator"] = "Инициатор";
	$fieldToolTipsdoc_int["Russian"]["doc_initiator"] = "";
	$placeHoldersdoc_int["Russian"]["doc_initiator"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_executor"] = "Исполнитель";
	$fieldToolTipsdoc_int["Russian"]["doc_executor"] = "";
	$placeHoldersdoc_int["Russian"]["doc_executor"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_name"] = "Тема";
	$fieldToolTipsdoc_int["Russian"]["doc_name"] = "";
	$placeHoldersdoc_int["Russian"]["doc_name"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_desc"] = "Описание";
	$fieldToolTipsdoc_int["Russian"]["doc_desc"] = "";
	$placeHoldersdoc_int["Russian"]["doc_desc"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_control_date"] = "Дата контроля";
	$fieldToolTipsdoc_int["Russian"]["doc_control_date"] = "";
	$placeHoldersdoc_int["Russian"]["doc_control_date"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_control_status"] = "На контроль";
	$fieldToolTipsdoc_int["Russian"]["doc_control_status"] = "";
	$placeHoldersdoc_int["Russian"]["doc_control_status"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_control_executiondate"] = "Дата исполнения";
	$fieldToolTipsdoc_int["Russian"]["doc_control_executiondate"] = "";
	$placeHoldersdoc_int["Russian"]["doc_control_executiondate"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_file"] = "Вложение";
	$fieldToolTipsdoc_int["Russian"]["doc_file"] = "";
	$placeHoldersdoc_int["Russian"]["doc_file"] = "";
	$fieldLabelsdoc_int["Russian"]["doc_destination_int"] = "Получатель(физ)";
	$fieldToolTipsdoc_int["Russian"]["doc_destination_int"] = "";
	$placeHoldersdoc_int["Russian"]["doc_destination_int"] = "";
	if (count($fieldToolTipsdoc_int["Russian"]))
		$tdatadoc_int[".isUseToolTips"] = true;
}


	$tdatadoc_int[".NCSearch"] = true;



$tdatadoc_int[".shortTableName"] = "doc_int";
$tdatadoc_int[".nSecOptions"] = 0;

$tdatadoc_int[".mainTableOwnerID"] = "";
$tdatadoc_int[".entityType"] = 0;
$tdatadoc_int[".connId"] = "itbase3_at_192_168_1_15";


$tdatadoc_int[".strOriginalTableName"] = "public.doc_int";

	



$tdatadoc_int[".showAddInPopup"] = false;

$tdatadoc_int[".showEditInPopup"] = false;

$tdatadoc_int[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatadoc_int[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatadoc_int[".listAjax"] = true;
//	temporary
$tdatadoc_int[".listAjax"] = false;

	$tdatadoc_int[".audit"] = true;

	$tdatadoc_int[".locking"] = true;


$pages = $tdatadoc_int[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatadoc_int[".edit"] = true;
	$tdatadoc_int[".afterEditAction"] = 1;
	$tdatadoc_int[".closePopupAfterEdit"] = 1;
	$tdatadoc_int[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatadoc_int[".add"] = true;
$tdatadoc_int[".afterAddAction"] = 1;
$tdatadoc_int[".closePopupAfterAdd"] = 1;
$tdatadoc_int[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatadoc_int[".list"] = true;
}



$tdatadoc_int[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatadoc_int[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatadoc_int[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatadoc_int[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatadoc_int[".printFriendly"] = true;
}



$tdatadoc_int[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatadoc_int[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatadoc_int[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatadoc_int[".isUseAjaxSuggest"] = true;

$tdatadoc_int[".rowHighlite"] = true;





$tdatadoc_int[".ajaxCodeSnippetAdded"] = false;

$tdatadoc_int[".buttonsAdded"] = false;

$tdatadoc_int[".addPageEvents"] = false;

// use timepicker for search panel
$tdatadoc_int[".isUseTimeForSearch"] = false;


$tdatadoc_int[".badgeColor"] = "00C2C5";


$tdatadoc_int[".allSearchFields"] = array();
$tdatadoc_int[".filterFields"] = array();
$tdatadoc_int[".requiredSearchFields"] = array();

$tdatadoc_int[".googleLikeFields"] = array();
$tdatadoc_int[".googleLikeFields"][] = "docint_id";
$tdatadoc_int[".googleLikeFields"][] = "doc_num";
$tdatadoc_int[".googleLikeFields"][] = "doc_date";
$tdatadoc_int[".googleLikeFields"][] = "doc_type";
$tdatadoc_int[".googleLikeFields"][] = "doc_destination";
$tdatadoc_int[".googleLikeFields"][] = "doc_initiator";
$tdatadoc_int[".googleLikeFields"][] = "doc_executor";
$tdatadoc_int[".googleLikeFields"][] = "doc_name";
$tdatadoc_int[".googleLikeFields"][] = "doc_desc";
$tdatadoc_int[".googleLikeFields"][] = "doc_control_date";
$tdatadoc_int[".googleLikeFields"][] = "doc_control_status";
$tdatadoc_int[".googleLikeFields"][] = "doc_control_executiondate";
$tdatadoc_int[".googleLikeFields"][] = "doc_file";
$tdatadoc_int[".googleLikeFields"][] = "doc_destination_int";



$tdatadoc_int[".tableType"] = "list";

$tdatadoc_int[".printerPageOrientation"] = 0;
$tdatadoc_int[".nPrinterPageScale"] = 100;

$tdatadoc_int[".nPrinterSplitRecords"] = 40;

$tdatadoc_int[".geocodingEnabled"] = false;





$tdatadoc_int[".isResizeColumns"] = true;





$tdatadoc_int[".pageSize"] = 20;

$tdatadoc_int[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatadoc_int[".strOrderBy"] = $tstrOrderBy;

$tdatadoc_int[".orderindexes"] = array();


$tdatadoc_int[".sqlHead"] = "SELECT docint_id,  	doc_num,  	doc_date,  	doc_type,  	doc_destination,  	doc_initiator,  	doc_executor,  	doc_name,  	doc_desc,  	doc_control_date,  	doc_control_status,  	doc_control_executiondate,  	doc_file,  	doc_destination_int";
$tdatadoc_int[".sqlFrom"] = "FROM \"public\".doc_int";
$tdatadoc_int[".sqlWhereExpr"] = "";
$tdatadoc_int[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatadoc_int[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatadoc_int[".arrGroupsPerPage"] = $arrGPP;

$tdatadoc_int[".highlightSearchResults"] = true;

$tableKeysdoc_int = array();
$tableKeysdoc_int[] = "docint_id";
$tdatadoc_int[".Keys"] = $tableKeysdoc_int;


$tdatadoc_int[".hideMobileList"] = array();




//	docint_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "docint_id";
	$fdata["GoodName"] = "docint_id";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","docint_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "docint_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "docint_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["docint_id"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "docint_id";
//	doc_num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "doc_num";
	$fdata["GoodName"] = "doc_num";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_num");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_num";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_num";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_num"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_num";
//	doc_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "doc_date";
	$fdata["GoodName"] = "doc_date";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_date");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "doc_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_date"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_date";
//	doc_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "doc_type";
	$fdata["GoodName"] = "doc_type";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_type");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_docint_type";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "docinttype_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "type_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_type"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_type";
//	doc_destination
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "doc_destination";
	$fdata["GoodName"] = "doc_destination";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_destination");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_partners";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "partner_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "partner_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_destination"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_destination";
//	doc_initiator
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "doc_initiator";
	$fdata["GoodName"] = "doc_initiator";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_initiator");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_initiator";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_initiator";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_initiator"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_initiator";
//	doc_executor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "doc_executor";
	$fdata["GoodName"] = "doc_executor";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_executor");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_executor";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_executor";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_executor"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_executor";
//	doc_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "doc_name";
	$fdata["GoodName"] = "doc_name";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_name"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_name";
//	doc_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "doc_desc";
	$fdata["GoodName"] = "doc_desc";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_desc"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_desc";
//	doc_control_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "doc_control_date";
	$fdata["GoodName"] = "doc_control_date";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_control_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_control_date"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_control_date";
//	doc_control_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "doc_control_status";
	$fdata["GoodName"] = "doc_control_status";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_control_status");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "doc_control_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatadoc_int["doc_control_status"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_control_status";
//	doc_control_executiondate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "doc_control_executiondate";
	$fdata["GoodName"] = "doc_control_executiondate";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_control_executiondate");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_executiondate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executiondate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_control_executiondate"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_control_executiondate";
//	doc_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "doc_file";
	$fdata["GoodName"] = "doc_file";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_file");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_file";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_file";

	
	
				$fdata["UploadFolder"] = "files/docint";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 20;

	
		$edata["maxTotalFilesSize"] = 80000;

	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_file"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_file";
//	doc_destination_int
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "doc_destination_int";
	$fdata["GoodName"] = "doc_destination_int";
	$fdata["ownerTable"] = "public.doc_int";
	$fdata["Label"] = GetFieldLabel("public_doc_int","doc_destination_int");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination_int";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination_int";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_int["doc_destination_int"] = $fdata;
		$tdatadoc_int[".searchableFields"][] = "doc_destination_int";


$tables_data["public.doc_int"]=&$tdatadoc_int;
$field_labels["public_doc_int"] = &$fieldLabelsdoc_int;
$fieldToolTips["public_doc_int"] = &$fieldToolTipsdoc_int;
$placeHolders["public_doc_int"] = &$placeHoldersdoc_int;
$page_titles["public_doc_int"] = &$pageTitlesdoc_int;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.doc_int"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.doc_int"] = array();



	
				$strOriginalDetailsTable="public.sotrudnik";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.sotrudnik";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "sotrudnik";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_int"][0] = $masterParams;
				$masterTablesData["public.doc_int"][0]["masterKeys"] = array();
	$masterTablesData["public.doc_int"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.doc_int"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.doc_int"][0]["detailKeys"] = array();
	$masterTablesData["public.doc_int"][0]["detailKeys"][]="doc_initiator";
				$masterTablesData["public.doc_int"][0]["detailKeys"][]="doc_executor";
		
	
				$strOriginalDetailsTable="public.spr_docint_type";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_docint_type";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_docint_type";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_int"][1] = $masterParams;
				$masterTablesData["public.doc_int"][1]["masterKeys"] = array();
	$masterTablesData["public.doc_int"][1]["masterKeys"][]="docinttype_id";
				$masterTablesData["public.doc_int"][1]["detailKeys"] = array();
	$masterTablesData["public.doc_int"][1]["detailKeys"][]="doc_type";
		
	
				$strOriginalDetailsTable="public.spr_partners";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_partners";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_partners";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_int"][2] = $masterParams;
				$masterTablesData["public.doc_int"][2]["masterKeys"] = array();
	$masterTablesData["public.doc_int"][2]["masterKeys"][]="partner_id";
				$masterTablesData["public.doc_int"][2]["detailKeys"] = array();
	$masterTablesData["public.doc_int"][2]["detailKeys"][]="doc_destination";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_doc_int()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "docint_id,  	doc_num,  	doc_date,  	doc_type,  	doc_destination,  	doc_initiator,  	doc_executor,  	doc_name,  	doc_desc,  	doc_control_date,  	doc_control_status,  	doc_control_executiondate,  	doc_file,  	doc_destination_int";
$proto0["m_strFrom"] = "FROM \"public\".doc_int";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "docint_id",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto6["m_sql"] = "docint_id";
$proto6["m_srcTableName"] = "public.doc_int";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_num",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto8["m_sql"] = "doc_num";
$proto8["m_srcTableName"] = "public.doc_int";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_date",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto10["m_sql"] = "doc_date";
$proto10["m_srcTableName"] = "public.doc_int";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_type",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto12["m_sql"] = "doc_type";
$proto12["m_srcTableName"] = "public.doc_int";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto14["m_sql"] = "doc_destination";
$proto14["m_srcTableName"] = "public.doc_int";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_initiator",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto16["m_sql"] = "doc_initiator";
$proto16["m_srcTableName"] = "public.doc_int";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_executor",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto18["m_sql"] = "doc_executor";
$proto18["m_srcTableName"] = "public.doc_int";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_name",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto20["m_sql"] = "doc_name";
$proto20["m_srcTableName"] = "public.doc_int";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_desc",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto22["m_sql"] = "doc_desc";
$proto22["m_srcTableName"] = "public.doc_int";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_date",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto24["m_sql"] = "doc_control_date";
$proto24["m_srcTableName"] = "public.doc_int";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto26["m_sql"] = "doc_control_status";
$proto26["m_srcTableName"] = "public.doc_int";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto28["m_sql"] = "doc_control_executiondate";
$proto28["m_srcTableName"] = "public.doc_int";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_file",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto30["m_sql"] = "doc_file";
$proto30["m_srcTableName"] = "public.doc_int";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination_int",
	"m_strTable" => "public.doc_int",
	"m_srcTableName" => "public.doc_int"
));

$proto32["m_sql"] = "doc_destination_int";
$proto32["m_srcTableName"] = "public.doc_int";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "public.doc_int";
$proto35["m_srcTableName"] = "public.doc_int";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "docint_id";
$proto35["m_columns"][] = "doc_num";
$proto35["m_columns"][] = "doc_date";
$proto35["m_columns"][] = "doc_type";
$proto35["m_columns"][] = "doc_destination";
$proto35["m_columns"][] = "doc_initiator";
$proto35["m_columns"][] = "doc_executor";
$proto35["m_columns"][] = "doc_name";
$proto35["m_columns"][] = "doc_desc";
$proto35["m_columns"][] = "doc_control_date";
$proto35["m_columns"][] = "doc_control_status";
$proto35["m_columns"][] = "doc_control_executiondate";
$proto35["m_columns"][] = "doc_file";
$proto35["m_columns"][] = "doc_destination_int";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "\"public\".doc_int";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "public.doc_int";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.doc_int";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_doc_int = createSqlQuery_doc_int();


	
		;

														

$tdatadoc_int[".sqlquery"] = $queryData_doc_int;



$tableEvents["public.doc_int"] = new eventsBase;
$tdatadoc_int[".hasEvents"] = false;

?>