<?php
$tdatadoc_out = array();
$tdatadoc_out[".searchableFields"] = array();
$tdatadoc_out[".ShortName"] = "doc_out";
$tdatadoc_out[".OwnerID"] = "";
$tdatadoc_out[".OriginalTable"] = "public.doc_out";


$tdatadoc_out[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatadoc_out[".originalPagesByType"] = $tdatadoc_out[".pagesByType"];
$tdatadoc_out[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatadoc_out[".originalPages"] = $tdatadoc_out[".pages"];
$tdatadoc_out[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatadoc_out[".originalDefaultPages"] = $tdatadoc_out[".defaultPages"];

//	field labels
$fieldLabelsdoc_out = array();
$fieldToolTipsdoc_out = array();
$pageTitlesdoc_out = array();
$placeHoldersdoc_out = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsdoc_out["Russian"] = array();
	$fieldToolTipsdoc_out["Russian"] = array();
	$placeHoldersdoc_out["Russian"] = array();
	$pageTitlesdoc_out["Russian"] = array();
	$fieldLabelsdoc_out["Russian"]["docout_id"] = "Docout Id";
	$fieldToolTipsdoc_out["Russian"]["docout_id"] = "";
	$placeHoldersdoc_out["Russian"]["docout_id"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_num"] = "№ исх.";
	$fieldToolTipsdoc_out["Russian"]["doc_num"] = "";
	$placeHoldersdoc_out["Russian"]["doc_num"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_date"] = "Дата исх.";
	$fieldToolTipsdoc_out["Russian"]["doc_date"] = "";
	$placeHoldersdoc_out["Russian"]["doc_date"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_answer"] = "Ответ на:";
	$fieldToolTipsdoc_out["Russian"]["doc_answer"] = "";
	$placeHoldersdoc_out["Russian"]["doc_answer"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_type"] = "Тип";
	$fieldToolTipsdoc_out["Russian"]["doc_type"] = "";
	$placeHoldersdoc_out["Russian"]["doc_type"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_destination"] = "Получатель";
	$fieldToolTipsdoc_out["Russian"]["doc_destination"] = "";
	$placeHoldersdoc_out["Russian"]["doc_destination"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_executor"] = "Исполнитель";
	$fieldToolTipsdoc_out["Russian"]["doc_executor"] = "";
	$placeHoldersdoc_out["Russian"]["doc_executor"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_name"] = "Тема";
	$fieldToolTipsdoc_out["Russian"]["doc_name"] = "";
	$placeHoldersdoc_out["Russian"]["doc_name"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_desc"] = "Описание";
	$fieldToolTipsdoc_out["Russian"]["doc_desc"] = "";
	$placeHoldersdoc_out["Russian"]["doc_desc"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_control_date"] = "Дата контроля";
	$fieldToolTipsdoc_out["Russian"]["doc_control_date"] = "";
	$placeHoldersdoc_out["Russian"]["doc_control_date"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_control_status"] = "На контроль";
	$fieldToolTipsdoc_out["Russian"]["doc_control_status"] = "";
	$placeHoldersdoc_out["Russian"]["doc_control_status"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_control_executiondate"] = "Дата исполнения";
	$fieldToolTipsdoc_out["Russian"]["doc_control_executiondate"] = "";
	$placeHoldersdoc_out["Russian"]["doc_control_executiondate"] = "";
	$fieldLabelsdoc_out["Russian"]["doc_file"] = "Вложение";
	$fieldToolTipsdoc_out["Russian"]["doc_file"] = "";
	$placeHoldersdoc_out["Russian"]["doc_file"] = "";
	if (count($fieldToolTipsdoc_out["Russian"]))
		$tdatadoc_out[".isUseToolTips"] = true;
}


	$tdatadoc_out[".NCSearch"] = true;



$tdatadoc_out[".shortTableName"] = "doc_out";
$tdatadoc_out[".nSecOptions"] = 0;

$tdatadoc_out[".mainTableOwnerID"] = "";
$tdatadoc_out[".entityType"] = 0;
$tdatadoc_out[".connId"] = "itbase3_at_192_168_1_15";


$tdatadoc_out[".strOriginalTableName"] = "public.doc_out";

	



$tdatadoc_out[".showAddInPopup"] = false;

$tdatadoc_out[".showEditInPopup"] = false;

$tdatadoc_out[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatadoc_out[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatadoc_out[".listAjax"] = true;
//	temporary
$tdatadoc_out[".listAjax"] = false;

	$tdatadoc_out[".audit"] = true;

	$tdatadoc_out[".locking"] = true;


$pages = $tdatadoc_out[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatadoc_out[".edit"] = true;
	$tdatadoc_out[".afterEditAction"] = 1;
	$tdatadoc_out[".closePopupAfterEdit"] = 1;
	$tdatadoc_out[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatadoc_out[".add"] = true;
$tdatadoc_out[".afterAddAction"] = 1;
$tdatadoc_out[".closePopupAfterAdd"] = 1;
$tdatadoc_out[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatadoc_out[".list"] = true;
}



$tdatadoc_out[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatadoc_out[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatadoc_out[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatadoc_out[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatadoc_out[".printFriendly"] = true;
}



$tdatadoc_out[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatadoc_out[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatadoc_out[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatadoc_out[".isUseAjaxSuggest"] = true;

$tdatadoc_out[".rowHighlite"] = true;





$tdatadoc_out[".ajaxCodeSnippetAdded"] = false;

$tdatadoc_out[".buttonsAdded"] = false;

$tdatadoc_out[".addPageEvents"] = false;

// use timepicker for search panel
$tdatadoc_out[".isUseTimeForSearch"] = false;


$tdatadoc_out[".badgeColor"] = "778899";


$tdatadoc_out[".allSearchFields"] = array();
$tdatadoc_out[".filterFields"] = array();
$tdatadoc_out[".requiredSearchFields"] = array();

$tdatadoc_out[".googleLikeFields"] = array();
$tdatadoc_out[".googleLikeFields"][] = "docout_id";
$tdatadoc_out[".googleLikeFields"][] = "doc_num";
$tdatadoc_out[".googleLikeFields"][] = "doc_date";
$tdatadoc_out[".googleLikeFields"][] = "doc_answer";
$tdatadoc_out[".googleLikeFields"][] = "doc_type";
$tdatadoc_out[".googleLikeFields"][] = "doc_destination";
$tdatadoc_out[".googleLikeFields"][] = "doc_executor";
$tdatadoc_out[".googleLikeFields"][] = "doc_name";
$tdatadoc_out[".googleLikeFields"][] = "doc_desc";
$tdatadoc_out[".googleLikeFields"][] = "doc_control_date";
$tdatadoc_out[".googleLikeFields"][] = "doc_control_status";
$tdatadoc_out[".googleLikeFields"][] = "doc_control_executiondate";
$tdatadoc_out[".googleLikeFields"][] = "doc_file";



$tdatadoc_out[".tableType"] = "list";

$tdatadoc_out[".printerPageOrientation"] = 0;
$tdatadoc_out[".nPrinterPageScale"] = 100;

$tdatadoc_out[".nPrinterSplitRecords"] = 40;

$tdatadoc_out[".geocodingEnabled"] = false;





$tdatadoc_out[".isResizeColumns"] = true;





$tdatadoc_out[".pageSize"] = 20;

$tdatadoc_out[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatadoc_out[".strOrderBy"] = $tstrOrderBy;

$tdatadoc_out[".orderindexes"] = array();


$tdatadoc_out[".sqlHead"] = "SELECT docout_id,  	doc_num,  	doc_date,  	doc_answer,  	doc_type,  	doc_destination,  	doc_executor,  	doc_name,  	doc_desc,  	doc_control_date,  	doc_control_status,  	doc_control_executiondate,  	doc_file";
$tdatadoc_out[".sqlFrom"] = "FROM \"public\".doc_out";
$tdatadoc_out[".sqlWhereExpr"] = "";
$tdatadoc_out[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatadoc_out[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatadoc_out[".arrGroupsPerPage"] = $arrGPP;

$tdatadoc_out[".highlightSearchResults"] = true;

$tableKeysdoc_out = array();
$tableKeysdoc_out[] = "docout_id";
$tdatadoc_out[".Keys"] = $tableKeysdoc_out;


$tdatadoc_out[".hideMobileList"] = array();




//	docout_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "docout_id";
	$fdata["GoodName"] = "docout_id";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","docout_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "docout_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "docout_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["docout_id"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "docout_id";
//	doc_num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "doc_num";
	$fdata["GoodName"] = "doc_num";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_num");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_num";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_num";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_num"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_num";
//	doc_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "doc_date";
	$fdata["GoodName"] = "doc_date";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_date");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "doc_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_date"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_date";
//	doc_answer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "doc_answer";
	$fdata["GoodName"] = "doc_answer";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_answer");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_answer";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_answer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.doc_in";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "docin_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "doc_outnum || ' от ' || doc_outdate || ' : ' || doc_name    ";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_answer"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_answer";
//	doc_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "doc_type";
	$fdata["GoodName"] = "doc_type";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_type");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_doc_type";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "doctype_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "type_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_type"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_type";
//	doc_destination
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "doc_destination";
	$fdata["GoodName"] = "doc_destination";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_destination");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_destination";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_destination";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_partners";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "partner_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "partner_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_destination"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_destination";
//	doc_executor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "doc_executor";
	$fdata["GoodName"] = "doc_executor";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_executor");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "doc_executor";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_executor";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_executor"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_executor";
//	doc_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "doc_name";
	$fdata["GoodName"] = "doc_name";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_name"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_name";
//	doc_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "doc_desc";
	$fdata["GoodName"] = "doc_desc";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=1024";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_desc"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_desc";
//	doc_control_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "doc_control_date";
	$fdata["GoodName"] = "doc_control_date";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_control_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_control_date"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_control_date";
//	doc_control_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "doc_control_status";
	$fdata["GoodName"] = "doc_control_status";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_control_status");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "doc_control_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatadoc_out["doc_control_status"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_control_status";
//	doc_control_executiondate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "doc_control_executiondate";
	$fdata["GoodName"] = "doc_control_executiondate";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_control_executiondate");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "doc_control_executiondate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_control_executiondate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_control_executiondate"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_control_executiondate";
//	doc_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "doc_file";
	$fdata["GoodName"] = "doc_file";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("public_doc_out","doc_file");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "doc_file";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doc_file";

	
	
				$fdata["UploadFolder"] = "files/docout";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 20;

	
		$edata["maxTotalFilesSize"] = 80000;

	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatadoc_out["doc_file"] = $fdata;
		$tdatadoc_out[".searchableFields"][] = "doc_file";


$tables_data["public.doc_out"]=&$tdatadoc_out;
$field_labels["public_doc_out"] = &$fieldLabelsdoc_out;
$fieldToolTips["public_doc_out"] = &$fieldToolTipsdoc_out;
$placeHolders["public_doc_out"] = &$placeHoldersdoc_out;
$page_titles["public_doc_out"] = &$pageTitlesdoc_out;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.doc_out"] = array();
//	public.doc_in
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_in";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_in";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_in");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.doc_out"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.doc_out"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.doc_out"][$dIndex]["masterKeys"][]="docout_id";

				$detailsTablesData["public.doc_out"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.doc_out"][$dIndex]["detailKeys"][]="doc_answer";
//	report_docin
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="report_docin";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		$detailsParam["dType"]=PAGE_REPORT;
	
		$detailsParam["dShortTable"] = "report_docin";
	$detailsParam["dCaptionTable"] = GetTableCaption("report_docin");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.doc_out"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.doc_out"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.doc_out"][$dIndex]["masterKeys"][]="docout_id";

				$detailsTablesData["public.doc_out"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.doc_out"][$dIndex]["detailKeys"][]="doc_answer";

// tables which are master tables for current table (detail)
$masterTablesData["public.doc_out"] = array();



	
				$strOriginalDetailsTable="public.doc_in";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.doc_in";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "doc_in";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_out"][0] = $masterParams;
				$masterTablesData["public.doc_out"][0]["masterKeys"] = array();
	$masterTablesData["public.doc_out"][0]["masterKeys"][]="docin_id";
				$masterTablesData["public.doc_out"][0]["detailKeys"] = array();
	$masterTablesData["public.doc_out"][0]["detailKeys"][]="doc_answer";
		
	
				$strOriginalDetailsTable="public.sotrudnik";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.sotrudnik";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "sotrudnik";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_out"][1] = $masterParams;
				$masterTablesData["public.doc_out"][1]["masterKeys"] = array();
	$masterTablesData["public.doc_out"][1]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.doc_out"][1]["detailKeys"] = array();
	$masterTablesData["public.doc_out"][1]["detailKeys"][]="doc_executor";
		
	
				$strOriginalDetailsTable="public.spr_doc_type";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_doc_type";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_doc_type";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_out"][2] = $masterParams;
				$masterTablesData["public.doc_out"][2]["masterKeys"] = array();
	$masterTablesData["public.doc_out"][2]["masterKeys"][]="doctype_id";
				$masterTablesData["public.doc_out"][2]["detailKeys"] = array();
	$masterTablesData["public.doc_out"][2]["detailKeys"][]="doc_type";
		
	
				$strOriginalDetailsTable="public.spr_partners";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_partners";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_partners";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.doc_out"][3] = $masterParams;
				$masterTablesData["public.doc_out"][3]["masterKeys"] = array();
	$masterTablesData["public.doc_out"][3]["masterKeys"][]="partner_id";
				$masterTablesData["public.doc_out"][3]["detailKeys"] = array();
	$masterTablesData["public.doc_out"][3]["detailKeys"][]="doc_destination";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_doc_out()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "docout_id,  	doc_num,  	doc_date,  	doc_answer,  	doc_type,  	doc_destination,  	doc_executor,  	doc_name,  	doc_desc,  	doc_control_date,  	doc_control_status,  	doc_control_executiondate,  	doc_file";
$proto0["m_strFrom"] = "FROM \"public\".doc_out";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "docout_id",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto6["m_sql"] = "docout_id";
$proto6["m_srcTableName"] = "public.doc_out";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_num",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto8["m_sql"] = "doc_num";
$proto8["m_srcTableName"] = "public.doc_out";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_date",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto10["m_sql"] = "doc_date";
$proto10["m_srcTableName"] = "public.doc_out";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_answer",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto12["m_sql"] = "doc_answer";
$proto12["m_srcTableName"] = "public.doc_out";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_type",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto14["m_sql"] = "doc_type";
$proto14["m_srcTableName"] = "public.doc_out";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_destination",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto16["m_sql"] = "doc_destination";
$proto16["m_srcTableName"] = "public.doc_out";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_executor",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto18["m_sql"] = "doc_executor";
$proto18["m_srcTableName"] = "public.doc_out";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_name",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto20["m_sql"] = "doc_name";
$proto20["m_srcTableName"] = "public.doc_out";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_desc",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto22["m_sql"] = "doc_desc";
$proto22["m_srcTableName"] = "public.doc_out";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_date",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto24["m_sql"] = "doc_control_date";
$proto24["m_srcTableName"] = "public.doc_out";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto26["m_sql"] = "doc_control_status";
$proto26["m_srcTableName"] = "public.doc_out";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto28["m_sql"] = "doc_control_executiondate";
$proto28["m_srcTableName"] = "public.doc_out";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_file",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "public.doc_out"
));

$proto30["m_sql"] = "doc_file";
$proto30["m_srcTableName"] = "public.doc_out";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto32=array();
$proto32["m_link"] = "SQLL_MAIN";
			$proto33=array();
$proto33["m_strName"] = "public.doc_out";
$proto33["m_srcTableName"] = "public.doc_out";
$proto33["m_columns"] = array();
$proto33["m_columns"][] = "docout_id";
$proto33["m_columns"][] = "doc_num";
$proto33["m_columns"][] = "doc_date";
$proto33["m_columns"][] = "doc_answer";
$proto33["m_columns"][] = "doc_type";
$proto33["m_columns"][] = "doc_destination";
$proto33["m_columns"][] = "doc_executor";
$proto33["m_columns"][] = "doc_name";
$proto33["m_columns"][] = "doc_desc";
$proto33["m_columns"][] = "doc_control_date";
$proto33["m_columns"][] = "doc_control_status";
$proto33["m_columns"][] = "doc_control_executiondate";
$proto33["m_columns"][] = "doc_file";
$obj = new SQLTable($proto33);

$proto32["m_table"] = $obj;
$proto32["m_sql"] = "\"public\".doc_out";
$proto32["m_alias"] = "";
$proto32["m_srcTableName"] = "public.doc_out";
$proto34=array();
$proto34["m_sql"] = "";
$proto34["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto34["m_column"]=$obj;
$proto34["m_contained"] = array();
$proto34["m_strCase"] = "";
$proto34["m_havingmode"] = false;
$proto34["m_inBrackets"] = false;
$proto34["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto34);

$proto32["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto32);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.doc_out";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_doc_out = createSqlQuery_doc_out();


	
		;

													

$tdatadoc_out[".sqlquery"] = $queryData_doc_out;



$tableEvents["public.doc_out"] = new eventsBase;
$tdatadoc_out[".hasEvents"] = false;

?>