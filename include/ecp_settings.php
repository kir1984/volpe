<?php
$tdataecp = array();
$tdataecp[".searchableFields"] = array();
$tdataecp[".ShortName"] = "ecp";
$tdataecp[".OwnerID"] = "";
$tdataecp[".OriginalTable"] = "public.ecp";


$tdataecp[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataecp[".originalPagesByType"] = $tdataecp[".pagesByType"];
$tdataecp[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataecp[".originalPages"] = $tdataecp[".pages"];
$tdataecp[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataecp[".originalDefaultPages"] = $tdataecp[".defaultPages"];

//	field labels
$fieldLabelsecp = array();
$fieldToolTipsecp = array();
$pageTitlesecp = array();
$placeHoldersecp = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsecp["Russian"] = array();
	$fieldToolTipsecp["Russian"] = array();
	$placeHoldersecp["Russian"] = array();
	$pageTitlesecp["Russian"] = array();
	$fieldLabelsecp["Russian"]["ecp_id"] = "Ecp Id";
	$fieldToolTipsecp["Russian"]["ecp_id"] = "";
	$placeHoldersecp["Russian"]["ecp_id"] = "";
	$fieldLabelsecp["Russian"]["ecp_user"] = "Пользователь";
	$fieldToolTipsecp["Russian"]["ecp_user"] = "Кто использует сертификат";
	$placeHoldersecp["Russian"]["ecp_user"] = "";
	$fieldLabelsecp["Russian"]["ecp_storage"] = "Хранилище";
	$fieldToolTipsecp["Russian"]["ecp_storage"] = "Где хранится, носитель";
	$placeHoldersecp["Russian"]["ecp_storage"] = "";
	$fieldLabelsecp["Russian"]["spr_ecp"] = "Сертификат";
	$fieldToolTipsecp["Russian"]["spr_ecp"] = "";
	$placeHoldersecp["Russian"]["spr_ecp"] = "";
	if (count($fieldToolTipsecp["Russian"]))
		$tdataecp[".isUseToolTips"] = true;
}


	$tdataecp[".NCSearch"] = true;



$tdataecp[".shortTableName"] = "ecp";
$tdataecp[".nSecOptions"] = 0;

$tdataecp[".mainTableOwnerID"] = "";
$tdataecp[".entityType"] = 0;
$tdataecp[".connId"] = "itbase3_at_192_168_1_15";


$tdataecp[".strOriginalTableName"] = "public.ecp";

	



$tdataecp[".showAddInPopup"] = false;

$tdataecp[".showEditInPopup"] = false;

$tdataecp[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataecp[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataecp[".listAjax"] = true;
//	temporary
$tdataecp[".listAjax"] = false;

	$tdataecp[".audit"] = true;

	$tdataecp[".locking"] = true;


$pages = $tdataecp[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataecp[".edit"] = true;
	$tdataecp[".afterEditAction"] = 1;
	$tdataecp[".closePopupAfterEdit"] = 1;
	$tdataecp[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataecp[".add"] = true;
$tdataecp[".afterAddAction"] = 1;
$tdataecp[".closePopupAfterAdd"] = 1;
$tdataecp[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataecp[".list"] = true;
}



$tdataecp[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataecp[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataecp[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataecp[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataecp[".printFriendly"] = true;
}



$tdataecp[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataecp[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataecp[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataecp[".isUseAjaxSuggest"] = true;

$tdataecp[".rowHighlite"] = true;





$tdataecp[".ajaxCodeSnippetAdded"] = false;

$tdataecp[".buttonsAdded"] = false;

$tdataecp[".addPageEvents"] = false;

// use timepicker for search panel
$tdataecp[".isUseTimeForSearch"] = false;


$tdataecp[".badgeColor"] = "2f4f4f";


$tdataecp[".allSearchFields"] = array();
$tdataecp[".filterFields"] = array();
$tdataecp[".requiredSearchFields"] = array();

$tdataecp[".googleLikeFields"] = array();
$tdataecp[".googleLikeFields"][] = "ecp_id";
$tdataecp[".googleLikeFields"][] = "ecp_user";
$tdataecp[".googleLikeFields"][] = "ecp_storage";
$tdataecp[".googleLikeFields"][] = "spr_ecp";



$tdataecp[".tableType"] = "list";

$tdataecp[".printerPageOrientation"] = 0;
$tdataecp[".nPrinterPageScale"] = 100;

$tdataecp[".nPrinterSplitRecords"] = 40;

$tdataecp[".geocodingEnabled"] = false;





$tdataecp[".isResizeColumns"] = true;





$tdataecp[".pageSize"] = 20;

$tdataecp[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataecp[".strOrderBy"] = $tstrOrderBy;

$tdataecp[".orderindexes"] = array();


$tdataecp[".sqlHead"] = "SELECT ecp_id,  	ecp_user,  	ecp_storage,  	spr_ecp";
$tdataecp[".sqlFrom"] = "FROM \"public\".ecp";
$tdataecp[".sqlWhereExpr"] = "";
$tdataecp[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataecp[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataecp[".arrGroupsPerPage"] = $arrGPP;

$tdataecp[".highlightSearchResults"] = true;

$tableKeysecp = array();
$tableKeysecp[] = "ecp_id";
$tdataecp[".Keys"] = $tableKeysecp;


$tdataecp[".hideMobileList"] = array();




//	ecp_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ecp_id";
	$fdata["GoodName"] = "ecp_id";
	$fdata["ownerTable"] = "public.ecp";
	$fdata["Label"] = GetFieldLabel("public_ecp","ecp_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "ecp_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataecp["ecp_id"] = $fdata;
		$tdataecp[".searchableFields"][] = "ecp_id";
//	ecp_user
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ecp_user";
	$fdata["GoodName"] = "ecp_user";
	$fdata["ownerTable"] = "public.ecp";
	$fdata["Label"] = GetFieldLabel("public_ecp","ecp_user");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "ecp_user";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_user";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataecp["ecp_user"] = $fdata;
		$tdataecp[".searchableFields"][] = "ecp_user";
//	ecp_storage
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ecp_storage";
	$fdata["GoodName"] = "ecp_storage";
	$fdata["ownerTable"] = "public.ecp";
	$fdata["Label"] = GetFieldLabel("public_ecp","ecp_storage");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ecp_storage";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_storage";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Реестр";
	$edata["LookupValues"][] = "RuToken";
	$edata["LookupValues"][] = "eToken";
	$edata["LookupValues"][] = "JaCarta";
	$edata["LookupValues"][] = "Флешка";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataecp["ecp_storage"] = $fdata;
		$tdataecp[".searchableFields"][] = "ecp_storage";
//	spr_ecp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "spr_ecp";
	$fdata["GoodName"] = "spr_ecp";
	$fdata["ownerTable"] = "public.ecp";
	$fdata["Label"] = GetFieldLabel("public_ecp","spr_ecp");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "spr_ecp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "spr_ecp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_ecp";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sprecp_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "ecp_number || ' на ' || ecp_owner";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataecp["spr_ecp"] = $fdata;
		$tdataecp[".searchableFields"][] = "spr_ecp";


$tables_data["public.ecp"]=&$tdataecp;
$field_labels["public_ecp"] = &$fieldLabelsecp;
$fieldToolTips["public_ecp"] = &$fieldToolTipsecp;
$placeHolders["public_ecp"] = &$placeHoldersecp;
$page_titles["public_ecp"] = &$pageTitlesecp;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.ecp"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.ecp"] = array();



	
				$strOriginalDetailsTable="public.sotrudnik";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.sotrudnik";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "sotrudnik";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.ecp"][0] = $masterParams;
				$masterTablesData["public.ecp"][0]["masterKeys"] = array();
	$masterTablesData["public.ecp"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.ecp"][0]["detailKeys"] = array();
	$masterTablesData["public.ecp"][0]["detailKeys"][]="ecp_user";
		
	
				$strOriginalDetailsTable="public.spr_ecp";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_ecp";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_ecp";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.ecp"][1] = $masterParams;
				$masterTablesData["public.ecp"][1]["masterKeys"] = array();
	$masterTablesData["public.ecp"][1]["masterKeys"][]="sprecp_id";
				$masterTablesData["public.ecp"][1]["detailKeys"] = array();
	$masterTablesData["public.ecp"][1]["detailKeys"][]="spr_ecp";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_ecp()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ecp_id,  	ecp_user,  	ecp_storage,  	spr_ecp";
$proto0["m_strFrom"] = "FROM \"public\".ecp";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_id",
	"m_strTable" => "public.ecp",
	"m_srcTableName" => "public.ecp"
));

$proto6["m_sql"] = "ecp_id";
$proto6["m_srcTableName"] = "public.ecp";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_user",
	"m_strTable" => "public.ecp",
	"m_srcTableName" => "public.ecp"
));

$proto8["m_sql"] = "ecp_user";
$proto8["m_srcTableName"] = "public.ecp";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_storage",
	"m_strTable" => "public.ecp",
	"m_srcTableName" => "public.ecp"
));

$proto10["m_sql"] = "ecp_storage";
$proto10["m_srcTableName"] = "public.ecp";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "spr_ecp",
	"m_strTable" => "public.ecp",
	"m_srcTableName" => "public.ecp"
));

$proto12["m_sql"] = "spr_ecp";
$proto12["m_srcTableName"] = "public.ecp";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "public.ecp";
$proto15["m_srcTableName"] = "public.ecp";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "ecp_id";
$proto15["m_columns"][] = "ecp_user";
$proto15["m_columns"][] = "ecp_storage";
$proto15["m_columns"][] = "spr_ecp";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "\"public\".ecp";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "public.ecp";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.ecp";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ecp = createSqlQuery_ecp();


	
		;

				

$tdataecp[".sqlquery"] = $queryData_ecp;



$tableEvents["public.ecp"] = new eventsBase;
$tdataecp[".hasEvents"] = false;

?>