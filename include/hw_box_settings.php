<?php
$tdatahw_box = array();
$tdatahw_box[".searchableFields"] = array();
$tdatahw_box[".ShortName"] = "hw_box";
$tdatahw_box[".OwnerID"] = "";
$tdatahw_box[".OriginalTable"] = "public.hw_box";


$tdatahw_box[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_box[".originalPagesByType"] = $tdatahw_box[".pagesByType"];
$tdatahw_box[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_box[".originalPages"] = $tdatahw_box[".pages"];
$tdatahw_box[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_box[".originalDefaultPages"] = $tdatahw_box[".defaultPages"];

//	field labels
$fieldLabelshw_box = array();
$fieldToolTipshw_box = array();
$pageTitleshw_box = array();
$placeHoldershw_box = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_box["Russian"] = array();
	$fieldToolTipshw_box["Russian"] = array();
	$placeHoldershw_box["Russian"] = array();
	$pageTitleshw_box["Russian"] = array();
	$fieldLabelshw_box["Russian"]["box_id"] = "Box Id";
	$fieldToolTipshw_box["Russian"]["box_id"] = "";
	$placeHoldershw_box["Russian"]["box_id"] = "";
	$fieldLabelshw_box["Russian"]["box_name"] = "Имя компьютера";
	$fieldToolTipshw_box["Russian"]["box_name"] = "";
	$placeHoldershw_box["Russian"]["box_name"] = "";
	$fieldLabelshw_box["Russian"]["box_inv"] = "Инв. номер";
	$fieldToolTipshw_box["Russian"]["box_inv"] = "";
	$placeHoldershw_box["Russian"]["box_inv"] = "";
	$fieldLabelshw_box["Russian"]["box_cpu"] = "Процессор";
	$fieldToolTipshw_box["Russian"]["box_cpu"] = "пример: i3-2450";
	$placeHoldershw_box["Russian"]["box_cpu"] = "";
	$fieldLabelshw_box["Russian"]["box_cpusocket"] = "Сокет";
	$fieldToolTipshw_box["Russian"]["box_cpusocket"] = "пример: 775";
	$placeHoldershw_box["Russian"]["box_cpusocket"] = "";
	$fieldLabelshw_box["Russian"]["box_ramtype"] = "DDR тип";
	$fieldToolTipshw_box["Russian"]["box_ramtype"] = "";
	$placeHoldershw_box["Russian"]["box_ramtype"] = "";
	$fieldLabelshw_box["Russian"]["box_ram"] = "RAM объем";
	$fieldToolTipshw_box["Russian"]["box_ram"] = "пример: 2";
	$placeHoldershw_box["Russian"]["box_ram"] = "";
	$fieldLabelshw_box["Russian"]["box_disktype1"] = "Диск1 тип";
	$fieldToolTipshw_box["Russian"]["box_disktype1"] = "";
	$placeHoldershw_box["Russian"]["box_disktype1"] = "";
	$fieldLabelshw_box["Russian"]["box_disk1"] = "Диск1 объем";
	$fieldToolTipshw_box["Russian"]["box_disk1"] = "пример: 500";
	$placeHoldershw_box["Russian"]["box_disk1"] = "";
	$fieldLabelshw_box["Russian"]["box_disktype2"] = "Диск2 тип";
	$fieldToolTipshw_box["Russian"]["box_disktype2"] = "";
	$placeHoldershw_box["Russian"]["box_disktype2"] = "";
	$fieldLabelshw_box["Russian"]["box_disk2"] = "Диск2 объем";
	$fieldToolTipshw_box["Russian"]["box_disk2"] = "пример: 500";
	$placeHoldershw_box["Russian"]["box_disk2"] = "";
	$fieldLabelshw_box["Russian"]["box_mb"] = "Материнская плата";
	$fieldToolTipshw_box["Russian"]["box_mb"] = "Полностью марка и модель";
	$placeHoldershw_box["Russian"]["box_mb"] = "";
	$fieldLabelshw_box["Russian"]["box_mac"] = "MAC-адрес";
	$fieldToolTipshw_box["Russian"]["box_mac"] = "пример: aa:bb:cc:dd:11:22";
	$placeHoldershw_box["Russian"]["box_mac"] = "";
	$fieldLabelshw_box["Russian"]["box_ip"] = "IP-адрес";
	$fieldToolTipshw_box["Russian"]["box_ip"] = "";
	$placeHoldershw_box["Russian"]["box_ip"] = "";
	if (count($fieldToolTipshw_box["Russian"]))
		$tdatahw_box[".isUseToolTips"] = true;
}


	$tdatahw_box[".NCSearch"] = true;



$tdatahw_box[".shortTableName"] = "hw_box";
$tdatahw_box[".nSecOptions"] = 0;

$tdatahw_box[".mainTableOwnerID"] = "";
$tdatahw_box[".entityType"] = 0;
$tdatahw_box[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_box[".strOriginalTableName"] = "public.hw_box";

	



$tdatahw_box[".showAddInPopup"] = false;

$tdatahw_box[".showEditInPopup"] = false;

$tdatahw_box[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_box[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_box[".listAjax"] = true;
//	temporary
$tdatahw_box[".listAjax"] = false;

	$tdatahw_box[".audit"] = true;

	$tdatahw_box[".locking"] = true;


$pages = $tdatahw_box[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_box[".edit"] = true;
	$tdatahw_box[".afterEditAction"] = 1;
	$tdatahw_box[".closePopupAfterEdit"] = 1;
	$tdatahw_box[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_box[".add"] = true;
$tdatahw_box[".afterAddAction"] = 1;
$tdatahw_box[".closePopupAfterAdd"] = 1;
$tdatahw_box[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_box[".list"] = true;
}



$tdatahw_box[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_box[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_box[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_box[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_box[".printFriendly"] = true;
}



$tdatahw_box[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_box[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_box[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_box[".isUseAjaxSuggest"] = true;

$tdatahw_box[".rowHighlite"] = true;





$tdatahw_box[".ajaxCodeSnippetAdded"] = false;

$tdatahw_box[".buttonsAdded"] = false;

$tdatahw_box[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_box[".isUseTimeForSearch"] = false;


$tdatahw_box[".badgeColor"] = "bc8f8f";


$tdatahw_box[".allSearchFields"] = array();
$tdatahw_box[".filterFields"] = array();
$tdatahw_box[".requiredSearchFields"] = array();

$tdatahw_box[".googleLikeFields"] = array();
$tdatahw_box[".googleLikeFields"][] = "box_id";
$tdatahw_box[".googleLikeFields"][] = "box_name";
$tdatahw_box[".googleLikeFields"][] = "box_inv";
$tdatahw_box[".googleLikeFields"][] = "box_cpu";
$tdatahw_box[".googleLikeFields"][] = "box_cpusocket";
$tdatahw_box[".googleLikeFields"][] = "box_ramtype";
$tdatahw_box[".googleLikeFields"][] = "box_ram";
$tdatahw_box[".googleLikeFields"][] = "box_disktype1";
$tdatahw_box[".googleLikeFields"][] = "box_disk1";
$tdatahw_box[".googleLikeFields"][] = "box_disktype2";
$tdatahw_box[".googleLikeFields"][] = "box_disk2";
$tdatahw_box[".googleLikeFields"][] = "box_mb";
$tdatahw_box[".googleLikeFields"][] = "box_mac";
$tdatahw_box[".googleLikeFields"][] = "box_ip";



$tdatahw_box[".tableType"] = "list";

$tdatahw_box[".printerPageOrientation"] = 0;
$tdatahw_box[".nPrinterPageScale"] = 100;

$tdatahw_box[".nPrinterSplitRecords"] = 40;

$tdatahw_box[".geocodingEnabled"] = false;




$tdatahw_box[".isDisplayLoading"] = true;

$tdatahw_box[".isResizeColumns"] = true;





$tdatahw_box[".pageSize"] = 20;

$tdatahw_box[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_box[".strOrderBy"] = $tstrOrderBy;

$tdatahw_box[".orderindexes"] = array();


$tdatahw_box[".sqlHead"] = "SELECT box_id,  	box_name,  	box_inv,  	box_cpu,  	box_cpusocket,  	box_ramtype,  	box_ram,  	box_disktype1,  	box_disk1,  	box_disktype2,  	box_disk2,  	box_mb,  	box_mac,  	box_ip";
$tdatahw_box[".sqlFrom"] = "FROM \"public\".hw_box";
$tdatahw_box[".sqlWhereExpr"] = "";
$tdatahw_box[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",
	'showRowCount' => 0,
	'hideEmpty' => 0,
);
$tdatahw_box[".arrGridTabs"] = $arrGridTabs;









//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_box[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_box[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_box[".highlightSearchResults"] = true;

$tableKeyshw_box = array();
$tableKeyshw_box[] = "box_id";
$tdatahw_box[".Keys"] = $tableKeyshw_box;


$tdatahw_box[".hideMobileList"] = array();




//	box_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "box_id";
	$fdata["GoodName"] = "box_id";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "box_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_id"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_id";
//	box_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "box_name";
	$fdata["GoodName"] = "box_name";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_name"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_name";
//	box_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "box_inv";
	$fdata["GoodName"] = "box_inv";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_inv");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "box_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterlist"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["masterprint"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_inv"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_inv";
//	box_cpu
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "box_cpu";
	$fdata["GoodName"] = "box_cpu";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_cpu");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_cpu";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_cpu";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_cpu"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_cpu";
//	box_cpusocket
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "box_cpusocket";
	$fdata["GoodName"] = "box_cpusocket";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_cpusocket");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_cpusocket";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_cpusocket";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_cpusocket"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_cpusocket";
//	box_ramtype
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "box_ramtype";
	$fdata["GoodName"] = "box_ramtype";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_ramtype");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_ramtype";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_ramtype";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "2";
	$edata["LookupValues"][] = "3";
	$edata["LookupValues"][] = "4";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_ramtype"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_ramtype";
//	box_ram
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "box_ram";
	$fdata["GoodName"] = "box_ram";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_ram");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_ram";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_ram";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_ram"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_ram";
//	box_disktype1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "box_disktype1";
	$fdata["GoodName"] = "box_disktype1";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_disktype1");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_disktype1";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_disktype1";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "HDD";
	$edata["LookupValues"][] = "SSD";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_disktype1"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_disktype1";
//	box_disk1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "box_disk1";
	$fdata["GoodName"] = "box_disk1";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_disk1");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_disk1";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_disk1";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_disk1"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_disk1";
//	box_disktype2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "box_disktype2";
	$fdata["GoodName"] = "box_disktype2";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_disktype2");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_disktype2";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_disktype2";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "HDD";
	$edata["LookupValues"][] = "SSD";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_disktype2"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_disktype2";
//	box_disk2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "box_disk2";
	$fdata["GoodName"] = "box_disk2";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_disk2");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_disk2";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_disk2";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_disk2"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_disk2";
//	box_mb
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "box_mb";
	$fdata["GoodName"] = "box_mb";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_mb");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_mb";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_mb";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_mb"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_mb";
//	box_mac
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "box_mac";
	$fdata["GoodName"] = "box_mac";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_mac");
	$fdata["FieldType"] = 13;

	
	
	
			

		$fdata["strField"] = "box_mac";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_mac";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_mac"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_mac";
//	box_ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "box_ip";
	$fdata["GoodName"] = "box_ip";
	$fdata["ownerTable"] = "public.hw_box";
	$fdata["Label"] = GetFieldLabel("public_hw_box","box_ip");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "box_ip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "box_ip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_box["box_ip"] = $fdata;
		$tdatahw_box[".searchableFields"][] = "box_ip";


$tables_data["public.hw_box"]=&$tdatahw_box;
$field_labels["public_hw_box"] = &$fieldLabelshw_box;
$fieldToolTips["public_hw_box"] = &$fieldToolTipshw_box;
$placeHolders["public_hw_box"] = &$placeHoldershw_box;
$page_titles["public_hw_box"] = &$pageTitleshw_box;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_box"] = array();
//	public.arm_po
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.arm_po";
		$detailsParam["dOriginalTable"] = "public.arm_po";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "arm_po";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_arm_po");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.hw_box"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.hw_box"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.hw_box"][$dIndex]["masterKeys"][]="box_id";

				$detailsTablesData["public.hw_box"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.hw_box"][$dIndex]["detailKeys"][]="boxid";
//	public.hw_monitor1
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_monitor1";
		$detailsParam["dOriginalTable"] = "public.hw_monitor1";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_monitor1";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_monitor1");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.hw_box"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.hw_box"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.hw_box"][$dIndex]["masterKeys"][]="box_id";

				$detailsTablesData["public.hw_box"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.hw_box"][$dIndex]["detailKeys"][]="boxid";

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_box"] = array();



	
				$strOriginalDetailsTable="public.arm";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.arm";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "arm";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_box"][0] = $masterParams;
				$masterTablesData["public.hw_box"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_box"][0]["masterKeys"][]="box_id";
				$masterTablesData["public.hw_box"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_box"][0]["detailKeys"][]="box_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_box()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "box_id,  	box_name,  	box_inv,  	box_cpu,  	box_cpusocket,  	box_ramtype,  	box_ram,  	box_disktype1,  	box_disk1,  	box_disktype2,  	box_disk2,  	box_mb,  	box_mac,  	box_ip";
$proto0["m_strFrom"] = "FROM \"public\".hw_box";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "box_id",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto6["m_sql"] = "box_id";
$proto6["m_srcTableName"] = "public.hw_box";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "box_name",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto8["m_sql"] = "box_name";
$proto8["m_srcTableName"] = "public.hw_box";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "box_inv",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto10["m_sql"] = "box_inv";
$proto10["m_srcTableName"] = "public.hw_box";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "box_cpu",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto12["m_sql"] = "box_cpu";
$proto12["m_srcTableName"] = "public.hw_box";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "box_cpusocket",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto14["m_sql"] = "box_cpusocket";
$proto14["m_srcTableName"] = "public.hw_box";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "box_ramtype",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto16["m_sql"] = "box_ramtype";
$proto16["m_srcTableName"] = "public.hw_box";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "box_ram",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto18["m_sql"] = "box_ram";
$proto18["m_srcTableName"] = "public.hw_box";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "box_disktype1",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto20["m_sql"] = "box_disktype1";
$proto20["m_srcTableName"] = "public.hw_box";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "box_disk1",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto22["m_sql"] = "box_disk1";
$proto22["m_srcTableName"] = "public.hw_box";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "box_disktype2",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto24["m_sql"] = "box_disktype2";
$proto24["m_srcTableName"] = "public.hw_box";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "box_disk2",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto26["m_sql"] = "box_disk2";
$proto26["m_srcTableName"] = "public.hw_box";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "box_mb",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto28["m_sql"] = "box_mb";
$proto28["m_srcTableName"] = "public.hw_box";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "box_mac",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto30["m_sql"] = "box_mac";
$proto30["m_srcTableName"] = "public.hw_box";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "box_ip",
	"m_strTable" => "public.hw_box",
	"m_srcTableName" => "public.hw_box"
));

$proto32["m_sql"] = "box_ip";
$proto32["m_srcTableName"] = "public.hw_box";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "public.hw_box";
$proto35["m_srcTableName"] = "public.hw_box";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "box_id";
$proto35["m_columns"][] = "box_name";
$proto35["m_columns"][] = "box_inv";
$proto35["m_columns"][] = "box_cpu";
$proto35["m_columns"][] = "box_cpusocket";
$proto35["m_columns"][] = "box_ramtype";
$proto35["m_columns"][] = "box_ram";
$proto35["m_columns"][] = "box_disktype1";
$proto35["m_columns"][] = "box_disk1";
$proto35["m_columns"][] = "box_disktype2";
$proto35["m_columns"][] = "box_disk2";
$proto35["m_columns"][] = "box_mb";
$proto35["m_columns"][] = "box_mac";
$proto35["m_columns"][] = "box_ip";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "\"public\".hw_box";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "public.hw_box";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_box";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_box = createSqlQuery_hw_box();


	
		;

														

$tdatahw_box[".sqlquery"] = $queryData_hw_box;



$tableEvents["public.hw_box"] = new eventsBase;
$tdatahw_box[".hasEvents"] = false;

?>