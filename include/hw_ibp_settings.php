<?php
$tdatahw_ibp = array();
$tdatahw_ibp[".searchableFields"] = array();
$tdatahw_ibp[".ShortName"] = "hw_ibp";
$tdatahw_ibp[".OwnerID"] = "";
$tdatahw_ibp[".OriginalTable"] = "public.hw_ibp";


$tdatahw_ibp[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_ibp[".originalPagesByType"] = $tdatahw_ibp[".pagesByType"];
$tdatahw_ibp[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_ibp[".originalPages"] = $tdatahw_ibp[".pages"];
$tdatahw_ibp[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_ibp[".originalDefaultPages"] = $tdatahw_ibp[".defaultPages"];

//	field labels
$fieldLabelshw_ibp = array();
$fieldToolTipshw_ibp = array();
$pageTitleshw_ibp = array();
$placeHoldershw_ibp = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_ibp["Russian"] = array();
	$fieldToolTipshw_ibp["Russian"] = array();
	$placeHoldershw_ibp["Russian"] = array();
	$pageTitleshw_ibp["Russian"] = array();
	$fieldLabelshw_ibp["Russian"]["ibp_id"] = "Ibp Id";
	$fieldToolTipshw_ibp["Russian"]["ibp_id"] = "";
	$placeHoldershw_ibp["Russian"]["ibp_id"] = "";
	$fieldLabelshw_ibp["Russian"]["ibp_name"] = "Наименование";
	$fieldToolTipshw_ibp["Russian"]["ibp_name"] = "Марка и модель";
	$placeHoldershw_ibp["Russian"]["ibp_name"] = "";
	$fieldLabelshw_ibp["Russian"]["ibp_inv"] = "Инв. номер";
	$fieldToolTipshw_ibp["Russian"]["ibp_inv"] = "";
	$placeHoldershw_ibp["Russian"]["ibp_inv"] = "";
	$fieldLabelshw_ibp["Russian"]["ibp_akkum"] = "Модель акк.";
	$fieldToolTipshw_ibp["Russian"]["ibp_akkum"] = "";
	$placeHoldershw_ibp["Russian"]["ibp_akkum"] = "";
	$fieldLabelshw_ibp["Russian"]["ibp_akkum_lastchange"] = "Последняя замена акк.";
	$fieldToolTipshw_ibp["Russian"]["ibp_akkum_lastchange"] = "";
	$placeHoldershw_ibp["Russian"]["ibp_akkum_lastchange"] = "";
	$fieldLabelshw_ibp["Russian"]["ibp_user"] = "Пользователь";
	$fieldToolTipshw_ibp["Russian"]["ibp_user"] = "";
	$placeHoldershw_ibp["Russian"]["ibp_user"] = "";
	if (count($fieldToolTipshw_ibp["Russian"]))
		$tdatahw_ibp[".isUseToolTips"] = true;
}


	$tdatahw_ibp[".NCSearch"] = true;



$tdatahw_ibp[".shortTableName"] = "hw_ibp";
$tdatahw_ibp[".nSecOptions"] = 0;

$tdatahw_ibp[".mainTableOwnerID"] = "";
$tdatahw_ibp[".entityType"] = 0;
$tdatahw_ibp[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_ibp[".strOriginalTableName"] = "public.hw_ibp";

	



$tdatahw_ibp[".showAddInPopup"] = false;

$tdatahw_ibp[".showEditInPopup"] = false;

$tdatahw_ibp[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_ibp[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_ibp[".listAjax"] = true;
//	temporary
$tdatahw_ibp[".listAjax"] = false;

	$tdatahw_ibp[".audit"] = true;

	$tdatahw_ibp[".locking"] = true;


$pages = $tdatahw_ibp[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_ibp[".edit"] = true;
	$tdatahw_ibp[".afterEditAction"] = 1;
	$tdatahw_ibp[".closePopupAfterEdit"] = 1;
	$tdatahw_ibp[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_ibp[".add"] = true;
$tdatahw_ibp[".afterAddAction"] = 1;
$tdatahw_ibp[".closePopupAfterAdd"] = 1;
$tdatahw_ibp[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_ibp[".list"] = true;
}



$tdatahw_ibp[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_ibp[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_ibp[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_ibp[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_ibp[".printFriendly"] = true;
}



$tdatahw_ibp[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_ibp[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_ibp[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_ibp[".isUseAjaxSuggest"] = true;

$tdatahw_ibp[".rowHighlite"] = true;





$tdatahw_ibp[".ajaxCodeSnippetAdded"] = false;

$tdatahw_ibp[".buttonsAdded"] = false;

$tdatahw_ibp[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_ibp[".isUseTimeForSearch"] = false;


$tdatahw_ibp[".badgeColor"] = "bc8f8f";


$tdatahw_ibp[".allSearchFields"] = array();
$tdatahw_ibp[".filterFields"] = array();
$tdatahw_ibp[".requiredSearchFields"] = array();

$tdatahw_ibp[".googleLikeFields"] = array();
$tdatahw_ibp[".googleLikeFields"][] = "ibp_id";
$tdatahw_ibp[".googleLikeFields"][] = "ibp_name";
$tdatahw_ibp[".googleLikeFields"][] = "ibp_inv";
$tdatahw_ibp[".googleLikeFields"][] = "ibp_akkum";
$tdatahw_ibp[".googleLikeFields"][] = "ibp_akkum_lastchange";
$tdatahw_ibp[".googleLikeFields"][] = "ibp_user";



$tdatahw_ibp[".tableType"] = "list";

$tdatahw_ibp[".printerPageOrientation"] = 0;
$tdatahw_ibp[".nPrinterPageScale"] = 100;

$tdatahw_ibp[".nPrinterSplitRecords"] = 40;

$tdatahw_ibp[".geocodingEnabled"] = false;




$tdatahw_ibp[".isDisplayLoading"] = true;

$tdatahw_ibp[".isResizeColumns"] = true;





$tdatahw_ibp[".pageSize"] = 20;

$tdatahw_ibp[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_ibp[".strOrderBy"] = $tstrOrderBy;

$tdatahw_ibp[".orderindexes"] = array();


$tdatahw_ibp[".sqlHead"] = "SELECT ibp_id,  	ibp_name,  	ibp_inv,  	ibp_akkum,  	ibp_akkum_lastchange,  	ibp_user";
$tdatahw_ibp[".sqlFrom"] = "FROM \"public\".hw_ibp";
$tdatahw_ibp[".sqlWhereExpr"] = "";
$tdatahw_ibp[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_ibp[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_ibp[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_ibp[".highlightSearchResults"] = true;

$tableKeyshw_ibp = array();
$tableKeyshw_ibp[] = "ibp_id";
$tdatahw_ibp[".Keys"] = $tableKeyshw_ibp;


$tdatahw_ibp[".hideMobileList"] = array();




//	ibp_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ibp_id";
	$fdata["GoodName"] = "ibp_id";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "ibp_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_id"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_id";
//	ibp_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ibp_name";
	$fdata["GoodName"] = "ibp_name";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ibp_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_name"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_name";
//	ibp_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ibp_inv";
	$fdata["GoodName"] = "ibp_inv";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_inv");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "ibp_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_inv"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_inv";
//	ibp_akkum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ibp_akkum";
	$fdata["GoodName"] = "ibp_akkum";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_akkum");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ibp_akkum";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_akkum";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_akkum"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_akkum";
//	ibp_akkum_lastchange
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ibp_akkum_lastchange";
	$fdata["GoodName"] = "ibp_akkum_lastchange";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_akkum_lastchange");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ibp_akkum_lastchange";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_akkum_lastchange";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_akkum_lastchange"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_akkum_lastchange";
//	ibp_user
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ibp_user";
	$fdata["GoodName"] = "ibp_user";
	$fdata["ownerTable"] = "public.hw_ibp";
	$fdata["Label"] = GetFieldLabel("public_hw_ibp","ibp_user");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "ibp_user";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ibp_user";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.sotrudnik";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sotrudnik_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "sotrudnik_fio";

				$edata["LookupWhere"] = "sotrudnik_dismissed != \"true\"";


	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_ibp["ibp_user"] = $fdata;
		$tdatahw_ibp[".searchableFields"][] = "ibp_user";


$tables_data["public.hw_ibp"]=&$tdatahw_ibp;
$field_labels["public_hw_ibp"] = &$fieldLabelshw_ibp;
$fieldToolTips["public_hw_ibp"] = &$fieldToolTipshw_ibp;
$placeHolders["public_hw_ibp"] = &$placeHoldershw_ibp;
$page_titles["public_hw_ibp"] = &$pageTitleshw_ibp;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_ibp"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_ibp"] = array();



	
				$strOriginalDetailsTable="public.sotrudnik";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.sotrudnik";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "sotrudnik";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_ibp"][0] = $masterParams;
				$masterTablesData["public.hw_ibp"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_ibp"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.hw_ibp"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_ibp"][0]["detailKeys"][]="ibp_user";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_ibp()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ibp_id,  	ibp_name,  	ibp_inv,  	ibp_akkum,  	ibp_akkum_lastchange,  	ibp_user";
$proto0["m_strFrom"] = "FROM \"public\".hw_ibp";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_id",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto6["m_sql"] = "ibp_id";
$proto6["m_srcTableName"] = "public.hw_ibp";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_name",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto8["m_sql"] = "ibp_name";
$proto8["m_srcTableName"] = "public.hw_ibp";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_inv",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto10["m_sql"] = "ibp_inv";
$proto10["m_srcTableName"] = "public.hw_ibp";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_akkum",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto12["m_sql"] = "ibp_akkum";
$proto12["m_srcTableName"] = "public.hw_ibp";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_akkum_lastchange",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto14["m_sql"] = "ibp_akkum_lastchange";
$proto14["m_srcTableName"] = "public.hw_ibp";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ibp_user",
	"m_strTable" => "public.hw_ibp",
	"m_srcTableName" => "public.hw_ibp"
));

$proto16["m_sql"] = "ibp_user";
$proto16["m_srcTableName"] = "public.hw_ibp";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "public.hw_ibp";
$proto19["m_srcTableName"] = "public.hw_ibp";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ibp_id";
$proto19["m_columns"][] = "ibp_name";
$proto19["m_columns"][] = "ibp_inv";
$proto19["m_columns"][] = "ibp_akkum";
$proto19["m_columns"][] = "ibp_akkum_lastchange";
$proto19["m_columns"][] = "ibp_user";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "\"public\".hw_ibp";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "public.hw_ibp";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_ibp";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_ibp = createSqlQuery_hw_ibp();


	
		;

						

$tdatahw_ibp[".sqlquery"] = $queryData_hw_ibp;



$tableEvents["public.hw_ibp"] = new eventsBase;
$tdatahw_ibp[".hasEvents"] = false;

?>