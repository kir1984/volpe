<?php
$tdatahw_monitor1 = array();
$tdatahw_monitor1[".searchableFields"] = array();
$tdatahw_monitor1[".ShortName"] = "hw_monitor1";
$tdatahw_monitor1[".OwnerID"] = "";
$tdatahw_monitor1[".OriginalTable"] = "public.hw_monitor1";


$tdatahw_monitor1[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_monitor1[".originalPagesByType"] = $tdatahw_monitor1[".pagesByType"];
$tdatahw_monitor1[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_monitor1[".originalPages"] = $tdatahw_monitor1[".pages"];
$tdatahw_monitor1[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_monitor1[".originalDefaultPages"] = $tdatahw_monitor1[".defaultPages"];

//	field labels
$fieldLabelshw_monitor1 = array();
$fieldToolTipshw_monitor1 = array();
$pageTitleshw_monitor1 = array();
$placeHoldershw_monitor1 = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_monitor1["Russian"] = array();
	$fieldToolTipshw_monitor1["Russian"] = array();
	$placeHoldershw_monitor1["Russian"] = array();
	$pageTitleshw_monitor1["Russian"] = array();
	$fieldLabelshw_monitor1["Russian"]["monitor1_id"] = "Monitor1 Id";
	$fieldToolTipshw_monitor1["Russian"]["monitor1_id"] = "";
	$placeHoldershw_monitor1["Russian"]["monitor1_id"] = "";
	$fieldLabelshw_monitor1["Russian"]["monitor1_name"] = "Наименование";
	$fieldToolTipshw_monitor1["Russian"]["monitor1_name"] = "тут работает ajax-поиск";
	$placeHoldershw_monitor1["Russian"]["monitor1_name"] = "";
	$fieldLabelshw_monitor1["Russian"]["monitor1_inv"] = "Инв. номер";
	$fieldToolTipshw_monitor1["Russian"]["monitor1_inv"] = "";
	$placeHoldershw_monitor1["Russian"]["monitor1_inv"] = "";
	$fieldLabelshw_monitor1["Russian"]["boxid"] = "Имя компьютера";
	$fieldToolTipshw_monitor1["Russian"]["boxid"] = "Полностью марка и модель";
	$placeHoldershw_monitor1["Russian"]["boxid"] = "";
	if (count($fieldToolTipshw_monitor1["Russian"]))
		$tdatahw_monitor1[".isUseToolTips"] = true;
}


	$tdatahw_monitor1[".NCSearch"] = true;



$tdatahw_monitor1[".shortTableName"] = "hw_monitor1";
$tdatahw_monitor1[".nSecOptions"] = 0;

$tdatahw_monitor1[".mainTableOwnerID"] = "";
$tdatahw_monitor1[".entityType"] = 0;
$tdatahw_monitor1[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_monitor1[".strOriginalTableName"] = "public.hw_monitor1";

	



$tdatahw_monitor1[".showAddInPopup"] = false;

$tdatahw_monitor1[".showEditInPopup"] = false;

$tdatahw_monitor1[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_monitor1[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_monitor1[".listAjax"] = true;
//	temporary
$tdatahw_monitor1[".listAjax"] = false;

	$tdatahw_monitor1[".audit"] = true;

	$tdatahw_monitor1[".locking"] = true;


$pages = $tdatahw_monitor1[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_monitor1[".edit"] = true;
	$tdatahw_monitor1[".afterEditAction"] = 1;
	$tdatahw_monitor1[".closePopupAfterEdit"] = 1;
	$tdatahw_monitor1[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_monitor1[".add"] = true;
$tdatahw_monitor1[".afterAddAction"] = 1;
$tdatahw_monitor1[".closePopupAfterAdd"] = 1;
$tdatahw_monitor1[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_monitor1[".list"] = true;
}



$tdatahw_monitor1[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_monitor1[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_monitor1[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_monitor1[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_monitor1[".printFriendly"] = true;
}



$tdatahw_monitor1[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_monitor1[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_monitor1[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_monitor1[".isUseAjaxSuggest"] = true;

$tdatahw_monitor1[".rowHighlite"] = true;





$tdatahw_monitor1[".ajaxCodeSnippetAdded"] = false;

$tdatahw_monitor1[".buttonsAdded"] = false;

$tdatahw_monitor1[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_monitor1[".isUseTimeForSearch"] = false;


$tdatahw_monitor1[".badgeColor"] = "bc8f8f";


$tdatahw_monitor1[".allSearchFields"] = array();
$tdatahw_monitor1[".filterFields"] = array();
$tdatahw_monitor1[".requiredSearchFields"] = array();

$tdatahw_monitor1[".googleLikeFields"] = array();
$tdatahw_monitor1[".googleLikeFields"][] = "monitor1_id";
$tdatahw_monitor1[".googleLikeFields"][] = "boxid";
$tdatahw_monitor1[".googleLikeFields"][] = "monitor1_name";
$tdatahw_monitor1[".googleLikeFields"][] = "monitor1_inv";



$tdatahw_monitor1[".tableType"] = "list";

$tdatahw_monitor1[".printerPageOrientation"] = 0;
$tdatahw_monitor1[".nPrinterPageScale"] = 100;

$tdatahw_monitor1[".nPrinterSplitRecords"] = 40;

$tdatahw_monitor1[".geocodingEnabled"] = false;




$tdatahw_monitor1[".isDisplayLoading"] = true;

$tdatahw_monitor1[".isResizeColumns"] = true;





$tdatahw_monitor1[".pageSize"] = 20;

$tdatahw_monitor1[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_monitor1[".strOrderBy"] = $tstrOrderBy;

$tdatahw_monitor1[".orderindexes"] = array();


$tdatahw_monitor1[".sqlHead"] = "SELECT monitor1_id,  	boxid,  	monitor1_name,  	monitor1_inv";
$tdatahw_monitor1[".sqlFrom"] = "FROM \"public\".hw_monitor1";
$tdatahw_monitor1[".sqlWhereExpr"] = "";
$tdatahw_monitor1[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_monitor1[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_monitor1[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_monitor1[".highlightSearchResults"] = true;

$tableKeyshw_monitor1 = array();
$tableKeyshw_monitor1[] = "monitor1_id";
$tdatahw_monitor1[".Keys"] = $tableKeyshw_monitor1;


$tdatahw_monitor1[".hideMobileList"] = array();




//	monitor1_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "monitor1_id";
	$fdata["GoodName"] = "monitor1_id";
	$fdata["ownerTable"] = "public.hw_monitor1";
	$fdata["Label"] = GetFieldLabel("public_hw_monitor1","monitor1_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "monitor1_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "monitor1_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_monitor1["monitor1_id"] = $fdata;
		$tdatahw_monitor1[".searchableFields"][] = "monitor1_id";
//	boxid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "boxid";
	$fdata["GoodName"] = "boxid";
	$fdata["ownerTable"] = "public.hw_monitor1";
	$fdata["Label"] = GetFieldLabel("public_hw_monitor1","boxid");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "boxid";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "boxid";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_box";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "box_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "box_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_monitor1["boxid"] = $fdata;
		$tdatahw_monitor1[".searchableFields"][] = "boxid";
//	monitor1_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "monitor1_name";
	$fdata["GoodName"] = "monitor1_name";
	$fdata["ownerTable"] = "public.hw_monitor1";
	$fdata["Label"] = GetFieldLabel("public_hw_monitor1","monitor1_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "monitor1_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "monitor1_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_monitor1["monitor1_name"] = $fdata;
		$tdatahw_monitor1[".searchableFields"][] = "monitor1_name";
//	monitor1_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "monitor1_inv";
	$fdata["GoodName"] = "monitor1_inv";
	$fdata["ownerTable"] = "public.hw_monitor1";
	$fdata["Label"] = GetFieldLabel("public_hw_monitor1","monitor1_inv");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "monitor1_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "monitor1_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_monitor1["monitor1_inv"] = $fdata;
		$tdatahw_monitor1[".searchableFields"][] = "monitor1_inv";


$tables_data["public.hw_monitor1"]=&$tdatahw_monitor1;
$field_labels["public_hw_monitor1"] = &$fieldLabelshw_monitor1;
$fieldToolTips["public_hw_monitor1"] = &$fieldToolTipshw_monitor1;
$placeHolders["public_hw_monitor1"] = &$placeHoldershw_monitor1;
$page_titles["public_hw_monitor1"] = &$pageTitleshw_monitor1;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_monitor1"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_monitor1"] = array();



	
				$strOriginalDetailsTable="public.hw_box";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.hw_box";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "hw_box";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_monitor1"][0] = $masterParams;
				$masterTablesData["public.hw_monitor1"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_monitor1"][0]["masterKeys"][]="box_id";
				$masterTablesData["public.hw_monitor1"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_monitor1"][0]["detailKeys"][]="boxid";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_monitor1()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "monitor1_id,  	boxid,  	monitor1_name,  	monitor1_inv";
$proto0["m_strFrom"] = "FROM \"public\".hw_monitor1";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "monitor1_id",
	"m_strTable" => "public.hw_monitor1",
	"m_srcTableName" => "public.hw_monitor1"
));

$proto6["m_sql"] = "monitor1_id";
$proto6["m_srcTableName"] = "public.hw_monitor1";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "boxid",
	"m_strTable" => "public.hw_monitor1",
	"m_srcTableName" => "public.hw_monitor1"
));

$proto8["m_sql"] = "boxid";
$proto8["m_srcTableName"] = "public.hw_monitor1";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "monitor1_name",
	"m_strTable" => "public.hw_monitor1",
	"m_srcTableName" => "public.hw_monitor1"
));

$proto10["m_sql"] = "monitor1_name";
$proto10["m_srcTableName"] = "public.hw_monitor1";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "monitor1_inv",
	"m_strTable" => "public.hw_monitor1",
	"m_srcTableName" => "public.hw_monitor1"
));

$proto12["m_sql"] = "monitor1_inv";
$proto12["m_srcTableName"] = "public.hw_monitor1";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "public.hw_monitor1";
$proto15["m_srcTableName"] = "public.hw_monitor1";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "monitor1_id";
$proto15["m_columns"][] = "boxid";
$proto15["m_columns"][] = "monitor1_name";
$proto15["m_columns"][] = "monitor1_inv";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "\"public\".hw_monitor1";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "public.hw_monitor1";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_monitor1";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_monitor1 = createSqlQuery_hw_monitor1();


	
		;

				

$tdatahw_monitor1[".sqlquery"] = $queryData_hw_monitor1;



$tableEvents["public.hw_monitor1"] = new eventsBase;
$tdatahw_monitor1[".hasEvents"] = false;

?>