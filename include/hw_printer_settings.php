<?php
$tdatahw_printer = array();
$tdatahw_printer[".searchableFields"] = array();
$tdatahw_printer[".ShortName"] = "hw_printer";
$tdatahw_printer[".OwnerID"] = "";
$tdatahw_printer[".OriginalTable"] = "public.hw_printer";


$tdatahw_printer[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_printer[".originalPagesByType"] = $tdatahw_printer[".pagesByType"];
$tdatahw_printer[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_printer[".originalPages"] = $tdatahw_printer[".pages"];
$tdatahw_printer[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_printer[".originalDefaultPages"] = $tdatahw_printer[".defaultPages"];

//	field labels
$fieldLabelshw_printer = array();
$fieldToolTipshw_printer = array();
$pageTitleshw_printer = array();
$placeHoldershw_printer = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_printer["Russian"] = array();
	$fieldToolTipshw_printer["Russian"] = array();
	$placeHoldershw_printer["Russian"] = array();
	$pageTitleshw_printer["Russian"] = array();
	$fieldLabelshw_printer["Russian"]["printer_id"] = "Printer Id";
	$fieldToolTipshw_printer["Russian"]["printer_id"] = "";
	$placeHoldershw_printer["Russian"]["printer_id"] = "";
	$fieldLabelshw_printer["Russian"]["printer_name"] = "Наименование";
	$fieldToolTipshw_printer["Russian"]["printer_name"] = "Марка и модель";
	$placeHoldershw_printer["Russian"]["printer_name"] = "";
	$fieldLabelshw_printer["Russian"]["printer_network"] = "Сеть";
	$fieldToolTipshw_printer["Russian"]["printer_network"] = "Есть/Нет";
	$placeHoldershw_printer["Russian"]["printer_network"] = "";
	$fieldLabelshw_printer["Russian"]["printer_ip"] = "IP-адрес";
	$fieldToolTipshw_printer["Russian"]["printer_ip"] = "Указывается если принтер сетевой";
	$placeHoldershw_printer["Russian"]["printer_ip"] = "";
	$fieldLabelshw_printer["Russian"]["printer_cartridge"] = "Картридж";
	$fieldToolTipshw_printer["Russian"]["printer_cartridge"] = "Используемый тип картриджа";
	$placeHoldershw_printer["Russian"]["printer_cartridge"] = "";
	$fieldLabelshw_printer["Russian"]["printer_inv"] = "Инв. номер";
	$fieldToolTipshw_printer["Russian"]["printer_inv"] = "";
	$placeHoldershw_printer["Russian"]["printer_inv"] = "";
	if (count($fieldToolTipshw_printer["Russian"]))
		$tdatahw_printer[".isUseToolTips"] = true;
}


	$tdatahw_printer[".NCSearch"] = true;



$tdatahw_printer[".shortTableName"] = "hw_printer";
$tdatahw_printer[".nSecOptions"] = 0;

$tdatahw_printer[".mainTableOwnerID"] = "";
$tdatahw_printer[".entityType"] = 0;
$tdatahw_printer[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_printer[".strOriginalTableName"] = "public.hw_printer";

	



$tdatahw_printer[".showAddInPopup"] = false;

$tdatahw_printer[".showEditInPopup"] = false;

$tdatahw_printer[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_printer[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_printer[".listAjax"] = true;
//	temporary
$tdatahw_printer[".listAjax"] = false;

	$tdatahw_printer[".audit"] = true;

	$tdatahw_printer[".locking"] = true;


$pages = $tdatahw_printer[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_printer[".edit"] = true;
	$tdatahw_printer[".afterEditAction"] = 1;
	$tdatahw_printer[".closePopupAfterEdit"] = 1;
	$tdatahw_printer[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_printer[".add"] = true;
$tdatahw_printer[".afterAddAction"] = 1;
$tdatahw_printer[".closePopupAfterAdd"] = 1;
$tdatahw_printer[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_printer[".list"] = true;
}



$tdatahw_printer[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_printer[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_printer[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_printer[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_printer[".printFriendly"] = true;
}



$tdatahw_printer[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_printer[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_printer[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_printer[".isUseAjaxSuggest"] = true;

$tdatahw_printer[".rowHighlite"] = true;





$tdatahw_printer[".ajaxCodeSnippetAdded"] = false;

$tdatahw_printer[".buttonsAdded"] = false;

$tdatahw_printer[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_printer[".isUseTimeForSearch"] = false;


$tdatahw_printer[".badgeColor"] = "bc8f8f";


$tdatahw_printer[".allSearchFields"] = array();
$tdatahw_printer[".filterFields"] = array();
$tdatahw_printer[".requiredSearchFields"] = array();

$tdatahw_printer[".googleLikeFields"] = array();
$tdatahw_printer[".googleLikeFields"][] = "printer_id";
$tdatahw_printer[".googleLikeFields"][] = "printer_name";
$tdatahw_printer[".googleLikeFields"][] = "printer_network";
$tdatahw_printer[".googleLikeFields"][] = "printer_ip";
$tdatahw_printer[".googleLikeFields"][] = "printer_cartridge";
$tdatahw_printer[".googleLikeFields"][] = "printer_inv";



$tdatahw_printer[".tableType"] = "list";

$tdatahw_printer[".printerPageOrientation"] = 0;
$tdatahw_printer[".nPrinterPageScale"] = 100;

$tdatahw_printer[".nPrinterSplitRecords"] = 40;

$tdatahw_printer[".geocodingEnabled"] = false;




$tdatahw_printer[".isDisplayLoading"] = true;

$tdatahw_printer[".isResizeColumns"] = true;





$tdatahw_printer[".pageSize"] = 20;

$tdatahw_printer[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_printer[".strOrderBy"] = $tstrOrderBy;

$tdatahw_printer[".orderindexes"] = array();


$tdatahw_printer[".sqlHead"] = "SELECT printer_id,  	printer_name,  	printer_network,  	printer_ip,  	printer_cartridge,  	printer_inv";
$tdatahw_printer[".sqlFrom"] = "FROM \"public\".hw_printer";
$tdatahw_printer[".sqlWhereExpr"] = "";
$tdatahw_printer[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_printer[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_printer[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_printer[".highlightSearchResults"] = true;

$tableKeyshw_printer = array();
$tableKeyshw_printer[] = "printer_id";
$tdatahw_printer[".Keys"] = $tableKeyshw_printer;


$tdatahw_printer[".hideMobileList"] = array();




//	printer_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "printer_id";
	$fdata["GoodName"] = "printer_id";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "printer_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_id"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_id";
//	printer_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "printer_name";
	$fdata["GoodName"] = "printer_name";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "printer_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_name"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_name";
//	printer_network
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "printer_network";
	$fdata["GoodName"] = "printer_network";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_network");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "printer_network";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_network";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Есть";
	$edata["LookupValues"][] = "Нет";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_network"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_network";
//	printer_ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "printer_ip";
	$fdata["GoodName"] = "printer_ip";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_ip");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "printer_ip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_ip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_ip"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_ip";
//	printer_cartridge
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "printer_cartridge";
	$fdata["GoodName"] = "printer_cartridge";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_cartridge");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "printer_cartridge";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_cartridge";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_cartridge"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_cartridge";
//	printer_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "printer_inv";
	$fdata["GoodName"] = "printer_inv";
	$fdata["ownerTable"] = "public.hw_printer";
	$fdata["Label"] = GetFieldLabel("public_hw_printer","printer_inv");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "printer_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "printer_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_printer["printer_inv"] = $fdata;
		$tdatahw_printer[".searchableFields"][] = "printer_inv";


$tables_data["public.hw_printer"]=&$tdatahw_printer;
$field_labels["public_hw_printer"] = &$fieldLabelshw_printer;
$fieldToolTips["public_hw_printer"] = &$fieldToolTipshw_printer;
$placeHolders["public_hw_printer"] = &$placeHoldershw_printer;
$page_titles["public_hw_printer"] = &$pageTitleshw_printer;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_printer"] = array();
//	public.cons_cartridge
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.cons_cartridge";
		$detailsParam["dOriginalTable"] = "public.cons_cartridge";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cons_cartridge";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_cons_cartridge");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.hw_printer"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.hw_printer"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.hw_printer"][$dIndex]["masterKeys"][]="printer_id";

				$detailsTablesData["public.hw_printer"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.hw_printer"][$dIndex]["detailKeys"][]="cartridge_inprinterid";

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_printer"] = array();



	
				$strOriginalDetailsTable="public.arm";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.arm";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "arm";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_printer"][0] = $masterParams;
				$masterTablesData["public.hw_printer"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_printer"][0]["masterKeys"][]="printer_id";
				$masterTablesData["public.hw_printer"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_printer"][0]["detailKeys"][]="printer_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_printer()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "printer_id,  	printer_name,  	printer_network,  	printer_ip,  	printer_cartridge,  	printer_inv";
$proto0["m_strFrom"] = "FROM \"public\".hw_printer";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_id",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto6["m_sql"] = "printer_id";
$proto6["m_srcTableName"] = "public.hw_printer";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_name",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto8["m_sql"] = "printer_name";
$proto8["m_srcTableName"] = "public.hw_printer";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_network",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto10["m_sql"] = "printer_network";
$proto10["m_srcTableName"] = "public.hw_printer";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_ip",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto12["m_sql"] = "printer_ip";
$proto12["m_srcTableName"] = "public.hw_printer";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_cartridge",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto14["m_sql"] = "printer_cartridge";
$proto14["m_srcTableName"] = "public.hw_printer";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "printer_inv",
	"m_strTable" => "public.hw_printer",
	"m_srcTableName" => "public.hw_printer"
));

$proto16["m_sql"] = "printer_inv";
$proto16["m_srcTableName"] = "public.hw_printer";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "public.hw_printer";
$proto19["m_srcTableName"] = "public.hw_printer";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "printer_id";
$proto19["m_columns"][] = "printer_name";
$proto19["m_columns"][] = "printer_network";
$proto19["m_columns"][] = "printer_ip";
$proto19["m_columns"][] = "printer_cartridge";
$proto19["m_columns"][] = "printer_inv";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "\"public\".hw_printer";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "public.hw_printer";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_printer";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_printer = createSqlQuery_hw_printer();


	
		;

						

$tdatahw_printer[".sqlquery"] = $queryData_hw_printer;



$tableEvents["public.hw_printer"] = new eventsBase;
$tdatahw_printer[".hasEvents"] = false;

?>