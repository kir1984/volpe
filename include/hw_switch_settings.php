<?php
$tdatahw_switch = array();
$tdatahw_switch[".searchableFields"] = array();
$tdatahw_switch[".ShortName"] = "hw_switch";
$tdatahw_switch[".OwnerID"] = "";
$tdatahw_switch[".OriginalTable"] = "public.hw_switch";


$tdatahw_switch[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_switch[".originalPagesByType"] = $tdatahw_switch[".pagesByType"];
$tdatahw_switch[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_switch[".originalPages"] = $tdatahw_switch[".pages"];
$tdatahw_switch[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_switch[".originalDefaultPages"] = $tdatahw_switch[".defaultPages"];

//	field labels
$fieldLabelshw_switch = array();
$fieldToolTipshw_switch = array();
$pageTitleshw_switch = array();
$placeHoldershw_switch = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_switch["Russian"] = array();
	$fieldToolTipshw_switch["Russian"] = array();
	$placeHoldershw_switch["Russian"] = array();
	$pageTitleshw_switch["Russian"] = array();
	$fieldLabelshw_switch["Russian"]["sw_id"] = "Sw Id";
	$fieldToolTipshw_switch["Russian"]["sw_id"] = "";
	$placeHoldershw_switch["Russian"]["sw_id"] = "";
	$fieldLabelshw_switch["Russian"]["sw_name"] = "Наименование";
	$fieldToolTipshw_switch["Russian"]["sw_name"] = "";
	$placeHoldershw_switch["Russian"]["sw_name"] = "";
	$fieldLabelshw_switch["Russian"]["sw_ip"] = "IP-адрес";
	$fieldToolTipshw_switch["Russian"]["sw_ip"] = "";
	$placeHoldershw_switch["Russian"]["sw_ip"] = "";
	$fieldLabelshw_switch["Russian"]["sw_location"] = "Местонахождение";
	$fieldToolTipshw_switch["Russian"]["sw_location"] = "";
	$placeHoldershw_switch["Russian"]["sw_location"] = "";
	if (count($fieldToolTipshw_switch["Russian"]))
		$tdatahw_switch[".isUseToolTips"] = true;
}


	$tdatahw_switch[".NCSearch"] = true;



$tdatahw_switch[".shortTableName"] = "hw_switch";
$tdatahw_switch[".nSecOptions"] = 0;

$tdatahw_switch[".mainTableOwnerID"] = "";
$tdatahw_switch[".entityType"] = 0;
$tdatahw_switch[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_switch[".strOriginalTableName"] = "public.hw_switch";

	



$tdatahw_switch[".showAddInPopup"] = false;

$tdatahw_switch[".showEditInPopup"] = false;

$tdatahw_switch[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_switch[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_switch[".listAjax"] = true;
//	temporary
$tdatahw_switch[".listAjax"] = false;

	$tdatahw_switch[".audit"] = true;

	$tdatahw_switch[".locking"] = true;


$pages = $tdatahw_switch[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_switch[".edit"] = true;
	$tdatahw_switch[".afterEditAction"] = 1;
	$tdatahw_switch[".closePopupAfterEdit"] = 1;
	$tdatahw_switch[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_switch[".add"] = true;
$tdatahw_switch[".afterAddAction"] = 1;
$tdatahw_switch[".closePopupAfterAdd"] = 1;
$tdatahw_switch[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_switch[".list"] = true;
}



$tdatahw_switch[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_switch[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_switch[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_switch[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_switch[".printFriendly"] = true;
}



$tdatahw_switch[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_switch[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_switch[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_switch[".isUseAjaxSuggest"] = true;

$tdatahw_switch[".rowHighlite"] = true;





$tdatahw_switch[".ajaxCodeSnippetAdded"] = false;

$tdatahw_switch[".buttonsAdded"] = false;

$tdatahw_switch[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_switch[".isUseTimeForSearch"] = false;


$tdatahw_switch[".badgeColor"] = "5F9EA0";


$tdatahw_switch[".allSearchFields"] = array();
$tdatahw_switch[".filterFields"] = array();
$tdatahw_switch[".requiredSearchFields"] = array();

$tdatahw_switch[".googleLikeFields"] = array();
$tdatahw_switch[".googleLikeFields"][] = "sw_id";
$tdatahw_switch[".googleLikeFields"][] = "sw_name";
$tdatahw_switch[".googleLikeFields"][] = "sw_ip";
$tdatahw_switch[".googleLikeFields"][] = "sw_location";



$tdatahw_switch[".tableType"] = "list";

$tdatahw_switch[".printerPageOrientation"] = 0;
$tdatahw_switch[".nPrinterPageScale"] = 100;

$tdatahw_switch[".nPrinterSplitRecords"] = 40;

$tdatahw_switch[".geocodingEnabled"] = false;





$tdatahw_switch[".isResizeColumns"] = true;





$tdatahw_switch[".pageSize"] = 20;

$tdatahw_switch[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_switch[".strOrderBy"] = $tstrOrderBy;

$tdatahw_switch[".orderindexes"] = array();


$tdatahw_switch[".sqlHead"] = "SELECT sw_id,  	sw_name,  	sw_ip,  	sw_location";
$tdatahw_switch[".sqlFrom"] = "FROM \"public\".hw_switch";
$tdatahw_switch[".sqlWhereExpr"] = "";
$tdatahw_switch[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_switch[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_switch[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_switch[".highlightSearchResults"] = true;

$tableKeyshw_switch = array();
$tableKeyshw_switch[] = "sw_id";
$tdatahw_switch[".Keys"] = $tableKeyshw_switch;


$tdatahw_switch[".hideMobileList"] = array();




//	sw_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "sw_id";
	$fdata["GoodName"] = "sw_id";
	$fdata["ownerTable"] = "public.hw_switch";
	$fdata["Label"] = GetFieldLabel("public_hw_switch","sw_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "sw_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sw_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_switch["sw_id"] = $fdata;
		$tdatahw_switch[".searchableFields"][] = "sw_id";
//	sw_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "sw_name";
	$fdata["GoodName"] = "sw_name";
	$fdata["ownerTable"] = "public.hw_switch";
	$fdata["Label"] = GetFieldLabel("public_hw_switch","sw_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "sw_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sw_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_switch["sw_name"] = $fdata;
		$tdatahw_switch[".searchableFields"][] = "sw_name";
//	sw_ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "sw_ip";
	$fdata["GoodName"] = "sw_ip";
	$fdata["ownerTable"] = "public.hw_switch";
	$fdata["Label"] = GetFieldLabel("public_hw_switch","sw_ip");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "sw_ip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sw_ip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_switch["sw_ip"] = $fdata;
		$tdatahw_switch[".searchableFields"][] = "sw_ip";
//	sw_location
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "sw_location";
	$fdata["GoodName"] = "sw_location";
	$fdata["ownerTable"] = "public.hw_switch";
	$fdata["Label"] = GetFieldLabel("public_hw_switch","sw_location");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "sw_location";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sw_location";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_location";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "location_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "location_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_switch["sw_location"] = $fdata;
		$tdatahw_switch[".searchableFields"][] = "sw_location";


$tables_data["public.hw_switch"]=&$tdatahw_switch;
$field_labels["public_hw_switch"] = &$fieldLabelshw_switch;
$fieldToolTips["public_hw_switch"] = &$fieldToolTipshw_switch;
$placeHolders["public_hw_switch"] = &$placeHoldershw_switch;
$page_titles["public_hw_switch"] = &$pageTitleshw_switch;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_switch"] = array();
//	public.spr_socket
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.spr_socket";
		$detailsParam["dOriginalTable"] = "public.spr_socket";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "spr_socket";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_spr_socket");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.hw_switch"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.hw_switch"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.hw_switch"][$dIndex]["masterKeys"][]="sw_id";

				$detailsTablesData["public.hw_switch"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.hw_switch"][$dIndex]["detailKeys"][]="switch_id";

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_switch"] = array();



	
				$strOriginalDetailsTable="public.spr_location";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_location";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_location";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_switch"][0] = $masterParams;
				$masterTablesData["public.hw_switch"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_switch"][0]["masterKeys"][]="location_id";
				$masterTablesData["public.hw_switch"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_switch"][0]["detailKeys"][]="sw_location";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_switch()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "sw_id,  	sw_name,  	sw_ip,  	sw_location";
$proto0["m_strFrom"] = "FROM \"public\".hw_switch";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "sw_id",
	"m_strTable" => "public.hw_switch",
	"m_srcTableName" => "public.hw_switch"
));

$proto6["m_sql"] = "sw_id";
$proto6["m_srcTableName"] = "public.hw_switch";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "sw_name",
	"m_strTable" => "public.hw_switch",
	"m_srcTableName" => "public.hw_switch"
));

$proto8["m_sql"] = "sw_name";
$proto8["m_srcTableName"] = "public.hw_switch";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "sw_ip",
	"m_strTable" => "public.hw_switch",
	"m_srcTableName" => "public.hw_switch"
));

$proto10["m_sql"] = "sw_ip";
$proto10["m_srcTableName"] = "public.hw_switch";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "sw_location",
	"m_strTable" => "public.hw_switch",
	"m_srcTableName" => "public.hw_switch"
));

$proto12["m_sql"] = "sw_location";
$proto12["m_srcTableName"] = "public.hw_switch";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "public.hw_switch";
$proto15["m_srcTableName"] = "public.hw_switch";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "sw_id";
$proto15["m_columns"][] = "sw_name";
$proto15["m_columns"][] = "sw_ip";
$proto15["m_columns"][] = "sw_location";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "\"public\".hw_switch";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "public.hw_switch";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_switch";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_switch = createSqlQuery_hw_switch();


	
		;

				

$tdatahw_switch[".sqlquery"] = $queryData_hw_switch;



$tableEvents["public.hw_switch"] = new eventsBase;
$tdatahw_switch[".hasEvents"] = false;

?>