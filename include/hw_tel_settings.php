<?php
$tdatahw_tel = array();
$tdatahw_tel[".searchableFields"] = array();
$tdatahw_tel[".ShortName"] = "hw_tel";
$tdatahw_tel[".OwnerID"] = "";
$tdatahw_tel[".OriginalTable"] = "public.hw_tel";


$tdatahw_tel[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatahw_tel[".originalPagesByType"] = $tdatahw_tel[".pagesByType"];
$tdatahw_tel[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatahw_tel[".originalPages"] = $tdatahw_tel[".pages"];
$tdatahw_tel[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatahw_tel[".originalDefaultPages"] = $tdatahw_tel[".defaultPages"];

//	field labels
$fieldLabelshw_tel = array();
$fieldToolTipshw_tel = array();
$pageTitleshw_tel = array();
$placeHoldershw_tel = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelshw_tel["Russian"] = array();
	$fieldToolTipshw_tel["Russian"] = array();
	$placeHoldershw_tel["Russian"] = array();
	$pageTitleshw_tel["Russian"] = array();
	$fieldLabelshw_tel["Russian"]["tel_id"] = "Tel Id";
	$fieldToolTipshw_tel["Russian"]["tel_id"] = "";
	$placeHoldershw_tel["Russian"]["tel_id"] = "";
	$fieldLabelshw_tel["Russian"]["tel_name"] = "Наименование";
	$fieldToolTipshw_tel["Russian"]["tel_name"] = "Марка и модель";
	$placeHoldershw_tel["Russian"]["tel_name"] = "";
	$fieldLabelshw_tel["Russian"]["tel_socket"] = "Розетка";
	$fieldToolTipshw_tel["Russian"]["tel_socket"] = "К которой подключен телефон";
	$placeHoldershw_tel["Russian"]["tel_socket"] = "";
	$fieldLabelshw_tel["Russian"]["tel_wire"] = "Провод ";
	$fieldToolTipshw_tel["Russian"]["tel_wire"] = "Проводной/Беспроводной";
	$placeHoldershw_tel["Russian"]["tel_wire"] = "";
	$fieldLabelshw_tel["Russian"]["tel_type"] = "Тип телефона";
	$fieldToolTipshw_tel["Russian"]["tel_type"] = "Аналог/IP";
	$placeHoldershw_tel["Russian"]["tel_type"] = "";
	$fieldLabelshw_tel["Russian"]["tel_ip"] = "IP-адрес";
	$fieldToolTipshw_tel["Russian"]["tel_ip"] = "Указывается, если телефон IP";
	$placeHoldershw_tel["Russian"]["tel_ip"] = "";
	$fieldLabelshw_tel["Russian"]["tel_number"] = "Номер телефона";
	$fieldToolTipshw_tel["Russian"]["tel_number"] = "";
	$placeHoldershw_tel["Russian"]["tel_number"] = "";
	$fieldLabelshw_tel["Russian"]["tel_inv"] = "Инв. номер";
	$fieldToolTipshw_tel["Russian"]["tel_inv"] = "";
	$placeHoldershw_tel["Russian"]["tel_inv"] = "";
	if (count($fieldToolTipshw_tel["Russian"]))
		$tdatahw_tel[".isUseToolTips"] = true;
}


	$tdatahw_tel[".NCSearch"] = true;



$tdatahw_tel[".shortTableName"] = "hw_tel";
$tdatahw_tel[".nSecOptions"] = 0;

$tdatahw_tel[".mainTableOwnerID"] = "";
$tdatahw_tel[".entityType"] = 0;
$tdatahw_tel[".connId"] = "itbase3_at_192_168_1_15";


$tdatahw_tel[".strOriginalTableName"] = "public.hw_tel";

	



$tdatahw_tel[".showAddInPopup"] = false;

$tdatahw_tel[".showEditInPopup"] = false;

$tdatahw_tel[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahw_tel[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatahw_tel[".listAjax"] = true;
//	temporary
$tdatahw_tel[".listAjax"] = false;

	$tdatahw_tel[".audit"] = true;

	$tdatahw_tel[".locking"] = true;


$pages = $tdatahw_tel[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatahw_tel[".edit"] = true;
	$tdatahw_tel[".afterEditAction"] = 1;
	$tdatahw_tel[".closePopupAfterEdit"] = 1;
	$tdatahw_tel[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatahw_tel[".add"] = true;
$tdatahw_tel[".afterAddAction"] = 1;
$tdatahw_tel[".closePopupAfterAdd"] = 1;
$tdatahw_tel[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatahw_tel[".list"] = true;
}



$tdatahw_tel[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatahw_tel[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatahw_tel[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatahw_tel[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatahw_tel[".printFriendly"] = true;
}



$tdatahw_tel[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatahw_tel[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatahw_tel[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatahw_tel[".isUseAjaxSuggest"] = true;

$tdatahw_tel[".rowHighlite"] = true;





$tdatahw_tel[".ajaxCodeSnippetAdded"] = false;

$tdatahw_tel[".buttonsAdded"] = false;

$tdatahw_tel[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahw_tel[".isUseTimeForSearch"] = false;


$tdatahw_tel[".badgeColor"] = "bc8f8f";


$tdatahw_tel[".allSearchFields"] = array();
$tdatahw_tel[".filterFields"] = array();
$tdatahw_tel[".requiredSearchFields"] = array();

$tdatahw_tel[".googleLikeFields"] = array();
$tdatahw_tel[".googleLikeFields"][] = "tel_id";
$tdatahw_tel[".googleLikeFields"][] = "tel_name";
$tdatahw_tel[".googleLikeFields"][] = "tel_socket";
$tdatahw_tel[".googleLikeFields"][] = "tel_wire";
$tdatahw_tel[".googleLikeFields"][] = "tel_type";
$tdatahw_tel[".googleLikeFields"][] = "tel_ip";
$tdatahw_tel[".googleLikeFields"][] = "tel_number";
$tdatahw_tel[".googleLikeFields"][] = "tel_inv";



$tdatahw_tel[".tableType"] = "list";

$tdatahw_tel[".printerPageOrientation"] = 0;
$tdatahw_tel[".nPrinterPageScale"] = 100;

$tdatahw_tel[".nPrinterSplitRecords"] = 40;

$tdatahw_tel[".geocodingEnabled"] = false;




$tdatahw_tel[".isDisplayLoading"] = true;

$tdatahw_tel[".isResizeColumns"] = true;





$tdatahw_tel[".pageSize"] = 20;

$tdatahw_tel[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahw_tel[".strOrderBy"] = $tstrOrderBy;

$tdatahw_tel[".orderindexes"] = array();


$tdatahw_tel[".sqlHead"] = "SELECT tel_id,  	tel_name,  	tel_socket,  	tel_wire,  	tel_type,  	tel_ip,  	tel_number,  	tel_inv";
$tdatahw_tel[".sqlFrom"] = "FROM \"public\".hw_tel";
$tdatahw_tel[".sqlWhereExpr"] = "";
$tdatahw_tel[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahw_tel[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahw_tel[".arrGroupsPerPage"] = $arrGPP;

$tdatahw_tel[".highlightSearchResults"] = true;

$tableKeyshw_tel = array();
$tableKeyshw_tel[] = "tel_id";
$tdatahw_tel[".Keys"] = $tableKeyshw_tel;


$tdatahw_tel[".hideMobileList"] = array();




//	tel_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "tel_id";
	$fdata["GoodName"] = "tel_id";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "tel_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_id"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_id";
//	tel_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "tel_name";
	$fdata["GoodName"] = "tel_name";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_name"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_name";
//	tel_socket
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "tel_socket";
	$fdata["GoodName"] = "tel_socket";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_socket");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_socket";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_socket";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_socket"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_socket";
//	tel_wire
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "tel_wire";
	$fdata["GoodName"] = "tel_wire";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_wire");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_wire";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_wire";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Проводной";
	$edata["LookupValues"][] = "Беспроводной";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_wire"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_wire";
//	tel_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "tel_type";
	$fdata["GoodName"] = "tel_type";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_type");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Аналоговый";
	$edata["LookupValues"][] = "IP";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_type"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_type";
//	tel_ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "tel_ip";
	$fdata["GoodName"] = "tel_ip";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_ip");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_ip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_ip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_ip"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_ip";
//	tel_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "tel_number";
	$fdata["GoodName"] = "tel_number";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_number");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "tel_number";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_number";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_number"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_number";
//	tel_inv
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "tel_inv";
	$fdata["GoodName"] = "tel_inv";
	$fdata["ownerTable"] = "public.hw_tel";
	$fdata["Label"] = GetFieldLabel("public_hw_tel","tel_inv");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "tel_inv";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_inv";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatahw_tel["tel_inv"] = $fdata;
		$tdatahw_tel[".searchableFields"][] = "tel_inv";


$tables_data["public.hw_tel"]=&$tdatahw_tel;
$field_labels["public_hw_tel"] = &$fieldLabelshw_tel;
$fieldToolTips["public_hw_tel"] = &$fieldToolTipshw_tel;
$placeHolders["public_hw_tel"] = &$placeHoldershw_tel;
$page_titles["public_hw_tel"] = &$pageTitleshw_tel;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.hw_tel"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.hw_tel"] = array();



	
				$strOriginalDetailsTable="public.arm";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.arm";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "arm";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.hw_tel"][0] = $masterParams;
				$masterTablesData["public.hw_tel"][0]["masterKeys"] = array();
	$masterTablesData["public.hw_tel"][0]["masterKeys"][]="tel_id";
				$masterTablesData["public.hw_tel"][0]["detailKeys"] = array();
	$masterTablesData["public.hw_tel"][0]["detailKeys"][]="tel_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_hw_tel()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "tel_id,  	tel_name,  	tel_socket,  	tel_wire,  	tel_type,  	tel_ip,  	tel_number,  	tel_inv";
$proto0["m_strFrom"] = "FROM \"public\".hw_tel";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_id",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto6["m_sql"] = "tel_id";
$proto6["m_srcTableName"] = "public.hw_tel";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_name",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto8["m_sql"] = "tel_name";
$proto8["m_srcTableName"] = "public.hw_tel";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_socket",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto10["m_sql"] = "tel_socket";
$proto10["m_srcTableName"] = "public.hw_tel";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_wire",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto12["m_sql"] = "tel_wire";
$proto12["m_srcTableName"] = "public.hw_tel";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_type",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto14["m_sql"] = "tel_type";
$proto14["m_srcTableName"] = "public.hw_tel";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_ip",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto16["m_sql"] = "tel_ip";
$proto16["m_srcTableName"] = "public.hw_tel";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_number",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto18["m_sql"] = "tel_number";
$proto18["m_srcTableName"] = "public.hw_tel";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_inv",
	"m_strTable" => "public.hw_tel",
	"m_srcTableName" => "public.hw_tel"
));

$proto20["m_sql"] = "tel_inv";
$proto20["m_srcTableName"] = "public.hw_tel";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "public.hw_tel";
$proto23["m_srcTableName"] = "public.hw_tel";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "tel_id";
$proto23["m_columns"][] = "tel_name";
$proto23["m_columns"][] = "tel_socket";
$proto23["m_columns"][] = "tel_wire";
$proto23["m_columns"][] = "tel_type";
$proto23["m_columns"][] = "tel_ip";
$proto23["m_columns"][] = "tel_number";
$proto23["m_columns"][] = "tel_inv";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "\"public\".hw_tel";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "public.hw_tel";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.hw_tel";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_hw_tel = createSqlQuery_hw_tel();


	
		;

								

$tdatahw_tel[".sqlquery"] = $queryData_hw_tel;



$tableEvents["public.hw_tel"] = new eventsBase;
$tdatahw_tel[".hasEvents"] = false;

?>