<?php
$tdatalicense_all = array();
$tdatalicense_all[".searchableFields"] = array();
$tdatalicense_all[".ShortName"] = "license_all";
$tdatalicense_all[".OwnerID"] = "";
$tdatalicense_all[".OriginalTable"] = "public.spr_license";


$tdatalicense_all[".pagesByType"] = my_json_decode( "{\"chart\":[\"chart\"],\"masterchart\":[\"masterchart\"],\"search\":[\"search\"]}" );
$tdatalicense_all[".originalPagesByType"] = $tdatalicense_all[".pagesByType"];
$tdatalicense_all[".pages"] = types2pages( my_json_decode( "{\"chart\":[\"chart\"],\"masterchart\":[\"masterchart\"],\"search\":[\"search\"]}" ) );
$tdatalicense_all[".originalPages"] = $tdatalicense_all[".pages"];
$tdatalicense_all[".defaultPages"] = my_json_decode( "{\"chart\":\"chart\",\"masterchart\":\"masterchart\",\"search\":\"search\"}" );
$tdatalicense_all[".originalDefaultPages"] = $tdatalicense_all[".defaultPages"];

//	field labels
$fieldLabelslicense_all = array();
$fieldToolTipslicense_all = array();
$pageTitleslicense_all = array();
$placeHolderslicense_all = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelslicense_all["Russian"] = array();
	$fieldToolTipslicense_all["Russian"] = array();
	$placeHolderslicense_all["Russian"] = array();
	$pageTitleslicense_all["Russian"] = array();
	$fieldLabelslicense_all["Russian"]["po_name"] = "Наименование";
	$fieldToolTipslicense_all["Russian"]["po_name"] = "";
	$placeHolderslicense_all["Russian"]["po_name"] = "";
	$fieldLabelslicense_all["Russian"]["SUM_licdog_quantity_"] = "Общее количество";
	$fieldToolTipslicense_all["Russian"]["SUM_licdog_quantity_"] = "";
	$placeHolderslicense_all["Russian"]["SUM_licdog_quantity_"] = "";
	if (count($fieldToolTipslicense_all["Russian"]))
		$tdatalicense_all[".isUseToolTips"] = true;
}


	$tdatalicense_all[".NCSearch"] = true;

	$tdatalicense_all[".ChartRefreshTime"] = 0;


$tdatalicense_all[".shortTableName"] = "license_all";
$tdatalicense_all[".nSecOptions"] = 0;

$tdatalicense_all[".mainTableOwnerID"] = "";
$tdatalicense_all[".entityType"] = 3;
$tdatalicense_all[".connId"] = "itbase3_at_192_168_1_15";


$tdatalicense_all[".strOriginalTableName"] = "public.spr_license";

	



$tdatalicense_all[".showAddInPopup"] = false;

$tdatalicense_all[".showEditInPopup"] = false;

$tdatalicense_all[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatalicense_all[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatalicense_all[".listAjax"] = false;
//	temporary
$tdatalicense_all[".listAjax"] = false;

	$tdatalicense_all[".audit"] = false;

	$tdatalicense_all[".locking"] = false;


$pages = $tdatalicense_all[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatalicense_all[".edit"] = true;
	$tdatalicense_all[".afterEditAction"] = 1;
	$tdatalicense_all[".closePopupAfterEdit"] = 1;
	$tdatalicense_all[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatalicense_all[".add"] = true;
$tdatalicense_all[".afterAddAction"] = 1;
$tdatalicense_all[".closePopupAfterAdd"] = 1;
$tdatalicense_all[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatalicense_all[".list"] = true;
}



$tdatalicense_all[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatalicense_all[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatalicense_all[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatalicense_all[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatalicense_all[".printFriendly"] = true;
}



$tdatalicense_all[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatalicense_all[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatalicense_all[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatalicense_all[".isUseAjaxSuggest"] = true;






$tdatalicense_all[".ajaxCodeSnippetAdded"] = false;

$tdatalicense_all[".buttonsAdded"] = false;

$tdatalicense_all[".addPageEvents"] = false;

// use timepicker for search panel
$tdatalicense_all[".isUseTimeForSearch"] = false;


$tdatalicense_all[".badgeColor"] = "6493EA";


$tdatalicense_all[".allSearchFields"] = array();
$tdatalicense_all[".filterFields"] = array();
$tdatalicense_all[".requiredSearchFields"] = array();

$tdatalicense_all[".googleLikeFields"] = array();
$tdatalicense_all[".googleLikeFields"][] = "po_name";
$tdatalicense_all[".googleLikeFields"][] = "SUM(licdog_quantity)";



$tdatalicense_all[".tableType"] = "chart";

$tdatalicense_all[".printerPageOrientation"] = 0;
$tdatalicense_all[".nPrinterPageScale"] = 100;

$tdatalicense_all[".nPrinterSplitRecords"] = 40;

$tdatalicense_all[".geocodingEnabled"] = false;



// chart settings
$tdatalicense_all[".chartType"] = "2DBar";
// end of chart settings








$tstrOrderBy = "ORDER BY (	SELECT
			po_name 
		FROM
			spr_po
		WHERE
			spr_license_dogovor.licdog_poname=spr_po.po_id)";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatalicense_all[".strOrderBy"] = $tstrOrderBy;

$tdatalicense_all[".orderindexes"] = array();
	$tdatalicense_all[".orderindexes"][] = array(1, (1 ? "ASC" : "DESC"), "(	SELECT
			po_name 
		FROM
			spr_po
		WHERE
			spr_license_dogovor.licdog_poname=spr_po.po_id)");



$tdatalicense_all[".sqlHead"] = "SELECT (	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id),  SUM(licdog_quantity) AS \"SUM(licdog_quantity)\"";
$tdatalicense_all[".sqlFrom"] = "FROM \"public\".spr_license_dogovor";
$tdatalicense_all[".sqlWhereExpr"] = "";
$tdatalicense_all[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatalicense_all[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatalicense_all[".arrGroupsPerPage"] = $arrGPP;

$tdatalicense_all[".highlightSearchResults"] = true;

$tableKeyslicense_all = array();
$tdatalicense_all[".Keys"] = $tableKeyslicense_all;


$tdatalicense_all[".hideMobileList"] = array();




//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("license_all","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_all["po_name"] = $fdata;
		$tdatalicense_all[".searchableFields"][] = "po_name";
//	SUM(licdog_quantity)
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "SUM(licdog_quantity)";
	$fdata["GoodName"] = "SUM_licdog_quantity_";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("license_all","SUM_licdog_quantity_");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "SUM(licdog_quantity)";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "SUM(licdog_quantity)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_all["SUM(licdog_quantity)"] = $fdata;
		$tdatalicense_all[".searchableFields"][] = "SUM(licdog_quantity)";

$tdatalicense_all[".chartLabelField"] = "po_name";
$tdatalicense_all[".chartSeries"] = array();
$tdatalicense_all[".chartSeries"][] = array( 
	"field" => "SUM(licdog_quantity)",
	"total" => ""
);
	$tdatalicense_all[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">license_all</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatalicense_all[".chartXml"] .= '<attr value="0">
			<attr value="name">SUM(licdog_quantity)</attr>';
	$tdatalicense_all[".chartXml"] .= '</attr>';
	$tdatalicense_all[".chartXml"] .= '<attr value="1">
		<attr value="name">po_name</attr>
	</attr>';
	$tdatalicense_all[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatalicense_all[".chartXml"] .= '<attr value="head">'.xmlencode("Имеющиеся лицензии").'</attr>
<attr value="foot">'.xmlencode("").'</attr>
<attr value="y_axis_label">'.xmlencode("lic_poname").'</attr>


<attr value="slegend">true</attr>
<attr value="sgrid">false</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatalicense_all[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatalicense_all[".chartXml"] .= '<attr value="0">
		<attr value="name">po_name</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_all","po_name")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatalicense_all[".chartXml"] .= '<attr value="1">
		<attr value="name">SUM(licdog_quantity)</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_all","SUM_licdog_quantity_")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatalicense_all[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">license_all</attr>
<attr value="short_table_name">license_all</attr>
</attr>

</chart>';

$tables_data["license_all"]=&$tdatalicense_all;
$field_labels["license_all"] = &$fieldLabelslicense_all;
$fieldToolTips["license_all"] = &$fieldToolTipslicense_all;
$placeHolders["license_all"] = &$placeHolderslicense_all;
$page_titles["license_all"] = &$pageTitleslicense_all;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["license_all"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["license_all"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_license_all()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id),  SUM(licdog_quantity) AS \"SUM(licdog_quantity)\"";
$proto0["m_strFrom"] = "FROM \"public\".spr_license_dogovor";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY (	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)"
));

$proto6["m_sql"] = "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)";
$proto6["m_srcTableName"] = "license_all";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$proto9=array();
$proto9["m_functiontype"] = "SQLF_SUM";
$proto9["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "licdog_quantity",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "license_all"
));

$proto9["m_arguments"][]=$obj;
$proto9["m_strFunctionName"] = "SUM";
$obj = new SQLFunctionCall($proto9);

$proto8["m_sql"] = "SUM(licdog_quantity)";
$proto8["m_srcTableName"] = "license_all";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "SUM(licdog_quantity)";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto11=array();
$proto11["m_link"] = "SQLL_MAIN";
			$proto12=array();
$proto12["m_strName"] = "public.spr_license_dogovor";
$proto12["m_srcTableName"] = "license_all";
$proto12["m_columns"] = array();
$proto12["m_columns"][] = "licdog_id";
$proto12["m_columns"][] = "licdog_poname";
$proto12["m_columns"][] = "licdog_type";
$proto12["m_columns"][] = "licdog_quantity";
$proto12["m_columns"][] = "licdog_number";
$proto12["m_columns"][] = "licdog_date";
$proto12["m_columns"][] = "licdog_contractor";
$proto12["m_columns"][] = "licdog_desc";
$proto12["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto12);

$proto11["m_table"] = $obj;
$proto11["m_sql"] = "\"public\".spr_license_dogovor";
$proto11["m_alias"] = "";
$proto11["m_srcTableName"] = "license_all";
$proto13=array();
$proto13["m_sql"] = "";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

$proto11["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto11);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto15=array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)"
));

$proto15["m_column"]=$obj;
$obj = new SQLGroupByItem($proto15);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
												$proto17=array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "(	SELECT  			po_name   		FROM  			spr_po  		WHERE  			spr_license_dogovor.licdog_poname=spr_po.po_id)"
));

$proto17["m_column"]=$obj;
$proto17["m_bAsc"] = 1;
$proto17["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto17);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="license_all";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_license_all = createSqlQuery_license_all();


	
		;

		

$tdatalicense_all[".sqlquery"] = $queryData_license_all;



$tableEvents["license_all"] = new eventsBase;
$tdatalicense_all[".hasEvents"] = false;

?>