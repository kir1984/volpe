<?php
$tdatalicense_all_used = array();
$tdatalicense_all_used[".searchableFields"] = array();
$tdatalicense_all_used[".ShortName"] = "license_all_used";
$tdatalicense_all_used[".OwnerID"] = "";
$tdatalicense_all_used[".OriginalTable"] = "public.spr_license_dogovor";


$tdatalicense_all_used[".pagesByType"] = my_json_decode( "{\"chart\":[\"chart\"],\"masterchart\":[\"masterchart\"],\"search\":[\"search\"]}" );
$tdatalicense_all_used[".originalPagesByType"] = $tdatalicense_all_used[".pagesByType"];
$tdatalicense_all_used[".pages"] = types2pages( my_json_decode( "{\"chart\":[\"chart\"],\"masterchart\":[\"masterchart\"],\"search\":[\"search\"]}" ) );
$tdatalicense_all_used[".originalPages"] = $tdatalicense_all_used[".pages"];
$tdatalicense_all_used[".defaultPages"] = my_json_decode( "{\"chart\":\"chart\",\"masterchart\":\"masterchart\",\"search\":\"search\"}" );
$tdatalicense_all_used[".originalDefaultPages"] = $tdatalicense_all_used[".defaultPages"];

//	field labels
$fieldLabelslicense_all_used = array();
$fieldToolTipslicense_all_used = array();
$pageTitleslicense_all_used = array();
$placeHolderslicense_all_used = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelslicense_all_used["Russian"] = array();
	$fieldToolTipslicense_all_used["Russian"] = array();
	$placeHolderslicense_all_used["Russian"] = array();
	$pageTitleslicense_all_used["Russian"] = array();
	$fieldLabelslicense_all_used["Russian"]["po_name"] = "Программное обеспечение";
	$fieldToolTipslicense_all_used["Russian"]["po_name"] = "";
	$placeHolderslicense_all_used["Russian"]["po_name"] = "";
	$fieldLabelslicense_all_used["Russian"]["licenses_used"] = "Используется";
	$fieldToolTipslicense_all_used["Russian"]["licenses_used"] = "";
	$placeHolderslicense_all_used["Russian"]["licenses_used"] = "";
	$fieldLabelslicense_all_used["Russian"]["licenses_all"] = "Имеющиеся лицензии";
	$fieldToolTipslicense_all_used["Russian"]["licenses_all"] = "";
	$placeHolderslicense_all_used["Russian"]["licenses_all"] = "";
	if (count($fieldToolTipslicense_all_used["Russian"]))
		$tdatalicense_all_used[".isUseToolTips"] = true;
}


	$tdatalicense_all_used[".NCSearch"] = true;

	$tdatalicense_all_used[".ChartRefreshTime"] = 0;


$tdatalicense_all_used[".shortTableName"] = "license_all_used";
$tdatalicense_all_used[".nSecOptions"] = 0;

$tdatalicense_all_used[".mainTableOwnerID"] = "";
$tdatalicense_all_used[".entityType"] = 3;
$tdatalicense_all_used[".connId"] = "itbase3_at_192_168_1_15";


$tdatalicense_all_used[".strOriginalTableName"] = "public.spr_license_dogovor";

	



$tdatalicense_all_used[".showAddInPopup"] = false;

$tdatalicense_all_used[".showEditInPopup"] = false;

$tdatalicense_all_used[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatalicense_all_used[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatalicense_all_used[".listAjax"] = false;
//	temporary
$tdatalicense_all_used[".listAjax"] = false;

	$tdatalicense_all_used[".audit"] = false;

	$tdatalicense_all_used[".locking"] = false;


$pages = $tdatalicense_all_used[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatalicense_all_used[".edit"] = true;
	$tdatalicense_all_used[".afterEditAction"] = 1;
	$tdatalicense_all_used[".closePopupAfterEdit"] = 1;
	$tdatalicense_all_used[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatalicense_all_used[".add"] = true;
$tdatalicense_all_used[".afterAddAction"] = 1;
$tdatalicense_all_used[".closePopupAfterAdd"] = 1;
$tdatalicense_all_used[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatalicense_all_used[".list"] = true;
}



$tdatalicense_all_used[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatalicense_all_used[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatalicense_all_used[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatalicense_all_used[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatalicense_all_used[".printFriendly"] = true;
}



$tdatalicense_all_used[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatalicense_all_used[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatalicense_all_used[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatalicense_all_used[".isUseAjaxSuggest"] = true;






$tdatalicense_all_used[".ajaxCodeSnippetAdded"] = false;

$tdatalicense_all_used[".buttonsAdded"] = false;

$tdatalicense_all_used[".addPageEvents"] = false;

// use timepicker for search panel
$tdatalicense_all_used[".isUseTimeForSearch"] = false;


$tdatalicense_all_used[".badgeColor"] = "4682B4";


$tdatalicense_all_used[".allSearchFields"] = array();
$tdatalicense_all_used[".filterFields"] = array();
$tdatalicense_all_used[".requiredSearchFields"] = array();

$tdatalicense_all_used[".googleLikeFields"] = array();
$tdatalicense_all_used[".googleLikeFields"][] = "po_name";
$tdatalicense_all_used[".googleLikeFields"][] = "licenses_all";
$tdatalicense_all_used[".googleLikeFields"][] = "licenses_used";



$tdatalicense_all_used[".tableType"] = "chart";

$tdatalicense_all_used[".printerPageOrientation"] = 0;
$tdatalicense_all_used[".nPrinterPageScale"] = 100;

$tdatalicense_all_used[".nPrinterSplitRecords"] = 40;

$tdatalicense_all_used[".geocodingEnabled"] = false;



// chart settings
$tdatalicense_all_used[".chartType"] = "2DBar";
// end of chart settings








$tstrOrderBy = "order by spr_po.po_name";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatalicense_all_used[".strOrderBy"] = $tstrOrderBy;

$tdatalicense_all_used[".orderindexes"] = array();
	$tdatalicense_all_used[".orderindexes"][] = array(1, (1 ? "ASC" : "DESC"), "\"public\".spr_po.po_name");



$tdatalicense_all_used[".sqlHead"] = "SELECT spr_po.po_name,  	SUM(distinct spr_license_dogovor.licdog_quantity) as licenses_all,  	COUNT(arm_po.po_key) as licenses_used";
$tdatalicense_all_used[".sqlFrom"] = "FROM spr_license_dogovor   		LEFT JOIN spr_license ON spr_license_dogovor.licdog_id = spr_license.lic_licdog  		LEFT JOIN arm_po ON arm_po.po_key = spr_license.lic_id  		LEFT JOIN spr_po ON spr_license_dogovor.licdog_poname = spr_po.po_id";
$tdatalicense_all_used[".sqlWhereExpr"] = "";
$tdatalicense_all_used[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatalicense_all_used[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatalicense_all_used[".arrGroupsPerPage"] = $arrGPP;

$tdatalicense_all_used[".highlightSearchResults"] = true;

$tableKeyslicense_all_used = array();
$tdatalicense_all_used[".Keys"] = $tableKeyslicense_all_used;


$tdatalicense_all_used[".hideMobileList"] = array();




//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("license_all_used","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "spr_po.po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_all_used["po_name"] = $fdata;
		$tdatalicense_all_used[".searchableFields"][] = "po_name";
//	licenses_all
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "licenses_all";
	$fdata["GoodName"] = "licenses_all";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("license_all_used","licenses_all");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licenses_all";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "SUM(distinct spr_license_dogovor.licdog_quantity)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_all_used["licenses_all"] = $fdata;
		$tdatalicense_all_used[".searchableFields"][] = "licenses_all";
//	licenses_used
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "licenses_used";
	$fdata["GoodName"] = "licenses_used";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("license_all_used","licenses_used");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licenses_used";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "COUNT(arm_po.po_key)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_all_used["licenses_used"] = $fdata;
		$tdatalicense_all_used[".searchableFields"][] = "licenses_used";

$tdatalicense_all_used[".chartLabelField"] = "po_name";
$tdatalicense_all_used[".chartSeries"] = array();
$tdatalicense_all_used[".chartSeries"][] = array( 
	"field" => "licenses_all",
	"total" => ""
);
$tdatalicense_all_used[".chartSeries"][] = array( 
	"field" => "licenses_used",
	"total" => ""
);
	$tdatalicense_all_used[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">license_all_used</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="0">
			<attr value="name">licenses_all</attr>';
	$tdatalicense_all_used[".chartXml"] .= '</attr>';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="1">
			<attr value="name">licenses_used</attr>';
	$tdatalicense_all_used[".chartXml"] .= '</attr>';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="2">
		<attr value="name">po_name</attr>
	</attr>';
	$tdatalicense_all_used[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatalicense_all_used[".chartXml"] .= '<attr value="head">'.xmlencode("Использование лицензий").'</attr>
<attr value="foot">'.xmlencode("Описание").'</attr>
<attr value="y_axis_label">'.xmlencode("").'</attr>


<attr value="slegend">true</attr>
<attr value="sgrid">false</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">false</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">1</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatalicense_all_used[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="0">
		<attr value="name">po_name</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_all_used","po_name")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="1">
		<attr value="name">licenses_all</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_all_used","licenses_all")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatalicense_all_used[".chartXml"] .= '<attr value="2">
		<attr value="name">licenses_used</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_all_used","licenses_used")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatalicense_all_used[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">license_all_used</attr>
<attr value="short_table_name">license_all_used</attr>
</attr>

</chart>';

$tables_data["license_all_used"]=&$tdatalicense_all_used;
$field_labels["license_all_used"] = &$fieldLabelslicense_all_used;
$fieldToolTips["license_all_used"] = &$fieldToolTipslicense_all_used;
$placeHolders["license_all_used"] = &$placeHolderslicense_all_used;
$page_titles["license_all_used"] = &$pageTitleslicense_all_used;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["license_all_used"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["license_all_used"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_license_all_used()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "spr_po.po_name,  	SUM(distinct spr_license_dogovor.licdog_quantity) as licenses_all,  	COUNT(arm_po.po_key) as licenses_used";
$proto0["m_strFrom"] = "FROM spr_license_dogovor   		LEFT JOIN spr_license ON spr_license_dogovor.licdog_id = spr_license.lic_licdog  		LEFT JOIN arm_po ON arm_po.po_key = spr_license.lic_id  		LEFT JOIN spr_po ON spr_license_dogovor.licdog_poname = spr_po.po_id";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "order by spr_po.po_name";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "license_all_used"
));

$proto6["m_sql"] = "spr_po.po_name";
$proto6["m_srcTableName"] = "license_all_used";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$proto9=array();
$proto9["m_functiontype"] = "SQLF_SUM";
$proto9["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "distinct spr_license_dogovor.licdog_quantity"
));

$proto9["m_arguments"][]=$obj;
$proto9["m_strFunctionName"] = "SUM";
$obj = new SQLFunctionCall($proto9);

$proto8["m_sql"] = "SUM(distinct spr_license_dogovor.licdog_quantity)";
$proto8["m_srcTableName"] = "license_all_used";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "licenses_all";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$proto12=array();
$proto12["m_functiontype"] = "SQLF_COUNT";
$proto12["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "po_key",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "license_all_used"
));

$proto12["m_arguments"][]=$obj;
$proto12["m_strFunctionName"] = "COUNT";
$obj = new SQLFunctionCall($proto12);

$proto11["m_sql"] = "COUNT(arm_po.po_key)";
$proto11["m_srcTableName"] = "license_all_used";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "licenses_used";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "public.spr_license_dogovor";
$proto15["m_srcTableName"] = "license_all_used";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "licdog_id";
$proto15["m_columns"][] = "licdog_poname";
$proto15["m_columns"][] = "licdog_type";
$proto15["m_columns"][] = "licdog_quantity";
$proto15["m_columns"][] = "licdog_number";
$proto15["m_columns"][] = "licdog_date";
$proto15["m_columns"][] = "licdog_contractor";
$proto15["m_columns"][] = "licdog_desc";
$proto15["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "spr_license_dogovor";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "license_all_used";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
												$proto18=array();
$proto18["m_link"] = "SQLL_LEFTJOIN";
			$proto19=array();
$proto19["m_strName"] = "public.spr_license";
$proto19["m_srcTableName"] = "license_all_used";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "lic_id";
$proto19["m_columns"][] = "lic_poname";
$proto19["m_columns"][] = "lic_type";
$proto19["m_columns"][] = "lic_desc";
$proto19["m_columns"][] = "lic_key";
$proto19["m_columns"][] = "lic_servicenumber";
$proto19["m_columns"][] = "lic_licdog";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "LEFT JOIN spr_license ON spr_license_dogovor.licdog_id = spr_license.lic_licdog";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "license_all_used";
$proto20=array();
$proto20["m_sql"] = "spr_license_dogovor.licdog_id = spr_license.lic_licdog";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "licdog_id",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "license_all_used"
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "= spr_license.lic_licdog";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
												$proto22=array();
$proto22["m_link"] = "SQLL_LEFTJOIN";
			$proto23=array();
$proto23["m_strName"] = "public.arm_po";
$proto23["m_srcTableName"] = "license_all_used";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "armpo_id";
$proto23["m_columns"][] = "boxid";
$proto23["m_columns"][] = "po_name";
$proto23["m_columns"][] = "po_version";
$proto23["m_columns"][] = "po_arch";
$proto23["m_columns"][] = "po_key";
$proto23["m_columns"][] = "po_desc";
$proto23["m_columns"][] = "box_id";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "LEFT JOIN arm_po ON arm_po.po_key = spr_license.lic_id";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "license_all_used";
$proto24=array();
$proto24["m_sql"] = "arm_po.po_key = spr_license.lic_id";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "po_key",
	"m_strTable" => "public.arm_po",
	"m_srcTableName" => "license_all_used"
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "= spr_license.lic_id";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
												$proto26=array();
$proto26["m_link"] = "SQLL_LEFTJOIN";
			$proto27=array();
$proto27["m_strName"] = "public.spr_po";
$proto27["m_srcTableName"] = "license_all_used";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "po_id";
$proto27["m_columns"][] = "po_name";
$proto27["m_columns"][] = "po_desc";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "LEFT JOIN spr_po ON spr_license_dogovor.licdog_poname = spr_po.po_id";
$proto26["m_alias"] = "";
$proto26["m_srcTableName"] = "license_all_used";
$proto28=array();
$proto28["m_sql"] = "spr_license_dogovor.licdog_poname = spr_po.po_id";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "licdog_poname",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "license_all_used"
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "= spr_po.po_id";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto30=array();
						$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "license_all_used"
));

$proto30["m_column"]=$obj;
$obj = new SQLGroupByItem($proto30);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
												$proto32=array();
						$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "license_all_used"
));

$proto32["m_column"]=$obj;
$proto32["m_bAsc"] = 1;
$proto32["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto32);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="license_all_used";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_license_all_used = createSqlQuery_license_all_used();


	
		;

			

$tdatalicense_all_used[".sqlquery"] = $queryData_license_all_used;



$tableEvents["license_all_used"] = new eventsBase;
$tdatalicense_all_used[".hasEvents"] = false;

?>