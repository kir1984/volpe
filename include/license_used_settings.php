<?php
$tdatalicense_used = array();
$tdatalicense_used[".searchableFields"] = array();
$tdatalicense_used[".ShortName"] = "license_used";
$tdatalicense_used[".OwnerID"] = "";
$tdatalicense_used[".OriginalTable"] = "public.license_used";


$tdatalicense_used[".pagesByType"] = my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" );
$tdatalicense_used[".originalPagesByType"] = $tdatalicense_used[".pagesByType"];
$tdatalicense_used[".pages"] = types2pages( my_json_decode( "{\"chart\":[\"chart\"],\"search\":[\"search\"]}" ) );
$tdatalicense_used[".originalPages"] = $tdatalicense_used[".pages"];
$tdatalicense_used[".defaultPages"] = my_json_decode( "{\"chart\":\"chart\",\"search\":\"search\"}" );
$tdatalicense_used[".originalDefaultPages"] = $tdatalicense_used[".defaultPages"];

//	field labels
$fieldLabelslicense_used = array();
$fieldToolTipslicense_used = array();
$pageTitleslicense_used = array();
$placeHolderslicense_used = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelslicense_used["Russian"] = array();
	$fieldToolTipslicense_used["Russian"] = array();
	$placeHolderslicense_used["Russian"] = array();
	$pageTitleslicense_used["Russian"] = array();
	$fieldLabelslicense_used["Russian"]["po_name"] = "Наименование";
	$fieldToolTipslicense_used["Russian"]["po_name"] = "";
	$placeHolderslicense_used["Russian"]["po_name"] = "";
	$fieldLabelslicense_used["Russian"]["count"] = "Количество";
	$fieldToolTipslicense_used["Russian"]["count"] = "";
	$placeHolderslicense_used["Russian"]["count"] = "";
	if (count($fieldToolTipslicense_used["Russian"]))
		$tdatalicense_used[".isUseToolTips"] = true;
}


	$tdatalicense_used[".NCSearch"] = true;

	$tdatalicense_used[".ChartRefreshTime"] = 0;


$tdatalicense_used[".shortTableName"] = "license_used";
$tdatalicense_used[".nSecOptions"] = 0;

$tdatalicense_used[".mainTableOwnerID"] = "";
$tdatalicense_used[".entityType"] = 3;
$tdatalicense_used[".connId"] = "itbase3_at_192_168_1_15";


$tdatalicense_used[".strOriginalTableName"] = "public.license_used";

	



$tdatalicense_used[".showAddInPopup"] = false;

$tdatalicense_used[".showEditInPopup"] = false;

$tdatalicense_used[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatalicense_used[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatalicense_used[".listAjax"] = false;
//	temporary
$tdatalicense_used[".listAjax"] = false;

	$tdatalicense_used[".audit"] = false;

	$tdatalicense_used[".locking"] = false;


$pages = $tdatalicense_used[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatalicense_used[".edit"] = true;
	$tdatalicense_used[".afterEditAction"] = 1;
	$tdatalicense_used[".closePopupAfterEdit"] = 1;
	$tdatalicense_used[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatalicense_used[".add"] = true;
$tdatalicense_used[".afterAddAction"] = 1;
$tdatalicense_used[".closePopupAfterAdd"] = 1;
$tdatalicense_used[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatalicense_used[".list"] = true;
}



$tdatalicense_used[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatalicense_used[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatalicense_used[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatalicense_used[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatalicense_used[".printFriendly"] = true;
}



$tdatalicense_used[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatalicense_used[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatalicense_used[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatalicense_used[".isUseAjaxSuggest"] = true;






$tdatalicense_used[".ajaxCodeSnippetAdded"] = false;

$tdatalicense_used[".buttonsAdded"] = false;

$tdatalicense_used[".addPageEvents"] = false;

// use timepicker for search panel
$tdatalicense_used[".isUseTimeForSearch"] = false;


$tdatalicense_used[".badgeColor"] = "6B8E23";


$tdatalicense_used[".allSearchFields"] = array();
$tdatalicense_used[".filterFields"] = array();
$tdatalicense_used[".requiredSearchFields"] = array();

$tdatalicense_used[".googleLikeFields"] = array();
$tdatalicense_used[".googleLikeFields"][] = "po_name";
$tdatalicense_used[".googleLikeFields"][] = "count";



$tdatalicense_used[".tableType"] = "chart";

$tdatalicense_used[".printerPageOrientation"] = 0;
$tdatalicense_used[".nPrinterPageScale"] = 100;

$tdatalicense_used[".nPrinterSplitRecords"] = 40;

$tdatalicense_used[".geocodingEnabled"] = false;



// chart settings
$tdatalicense_used[".chartType"] = "2DBar";
// end of chart settings








$tstrOrderBy = "ORDER BY sp.po_name";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatalicense_used[".strOrderBy"] = $tstrOrderBy;

$tdatalicense_used[".orderindexes"] = array();
	$tdatalicense_used[".orderindexes"][] = array(1, (1 ? "ASC" : "DESC"), "sp.po_name");



$tdatalicense_used[".sqlHead"] = "SELECT sp.po_name,  COUNT(*)";
$tdatalicense_used[".sqlFrom"] = "FROM \"public\".arm_po AS ap  INNER JOIN \"public\".spr_license AS sl ON ap.po_key = sl.lic_id  INNER JOIN \"public\".spr_po AS sp ON sl.lic_poname = sp.po_id";
$tdatalicense_used[".sqlWhereExpr"] = "";
$tdatalicense_used[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatalicense_used[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatalicense_used[".arrGroupsPerPage"] = $arrGPP;

$tdatalicense_used[".highlightSearchResults"] = true;

$tableKeyslicense_used = array();
$tdatalicense_used[".Keys"] = $tableKeyslicense_used;


$tdatalicense_used[".hideMobileList"] = array();




//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("license_used","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sp.po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_used["po_name"] = $fdata;
		$tdatalicense_used[".searchableFields"][] = "po_name";
//	count
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "count";
	$fdata["GoodName"] = "count";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("license_used","count");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "count";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "COUNT(*)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatalicense_used["count"] = $fdata;
		$tdatalicense_used[".searchableFields"][] = "count";

$tdatalicense_used[".chartLabelField"] = "po_name";
$tdatalicense_used[".chartSeries"] = array();
$tdatalicense_used[".chartSeries"][] = array( 
	"field" => "count",
	"total" => ""
);
	$tdatalicense_used[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">license_used</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatalicense_used[".chartXml"] .= '<attr value="0">
			<attr value="name">count</attr>';
	$tdatalicense_used[".chartXml"] .= '</attr>';
	$tdatalicense_used[".chartXml"] .= '<attr value="1">
		<attr value="name">po_name</attr>
	</attr>';
	$tdatalicense_used[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatalicense_used[".chartXml"] .= '<attr value="head">'.xmlencode("Используемые лицензии").'</attr>
<attr value="foot">'.xmlencode("").'</attr>
<attr value="y_axis_label">'.xmlencode("lic_quantity").'</attr>


<attr value="slegend">false</attr>
<attr value="sgrid">false</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatalicense_used[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatalicense_used[".chartXml"] .= '<attr value="0">
		<attr value="name">po_name</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_used","po_name")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatalicense_used[".chartXml"] .= '<attr value="1">
		<attr value="name">count</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("license_used","count")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatalicense_used[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">license_used</attr>
<attr value="short_table_name">license_used</attr>
</attr>

</chart>';

$tables_data["license_used"]=&$tdatalicense_used;
$field_labels["license_used"] = &$fieldLabelslicense_used;
$fieldToolTips["license_used"] = &$fieldToolTipslicense_used;
$placeHolders["license_used"] = &$placeHolderslicense_used;
$page_titles["license_used"] = &$pageTitleslicense_used;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["license_used"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["license_used"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_license_used()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "sp.po_name,  COUNT(*)";
$proto0["m_strFrom"] = "FROM \"public\".arm_po AS ap  INNER JOIN \"public\".spr_license AS sl ON ap.po_key = sl.lic_id  INNER JOIN \"public\".spr_po AS sp ON sl.lic_poname = sp.po_id";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY sp.po_name";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "sp",
	"m_srcTableName" => "license_used"
));

$proto6["m_sql"] = "sp.po_name";
$proto6["m_srcTableName"] = "license_used";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$proto9=array();
$proto9["m_functiontype"] = "SQLF_COUNT";
$proto9["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "*"
));

$proto9["m_arguments"][]=$obj;
$proto9["m_strFunctionName"] = "COUNT";
$obj = new SQLFunctionCall($proto9);

$proto8["m_sql"] = "COUNT(*)";
$proto8["m_srcTableName"] = "license_used";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto11=array();
$proto11["m_link"] = "SQLL_MAIN";
			$proto12=array();
$proto12["m_strName"] = "public.arm_po";
$proto12["m_srcTableName"] = "license_used";
$proto12["m_columns"] = array();
$proto12["m_columns"][] = "armpo_id";
$proto12["m_columns"][] = "boxid";
$proto12["m_columns"][] = "po_name";
$proto12["m_columns"][] = "po_version";
$proto12["m_columns"][] = "po_arch";
$proto12["m_columns"][] = "po_key";
$proto12["m_columns"][] = "po_desc";
$proto12["m_columns"][] = "box_id";
$obj = new SQLTable($proto12);

$proto11["m_table"] = $obj;
$proto11["m_sql"] = "\"public\".arm_po AS ap";
$proto11["m_alias"] = "ap";
$proto11["m_srcTableName"] = "license_used";
$proto13=array();
$proto13["m_sql"] = "";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

$proto11["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto11);

$proto0["m_fromlist"][]=$obj;
												$proto15=array();
$proto15["m_link"] = "SQLL_INNERJOIN";
			$proto16=array();
$proto16["m_strName"] = "public.spr_license";
$proto16["m_srcTableName"] = "license_used";
$proto16["m_columns"] = array();
$proto16["m_columns"][] = "lic_id";
$proto16["m_columns"][] = "lic_poname";
$proto16["m_columns"][] = "lic_type";
$proto16["m_columns"][] = "lic_desc";
$proto16["m_columns"][] = "lic_key";
$proto16["m_columns"][] = "lic_servicenumber";
$proto16["m_columns"][] = "lic_licdog";
$obj = new SQLTable($proto16);

$proto15["m_table"] = $obj;
$proto15["m_sql"] = "INNER JOIN \"public\".spr_license AS sl ON ap.po_key = sl.lic_id";
$proto15["m_alias"] = "sl";
$proto15["m_srcTableName"] = "license_used";
$proto17=array();
$proto17["m_sql"] = "ap.po_key = sl.lic_id";
$proto17["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "po_key",
	"m_strTable" => "ap",
	"m_srcTableName" => "license_used"
));

$proto17["m_column"]=$obj;
$proto17["m_contained"] = array();
$proto17["m_strCase"] = "= sl.lic_id";
$proto17["m_havingmode"] = false;
$proto17["m_inBrackets"] = false;
$proto17["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto17);

$proto15["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto15);

$proto0["m_fromlist"][]=$obj;
												$proto19=array();
$proto19["m_link"] = "SQLL_INNERJOIN";
			$proto20=array();
$proto20["m_strName"] = "public.spr_po";
$proto20["m_srcTableName"] = "license_used";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "po_id";
$proto20["m_columns"][] = "po_name";
$proto20["m_columns"][] = "po_desc";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "INNER JOIN \"public\".spr_po AS sp ON sl.lic_poname = sp.po_id";
$proto19["m_alias"] = "sp";
$proto19["m_srcTableName"] = "license_used";
$proto21=array();
$proto21["m_sql"] = "sl.lic_poname = sp.po_id";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "lic_poname",
	"m_strTable" => "sl",
	"m_srcTableName" => "license_used"
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "= sp.po_id";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "sp",
	"m_srcTableName" => "license_used"
));

$proto23["m_column"]=$obj;
$obj = new SQLGroupByItem($proto23);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
												$proto25=array();
						$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "sp",
	"m_srcTableName" => "license_used"
));

$proto25["m_column"]=$obj;
$proto25["m_bAsc"] = 1;
$proto25["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto25);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="license_used";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_license_used = createSqlQuery_license_used();


	
		;

		

$tdatalicense_used[".sqlquery"] = $queryData_license_used;



$tableEvents["license_used"] = new eventsBase;
$tdatalicense_used[".hasEvents"] = false;

?>