<?php
require_once(getabspath("classes/cipherer.php"));



$tdatalicenses = array();
$tdatalicenses[".ShortName"] = "licenses";

$tdatalicenses[".pagesByType"] = my_json_decode( "{\"dashboard\":[\"dashboard\"],\"search\":[\"search\"]}" );
$tdatalicenses[".originalPagesByType"] = $tdatalicenses[".pagesByType"];
$tdatalicenses[".pages"] = types2pages( my_json_decode( "{\"dashboard\":[\"dashboard\"],\"search\":[\"search\"]}" ) );
$tdatalicenses[".originalPages"] = $tdatalicenses[".pages"];
$tdatalicenses[".defaultPages"] = my_json_decode( "{\"dashboard\":\"dashboard\",\"search\":\"search\"}" );
$tdatalicenses[".originalDefaultPages"] = $tdatalicenses[".defaultPages"];


//	field labels
$fieldLabelslicenses = array();
$pageTitleslicenses = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelslicenses["Russian"] = array();
	$fieldLabelslicenses["Russian"]["license_all_po_name"] = "Наименование";
	$fieldLabelslicenses["Russian"]["license_used_po_name"] = "Наименование";
	$fieldLabelslicenses["Russian"]["report_licenses_used_po_name"] = "Наименование ПО";
	$fieldLabelslicenses["Russian"]["report_licenses_used_lic_key"] = "Ключ";
	$fieldLabelslicenses["Russian"]["report_licenses_used_lic_quantity"] = "Имеющиеся лицензии";
	$fieldLabelslicenses["Russian"]["report_licenses_used_count"] = "Используемые лицензии";
}

//	search fields
$tdatalicenses[".searchFields"] = array();
$dashField = array();
$dashField[] = array( "table"=>"license_all", "field"=>"po_name" );
$tdatalicenses[".searchFields"]["license_all_po_name"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"license_used", "field"=>"po_name" );
$tdatalicenses[".searchFields"]["license_used_po_name"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"report_licenses_used", "field"=>"po_name" );
$tdatalicenses[".searchFields"]["report_licenses_used_po_name"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"report_licenses_used", "field"=>"lic_key" );
$tdatalicenses[".searchFields"]["report_licenses_used_lic_key"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"report_licenses_used", "field"=>"lic_quantity" );
$tdatalicenses[".searchFields"]["report_licenses_used_lic_quantity"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"report_licenses_used", "field"=>"count" );
$tdatalicenses[".searchFields"]["report_licenses_used_count"] = $dashField;

// all search fields
$tdatalicenses[".allSearchFields"] = array();
$tdatalicenses[".allSearchFields"][] = "license_all_po_name";
$tdatalicenses[".allSearchFields"][] = "license_used_po_name";
$tdatalicenses[".allSearchFields"][] = "report_licenses_used_po_name";
$tdatalicenses[".allSearchFields"][] = "report_licenses_used_lic_key";
$tdatalicenses[".allSearchFields"][] = "report_licenses_used_lic_quantity";
$tdatalicenses[".allSearchFields"][] = "report_licenses_used_count";

// good like search fields
$tdatalicenses[".googleLikeFields"] = array();
$tdatalicenses[".googleLikeFields"][] = "license_all_po_name";
$tdatalicenses[".googleLikeFields"][] = "license_used_po_name";
$tdatalicenses[".googleLikeFields"][] = "report_licenses_used_po_name";
$tdatalicenses[".googleLikeFields"][] = "report_licenses_used_lic_key";
$tdatalicenses[".googleLikeFields"][] = "report_licenses_used_lic_quantity";
$tdatalicenses[".googleLikeFields"][] = "report_licenses_used_count";

$tdatalicenses[".dashElements"] = array();

	$dbelement = array( "elementName" => "license_all_chart", "table" => "license_all",
		 "pageName" => "","type" => 1);
	$dbelement["cellName"] = "cell_0_0";

			

	$tdatalicenses[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "license_used_chart", "table" => "license_used",
		 "pageName" => "","type" => 1);
	$dbelement["cellName"] = "cell_0_1";

			

	$tdatalicenses[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "report_licenses_used_report", "table" => "report_licenses_used",
		 "pageName" => "","type" => 2);
	$dbelement["cellName"] = "cell_1_0";

			

	$tdatalicenses[".dashElements"][] = $dbelement;

$tdatalicenses[".shortTableName"] = "licenses";
$tdatalicenses[".entityType"] = 4;



$tableEvents["licenses"] = new eventsBase;
$tdatalicenses[".hasEvents"] = false;


$tdatalicenses[".tableType"] = "dashboard";



$tdatalicenses[".addPageEvents"] = false;

$tdatalicenses[".isUseAjaxSuggest"] = true;

$tables_data["licenses"]=&$tdatalicenses;
$field_labels["licenses"] = &$fieldLabelslicenses;
$page_titles["licenses"] = &$pageTitleslicenses;

?>