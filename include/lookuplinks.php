<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

		if( !isset( $lookupTableLinks["public.hw_box"] ) ) {
			$lookupTableLinks["public.hw_box"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_box"]["arm.box_id"] )) {
			$lookupTableLinks["public.hw_box"]["arm.box_id"] = array();
		}
		$lookupTableLinks["public.hw_box"]["arm.box_id"]["edit"] = array("table" => "public.arm", "field" => "box_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.hw_tel"] ) ) {
			$lookupTableLinks["public.hw_tel"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_tel"]["arm.tel_id"] )) {
			$lookupTableLinks["public.hw_tel"]["arm.tel_id"] = array();
		}
		$lookupTableLinks["public.hw_tel"]["arm.tel_id"]["edit"] = array("table" => "public.arm", "field" => "tel_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.hw_printer"] ) ) {
			$lookupTableLinks["public.hw_printer"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_printer"]["arm.printer_id"] )) {
			$lookupTableLinks["public.hw_printer"]["arm.printer_id"] = array();
		}
		$lookupTableLinks["public.hw_printer"]["arm.printer_id"]["edit"] = array("table" => "public.arm", "field" => "printer_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["arm.sotrudnik_id"] )) {
			$lookupTableLinks["public.sotrudnik"]["arm.sotrudnik_id"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["arm.sotrudnik_id"]["edit"] = array("table" => "public.arm", "field" => "sotrudnik_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_location"] ) ) {
			$lookupTableLinks["public.spr_location"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_location"]["arm.loc_id"] )) {
			$lookupTableLinks["public.spr_location"]["arm.loc_id"] = array();
		}
		$lookupTableLinks["public.spr_location"]["arm.loc_id"]["edit"] = array("table" => "public.arm", "field" => "loc_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_socket"] ) ) {
			$lookupTableLinks["public.spr_socket"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_socket"]["arm.socket_id"] )) {
			$lookupTableLinks["public.spr_socket"]["arm.socket_id"] = array();
		}
		$lookupTableLinks["public.spr_socket"]["arm.socket_id"]["edit"] = array("table" => "public.arm", "field" => "socket_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["hw_ibp.ibp_user"] )) {
			$lookupTableLinks["public.sotrudnik"]["hw_ibp.ibp_user"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["hw_ibp.ibp_user"]["edit"] = array("table" => "public.hw_ibp", "field" => "ibp_user", "page" => "edit");
		if( !isset( $lookupTableLinks["public.hw_box"] ) ) {
			$lookupTableLinks["public.hw_box"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_box"]["hw_monitor1.boxid"] )) {
			$lookupTableLinks["public.hw_box"]["hw_monitor1.boxid"] = array();
		}
		$lookupTableLinks["public.hw_box"]["hw_monitor1.boxid"]["edit"] = array("table" => "public.hw_monitor1", "field" => "boxid", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_department"] ) ) {
			$lookupTableLinks["public.spr_department"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_department"]["sotrudnik.sotrudnik_dep"] )) {
			$lookupTableLinks["public.spr_department"]["sotrudnik.sotrudnik_dep"] = array();
		}
		$lookupTableLinks["public.spr_department"]["sotrudnik.sotrudnik_dep"]["edit"] = array("table" => "public.sotrudnik", "field" => "sotrudnik_dep", "page" => "edit");
		if( !isset( $lookupTableLinks["public.hw_box"] ) ) {
			$lookupTableLinks["public.hw_box"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_box"]["arm_po.boxid"] )) {
			$lookupTableLinks["public.hw_box"]["arm_po.boxid"] = array();
		}
		$lookupTableLinks["public.hw_box"]["arm_po.boxid"]["edit"] = array("table" => "public.arm_po", "field" => "boxid", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["arm_po.po_name"] )) {
			$lookupTableLinks["public.spr_po"]["arm_po.po_name"] = array();
		}
		$lookupTableLinks["public.spr_po"]["arm_po.po_name"]["edit"] = array("table" => "public.arm_po", "field" => "po_name", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_license"] ) ) {
			$lookupTableLinks["public.spr_license"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_license"]["arm_po.po_key"] )) {
			$lookupTableLinks["public.spr_license"]["arm_po.po_key"] = array();
		}
		$lookupTableLinks["public.spr_license"]["arm_po.po_key"]["edit"] = array("table" => "public.arm_po", "field" => "po_key", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["ecp.ecp_user"] )) {
			$lookupTableLinks["public.sotrudnik"]["ecp.ecp_user"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["ecp.ecp_user"]["edit"] = array("table" => "public.ecp", "field" => "ecp_user", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_ecp"] ) ) {
			$lookupTableLinks["public.spr_ecp"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_ecp"]["ecp.spr_ecp"] )) {
			$lookupTableLinks["public.spr_ecp"]["ecp.spr_ecp"] = array();
		}
		$lookupTableLinks["public.spr_ecp"]["ecp.spr_ecp"]["edit"] = array("table" => "public.ecp", "field" => "spr_ecp", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_location"] ) ) {
			$lookupTableLinks["public.spr_location"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_location"]["hw_switch.sw_location"] )) {
			$lookupTableLinks["public.spr_location"]["hw_switch.sw_location"] = array();
		}
		$lookupTableLinks["public.spr_location"]["hw_switch.sw_location"]["edit"] = array("table" => "public.hw_switch", "field" => "sw_location", "page" => "edit");
		if( !isset( $lookupTableLinks["public.hw_switch"] ) ) {
			$lookupTableLinks["public.hw_switch"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_switch"]["spr_socket.switch_id"] )) {
			$lookupTableLinks["public.hw_switch"]["spr_socket.switch_id"] = array();
		}
		$lookupTableLinks["public.hw_switch"]["spr_socket.switch_id"]["edit"] = array("table" => "public.spr_socket", "field" => "switch_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["spr_license.lic_poname"] )) {
			$lookupTableLinks["public.spr_po"]["spr_license.lic_poname"] = array();
		}
		$lookupTableLinks["public.spr_po"]["spr_license.lic_poname"]["edit"] = array("table" => "public.spr_license", "field" => "lic_poname", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_license_dogovor1111111"] ) ) {
			$lookupTableLinks["public.spr_license_dogovor1111111"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_license_dogovor1111111"]["spr_license.lic_licdog"] )) {
			$lookupTableLinks["public.spr_license_dogovor1111111"]["spr_license.lic_licdog"] = array();
		}
		$lookupTableLinks["public.spr_license_dogovor1111111"]["spr_license.lic_licdog"]["edit"] = array("table" => "public.spr_license", "field" => "lic_licdog", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["spr_license_dogovor.licdog_poname"] )) {
			$lookupTableLinks["public.spr_po"]["spr_license_dogovor.licdog_poname"] = array();
		}
		$lookupTableLinks["public.spr_po"]["spr_license_dogovor.licdog_poname"]["edit"] = array("table" => "public.spr_license_dogovor", "field" => "licdog_poname", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["spr_license_dogovor1.licdog_poname"] )) {
			$lookupTableLinks["public.spr_po"]["spr_license_dogovor1.licdog_poname"] = array();
		}
		$lookupTableLinks["public.spr_po"]["spr_license_dogovor1.licdog_poname"]["edit"] = array("table" => "public.spr_license_dogovor1", "field" => "licdog_poname", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["spr_license_dogovor11.licdog_poname"] )) {
			$lookupTableLinks["public.spr_po"]["spr_license_dogovor11.licdog_poname"] = array();
		}
		$lookupTableLinks["public.spr_po"]["spr_license_dogovor11.licdog_poname"]["edit"] = array("table" => "public.spr_license_dogovor11", "field" => "licdog_poname", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_po"] ) ) {
			$lookupTableLinks["public.spr_po"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_po"]["spr_license_dogovor111.licdog_poname"] )) {
			$lookupTableLinks["public.spr_po"]["spr_license_dogovor111.licdog_poname"] = array();
		}
		$lookupTableLinks["public.spr_po"]["spr_license_dogovor111.licdog_poname"]["edit"] = array("table" => "public.spr_license_dogovor111", "field" => "licdog_poname", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_doc_type"] ) ) {
			$lookupTableLinks["public.spr_doc_type"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_doc_type"]["doc_in.doc_type"] )) {
			$lookupTableLinks["public.spr_doc_type"]["doc_in.doc_type"] = array();
		}
		$lookupTableLinks["public.spr_doc_type"]["doc_in.doc_type"]["edit"] = array("table" => "public.doc_in", "field" => "doc_type", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_partners"] ) ) {
			$lookupTableLinks["public.spr_partners"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_partners"]["doc_in.doc_source"] )) {
			$lookupTableLinks["public.spr_partners"]["doc_in.doc_source"] = array();
		}
		$lookupTableLinks["public.spr_partners"]["doc_in.doc_source"]["edit"] = array("table" => "public.doc_in", "field" => "doc_source", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination"]["edit"] = array("table" => "public.doc_in", "field" => "doc_destination", "page" => "edit");
		if( !isset( $lookupTableLinks["public.doc_out"] ) ) {
			$lookupTableLinks["public.doc_out"] = array();
		}
		if( !isset( $lookupTableLinks["public.doc_out"]["doc_in.doc_answer"] )) {
			$lookupTableLinks["public.doc_out"]["doc_in.doc_answer"] = array();
		}
		$lookupTableLinks["public.doc_out"]["doc_in.doc_answer"]["edit"] = array("table" => "public.doc_in", "field" => "doc_answer", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_in.doc_control_executor"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_in.doc_control_executor"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_in.doc_control_executor"]["edit"] = array("table" => "public.doc_in", "field" => "doc_control_executor", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination2"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination2"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination2"]["edit"] = array("table" => "public.doc_in", "field" => "doc_destination2", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination3"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination3"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination3"]["edit"] = array("table" => "public.doc_in", "field" => "doc_destination3", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination4"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination4"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_in.doc_destination4"]["edit"] = array("table" => "public.doc_in", "field" => "doc_destination4", "page" => "edit");
		if( !isset( $lookupTableLinks["public.doc_in"] ) ) {
			$lookupTableLinks["public.doc_in"] = array();
		}
		if( !isset( $lookupTableLinks["public.doc_in"]["doc_out.doc_answer"] )) {
			$lookupTableLinks["public.doc_in"]["doc_out.doc_answer"] = array();
		}
		$lookupTableLinks["public.doc_in"]["doc_out.doc_answer"]["edit"] = array("table" => "public.doc_out", "field" => "doc_answer", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_doc_type"] ) ) {
			$lookupTableLinks["public.spr_doc_type"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_doc_type"]["doc_out.doc_type"] )) {
			$lookupTableLinks["public.spr_doc_type"]["doc_out.doc_type"] = array();
		}
		$lookupTableLinks["public.spr_doc_type"]["doc_out.doc_type"]["edit"] = array("table" => "public.doc_out", "field" => "doc_type", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_partners"] ) ) {
			$lookupTableLinks["public.spr_partners"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_partners"]["doc_out.doc_destination"] )) {
			$lookupTableLinks["public.spr_partners"]["doc_out.doc_destination"] = array();
		}
		$lookupTableLinks["public.spr_partners"]["doc_out.doc_destination"]["edit"] = array("table" => "public.doc_out", "field" => "doc_destination", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_out.doc_executor"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_out.doc_executor"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_out.doc_executor"]["edit"] = array("table" => "public.doc_out", "field" => "doc_executor", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_docint_type"] ) ) {
			$lookupTableLinks["public.spr_docint_type"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_docint_type"]["doc_int.doc_type"] )) {
			$lookupTableLinks["public.spr_docint_type"]["doc_int.doc_type"] = array();
		}
		$lookupTableLinks["public.spr_docint_type"]["doc_int.doc_type"]["edit"] = array("table" => "public.doc_int", "field" => "doc_type", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_partners"] ) ) {
			$lookupTableLinks["public.spr_partners"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_partners"]["doc_int.doc_destination"] )) {
			$lookupTableLinks["public.spr_partners"]["doc_int.doc_destination"] = array();
		}
		$lookupTableLinks["public.spr_partners"]["doc_int.doc_destination"]["edit"] = array("table" => "public.doc_int", "field" => "doc_destination", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_int.doc_initiator"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_int.doc_initiator"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_int.doc_initiator"]["edit"] = array("table" => "public.doc_int", "field" => "doc_initiator", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_int.doc_executor"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_int.doc_executor"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_int.doc_executor"]["edit"] = array("table" => "public.doc_int", "field" => "doc_executor", "page" => "edit");
		if( !isset( $lookupTableLinks["public.sotrudnik"] ) ) {
			$lookupTableLinks["public.sotrudnik"] = array();
		}
		if( !isset( $lookupTableLinks["public.sotrudnik"]["doc_int.doc_destination_int"] )) {
			$lookupTableLinks["public.sotrudnik"]["doc_int.doc_destination_int"] = array();
		}
		$lookupTableLinks["public.sotrudnik"]["doc_int.doc_destination_int"]["edit"] = array("table" => "public.doc_int", "field" => "doc_destination_int", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_partners"] ) ) {
			$lookupTableLinks["public.spr_partners"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_partners"]["report_docin.doc_source"] )) {
			$lookupTableLinks["public.spr_partners"]["report_docin.doc_source"] = array();
		}
		$lookupTableLinks["public.spr_partners"]["report_docin.doc_source"]["search"] = array("table" => "report_docin", "field" => "doc_source", "page" => "search");
		if( !isset( $lookupTableLinks["public.doc_out"] ) ) {
			$lookupTableLinks["public.doc_out"] = array();
		}
		if( !isset( $lookupTableLinks["public.doc_out"]["report_docin.doc_answer"] )) {
			$lookupTableLinks["public.doc_out"]["report_docin.doc_answer"] = array();
		}
		$lookupTableLinks["public.doc_out"]["report_docin.doc_answer"]["search"] = array("table" => "report_docin", "field" => "doc_answer", "page" => "search");
		if( !isset( $lookupTableLinks["public.doc_in"] ) ) {
			$lookupTableLinks["public.doc_in"] = array();
		}
		if( !isset( $lookupTableLinks["public.doc_in"]["report_docout.doc_answer"] )) {
			$lookupTableLinks["public.doc_in"]["report_docout.doc_answer"] = array();
		}
		$lookupTableLinks["public.doc_in"]["report_docout.doc_answer"]["search"] = array("table" => "report_docout", "field" => "doc_answer", "page" => "search");
		if( !isset( $lookupTableLinks["public.hw_printer"] ) ) {
			$lookupTableLinks["public.hw_printer"] = array();
		}
		if( !isset( $lookupTableLinks["public.hw_printer"]["cons_cartridge.cartridge_inprinterid"] )) {
			$lookupTableLinks["public.hw_printer"]["cons_cartridge.cartridge_inprinterid"] = array();
		}
		$lookupTableLinks["public.hw_printer"]["cons_cartridge.cartridge_inprinterid"]["edit"] = array("table" => "public.cons_cartridge", "field" => "cartridge_inprinterid", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_location"] ) ) {
			$lookupTableLinks["public.spr_location"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_location"]["cons_cartridge.cartridge_storage"] )) {
			$lookupTableLinks["public.spr_location"]["cons_cartridge.cartridge_storage"] = array();
		}
		$lookupTableLinks["public.spr_location"]["cons_cartridge.cartridge_storage"]["edit"] = array("table" => "public.cons_cartridge", "field" => "cartridge_storage", "page" => "edit");
		if( !isset( $lookupTableLinks["public.cons_cartridge"] ) ) {
			$lookupTableLinks["public.cons_cartridge"] = array();
		}
		if( !isset( $lookupTableLinks["public.cons_cartridge"]["service_refilling.conscart_id"] )) {
			$lookupTableLinks["public.cons_cartridge"]["service_refilling.conscart_id"] = array();
		}
		$lookupTableLinks["public.cons_cartridge"]["service_refilling.conscart_id"]["edit"] = array("table" => "public.service_refilling", "field" => "conscart_id", "page" => "edit");
		if( !isset( $lookupTableLinks["public.spr_partners"] ) ) {
			$lookupTableLinks["public.spr_partners"] = array();
		}
		if( !isset( $lookupTableLinks["public.spr_partners"]["service_refilling.ref_partner"] )) {
			$lookupTableLinks["public.spr_partners"]["service_refilling.ref_partner"] = array();
		}
		$lookupTableLinks["public.spr_partners"]["service_refilling.ref_partner"]["edit"] = array("table" => "public.service_refilling", "field" => "ref_partner", "page" => "edit");
}

?>