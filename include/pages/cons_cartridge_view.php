<?php
			$optionsArray = array( 'pdf' => array( 'pdfView' => false ),
'master' => array( 'public.hw_printer' => array( 'preview' => false ),
'public.spr_location' => array( 'preview' => false ) ),
'fields' => array( 'gridFields' => array( 'cartridge_type',
'cartridge_name',
'cartridge_desc',
'cartridge_inv',
'cartridge_inprinterid',
'cartridge_storage',
'cartridge_status' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'cartridge_type' => array( 'integrated_edit_field',
'integrated_edit_field1' ),
'cartridge_name' => array( 'integrated_edit_field2',
'integrated_edit_field9' ),
'cartridge_desc' => array( 'integrated_edit_field3',
'integrated_edit_field13' ),
'cartridge_inv' => array( 'integrated_edit_field4',
'integrated_edit_field8' ),
'cartridge_inprinterid' => array( 'integrated_edit_field5',
'integrated_edit_field10' ),
'cartridge_storage' => array( 'integrated_edit_field6',
'integrated_edit_field11' ),
'cartridge_status' => array( 'integrated_edit_field7',
'integrated_edit_field12' ) ) ),
'pageLinks' => array( 'edit' => true,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'top' => array( 'view_header' ),
'above-grid' => array(  ),
'below-grid' => array( 'view_back_list',
'view_close',
'prev',
'next',
'hamburger' ),
'grid' => array( 'integrated_edit_field8',
'integrated_edit_field4',
'integrated_edit_field1',
'integrated_edit_field',
'integrated_edit_field9',
'integrated_edit_field2',
'integrated_edit_field10',
'integrated_edit_field5',
'integrated_edit_field11',
'integrated_edit_field6',
'integrated_edit_field12',
'integrated_edit_field7',
'integrated_edit_field13',
'integrated_edit_field3' ) ),
'formXtTags' => array( 'above-grid' => array(  ) ),
'itemForms' => array( 'view_header' => 'top',
'view_back_list' => 'below-grid',
'view_close' => 'below-grid',
'prev' => 'below-grid',
'next' => 'below-grid',
'hamburger' => 'below-grid',
'integrated_edit_field8' => 'grid',
'integrated_edit_field4' => 'grid',
'integrated_edit_field1' => 'grid',
'integrated_edit_field' => 'grid',
'integrated_edit_field9' => 'grid',
'integrated_edit_field2' => 'grid',
'integrated_edit_field10' => 'grid',
'integrated_edit_field5' => 'grid',
'integrated_edit_field11' => 'grid',
'integrated_edit_field6' => 'grid',
'integrated_edit_field12' => 'grid',
'integrated_edit_field7' => 'grid',
'integrated_edit_field13' => 'grid',
'integrated_edit_field3' => 'grid' ),
'itemLocations' => array( 'integrated_edit_field8' => array( 'location' => 'grid',
'cellId' => 'c4' ),
'integrated_edit_field4' => array( 'location' => 'grid',
'cellId' => 'c2' ),
'integrated_edit_field1' => array( 'location' => 'grid',
'cellId' => 'c' ),
'integrated_edit_field' => array( 'location' => 'grid',
'cellId' => 'c3' ),
'integrated_edit_field9' => array( 'location' => 'grid',
'cellId' => 'c5' ),
'integrated_edit_field2' => array( 'location' => 'grid',
'cellId' => 'c6' ),
'integrated_edit_field10' => array( 'location' => 'grid',
'cellId' => 'c7' ),
'integrated_edit_field5' => array( 'location' => 'grid',
'cellId' => 'c8' ),
'integrated_edit_field11' => array( 'location' => 'grid',
'cellId' => 'c9' ),
'integrated_edit_field6' => array( 'location' => 'grid',
'cellId' => 'c10' ),
'integrated_edit_field12' => array( 'location' => 'grid',
'cellId' => 'c11' ),
'integrated_edit_field7' => array( 'location' => 'grid',
'cellId' => 'c12' ),
'integrated_edit_field13' => array( 'location' => 'grid',
'cellId' => 'c13' ),
'integrated_edit_field3' => array( 'location' => 'grid',
'cellId' => 'c14' ) ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'view_header' => array( 'view_header' ),
'view_back_list' => array( 'view_back_list' ),
'view_close' => array( 'view_close' ),
'hamburger' => array( 'hamburger' ),
'view_edit' => array( 'view_edit' ),
'edit_field' => array( 'integrated_edit_field',
'integrated_edit_field2',
'integrated_edit_field3',
'integrated_edit_field4',
'integrated_edit_field5',
'integrated_edit_field6',
'integrated_edit_field7' ),
'edit_field_label' => array( 'integrated_edit_field1',
'integrated_edit_field8',
'integrated_edit_field9',
'integrated_edit_field10',
'integrated_edit_field11',
'integrated_edit_field12',
'integrated_edit_field13' ),
'next' => array( 'next' ),
'prev' => array( 'prev' ) ),
'cellMaps' => array( 'grid' => array( 'cells' => array( 'c4' => array( 'cols' => array( 0 ),
'rows' => array( 0 ),
'tags' => array( 'cartridge_inv_fieldblock' ),
'items' => array( 'integrated_edit_field8' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c2' => array( 'cols' => array( 1 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field4' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c' => array( 'cols' => array( 0 ),
'rows' => array( 1 ),
'tags' => array( 'cartridge_type_fieldblock' ),
'items' => array( 'integrated_edit_field1' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c3' => array( 'cols' => array( 1 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c5' => array( 'cols' => array( 0 ),
'rows' => array( 2 ),
'tags' => array( 'cartridge_name_fieldblock' ),
'items' => array( 'integrated_edit_field9' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c6' => array( 'cols' => array( 1 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field2' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c7' => array( 'cols' => array( 0 ),
'rows' => array( 3 ),
'tags' => array( 'cartridge_inprinterid_fieldblock' ),
'items' => array( 'integrated_edit_field10' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c8' => array( 'cols' => array( 1 ),
'rows' => array( 3 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field5' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c9' => array( 'cols' => array( 0 ),
'rows' => array( 4 ),
'tags' => array( 'cartridge_storage_fieldblock' ),
'items' => array( 'integrated_edit_field11' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c10' => array( 'cols' => array( 1 ),
'rows' => array( 4 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field6' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c11' => array( 'cols' => array( 0 ),
'rows' => array( 5 ),
'tags' => array( 'cartridge_status_fieldblock' ),
'items' => array( 'integrated_edit_field12' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c12' => array( 'cols' => array( 1 ),
'rows' => array( 5 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field7' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c13' => array( 'cols' => array( 0 ),
'rows' => array( 6 ),
'tags' => array( 'cartridge_desc_fieldblock' ),
'items' => array( 'integrated_edit_field13' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c14' => array( 'cols' => array( 1 ),
'rows' => array( 6 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field3' ),
'fixedAtServer' => true,
'fixedAtClient' => false ) ),
'width' => 2,
'height' => 7 ) ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'view',
'breadcrumb' => false,
'nextPrev' => true ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'view',
'type' => 'view',
'layoutId' => 'nomenu',
'disabled' => 0,
'default' => 0,
'forms' => array( 'top' => array( 'modelId' => 'view-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'view_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'view-above-grid',
'grid' => array(  ),
'cells' => array(  ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'view-below-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'view_back_list',
'view_close' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'prev',
'next',
'hamburger' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'simple-edit',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c4' ),
array( 'cell' => 'c2' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c' ),
array( 'cell' => 'c3' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c5' ),
array( 'cell' => 'c6' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c7' ),
array( 'cell' => 'c8' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c9' ),
array( 'cell' => 'c10' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c11' ),
array( 'cell' => 'c12' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c13' ),
array( 'cell' => 'c14' ) ),
'section' => '' ) ),
'cells' => array( 'c4' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field8' ),
'field' => 'cartridge_inv' ),
'c2' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field4' ),
'field' => 'cartridge_inv' ),
'c' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field1' ),
'field' => 'cartridge_type' ),
'c3' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field' ),
'field' => 'cartridge_type' ),
'c5' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field9' ),
'field' => 'cartridge_name' ),
'c6' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field2' ),
'field' => 'cartridge_name' ),
'c7' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field10' ),
'field' => 'cartridge_inprinterid' ),
'c8' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field5' ),
'field' => 'cartridge_inprinterid' ),
'c9' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field11' ),
'field' => 'cartridge_storage' ),
'c10' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field6' ),
'field' => 'cartridge_storage' ),
'c11' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field12' ),
'field' => 'cartridge_status' ),
'c12' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field7' ),
'field' => 'cartridge_status' ),
'c13' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field13' ),
'field' => 'cartridge_desc' ),
'c14' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field3' ),
'field' => 'cartridge_desc' ) ),
'deferredItems' => array(  ),
'columnCount' => 1,
'inlineLabels' => true,
'separateLabels' => true ) ),
'items' => array( 'view_header' => array( 'type' => 'view_header' ),
'view_back_list' => array( 'type' => 'view_back_list' ),
'view_close' => array( 'type' => 'view_close' ),
'hamburger' => array( 'type' => 'hamburger',
'items' => array( 'view_edit' ) ),
'view_edit' => array( 'type' => 'view_edit' ),
'integrated_edit_field' => array( 'field' => 'cartridge_type',
'type' => 'edit_field' ),
'integrated_edit_field1' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_type' ),
'integrated_edit_field2' => array( 'field' => 'cartridge_name',
'type' => 'edit_field' ),
'integrated_edit_field3' => array( 'field' => 'cartridge_desc',
'type' => 'edit_field' ),
'integrated_edit_field4' => array( 'field' => 'cartridge_inv',
'type' => 'edit_field' ),
'integrated_edit_field5' => array( 'field' => 'cartridge_inprinterid',
'type' => 'edit_field' ),
'integrated_edit_field6' => array( 'field' => 'cartridge_storage',
'type' => 'edit_field' ),
'integrated_edit_field7' => array( 'field' => 'cartridge_status',
'type' => 'edit_field' ),
'integrated_edit_field8' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_inv' ),
'integrated_edit_field9' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_name' ),
'integrated_edit_field10' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_inprinterid' ),
'integrated_edit_field11' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_storage' ),
'integrated_edit_field12' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_status' ),
'integrated_edit_field13' => array( 'type' => 'edit_field_label',
'field' => 'cartridge_desc' ),
'next' => array( 'type' => 'next' ),
'prev' => array( 'type' => 'prev' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>