<?php
			$optionsArray = array( 'totals' => array( 'docin_id' => array( 'totalsType' => '' ),
'doc_control_dayleft' => array( 'totalsType' => '' ),
'doc_control_expiredstatus' => array( 'totalsType' => '' ),
'doc_num' => array( 'totalsType' => '' ),
'doc_date' => array( 'totalsType' => '' ),
'doc_type' => array( 'totalsType' => '' ),
'doc_source' => array( 'totalsType' => '' ),
'doc_outnum' => array( 'totalsType' => '' ),
'doc_outdate' => array( 'totalsType' => '' ),
'doc_destination' => array( 'totalsType' => '' ),
'doc_name' => array( 'totalsType' => '' ),
'doc_desc' => array( 'totalsType' => '' ),
'doc_resolution' => array( 'totalsType' => '' ),
'doc_answer' => array( 'totalsType' => '' ),
'doc_control_date' => array( 'totalsType' => '' ),
'doc_control_status' => array( 'totalsType' => '' ),
'doc_control_executiondate' => array( 'totalsType' => '' ),
'doc_control_executionmark' => array( 'totalsType' => '' ),
'doc_control_executor' => array( 'totalsType' => '' ),
'doc_control_executordate' => array( 'totalsType' => '' ),
'doc_file' => array( 'totalsType' => '' ),
'doc_destination2' => array( 'totalsType' => '' ),
'doc_destination3' => array( 'totalsType' => '' ),
'doc_destination4' => array( 'totalsType' => '' ) ),
'fields' => array( 'gridFields' => array( 'doc_num',
'doc_date',
'doc_type',
'doc_source',
'doc_outnum',
'doc_outdate',
'doc_destination',
'doc_name',
'doc_desc',
'doc_resolution',
'doc_answer',
'doc_control_date',
'doc_control_status',
'doc_control_executiondate',
'doc_control_executionmark',
'doc_control_executor',
'doc_control_executordate',
'doc_file',
'doc_destination2',
'doc_destination3',
'doc_destination4',
'doc_control_dayleft',
'doc_control_expiredstatus' ),
'exportFields' => array( 'doc_num',
'doc_date',
'doc_type',
'doc_source',
'doc_outnum',
'doc_outdate',
'doc_destination',
'doc_name',
'doc_desc',
'doc_resolution',
'doc_answer',
'doc_control_date',
'doc_control_status',
'doc_control_executiondate',
'doc_control_executionmark',
'doc_control_executor',
'doc_control_executordate',
'doc_file',
'doc_destination2',
'doc_destination3',
'doc_destination4',
'doc_control_dayleft',
'doc_control_expiredstatus' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'doc_num' => array( 'export_field1' ),
'doc_date' => array( 'export_field2' ),
'doc_type' => array( 'export_field3' ),
'doc_source' => array( 'export_field4' ),
'doc_outnum' => array( 'export_field5' ),
'doc_outdate' => array( 'export_field6' ),
'doc_destination' => array( 'export_field7' ),
'doc_name' => array( 'export_field8' ),
'doc_desc' => array( 'export_field9' ),
'doc_resolution' => array( 'export_field10' ),
'doc_answer' => array( 'export_field11' ),
'doc_control_date' => array( 'export_field12' ),
'doc_control_status' => array( 'export_field13' ),
'doc_control_executiondate' => array( 'export_field14' ),
'doc_control_executionmark' => array( 'export_field15' ),
'doc_control_executor' => array( 'export_field16' ),
'doc_control_executordate' => array( 'export_field17' ),
'doc_file' => array( 'export_field18' ),
'doc_destination2' => array( 'export_field' ),
'doc_destination3' => array( 'export_field19' ),
'doc_destination4' => array( 'export_field20' ),
'doc_control_dayleft' => array( 'export_field21' ),
'doc_control_expiredstatus' => array( 'export_field22' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'export_header' ),
'grid' => array( 'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field',
'export_field19',
'export_field20',
'export_field21',
'export_field22' ),
'footer' => array( 'export_export',
'export_cancel' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'export_header' => 'top',
'export_field1' => 'grid',
'export_field2' => 'grid',
'export_field3' => 'grid',
'export_field4' => 'grid',
'export_field5' => 'grid',
'export_field6' => 'grid',
'export_field7' => 'grid',
'export_field8' => 'grid',
'export_field9' => 'grid',
'export_field10' => 'grid',
'export_field11' => 'grid',
'export_field12' => 'grid',
'export_field13' => 'grid',
'export_field14' => 'grid',
'export_field15' => 'grid',
'export_field16' => 'grid',
'export_field17' => 'grid',
'export_field18' => 'grid',
'export_field' => 'grid',
'export_field19' => 'grid',
'export_field20' => 'grid',
'export_field21' => 'grid',
'export_field22' => 'grid',
'export_export' => 'footer',
'export_cancel' => 'footer' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'export_header' => array( 'export_header' ),
'export_export' => array( 'export_export' ),
'export_cancel' => array( 'export_cancel' ),
'export_field' => array( 'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field',
'export_field19',
'export_field20',
'export_field21',
'export_field22' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'export' => array( 'format' => 2,
'selectFields' => false,
'delimiter' => ',',
'selectDelimiter' => false,
'exportFileTypes' => array( 'excel' => true,
'word' => true,
'csv' => true,
'xml' => false ) ) );
			$pageArray = array( 'id' => 'export',
'type' => 'export',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'export-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'export-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field',
'export_field19',
'export_field20',
'export_field21',
'export_field22' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'footer' => array( 'modelId' => 'export-footer',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'export_export',
'export_cancel' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'export_header' => array( 'type' => 'export_header' ),
'export_export' => array( 'type' => 'export_export' ),
'export_cancel' => array( 'type' => 'export_cancel' ),
'export_field1' => array( 'field' => 'doc_num',
'type' => 'export_field' ),
'export_field2' => array( 'field' => 'doc_date',
'type' => 'export_field' ),
'export_field3' => array( 'field' => 'doc_type',
'type' => 'export_field' ),
'export_field4' => array( 'field' => 'doc_source',
'type' => 'export_field' ),
'export_field5' => array( 'field' => 'doc_outnum',
'type' => 'export_field' ),
'export_field6' => array( 'field' => 'doc_outdate',
'type' => 'export_field' ),
'export_field7' => array( 'field' => 'doc_destination',
'type' => 'export_field' ),
'export_field8' => array( 'field' => 'doc_name',
'type' => 'export_field' ),
'export_field9' => array( 'field' => 'doc_desc',
'type' => 'export_field' ),
'export_field10' => array( 'field' => 'doc_resolution',
'type' => 'export_field' ),
'export_field11' => array( 'field' => 'doc_answer',
'type' => 'export_field' ),
'export_field12' => array( 'field' => 'doc_control_date',
'type' => 'export_field' ),
'export_field13' => array( 'field' => 'doc_control_status',
'type' => 'export_field' ),
'export_field14' => array( 'field' => 'doc_control_executiondate',
'type' => 'export_field' ),
'export_field15' => array( 'field' => 'doc_control_executionmark',
'type' => 'export_field' ),
'export_field16' => array( 'field' => 'doc_control_executor',
'type' => 'export_field' ),
'export_field17' => array( 'field' => 'doc_control_executordate',
'type' => 'export_field' ),
'export_field18' => array( 'field' => 'doc_file',
'type' => 'export_field' ),
'export_field' => array( 'field' => 'doc_destination2',
'type' => 'export_field' ),
'export_field19' => array( 'field' => 'doc_destination3',
'type' => 'export_field' ),
'export_field20' => array( 'field' => 'doc_destination4',
'type' => 'export_field' ),
'export_field21' => array( 'field' => 'doc_control_dayleft',
'type' => 'export_field' ),
'export_field22' => array( 'field' => 'doc_control_expiredstatus',
'type' => 'export_field' ) ),
'dbProps' => array(  ),
'version' => 4,
'exportFormat' => 2,
'exportDelimiter' => ',',
'exportSelectDelimiter' => false,
'exportSelectFields' => false );
		?>