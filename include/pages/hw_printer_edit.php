<?php
			$optionsArray = array( 'details' => array( 'public.cons_cartridge' => array( 'displayPreview' => 2,
'previewPageId' => '' ) ),
'master' => array( 'public.arm' => array( 'preview' => false ) ),
'captcha' => array( 'captcha' => false ),
'fields' => array( 'gridFields' => array( 'printer_name',
'printer_network',
'printer_ip',
'printer_cartridge',
'printer_inv' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'updateOnEditFields' => array(  ),
'fieldItems' => array( 'printer_name' => array( 'integrated_edit_field',
'integrated_edit_field5',
'integrated_edit_field6' ),
'printer_network' => array( 'integrated_edit_field1',
'integrated_edit_field7',
'integrated_edit_field8' ),
'printer_ip' => array( 'integrated_edit_field2',
'integrated_edit_field9',
'integrated_edit_field10' ),
'printer_cartridge' => array( 'integrated_edit_field3',
'integrated_edit_field11',
'integrated_edit_field12' ),
'printer_inv' => array( 'integrated_edit_field4',
'integrated_edit_field13',
'integrated_edit_field14' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => true,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'top' => array( 'edit_header' ),
'above-grid' => array( 'edit_message' ),
'below-grid' => array( 'edit_save',
'edit_back_list',
'edit_close',
'prev',
'next',
'hamburger' ),
'grid' => array( 'integrated_edit_field5',
'integrated_edit_field',
'integrated_edit_field6',
'integrated_edit_field7',
'integrated_edit_field1',
'integrated_edit_field8',
'integrated_edit_field9',
'integrated_edit_field2',
'integrated_edit_field10',
'integrated_edit_field11',
'integrated_edit_field3',
'integrated_edit_field12',
'integrated_edit_field13',
'integrated_edit_field4',
'integrated_edit_field14' ) ),
'formXtTags' => array( 'above-grid' => array( 'message_block' ) ),
'itemForms' => array( 'edit_header' => 'top',
'edit_message' => 'above-grid',
'edit_save' => 'below-grid',
'edit_back_list' => 'below-grid',
'edit_close' => 'below-grid',
'prev' => 'below-grid',
'next' => 'below-grid',
'hamburger' => 'below-grid',
'integrated_edit_field5' => 'grid',
'integrated_edit_field' => 'grid',
'integrated_edit_field6' => 'grid',
'integrated_edit_field7' => 'grid',
'integrated_edit_field1' => 'grid',
'integrated_edit_field8' => 'grid',
'integrated_edit_field9' => 'grid',
'integrated_edit_field2' => 'grid',
'integrated_edit_field10' => 'grid',
'integrated_edit_field11' => 'grid',
'integrated_edit_field3' => 'grid',
'integrated_edit_field12' => 'grid',
'integrated_edit_field13' => 'grid',
'integrated_edit_field4' => 'grid',
'integrated_edit_field14' => 'grid' ),
'itemLocations' => array( 'integrated_edit_field5' => array( 'location' => 'grid',
'cellId' => 'c4' ),
'integrated_edit_field' => array( 'location' => 'grid',
'cellId' => 'c2' ),
'integrated_edit_field6' => array( 'location' => 'grid',
'cellId' => 'c' ),
'integrated_edit_field7' => array( 'location' => 'grid',
'cellId' => 'c5' ),
'integrated_edit_field1' => array( 'location' => 'grid',
'cellId' => 'c3' ),
'integrated_edit_field8' => array( 'location' => 'grid',
'cellId' => 'c6' ),
'integrated_edit_field9' => array( 'location' => 'grid',
'cellId' => 'c7' ),
'integrated_edit_field2' => array( 'location' => 'grid',
'cellId' => 'c8' ),
'integrated_edit_field10' => array( 'location' => 'grid',
'cellId' => 'c9' ),
'integrated_edit_field11' => array( 'location' => 'grid',
'cellId' => 'c10' ),
'integrated_edit_field3' => array( 'location' => 'grid',
'cellId' => 'c11' ),
'integrated_edit_field12' => array( 'location' => 'grid',
'cellId' => 'c12' ),
'integrated_edit_field13' => array( 'location' => 'grid',
'cellId' => 'c13' ),
'integrated_edit_field4' => array( 'location' => 'grid',
'cellId' => 'c14' ),
'integrated_edit_field14' => array( 'location' => 'grid',
'cellId' => 'c15' ) ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'edit_header' => array( 'edit_header' ),
'hamburger' => array( 'hamburger' ),
'edit_reset' => array( 'edit_reset' ),
'edit_message' => array( 'edit_message' ),
'edit_save' => array( 'edit_save' ),
'edit_back_list' => array( 'edit_back_list' ),
'edit_close' => array( 'edit_close' ),
'edit_view' => array( 'edit_view' ),
'edit_field' => array( 'integrated_edit_field',
'integrated_edit_field1',
'integrated_edit_field2',
'integrated_edit_field3',
'integrated_edit_field4' ),
'next' => array( 'next' ),
'prev' => array( 'prev' ),
'edit_field_label' => array( 'integrated_edit_field5',
'integrated_edit_field7',
'integrated_edit_field9',
'integrated_edit_field11',
'integrated_edit_field13' ),
'edit_field_tooltip' => array( 'integrated_edit_field6',
'integrated_edit_field8',
'integrated_edit_field10',
'integrated_edit_field12',
'integrated_edit_field14' ) ),
'cellMaps' => array( 'grid' => array( 'cells' => array( 'c4' => array( 'cols' => array( 0 ),
'rows' => array( 0 ),
'tags' => array( 'printer_name_fieldblock' ),
'items' => array( 'integrated_edit_field5' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c2' => array( 'cols' => array( 1 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c' => array( 'cols' => array( 2 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field6' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c5' => array( 'cols' => array( 0 ),
'rows' => array( 1 ),
'tags' => array( 'printer_network_fieldblock' ),
'items' => array( 'integrated_edit_field7' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c3' => array( 'cols' => array( 1 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field1' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c6' => array( 'cols' => array( 2 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field8' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c7' => array( 'cols' => array( 0 ),
'rows' => array( 2 ),
'tags' => array( 'printer_ip_fieldblock' ),
'items' => array( 'integrated_edit_field9' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c8' => array( 'cols' => array( 1 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field2' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c9' => array( 'cols' => array( 2 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field10' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c10' => array( 'cols' => array( 0 ),
'rows' => array( 3 ),
'tags' => array( 'printer_cartridge_fieldblock' ),
'items' => array( 'integrated_edit_field11' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c11' => array( 'cols' => array( 1 ),
'rows' => array( 3 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field3' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c12' => array( 'cols' => array( 2 ),
'rows' => array( 3 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field12' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c13' => array( 'cols' => array( 0 ),
'rows' => array( 4 ),
'tags' => array( 'printer_inv_fieldblock' ),
'items' => array( 'integrated_edit_field13' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c14' => array( 'cols' => array( 1 ),
'rows' => array( 4 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field4' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c15' => array( 'cols' => array( 2 ),
'rows' => array( 4 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field14' ),
'fixedAtServer' => true,
'fixedAtClient' => false ) ),
'width' => 3,
'height' => 5 ) ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'edit',
'breadcrumb' => false,
'nextPrev' => true ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'edit' => array( 'updateSelected' => false ) );
			$pageArray = array( 'id' => 'edit',
'type' => 'edit',
'layoutId' => 'nomenu',
'disabled' => 0,
'default' => 0,
'forms' => array( 'top' => array( 'modelId' => 'edit-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'edit-above-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_message' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'edit-below-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_save',
'edit_back_list',
'edit_close' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'prev',
'next',
'hamburger' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'simple-edit',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c4' ),
array( 'cell' => 'c2' ),
array( 'cell' => 'c' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c5' ),
array( 'cell' => 'c3' ),
array( 'cell' => 'c6' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c7' ),
array( 'cell' => 'c8' ),
array( 'cell' => 'c9' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c10' ),
array( 'cell' => 'c11' ),
array( 'cell' => 'c12' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c13' ),
array( 'cell' => 'c14' ),
array( 'cell' => 'c15' ) ),
'section' => '' ) ),
'cells' => array( 'c4' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field5' ),
'field' => 'printer_name' ),
'c2' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field' ),
'field' => 'printer_name' ),
'c' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field6' ),
'field' => 'printer_name' ),
'c5' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field7' ),
'field' => 'printer_network' ),
'c3' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field1' ),
'field' => 'printer_network' ),
'c6' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field8' ),
'field' => 'printer_network' ),
'c7' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field9' ),
'field' => 'printer_ip' ),
'c8' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field2' ),
'field' => 'printer_ip' ),
'c9' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field10' ),
'field' => 'printer_ip' ),
'c10' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field11' ),
'field' => 'printer_cartridge' ),
'c11' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field3' ),
'field' => 'printer_cartridge' ),
'c12' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field12' ),
'field' => 'printer_cartridge' ),
'c13' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field13' ),
'field' => 'printer_inv' ),
'c14' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field4' ),
'field' => 'printer_inv' ),
'c15' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field14' ),
'field' => 'printer_inv' ) ),
'deferredItems' => array(  ),
'columnCount' => 1,
'inlineLabels' => true,
'separateLabels' => true ) ),
'items' => array( 'edit_header' => array( 'type' => 'edit_header' ),
'hamburger' => array( 'type' => 'hamburger',
'items' => array( 'edit_reset',
'edit_view' ) ),
'edit_reset' => array( 'type' => 'edit_reset' ),
'edit_message' => array( 'type' => 'edit_message' ),
'edit_save' => array( 'type' => 'edit_save' ),
'edit_back_list' => array( 'type' => 'edit_back_list' ),
'edit_close' => array( 'type' => 'edit_close' ),
'edit_view' => array( 'type' => 'edit_view' ),
'integrated_edit_field' => array( 'field' => 'printer_name',
'type' => 'edit_field' ),
'integrated_edit_field1' => array( 'field' => 'printer_network',
'type' => 'edit_field',
'updateOnEdit' => false ),
'integrated_edit_field2' => array( 'field' => 'printer_ip',
'type' => 'edit_field' ),
'integrated_edit_field3' => array( 'field' => 'printer_cartridge',
'type' => 'edit_field' ),
'integrated_edit_field4' => array( 'field' => 'printer_inv',
'type' => 'edit_field',
'updateOnEdit' => false ),
'next' => array( 'type' => 'next' ),
'prev' => array( 'type' => 'prev' ),
'integrated_edit_field5' => array( 'type' => 'edit_field_label',
'field' => 'printer_name' ),
'integrated_edit_field6' => array( 'type' => 'edit_field_tooltip',
'field' => 'printer_name' ),
'integrated_edit_field7' => array( 'type' => 'edit_field_label',
'field' => 'printer_network' ),
'integrated_edit_field8' => array( 'type' => 'edit_field_tooltip',
'field' => 'printer_network' ),
'integrated_edit_field9' => array( 'type' => 'edit_field_label',
'field' => 'printer_ip' ),
'integrated_edit_field10' => array( 'type' => 'edit_field_tooltip',
'field' => 'printer_ip' ),
'integrated_edit_field11' => array( 'type' => 'edit_field_label',
'field' => 'printer_cartridge' ),
'integrated_edit_field12' => array( 'type' => 'edit_field_tooltip',
'field' => 'printer_cartridge' ),
'integrated_edit_field13' => array( 'type' => 'edit_field_label',
'field' => 'printer_inv' ),
'integrated_edit_field14' => array( 'type' => 'edit_field_tooltip',
'field' => 'printer_inv' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>