<?php
			$optionsArray = array( 'details' => array( 'public.spr_socket' => array( 'displayPreview' => 2,
'previewPageId' => '' ) ),
'master' => array( 'public.spr_location' => array( 'preview' => false ) ),
'captcha' => array( 'captcha' => false ),
'fields' => array( 'gridFields' => array( 'sw_name',
'sw_ip',
'sw_location' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'updateOnEditFields' => array(  ),
'fieldItems' => array( 'sw_name' => array( 'integrated_edit_field',
'integrated_edit_field3',
'integrated_edit_field4' ),
'sw_ip' => array( 'integrated_edit_field1',
'integrated_edit_field5',
'integrated_edit_field6' ),
'sw_location' => array( 'integrated_edit_field2',
'integrated_edit_field7',
'integrated_edit_field8' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => true,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'top' => array( 'edit_header' ),
'above-grid' => array( 'edit_message' ),
'below-grid' => array( 'edit_save',
'edit_back_list',
'edit_close',
'hamburger' ),
'grid' => array( 'integrated_edit_field3',
'integrated_edit_field',
'integrated_edit_field4',
'integrated_edit_field5',
'integrated_edit_field1',
'integrated_edit_field6',
'integrated_edit_field7',
'integrated_edit_field2',
'integrated_edit_field8' ) ),
'formXtTags' => array( 'above-grid' => array( 'message_block' ) ),
'itemForms' => array( 'edit_header' => 'top',
'edit_message' => 'above-grid',
'edit_save' => 'below-grid',
'edit_back_list' => 'below-grid',
'edit_close' => 'below-grid',
'hamburger' => 'below-grid',
'integrated_edit_field3' => 'grid',
'integrated_edit_field' => 'grid',
'integrated_edit_field4' => 'grid',
'integrated_edit_field5' => 'grid',
'integrated_edit_field1' => 'grid',
'integrated_edit_field6' => 'grid',
'integrated_edit_field7' => 'grid',
'integrated_edit_field2' => 'grid',
'integrated_edit_field8' => 'grid' ),
'itemLocations' => array( 'integrated_edit_field3' => array( 'location' => 'grid',
'cellId' => 'c4' ),
'integrated_edit_field' => array( 'location' => 'grid',
'cellId' => 'c2' ),
'integrated_edit_field4' => array( 'location' => 'grid',
'cellId' => 'c' ),
'integrated_edit_field5' => array( 'location' => 'grid',
'cellId' => 'c5' ),
'integrated_edit_field1' => array( 'location' => 'grid',
'cellId' => 'c3' ),
'integrated_edit_field6' => array( 'location' => 'grid',
'cellId' => 'c6' ),
'integrated_edit_field7' => array( 'location' => 'grid',
'cellId' => 'c7' ),
'integrated_edit_field2' => array( 'location' => 'grid',
'cellId' => 'c8' ),
'integrated_edit_field8' => array( 'location' => 'grid',
'cellId' => 'c9' ) ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'edit_header' => array( 'edit_header' ),
'hamburger' => array( 'hamburger' ),
'edit_reset' => array( 'edit_reset' ),
'edit_message' => array( 'edit_message' ),
'edit_save' => array( 'edit_save' ),
'edit_back_list' => array( 'edit_back_list' ),
'edit_close' => array( 'edit_close' ),
'edit_view' => array( 'edit_view' ),
'edit_field' => array( 'integrated_edit_field',
'integrated_edit_field1',
'integrated_edit_field2' ),
'edit_field_label' => array( 'integrated_edit_field3',
'integrated_edit_field5',
'integrated_edit_field7' ),
'edit_field_tooltip' => array( 'integrated_edit_field4',
'integrated_edit_field6',
'integrated_edit_field8' ) ),
'cellMaps' => array( 'grid' => array( 'cells' => array( 'c4' => array( 'cols' => array( 0 ),
'rows' => array( 0 ),
'tags' => array( 'sw_name_fieldblock' ),
'items' => array( 'integrated_edit_field3' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c2' => array( 'cols' => array( 1 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c' => array( 'cols' => array( 2 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field4' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c5' => array( 'cols' => array( 0 ),
'rows' => array( 1 ),
'tags' => array( 'sw_ip_fieldblock' ),
'items' => array( 'integrated_edit_field5' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c3' => array( 'cols' => array( 1 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field1' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c6' => array( 'cols' => array( 2 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field6' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c7' => array( 'cols' => array( 0 ),
'rows' => array( 2 ),
'tags' => array( 'sw_location_fieldblock' ),
'items' => array( 'integrated_edit_field7' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c8' => array( 'cols' => array( 1 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field2' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c9' => array( 'cols' => array( 2 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field8' ),
'fixedAtServer' => true,
'fixedAtClient' => false ) ),
'width' => 3,
'height' => 3 ) ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'edit',
'breadcrumb' => false,
'nextPrev' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'edit' => array( 'updateSelected' => false ) );
			$pageArray = array( 'id' => 'edit',
'type' => 'edit',
'layoutId' => 'nomenu',
'disabled' => 0,
'default' => 0,
'forms' => array( 'top' => array( 'modelId' => 'edit-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'edit-above-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_message' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'edit-below-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'edit_save',
'edit_back_list',
'edit_close' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'hamburger' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'simple-edit',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c4' ),
array( 'cell' => 'c2' ),
array( 'cell' => 'c' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c5' ),
array( 'cell' => 'c3' ),
array( 'cell' => 'c6' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c7' ),
array( 'cell' => 'c8' ),
array( 'cell' => 'c9' ) ),
'section' => '' ) ),
'cells' => array( 'c4' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field3' ),
'field' => 'sw_name' ),
'c2' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field' ),
'field' => 'sw_name' ),
'c' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field4' ),
'field' => 'sw_name' ),
'c5' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field5' ),
'field' => 'sw_ip' ),
'c3' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field1' ),
'field' => 'sw_ip' ),
'c6' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field6' ),
'field' => 'sw_ip' ),
'c7' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field7' ),
'field' => 'sw_location' ),
'c8' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field2' ),
'field' => 'sw_location' ),
'c9' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field8' ),
'field' => 'sw_location' ) ),
'deferredItems' => array(  ),
'columnCount' => 1,
'inlineLabels' => true,
'separateLabels' => true ) ),
'items' => array( 'edit_header' => array( 'type' => 'edit_header' ),
'hamburger' => array( 'type' => 'hamburger',
'items' => array( 'edit_reset',
'edit_view' ) ),
'edit_reset' => array( 'type' => 'edit_reset' ),
'edit_message' => array( 'type' => 'edit_message' ),
'edit_save' => array( 'type' => 'edit_save' ),
'edit_back_list' => array( 'type' => 'edit_back_list' ),
'edit_close' => array( 'type' => 'edit_close' ),
'edit_view' => array( 'type' => 'edit_view' ),
'integrated_edit_field' => array( 'field' => 'sw_name',
'type' => 'edit_field' ),
'integrated_edit_field1' => array( 'field' => 'sw_ip',
'type' => 'edit_field' ),
'integrated_edit_field2' => array( 'field' => 'sw_location',
'type' => 'edit_field',
'updateOnEdit' => false ),
'integrated_edit_field3' => array( 'type' => 'edit_field_label',
'field' => 'sw_name' ),
'integrated_edit_field4' => array( 'type' => 'edit_field_tooltip',
'field' => 'sw_name' ),
'integrated_edit_field5' => array( 'type' => 'edit_field_label',
'field' => 'sw_ip' ),
'integrated_edit_field6' => array( 'type' => 'edit_field_tooltip',
'field' => 'sw_ip' ),
'integrated_edit_field7' => array( 'type' => 'edit_field_label',
'field' => 'sw_location' ),
'integrated_edit_field8' => array( 'type' => 'edit_field_tooltip',
'field' => 'sw_location' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>