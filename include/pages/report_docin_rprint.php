<?php
			$optionsArray = array( 'pdf' => array( 'pdfView' => false ),
'master' => array( 'public.spr_partners' => array( 'preview' => false ),
'public.doc_out' => array( 'preview' => false ) ),
'fields' => array( 'gridFields' => array( 'type_name',
'doc_num',
'doc_source',
'doc_outnum',
'doc_outdate',
'doc_name',
'doc_desc',
'doc_resolution',
'doc_answer',
'doc_control_status',
'doc_control_executiondate',
'doc_control_executionmark',
'doc_control_executordate',
'doc_file' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'type_name' => array( 'report_grid_field' ),
'doc_num' => array( 'report_grid_field1' ),
'doc_source' => array( 'report_grid_field4' ),
'doc_outnum' => array( 'report_grid_field5' ),
'doc_outdate' => array( 'report_grid_field6' ),
'doc_name' => array( 'report_grid_field8' ),
'doc_desc' => array( 'report_grid_field9' ),
'doc_resolution' => array( 'report_grid_field10' ),
'doc_answer' => array( 'report_grid_field11' ),
'doc_control_status' => array( 'report_grid_field13' ),
'doc_control_executiondate' => array( 'report_grid_field14' ),
'doc_control_executionmark' => array( 'report_grid_field15' ),
'doc_control_executordate' => array( 'report_grid_field17' ),
'doc_file' => array( 'report_grid_field18' ),
'doc_control_date' => array( 'report_group_field' ),
'executor' => array( 'report_group_field1' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'top' => array( 'print_header',
'print_subheader' ),
'above-grid' => array( 'print_pages' ),
'below-grid' => array(  ),
'grid' => array( 'report_group_field',
'report_group_field1',
'report_grid_field1',
'report_grid_field4',
'report_grid_field5',
'report_grid_field6',
'report_grid_field8',
'report_grid_field9',
'report_grid_field10',
'report_grid_field11',
'report_grid_field13',
'report_grid_field14',
'report_grid_field15',
'report_grid_field17',
'report_grid_field18',
'report_grid_field' ) ),
'formXtTags' => array( 'above-grid' => array( 'print_pages' ),
'below-grid' => array(  ) ),
'itemForms' => array( 'print_header' => 'top',
'print_subheader' => 'top',
'print_pages' => 'above-grid',
'report_group_field' => 'grid',
'report_group_field1' => 'grid',
'report_grid_field1' => 'grid',
'report_grid_field4' => 'grid',
'report_grid_field5' => 'grid',
'report_grid_field6' => 'grid',
'report_grid_field8' => 'grid',
'report_grid_field9' => 'grid',
'report_grid_field10' => 'grid',
'report_grid_field11' => 'grid',
'report_grid_field13' => 'grid',
'report_grid_field14' => 'grid',
'report_grid_field15' => 'grid',
'report_grid_field17' => 'grid',
'report_grid_field18' => 'grid',
'report_grid_field' => 'grid' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'print_header' => array( 'print_header' ),
'print_subheader' => array( 'print_subheader' ),
'print_pages' => array( 'print_pages' ),
'report_grid_field' => array( 'report_grid_field',
'report_grid_field1',
'report_grid_field4',
'report_grid_field5',
'report_grid_field6',
'report_grid_field8',
'report_grid_field9',
'report_grid_field10',
'report_grid_field11',
'report_grid_field13',
'report_grid_field14',
'report_grid_field15',
'report_grid_field17',
'report_grid_field18' ),
'report_group_field' => array( 'report_group_field',
'report_group_field1' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array( 'print_pages' => array( 'tag' => 'PRINT_PAGES',
'type' => 2 ) ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'rprint',
'breadcrumb' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'rprint',
'type' => 'rprint',
'layoutId' => 'basic',
'disabled' => 0,
'default' => 0,
'forms' => array( 'top' => array( 'modelId' => 'print-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c2' => array( 'model' => 'c2',
'items' => array( 'print_header',
'print_subheader' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'print-above-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'print_pages' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'print-below-grid',
'grid' => array(  ),
'cells' => array(  ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'report-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'report_group_field',
'report_group_field1' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'report_grid_field1',
'report_grid_field4',
'report_grid_field5',
'report_grid_field6',
'report_grid_field8',
'report_grid_field9',
'report_grid_field10',
'report_grid_field11',
'report_grid_field13',
'report_grid_field14',
'report_grid_field15',
'report_grid_field17',
'report_grid_field18',
'report_grid_field' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'print_header' => array( 'type' => 'print_header' ),
'print_subheader' => array( 'type' => 'print_subheader' ),
'print_pages' => array( 'type' => 'print_pages' ),
'report_grid_field' => array( 'field' => 'type_name',
'type' => 'report_grid_field' ),
'report_grid_field1' => array( 'field' => 'doc_num',
'type' => 'report_grid_field' ),
'report_grid_field4' => array( 'field' => 'doc_source',
'type' => 'report_grid_field' ),
'report_grid_field5' => array( 'field' => 'doc_outnum',
'type' => 'report_grid_field' ),
'report_grid_field6' => array( 'field' => 'doc_outdate',
'type' => 'report_grid_field' ),
'report_grid_field8' => array( 'field' => 'doc_name',
'type' => 'report_grid_field' ),
'report_grid_field9' => array( 'field' => 'doc_desc',
'type' => 'report_grid_field' ),
'report_grid_field10' => array( 'field' => 'doc_resolution',
'type' => 'report_grid_field' ),
'report_grid_field11' => array( 'field' => 'doc_answer',
'type' => 'report_grid_field' ),
'report_grid_field13' => array( 'field' => 'doc_control_status',
'type' => 'report_grid_field' ),
'report_grid_field14' => array( 'field' => 'doc_control_executiondate',
'type' => 'report_grid_field' ),
'report_grid_field15' => array( 'field' => 'doc_control_executionmark',
'type' => 'report_grid_field' ),
'report_grid_field17' => array( 'field' => 'doc_control_executordate',
'type' => 'report_grid_field' ),
'report_grid_field18' => array( 'field' => 'doc_file',
'type' => 'report_grid_field' ),
'report_group_field' => array( 'field' => 'doc_control_date',
'type' => 'report_group_field' ),
'report_group_field1' => array( 'field' => 'executor',
'type' => 'report_group_field' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>