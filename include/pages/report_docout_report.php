<?php
			$optionsArray = array( 'pdf' => array( 'pdfView' => false ),
'master' => array( 'public.doc_in' => array( 'preview' => true ) ),
'listSearch' => array( 'alwaysOnPanelFields' => array(  ),
'searchPanel' => true,
'fixedSearchPanel' => false,
'simpleSearchOptions' => false,
'searchSaving' => false ),
'fields' => array( 'gridFields' => array( 'type_name',
'doc_num',
'doc_date',
'doc_answer',
'partner_name',
'doc_name',
'doc_desc',
'doc_control_status',
'doc_control_executiondate',
'doc_file' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array( 'doc_answer',
'doc_control_date',
'sotrudnik_fio',
'partner_name',
'type_name',
'doc_num',
'doc_name',
'doc_file',
'doc_desc',
'doc_date',
'doc_control_status',
'doc_control_executiondate' ),
'filterFields' => array(  ),
'fieldItems' => array( 'type_name' => array( 'report_grid_field' ),
'doc_num' => array( 'report_grid_field1' ),
'doc_date' => array( 'report_grid_field2' ),
'doc_answer' => array( 'report_grid_field3' ),
'partner_name' => array( 'report_grid_field4' ),
'doc_name' => array( 'report_grid_field7' ),
'doc_desc' => array( 'report_grid_field8' ),
'doc_control_status' => array( 'report_grid_field10' ),
'doc_control_executiondate' => array( 'report_grid_field11' ),
'doc_file' => array( 'report_grid_field12' ),
'doc_control_date' => array( 'report_group_field' ),
'sotrudnik_fio' => array( 'report_group_field1' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'left' => array( 'logo1',
'expand_button',
'menu',
'search_panel' ),
'supertop' => array( 'collapse_button',
'breadcrumb',
'simple_search',
'list_options',
'loginform_login',
'username_button' ),
'grid' => array( 'report_group_field',
'report_group_field1',
'report_grid_field1',
'report_grid_field2',
'report_grid_field3',
'report_grid_field7',
'report_grid_field8',
'report_grid_field10',
'report_grid_field11',
'report_grid_field12',
'report_grid_field',
'report_grid_field4' ),
'top' => array( 'master_info' ),
'above-grid' => array( 'details_preview',
'grid_alldetails_link',
'grid_details_link',
'details_found',
'page_size',
'print_panel' ),
'below-grid' => array( 'pagination' ) ),
'formXtTags' => array( 'top' => array( 'mastertable_block' ),
'below-grid' => array( 'pagination' ) ),
'itemForms' => array( 'logo1' => 'left',
'expand_button' => 'left',
'menu' => 'left',
'search_panel' => 'left',
'collapse_button' => 'supertop',
'breadcrumb' => 'supertop',
'simple_search' => 'supertop',
'list_options' => 'supertop',
'loginform_login' => 'supertop',
'username_button' => 'supertop',
'report_group_field' => 'grid',
'report_group_field1' => 'grid',
'report_grid_field1' => 'grid',
'report_grid_field2' => 'grid',
'report_grid_field3' => 'grid',
'report_grid_field7' => 'grid',
'report_grid_field8' => 'grid',
'report_grid_field10' => 'grid',
'report_grid_field11' => 'grid',
'report_grid_field12' => 'grid',
'report_grid_field' => 'grid',
'report_grid_field4' => 'grid',
'master_info' => 'top',
'details_preview' => 'above-grid',
'grid_alldetails_link' => 'above-grid',
'grid_details_link' => 'above-grid',
'details_found' => 'above-grid',
'page_size' => 'above-grid',
'print_panel' => 'above-grid',
'pagination' => 'below-grid' ),
'itemLocations' => array(  ),
'itemVisiblity' => array( 'expand_button' => 5,
'menu' => 3,
'search_panel' => 5,
'collapse_button' => 5,
'simple_search' => 3,
'list_options' => 3,
'loginform_login' => 3,
'username_button' => 3,
'print_panel' => 5 ) ),
'itemsByType' => array( 'page_size' => array( 'page_size' ),
'details_found' => array( 'details_found' ),
'breadcrumb' => array( 'breadcrumb' ),
'menu' => array( 'menu' ),
'simple_search' => array( 'simple_search' ),
'pagination' => array( 'pagination' ),
'list_options' => array( 'list_options' ),
'export_report_excel' => array( 'export_report_excel' ),
'export_report_word' => array( 'export_report_word' ),
'search_panel' => array( 'search_panel' ),
'show_search_panel' => array( 'show_search_panel' ),
'-' => array( '-',
'-1' ),
'hide_search_panel' => array( 'hide_search_panel' ),
'search_panel_field' => array( 'search_panel_field',
'search_panel_field1',
'search_panel_field2',
'search_panel_field3',
'search_panel_field4',
'search_panel_field5',
'search_panel_field6',
'search_panel_field7',
'search_panel_field8',
'search_panel_field9',
'search_panel_field10',
'search_panel_field11' ),
'username_button' => array( 'username_button' ),
'loginform_login' => array( 'loginform_login' ),
'logout_link' => array( 'logout_link' ),
'adminarea_link' => array( 'adminarea_link' ),
'collapse_button' => array( 'collapse_button' ),
'print_panel' => array( 'print_panel' ),
'print_scope' => array( 'print_scope' ),
'print_button' => array( 'print_button' ),
'print_records' => array( 'print_records' ),
'advsearch_link' => array( 'advsearch_link' ),
'report_grid_field' => array( 'report_grid_field',
'report_grid_field1',
'report_grid_field2',
'report_grid_field3',
'report_grid_field4',
'report_grid_field7',
'report_grid_field8',
'report_grid_field10',
'report_grid_field11',
'report_grid_field12' ),
'master_info' => array( 'master_info' ),
'details_preview' => array( 'details_preview' ),
'grid_alldetails_link' => array( 'grid_alldetails_link' ),
'grid_details_link' => array( 'grid_details_link' ),
'report_group_field' => array( 'report_group_field',
'report_group_field1' ),
'logo' => array( 'logo1' ),
'expand_button' => array( 'expand_button' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 0 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array( 'details_found' => array( 'tag' => 'DISPLAYING',
'type' => 2 ) ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'report',
'breadcrumb' => true ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'report',
'type' => 'report',
'layoutId' => 'leftbar',
'disabled' => 0,
'default' => 0,
'forms' => array( 'left' => array( 'modelId' => 'leftbar-menu',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c0' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c0' => array( 'model' => 'c0',
'items' => array( 'logo1',
'expand_button' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c1' => array( 'model' => 'c1',
'items' => array( 'menu',
'search_panel' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'supertop' => array( 'modelId' => 'leftbar-top-chart',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'collapse_button',
'breadcrumb' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'simple_search',
'list_options',
'loginform_login',
'username_button' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'report-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'report_group_field',
'report_group_field1' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'report_grid_field1',
'report_grid_field2',
'report_grid_field3',
'report_grid_field7',
'report_grid_field8',
'report_grid_field10',
'report_grid_field11',
'report_grid_field12',
'report_grid_field',
'report_grid_field4' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'list-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c3' ) ),
'section' => '' ) ),
'cells' => array( 'c3' => array( 'model' => 'c3',
'items' => array( 'master_info' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'report-above-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'details_preview',
'grid_alldetails_link',
'grid_details_link' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ),
'c2' => array( 'model' => 'c2',
'items' => array( 'details_found',
'page_size',
'print_panel' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'report-below-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'pagination' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'page_size' => array( 'type' => 'page_size' ),
'details_found' => array( 'type' => 'details_found' ),
'breadcrumb' => array( 'type' => 'breadcrumb' ),
'menu' => array( 'type' => 'menu' ),
'simple_search' => array( 'type' => 'simple_search' ),
'pagination' => array( 'type' => 'pagination' ),
'list_options' => array( 'type' => 'list_options',
'items' => array( 'advsearch_link',
'show_search_panel',
'hide_search_panel',
'-1',
'export_report_excel',
'export_report_word' ) ),
'export_report_excel' => array( 'type' => 'export_report_excel' ),
'export_report_word' => array( 'type' => 'export_report_word' ),
'search_panel' => array( 'type' => 'search_panel',
'items' => array( 'search_panel_field',
'search_panel_field11',
'search_panel_field7',
'search_panel_field6',
'search_panel_field1',
'search_panel_field10',
'search_panel_field9',
'search_panel_field8',
'search_panel_field5',
'search_panel_field4',
'search_panel_field3',
'search_panel_field2' ) ),
'show_search_panel' => array( 'type' => 'show_search_panel' ),
'-' => array( 'type' => '-' ),
'hide_search_panel' => array( 'type' => 'hide_search_panel' ),
'search_panel_field' => array( 'field' => 'doc_answer',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field1' => array( 'field' => 'type_name',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field2' => array( 'field' => 'doc_control_executiondate',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field3' => array( 'field' => 'doc_control_status',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field4' => array( 'field' => 'doc_date',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field5' => array( 'field' => 'doc_desc',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field6' => array( 'field' => 'partner_name',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field7' => array( 'field' => 'sotrudnik_fio',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field8' => array( 'field' => 'doc_file',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field9' => array( 'field' => 'doc_name',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field10' => array( 'field' => 'doc_num',
'type' => 'search_panel_field',
'required' => false ),
'search_panel_field11' => array( 'field' => 'doc_control_date',
'type' => 'search_panel_field',
'required' => false ),
'username_button' => array( 'type' => 'username_button',
'items' => array( 'logout_link',
'adminarea_link' ) ),
'loginform_login' => array( 'type' => 'loginform_login',
'popup' => false ),
'logout_link' => array( 'type' => 'logout_link' ),
'adminarea_link' => array( 'type' => 'adminarea_link' ),
'collapse_button' => array( 'type' => 'collapse_button' ),
'print_panel' => array( 'type' => 'print_panel',
'items' => array( 'print_scope',
'print_records',
'print_button' ) ),
'print_scope' => array( 'type' => 'print_scope' ),
'print_button' => array( 'type' => 'print_button' ),
'print_records' => array( 'type' => 'print_records' ),
'advsearch_link' => array( 'type' => 'advsearch_link' ),
'-1' => array( 'type' => '-' ),
'report_grid_field' => array( 'field' => 'type_name',
'type' => 'report_grid_field' ),
'report_grid_field1' => array( 'field' => 'doc_num',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field2' => array( 'field' => 'doc_date',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field3' => array( 'field' => 'doc_answer',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field4' => array( 'field' => 'partner_name',
'type' => 'report_grid_field' ),
'report_grid_field7' => array( 'field' => 'doc_name',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field8' => array( 'field' => 'doc_desc',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field10' => array( 'field' => 'doc_control_status',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field11' => array( 'field' => 'doc_control_executiondate',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'report_grid_field12' => array( 'field' => 'doc_file',
'type' => 'report_grid_field',
'inlineAdd' => true,
'inlineEdit' => true ),
'master_info' => array( 'type' => 'master_info',
'tables' => array( 'public.spr_partners' => 'true',
'public.spr_doc_type' => 'true',
'public.doc_in' => 'true',
'public.sotrudnik' => 'true' ) ),
'details_preview' => array( 'type' => 'details_preview',
'table' => 'public.doc_in',
'items' => array(  ),
'popup' => false,
'proceedLink' => true,
'pageId' => 'list' ),
'grid_alldetails_link' => array( 'type' => 'grid_alldetails_link' ),
'grid_details_link' => array( 'type' => 'grid_details_link',
'table' => 'public.doc_in',
'badge' => true,
'hideIfNone' => false,
'showCount' => true ),
'report_group_field' => array( 'field' => 'doc_control_date',
'type' => 'report_group_field' ),
'report_group_field1' => array( 'field' => 'sotrudnik_fio',
'type' => 'report_group_field' ),
'logo1' => array( 'type' => 'logo' ),
'expand_button' => array( 'type' => 'expand_button' ) ),
'dbProps' => array(  ),
'version' => 4,
'pageWidth' => 'full' );
		?>