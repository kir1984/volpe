<?php
			$optionsArray = array( 'fields' => array( 'gridFields' => array( 'ecp_number',
'ecp_owner',
'ecp_ud',
'ecp_date',
'ecp_dateexp_cert',
'ecp_dateexp',
'ecp_desc',
'ecp_status',
'ecp_daysleft' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'ecp_number' => array( 'import_field1' ),
'ecp_owner' => array( 'import_field2' ),
'ecp_ud' => array( 'import_field3' ),
'ecp_date' => array( 'import_field4' ),
'ecp_dateexp_cert' => array( 'import_field5' ),
'ecp_dateexp' => array( 'import_field' ),
'ecp_desc' => array( 'import_field6' ),
'ecp_status' => array( 'import_field7' ),
'ecp_daysleft' => array( 'import_field8' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'import_header' ),
'grid' => array( 'import_field1',
'import_field2',
'import_field3',
'import_field4',
'import_field5',
'import_field',
'import_field6',
'import_field7',
'import_field8' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'import_header' => 'top',
'import_field1' => 'grid',
'import_field2' => 'grid',
'import_field3' => 'grid',
'import_field4' => 'grid',
'import_field5' => 'grid',
'import_field' => 'grid',
'import_field6' => 'grid',
'import_field7' => 'grid',
'import_field8' => 'grid' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'import_header' => array( 'import_header' ),
'import_field' => array( 'import_field1',
'import_field2',
'import_field3',
'import_field4',
'import_field5',
'import_field',
'import_field6',
'import_field7',
'import_field8' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'import',
'type' => 'import',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'import-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'import_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'import-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'import_field1',
'import_field2',
'import_field3',
'import_field4',
'import_field5',
'import_field',
'import_field6',
'import_field7',
'import_field8' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'import_header' => array( 'type' => 'import_header' ),
'import_field1' => array( 'field' => 'ecp_number',
'type' => 'import_field' ),
'import_field2' => array( 'field' => 'ecp_owner',
'type' => 'import_field' ),
'import_field3' => array( 'field' => 'ecp_ud',
'type' => 'import_field' ),
'import_field4' => array( 'field' => 'ecp_date',
'type' => 'import_field' ),
'import_field5' => array( 'field' => 'ecp_dateexp_cert',
'type' => 'import_field' ),
'import_field' => array( 'field' => 'ecp_dateexp',
'type' => 'import_field' ),
'import_field6' => array( 'field' => 'ecp_desc',
'type' => 'import_field' ),
'import_field7' => array( 'field' => 'ecp_status',
'type' => 'import_field' ),
'import_field8' => array( 'field' => 'ecp_daysleft',
'type' => 'import_field' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>