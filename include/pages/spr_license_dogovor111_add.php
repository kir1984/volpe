<?php
			$optionsArray = array( 'captcha' => array( 'captcha' => false ),
'fields' => array( 'gridFields' => array( 'licdog_poname',
'licdog_number',
'licdog_licnumber' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'licdog_poname' => array( 'integrated_edit_field',
'integrated_edit_field8',
'integrated_edit_field9' ),
'licdog_number' => array( 'integrated_edit_field3',
'integrated_edit_field14',
'integrated_edit_field15' ),
'licdog_licnumber' => array( 'integrated_edit_field7',
'integrated_edit_field22',
'integrated_edit_field23' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'top' => array( 'add_header' ),
'above-grid' => array( 'add_message' ),
'below-grid' => array( 'add_save',
'add_back_list',
'add_cancel' ),
'grid' => array( 'integrated_edit_field8',
'integrated_edit_field',
'integrated_edit_field9',
'integrated_edit_field14',
'integrated_edit_field3',
'integrated_edit_field15',
'integrated_edit_field22',
'integrated_edit_field7',
'integrated_edit_field23' ) ),
'formXtTags' => array( 'above-grid' => array( 'message_block' ) ),
'itemForms' => array( 'add_header' => 'top',
'add_message' => 'above-grid',
'add_save' => 'below-grid',
'add_back_list' => 'below-grid',
'add_cancel' => 'below-grid',
'integrated_edit_field8' => 'grid',
'integrated_edit_field' => 'grid',
'integrated_edit_field9' => 'grid',
'integrated_edit_field14' => 'grid',
'integrated_edit_field3' => 'grid',
'integrated_edit_field15' => 'grid',
'integrated_edit_field22' => 'grid',
'integrated_edit_field7' => 'grid',
'integrated_edit_field23' => 'grid' ),
'itemLocations' => array( 'integrated_edit_field8' => array( 'location' => 'grid',
'cellId' => 'c4' ),
'integrated_edit_field' => array( 'location' => 'grid',
'cellId' => 'c2' ),
'integrated_edit_field9' => array( 'location' => 'grid',
'cellId' => 'c' ),
'integrated_edit_field14' => array( 'location' => 'grid',
'cellId' => 'c10' ),
'integrated_edit_field3' => array( 'location' => 'grid',
'cellId' => 'c11' ),
'integrated_edit_field15' => array( 'location' => 'grid',
'cellId' => 'c12' ),
'integrated_edit_field22' => array( 'location' => 'grid',
'cellId' => 'c22' ),
'integrated_edit_field7' => array( 'location' => 'grid',
'cellId' => 'c23' ),
'integrated_edit_field23' => array( 'location' => 'grid',
'cellId' => 'c24' ) ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'add_header' => array( 'add_header' ),
'add_back_list' => array( 'add_back_list' ),
'add_cancel' => array( 'add_cancel' ),
'add_message' => array( 'add_message' ),
'add_save' => array( 'add_save' ),
'edit_field' => array( 'integrated_edit_field',
'integrated_edit_field3',
'integrated_edit_field7' ),
'edit_field_label' => array( 'integrated_edit_field8',
'integrated_edit_field14',
'integrated_edit_field22' ),
'edit_field_tooltip' => array( 'integrated_edit_field9',
'integrated_edit_field15',
'integrated_edit_field23' ) ),
'cellMaps' => array( 'grid' => array( 'cells' => array( 'c4' => array( 'cols' => array( 0 ),
'rows' => array( 0 ),
'tags' => array( 'licdog_poname_fieldblock' ),
'items' => array( 'integrated_edit_field8' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c2' => array( 'cols' => array( 1 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c' => array( 'cols' => array( 2 ),
'rows' => array( 0 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field9' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c10' => array( 'cols' => array( 0 ),
'rows' => array( 1 ),
'tags' => array( 'licdog_number_fieldblock' ),
'items' => array( 'integrated_edit_field14' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c11' => array( 'cols' => array( 1 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field3' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c12' => array( 'cols' => array( 2 ),
'rows' => array( 1 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field15' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c22' => array( 'cols' => array( 0 ),
'rows' => array( 2 ),
'tags' => array( 'licdog_licnumber_fieldblock' ),
'items' => array( 'integrated_edit_field22' ),
'fixedAtServer' => false,
'fixedAtClient' => false ),
'c23' => array( 'cols' => array( 1 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field7' ),
'fixedAtServer' => true,
'fixedAtClient' => false ),
'c24' => array( 'cols' => array( 2 ),
'rows' => array( 2 ),
'tags' => array(  ),
'items' => array( 'integrated_edit_field23' ),
'fixedAtServer' => true,
'fixedAtClient' => false ) ),
'width' => 3,
'height' => 3 ) ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'misc' => array( 'type' => 'add',
'breadcrumb' => false ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ) );
			$pageArray = array( 'id' => 'add',
'type' => 'add',
'layoutId' => 'nomenu',
'disabled' => 0,
'default' => 0,
'forms' => array( 'top' => array( 'modelId' => 'add-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'add_header' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'above-grid' => array( 'modelId' => 'add-above-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'add_message' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'below-grid' => array( 'modelId' => 'add-below-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'add_save',
'add_back_list',
'add_cancel' ),
'_t' => 'Map',
'_i' => array(  ),
'_s' => 0 ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'simple-edit',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c4' ),
array( 'cell' => 'c2' ),
array( 'cell' => 'c' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c10' ),
array( 'cell' => 'c11' ),
array( 'cell' => 'c12' ) ),
'section' => '' ),
array( 'cells' => array( array( 'cell' => 'c22' ),
array( 'cell' => 'c23' ),
array( 'cell' => 'c24' ) ),
'section' => '' ) ),
'cells' => array( 'c4' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field8' ),
'field' => 'licdog_poname' ),
'c2' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field' ),
'field' => 'licdog_poname' ),
'c' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field9' ),
'field' => 'licdog_poname' ),
'c10' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field14' ),
'field' => 'licdog_number' ),
'c11' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field3' ),
'field' => 'licdog_number' ),
'c12' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field15' ),
'field' => 'licdog_number' ),
'c22' => array( 'model' => 'c4',
'items' => array( 'integrated_edit_field22' ),
'field' => 'licdog_licnumber' ),
'c23' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field7' ),
'field' => 'licdog_licnumber' ),
'c24' => array( 'model' => 'c2',
'items' => array( 'integrated_edit_field23' ),
'field' => 'licdog_licnumber' ) ),
'deferredItems' => array(  ),
'columnCount' => 1,
'inlineLabels' => true,
'separateLabels' => true ) ),
'items' => array( 'add_header' => array( 'type' => 'add_header' ),
'add_back_list' => array( 'type' => 'add_back_list' ),
'add_cancel' => array( 'type' => 'add_cancel' ),
'add_message' => array( 'type' => 'add_message' ),
'add_save' => array( 'type' => 'add_save' ),
'integrated_edit_field' => array( 'field' => 'licdog_poname',
'type' => 'edit_field' ),
'integrated_edit_field3' => array( 'field' => 'licdog_number',
'type' => 'edit_field' ),
'integrated_edit_field7' => array( 'field' => 'licdog_licnumber',
'type' => 'edit_field' ),
'integrated_edit_field8' => array( 'type' => 'edit_field_label',
'field' => 'licdog_poname' ),
'integrated_edit_field9' => array( 'type' => 'edit_field_tooltip',
'field' => 'licdog_poname' ),
'integrated_edit_field14' => array( 'type' => 'edit_field_label',
'field' => 'licdog_number' ),
'integrated_edit_field15' => array( 'type' => 'edit_field_tooltip',
'field' => 'licdog_number' ),
'integrated_edit_field22' => array( 'type' => 'edit_field_label',
'field' => 'licdog_licnumber' ),
'integrated_edit_field23' => array( 'type' => 'edit_field_tooltip',
'field' => 'licdog_licnumber' ) ),
'dbProps' => array(  ),
'version' => 4 );
		?>