<?php
$tdatareport_docin = array();
$tdatareport_docin[".searchableFields"] = array();
$tdatareport_docin[".ShortName"] = "report_docin";
$tdatareport_docin[".OwnerID"] = "";
$tdatareport_docin[".OriginalTable"] = "public.doc_in";


$tdatareport_docin[".pagesByType"] = my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" );
$tdatareport_docin[".originalPagesByType"] = $tdatareport_docin[".pagesByType"];
$tdatareport_docin[".pages"] = types2pages( my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" ) );
$tdatareport_docin[".originalPages"] = $tdatareport_docin[".pages"];
$tdatareport_docin[".defaultPages"] = my_json_decode( "{\"masterreport\":\"masterreport\",\"masterrprint\":\"masterrprint\",\"report\":\"report\",\"rprint\":\"rprint\",\"search\":\"search\"}" );
$tdatareport_docin[".originalDefaultPages"] = $tdatareport_docin[".defaultPages"];

//	field labels
$fieldLabelsreport_docin = array();
$fieldToolTipsreport_docin = array();
$pageTitlesreport_docin = array();
$placeHoldersreport_docin = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsreport_docin["Russian"] = array();
	$fieldToolTipsreport_docin["Russian"] = array();
	$placeHoldersreport_docin["Russian"] = array();
	$pageTitlesreport_docin["Russian"] = array();
	$fieldLabelsreport_docin["Russian"]["doc_num"] = "№ вх.";
	$fieldToolTipsreport_docin["Russian"]["doc_num"] = "";
	$placeHoldersreport_docin["Russian"]["doc_num"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_date"] = "Дата вх.";
	$fieldToolTipsreport_docin["Russian"]["doc_date"] = "";
	$placeHoldersreport_docin["Russian"]["doc_date"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_source"] = "Отправитель";
	$fieldToolTipsreport_docin["Russian"]["doc_source"] = "";
	$placeHoldersreport_docin["Russian"]["doc_source"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_outnum"] = "№ исх.отправ.";
	$fieldToolTipsreport_docin["Russian"]["doc_outnum"] = "";
	$placeHoldersreport_docin["Russian"]["doc_outnum"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_outdate"] = "Дата исх.отправ";
	$fieldToolTipsreport_docin["Russian"]["doc_outdate"] = "";
	$placeHoldersreport_docin["Russian"]["doc_outdate"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_name"] = "Тема";
	$fieldToolTipsreport_docin["Russian"]["doc_name"] = "";
	$placeHoldersreport_docin["Russian"]["doc_name"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_desc"] = "Описание";
	$fieldToolTipsreport_docin["Russian"]["doc_desc"] = "";
	$placeHoldersreport_docin["Russian"]["doc_desc"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_resolution"] = "Резолюция";
	$fieldToolTipsreport_docin["Russian"]["doc_resolution"] = "";
	$placeHoldersreport_docin["Russian"]["doc_resolution"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_answer"] = "Наш ответ";
	$fieldToolTipsreport_docin["Russian"]["doc_answer"] = "";
	$placeHoldersreport_docin["Russian"]["doc_answer"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_control_date"] = "Дата контроля";
	$fieldToolTipsreport_docin["Russian"]["doc_control_date"] = "";
	$placeHoldersreport_docin["Russian"]["doc_control_date"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_control_status"] = "На контроль";
	$fieldToolTipsreport_docin["Russian"]["doc_control_status"] = "";
	$placeHoldersreport_docin["Russian"]["doc_control_status"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_control_executiondate"] = "Дата исполнения";
	$fieldToolTipsreport_docin["Russian"]["doc_control_executiondate"] = "";
	$placeHoldersreport_docin["Russian"]["doc_control_executiondate"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_control_executionmark"] = "Отметка об исполнении";
	$fieldToolTipsreport_docin["Russian"]["doc_control_executionmark"] = "";
	$placeHoldersreport_docin["Russian"]["doc_control_executionmark"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_control_executordate"] = "Получено исполнителем";
	$fieldToolTipsreport_docin["Russian"]["doc_control_executordate"] = "";
	$placeHoldersreport_docin["Russian"]["doc_control_executordate"] = "";
	$fieldLabelsreport_docin["Russian"]["doc_file"] = "Вложение";
	$fieldToolTipsreport_docin["Russian"]["doc_file"] = "";
	$placeHoldersreport_docin["Russian"]["doc_file"] = "";
	$fieldLabelsreport_docin["Russian"]["type_name"] = "Тип";
	$fieldToolTipsreport_docin["Russian"]["type_name"] = "";
	$placeHoldersreport_docin["Russian"]["type_name"] = "";
	$fieldLabelsreport_docin["Russian"]["executor"] = "Исполнитель";
	$fieldToolTipsreport_docin["Russian"]["executor"] = "";
	$placeHoldersreport_docin["Russian"]["executor"] = "";
	if (count($fieldToolTipsreport_docin["Russian"]))
		$tdatareport_docin[".isUseToolTips"] = true;
}


	$tdatareport_docin[".NCSearch"] = true;



$tdatareport_docin[".shortTableName"] = "report_docin";
$tdatareport_docin[".nSecOptions"] = 0;

$tdatareport_docin[".mainTableOwnerID"] = "";
$tdatareport_docin[".entityType"] = 2;
$tdatareport_docin[".connId"] = "itbase3_at_192_168_1_15";


$tdatareport_docin[".strOriginalTableName"] = "public.doc_in";

	



$tdatareport_docin[".showAddInPopup"] = false;

$tdatareport_docin[".showEditInPopup"] = false;

$tdatareport_docin[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatareport_docin[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatareport_docin[".listAjax"] = false;
//	temporary
$tdatareport_docin[".listAjax"] = false;

	$tdatareport_docin[".audit"] = false;

	$tdatareport_docin[".locking"] = false;


$pages = $tdatareport_docin[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatareport_docin[".edit"] = true;
	$tdatareport_docin[".afterEditAction"] = 1;
	$tdatareport_docin[".closePopupAfterEdit"] = 1;
	$tdatareport_docin[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatareport_docin[".add"] = true;
$tdatareport_docin[".afterAddAction"] = 1;
$tdatareport_docin[".closePopupAfterAdd"] = 1;
$tdatareport_docin[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatareport_docin[".list"] = true;
}



$tdatareport_docin[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatareport_docin[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatareport_docin[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatareport_docin[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatareport_docin[".printFriendly"] = true;
}



$tdatareport_docin[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatareport_docin[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatareport_docin[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatareport_docin[".isUseAjaxSuggest"] = true;






$tdatareport_docin[".ajaxCodeSnippetAdded"] = false;

$tdatareport_docin[".buttonsAdded"] = false;

$tdatareport_docin[".addPageEvents"] = false;

// use timepicker for search panel
$tdatareport_docin[".isUseTimeForSearch"] = false;


$tdatareport_docin[".badgeColor"] = "5F9EA0";


$tdatareport_docin[".allSearchFields"] = array();
$tdatareport_docin[".filterFields"] = array();
$tdatareport_docin[".requiredSearchFields"] = array();

$tdatareport_docin[".googleLikeFields"] = array();
$tdatareport_docin[".googleLikeFields"][] = "doc_num";
$tdatareport_docin[".googleLikeFields"][] = "doc_date";
$tdatareport_docin[".googleLikeFields"][] = "type_name";
$tdatareport_docin[".googleLikeFields"][] = "doc_source";
$tdatareport_docin[".googleLikeFields"][] = "doc_outnum";
$tdatareport_docin[".googleLikeFields"][] = "doc_outdate";
$tdatareport_docin[".googleLikeFields"][] = "doc_name";
$tdatareport_docin[".googleLikeFields"][] = "doc_desc";
$tdatareport_docin[".googleLikeFields"][] = "doc_resolution";
$tdatareport_docin[".googleLikeFields"][] = "doc_answer";
$tdatareport_docin[".googleLikeFields"][] = "doc_control_date";
$tdatareport_docin[".googleLikeFields"][] = "doc_control_status";
$tdatareport_docin[".googleLikeFields"][] = "doc_control_executiondate";
$tdatareport_docin[".googleLikeFields"][] = "doc_control_executionmark";
$tdatareport_docin[".googleLikeFields"][] = "executor";
$tdatareport_docin[".googleLikeFields"][] = "doc_control_executordate";
$tdatareport_docin[".googleLikeFields"][] = "doc_file";



$tdatareport_docin[".tableType"] = "report";

$tdatareport_docin[".printerPageOrientation"] = 1;
$tdatareport_docin[".nPrinterPageScale"] = 100;

$tdatareport_docin[".nPrinterSplitRecords"] = 40;

$tdatareport_docin[".geocodingEnabled"] = false;

//report settings
$tdatareport_docin[".printReportLayout"] = 2;

$tdatareport_docin[".reportPrintPartitionType"] = 1;
$tdatareport_docin[".reportPrintGroupsPerPage"] = 3;
$tdatareport_docin[".lowGroup"] = 2;



$tdatareport_docin[".reportGroupFields"] = true;
$tdatareport_docin[".pageSize"] = 5;
$reportGroupFieldsList = array();
$reportGroupFields = array();
	$reportGroupFieldsList []= "doc_control_date";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "doc_control_date";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 1;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
	$reportGroupFieldsList []= "executor";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "executor";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 2;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
$tdatareport_docin[".reportGroupFieldsData"] = $reportGroupFields;
$tdatareport_docin[".reportGroupFieldsList"] = $reportGroupFieldsList;






$tdatareport_docin[".repShowDet"] = true;

$tdatareport_docin[".reportLayout"] = 2;

//end of report settings










$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatareport_docin[".strOrderBy"] = $tstrOrderBy;

$tdatareport_docin[".orderindexes"] = array();


$tdatareport_docin[".sqlHead"] = "SELECT \"public\".doc_in.doc_num,  \"public\".doc_in.doc_date,  \"public\".spr_doc_type.type_name,  \"public\".doc_in.doc_source,  \"public\".doc_in.doc_outnum,  \"public\".doc_in.doc_outdate,  \"public\".doc_in.doc_name,  \"public\".doc_in.doc_desc,  \"public\".doc_in.doc_resolution,  \"public\".doc_in.doc_answer,  \"public\".doc_in.doc_control_date,  \"public\".doc_in.doc_control_status,  \"public\".doc_in.doc_control_executiondate,  \"public\".doc_in.doc_control_executionmark,  \"public\".sotrudnik.sotrudnik_fio AS executor,  \"public\".doc_in.doc_control_executordate,  \"public\".doc_in.doc_file";
$tdatareport_docin[".sqlFrom"] = "FROM \"public\".doc_in  INNER JOIN \"public\".spr_partners ON \"public\".doc_in.doc_source = \"public\".spr_partners.partner_id  INNER JOIN \"public\".sotrudnik ON \"public\".doc_in.doc_control_executor = \"public\".sotrudnik.sotrudnik_id  INNER JOIN \"public\".spr_doc_type ON \"public\".doc_in.doc_type = \"public\".spr_doc_type.doctype_id";
$tdatareport_docin[".sqlWhereExpr"] = "(\"public\".doc_in.doc_control_status =TRUE) AND (\"public\".doc_in.doc_control_executiondate IS NULL)";
$tdatareport_docin[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatareport_docin[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatareport_docin[".arrGroupsPerPage"] = $arrGPP;

$tdatareport_docin[".highlightSearchResults"] = true;

$tableKeysreport_docin = array();
$tdatareport_docin[".Keys"] = $tableKeysreport_docin;


$tdatareport_docin[".hideMobileList"] = array();




//	doc_num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "doc_num";
	$fdata["GoodName"] = "doc_num";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_num");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_num";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_num";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_num"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_num";
//	doc_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "doc_date";
	$fdata["GoodName"] = "doc_date";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_date");
	$fdata["FieldType"] = 7;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_date"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_date";
//	type_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "type_name";
	$fdata["GoodName"] = "type_name";
	$fdata["ownerTable"] = "public.spr_doc_type";
	$fdata["Label"] = GetFieldLabel("report_docin","type_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "type_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_doc_type.type_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["type_name"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "type_name";
//	doc_source
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "doc_source";
	$fdata["GoodName"] = "doc_source";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_source");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_source";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_source";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_partners";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "partner_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "partner_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_source"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_source";
//	doc_outnum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "doc_outnum";
	$fdata["GoodName"] = "doc_outnum";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_outnum");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_outnum";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_outnum";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_outnum"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_outnum";
//	doc_outdate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "doc_outdate";
	$fdata["GoodName"] = "doc_outdate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_outdate");
	$fdata["FieldType"] = 7;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_outdate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_outdate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_outdate"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_outdate";
//	doc_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "doc_name";
	$fdata["GoodName"] = "doc_name";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_name"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_name";
//	doc_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "doc_desc";
	$fdata["GoodName"] = "doc_desc";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_desc");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_desc"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_desc";
//	doc_resolution
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "doc_resolution";
	$fdata["GoodName"] = "doc_resolution";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_resolution");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_resolution";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_resolution";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "В работу";
	$edata["LookupValues"][] = "Без исполнения";
	$edata["LookupValues"][] = "Для информации";
	$edata["LookupValues"][] = "Для сведения";
	$edata["LookupValues"][] = "К исполнению";
	$edata["LookupValues"][] = "К исполнению (срочно!)";
	$edata["LookupValues"][] = "На доработку";
	$edata["LookupValues"][] = "На контроль";
	$edata["LookupValues"][] = "Прошу подготовить ответ";
	$edata["LookupValues"][] = "Принять участие";
	$edata["LookupValues"][] = "Принять участие (обязательно)";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_resolution"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_resolution";
//	doc_answer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "doc_answer";
	$fdata["GoodName"] = "doc_answer";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_answer");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_answer";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_answer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.doc_out";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "docout_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "doc_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_answer"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_answer";
//	doc_control_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "doc_control_date";
	$fdata["GoodName"] = "doc_control_date";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_control_date");
	$fdata["FieldType"] = 135;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_control_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_control_date"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_control_date";
//	doc_control_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "doc_control_status";
	$fdata["GoodName"] = "doc_control_status";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_control_status");
	$fdata["FieldType"] = 11;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_control_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatareport_docin["doc_control_status"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_control_status";
//	doc_control_executiondate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "doc_control_executiondate";
	$fdata["GoodName"] = "doc_control_executiondate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_control_executiondate");
	$fdata["FieldType"] = 135;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_executiondate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_control_executiondate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_control_executiondate"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_control_executiondate";
//	doc_control_executionmark
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "doc_control_executionmark";
	$fdata["GoodName"] = "doc_control_executionmark";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_control_executionmark");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_executionmark";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_control_executionmark";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Без исполнения в документы";
	$edata["LookupValues"][] = "В работе";
	$edata["LookupValues"][] = "Для сведения";
	$edata["LookupValues"][] = "Исполнено";
	$edata["LookupValues"][] = "Исполнено по электронной почте";
	$edata["LookupValues"][] = "Исполнено через Бюджетирование";
	$edata["LookupValues"][] = "Исполнено через ЕИСМС";
	$edata["LookupValues"][] = "Исполнено через личный кабинет";
	$edata["LookupValues"][] = "Исполнено через Росстат";
	$edata["LookupValues"][] = "Исполнено через ЭБ";
	$edata["LookupValues"][] = "Отправлено экспресс-почтой";
	$edata["LookupValues"][] = "Снято с контроля";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_control_executionmark"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_control_executionmark";
//	executor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "executor";
	$fdata["GoodName"] = "executor";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("report_docin","executor");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "sotrudnik_fio";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".sotrudnik.sotrudnik_fio";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["executor"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "executor";
//	doc_control_executordate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "doc_control_executordate";
	$fdata["GoodName"] = "doc_control_executordate";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_control_executordate");
	$fdata["FieldType"] = 135;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_executordate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_control_executordate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_control_executordate"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_control_executordate";
//	doc_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "doc_file";
	$fdata["GoodName"] = "doc_file";
	$fdata["ownerTable"] = "public.doc_in";
	$fdata["Label"] = GetFieldLabel("report_docin","doc_file");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_file";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_in.doc_file";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 20;

	
		$edata["maxTotalFilesSize"] = 80000;

	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docin["doc_file"] = $fdata;
		$tdatareport_docin[".searchableFields"][] = "doc_file";


$tables_data["report_docin"]=&$tdatareport_docin;
$field_labels["report_docin"] = &$fieldLabelsreport_docin;
$fieldToolTips["report_docin"] = &$fieldToolTipsreport_docin;
$placeHolders["report_docin"] = &$placeHoldersreport_docin;
$page_titles["report_docin"] = &$pageTitlesreport_docin;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["report_docin"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["report_docin"] = array();



	
				$strOriginalDetailsTable="public.spr_partners";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_partners";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_partners";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["report_docin"][0] = $masterParams;
				$masterTablesData["report_docin"][0]["masterKeys"] = array();
	$masterTablesData["report_docin"][0]["masterKeys"][]="partner_id";
				$masterTablesData["report_docin"][0]["detailKeys"] = array();
	$masterTablesData["report_docin"][0]["detailKeys"][]="doc_source";
		
	
				$strOriginalDetailsTable="public.doc_out";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.doc_out";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "doc_out";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["report_docin"][1] = $masterParams;
				$masterTablesData["report_docin"][1]["masterKeys"] = array();
	$masterTablesData["report_docin"][1]["masterKeys"][]="docout_id";
				$masterTablesData["report_docin"][1]["detailKeys"] = array();
	$masterTablesData["report_docin"][1]["detailKeys"][]="doc_answer";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_report_docin()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"public\".doc_in.doc_num,  \"public\".doc_in.doc_date,  \"public\".spr_doc_type.type_name,  \"public\".doc_in.doc_source,  \"public\".doc_in.doc_outnum,  \"public\".doc_in.doc_outdate,  \"public\".doc_in.doc_name,  \"public\".doc_in.doc_desc,  \"public\".doc_in.doc_resolution,  \"public\".doc_in.doc_answer,  \"public\".doc_in.doc_control_date,  \"public\".doc_in.doc_control_status,  \"public\".doc_in.doc_control_executiondate,  \"public\".doc_in.doc_control_executionmark,  \"public\".sotrudnik.sotrudnik_fio AS executor,  \"public\".doc_in.doc_control_executordate,  \"public\".doc_in.doc_file";
$proto0["m_strFrom"] = "FROM \"public\".doc_in  INNER JOIN \"public\".spr_partners ON \"public\".doc_in.doc_source = \"public\".spr_partners.partner_id  INNER JOIN \"public\".sotrudnik ON \"public\".doc_in.doc_control_executor = \"public\".sotrudnik.sotrudnik_id  INNER JOIN \"public\".spr_doc_type ON \"public\".doc_in.doc_type = \"public\".spr_doc_type.doctype_id";
$proto0["m_strWhere"] = "(\"public\".doc_in.doc_control_status =TRUE) AND (\"public\".doc_in.doc_control_executiondate IS NULL)";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "(\"public\".doc_in.doc_control_status =TRUE) AND (\"public\".doc_in.doc_control_executiondate IS NULL)";
$proto2["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "(\"public\".doc_in.doc_control_status =TRUE) AND (\"public\".doc_in.doc_control_executiondate IS NULL)"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
						$proto4=array();
$proto4["m_sql"] = "\"public\".doc_in.doc_control_status =TRUE";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "=TRUE";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = true;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

			$proto2["m_contained"][]=$obj;
						$proto6=array();
$proto6["m_sql"] = "\"public\".doc_in.doc_control_executiondate IS NULL";
$proto6["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto6["m_column"]=$obj;
$proto6["m_contained"] = array();
$proto6["m_strCase"] = "IS NULL";
$proto6["m_havingmode"] = false;
$proto6["m_inBrackets"] = true;
$proto6["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto6);

			$proto2["m_contained"][]=$obj;
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto8=array();
$proto8["m_sql"] = "";
$proto8["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto8["m_column"]=$obj;
$proto8["m_contained"] = array();
$proto8["m_strCase"] = "";
$proto8["m_havingmode"] = false;
$proto8["m_inBrackets"] = false;
$proto8["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto8);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_num",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto10["m_sql"] = "\"public\".doc_in.doc_num";
$proto10["m_srcTableName"] = "report_docin";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_date",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto12["m_sql"] = "\"public\".doc_in.doc_date";
$proto12["m_srcTableName"] = "report_docin";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "type_name",
	"m_strTable" => "public.spr_doc_type",
	"m_srcTableName" => "report_docin"
));

$proto14["m_sql"] = "\"public\".spr_doc_type.type_name";
$proto14["m_srcTableName"] = "report_docin";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_source",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto16["m_sql"] = "\"public\".doc_in.doc_source";
$proto16["m_srcTableName"] = "report_docin";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_outnum",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto18["m_sql"] = "\"public\".doc_in.doc_outnum";
$proto18["m_srcTableName"] = "report_docin";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_outdate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto20["m_sql"] = "\"public\".doc_in.doc_outdate";
$proto20["m_srcTableName"] = "report_docin";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_name",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto22["m_sql"] = "\"public\".doc_in.doc_name";
$proto22["m_srcTableName"] = "report_docin";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_desc",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto24["m_sql"] = "\"public\".doc_in.doc_desc";
$proto24["m_srcTableName"] = "report_docin";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_resolution",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto26["m_sql"] = "\"public\".doc_in.doc_resolution";
$proto26["m_srcTableName"] = "report_docin";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_answer",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto28["m_sql"] = "\"public\".doc_in.doc_answer";
$proto28["m_srcTableName"] = "report_docin";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_date",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto30["m_sql"] = "\"public\".doc_in.doc_control_date";
$proto30["m_srcTableName"] = "report_docin";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto32["m_sql"] = "\"public\".doc_in.doc_control_status";
$proto32["m_srcTableName"] = "report_docin";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto34["m_sql"] = "\"public\".doc_in.doc_control_executiondate";
$proto34["m_srcTableName"] = "report_docin";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executionmark",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto36["m_sql"] = "\"public\".doc_in.doc_control_executionmark";
$proto36["m_srcTableName"] = "report_docin";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_fio",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "report_docin"
));

$proto38["m_sql"] = "\"public\".sotrudnik.sotrudnik_fio";
$proto38["m_srcTableName"] = "report_docin";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "executor";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executordate",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto40["m_sql"] = "\"public\".doc_in.doc_control_executordate";
$proto40["m_srcTableName"] = "report_docin";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_file",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto42["m_sql"] = "\"public\".doc_in.doc_file";
$proto42["m_srcTableName"] = "report_docin";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto44=array();
$proto44["m_link"] = "SQLL_MAIN";
			$proto45=array();
$proto45["m_strName"] = "public.doc_in";
$proto45["m_srcTableName"] = "report_docin";
$proto45["m_columns"] = array();
$proto45["m_columns"][] = "docin_id";
$proto45["m_columns"][] = "doc_num";
$proto45["m_columns"][] = "doc_date";
$proto45["m_columns"][] = "doc_type";
$proto45["m_columns"][] = "doc_source";
$proto45["m_columns"][] = "doc_outnum";
$proto45["m_columns"][] = "doc_outdate";
$proto45["m_columns"][] = "doc_destination";
$proto45["m_columns"][] = "doc_name";
$proto45["m_columns"][] = "doc_desc";
$proto45["m_columns"][] = "doc_resolution";
$proto45["m_columns"][] = "doc_answer";
$proto45["m_columns"][] = "doc_control_date";
$proto45["m_columns"][] = "doc_control_status";
$proto45["m_columns"][] = "doc_control_executiondate";
$proto45["m_columns"][] = "doc_control_executionmark";
$proto45["m_columns"][] = "doc_control_executor";
$proto45["m_columns"][] = "doc_control_executordate";
$proto45["m_columns"][] = "doc_file";
$proto45["m_columns"][] = "doc_destination2";
$proto45["m_columns"][] = "doc_destination3";
$proto45["m_columns"][] = "doc_destination4";
$obj = new SQLTable($proto45);

$proto44["m_table"] = $obj;
$proto44["m_sql"] = "\"public\".doc_in";
$proto44["m_alias"] = "";
$proto44["m_srcTableName"] = "report_docin";
$proto46=array();
$proto46["m_sql"] = "";
$proto46["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto46["m_column"]=$obj;
$proto46["m_contained"] = array();
$proto46["m_strCase"] = "";
$proto46["m_havingmode"] = false;
$proto46["m_inBrackets"] = false;
$proto46["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto46);

$proto44["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto44);

$proto0["m_fromlist"][]=$obj;
												$proto48=array();
$proto48["m_link"] = "SQLL_INNERJOIN";
			$proto49=array();
$proto49["m_strName"] = "public.spr_partners";
$proto49["m_srcTableName"] = "report_docin";
$proto49["m_columns"] = array();
$proto49["m_columns"][] = "partner_id";
$proto49["m_columns"][] = "partner_name";
$proto49["m_columns"][] = "partner_adress";
$proto49["m_columns"][] = "partner_telephone";
$proto49["m_columns"][] = "partner_contact";
$obj = new SQLTable($proto49);

$proto48["m_table"] = $obj;
$proto48["m_sql"] = "INNER JOIN \"public\".spr_partners ON \"public\".doc_in.doc_source = \"public\".spr_partners.partner_id";
$proto48["m_alias"] = "";
$proto48["m_srcTableName"] = "report_docin";
$proto50=array();
$proto50["m_sql"] = "\"public\".doc_in.doc_source = \"public\".spr_partners.partner_id";
$proto50["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_source",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto50["m_column"]=$obj;
$proto50["m_contained"] = array();
$proto50["m_strCase"] = "= \"public\".spr_partners.partner_id";
$proto50["m_havingmode"] = false;
$proto50["m_inBrackets"] = false;
$proto50["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto50);

$proto48["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto48);

$proto0["m_fromlist"][]=$obj;
												$proto52=array();
$proto52["m_link"] = "SQLL_INNERJOIN";
			$proto53=array();
$proto53["m_strName"] = "public.sotrudnik";
$proto53["m_srcTableName"] = "report_docin";
$proto53["m_columns"] = array();
$proto53["m_columns"][] = "sotrudnik_id";
$proto53["m_columns"][] = "sotrudnik_fio";
$proto53["m_columns"][] = "sotrudnik_login";
$proto53["m_columns"][] = "sotrudnik_dep";
$proto53["m_columns"][] = "sotrudnik_photo";
$proto53["m_columns"][] = "sotrudnik_dismissed";
$obj = new SQLTable($proto53);

$proto52["m_table"] = $obj;
$proto52["m_sql"] = "INNER JOIN \"public\".sotrudnik ON \"public\".doc_in.doc_control_executor = \"public\".sotrudnik.sotrudnik_id";
$proto52["m_alias"] = "";
$proto52["m_srcTableName"] = "report_docin";
$proto54=array();
$proto54["m_sql"] = "\"public\".doc_in.doc_control_executor = \"public\".sotrudnik.sotrudnik_id";
$proto54["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_control_executor",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto54["m_column"]=$obj;
$proto54["m_contained"] = array();
$proto54["m_strCase"] = "= \"public\".sotrudnik.sotrudnik_id";
$proto54["m_havingmode"] = false;
$proto54["m_inBrackets"] = false;
$proto54["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto54);

$proto52["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto52);

$proto0["m_fromlist"][]=$obj;
												$proto56=array();
$proto56["m_link"] = "SQLL_INNERJOIN";
			$proto57=array();
$proto57["m_strName"] = "public.spr_doc_type";
$proto57["m_srcTableName"] = "report_docin";
$proto57["m_columns"] = array();
$proto57["m_columns"][] = "doctype_id";
$proto57["m_columns"][] = "type_name";
$obj = new SQLTable($proto57);

$proto56["m_table"] = $obj;
$proto56["m_sql"] = "INNER JOIN \"public\".spr_doc_type ON \"public\".doc_in.doc_type = \"public\".spr_doc_type.doctype_id";
$proto56["m_alias"] = "";
$proto56["m_srcTableName"] = "report_docin";
$proto58=array();
$proto58["m_sql"] = "\"public\".doc_in.doc_type = \"public\".spr_doc_type.doctype_id";
$proto58["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_type",
	"m_strTable" => "public.doc_in",
	"m_srcTableName" => "report_docin"
));

$proto58["m_column"]=$obj;
$proto58["m_contained"] = array();
$proto58["m_strCase"] = "= \"public\".spr_doc_type.doctype_id";
$proto58["m_havingmode"] = false;
$proto58["m_inBrackets"] = false;
$proto58["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto58);

$proto56["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto56);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="report_docin";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_report_docin = createSqlQuery_report_docin();


	
		;

																	

$tdatareport_docin[".sqlquery"] = $queryData_report_docin;



$tableEvents["report_docin"] = new eventsBase;
$tdatareport_docin[".hasEvents"] = false;

?>