<?php
$tdatareport_docout = array();
$tdatareport_docout[".searchableFields"] = array();
$tdatareport_docout[".ShortName"] = "report_docout";
$tdatareport_docout[".OwnerID"] = "";
$tdatareport_docout[".OriginalTable"] = "public.doc_out";


$tdatareport_docout[".pagesByType"] = my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" );
$tdatareport_docout[".originalPagesByType"] = $tdatareport_docout[".pagesByType"];
$tdatareport_docout[".pages"] = types2pages( my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" ) );
$tdatareport_docout[".originalPages"] = $tdatareport_docout[".pages"];
$tdatareport_docout[".defaultPages"] = my_json_decode( "{\"masterreport\":\"masterreport\",\"masterrprint\":\"masterrprint\",\"report\":\"report\",\"rprint\":\"rprint\",\"search\":\"search\"}" );
$tdatareport_docout[".originalDefaultPages"] = $tdatareport_docout[".defaultPages"];

//	field labels
$fieldLabelsreport_docout = array();
$fieldToolTipsreport_docout = array();
$pageTitlesreport_docout = array();
$placeHoldersreport_docout = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsreport_docout["Russian"] = array();
	$fieldToolTipsreport_docout["Russian"] = array();
	$placeHoldersreport_docout["Russian"] = array();
	$pageTitlesreport_docout["Russian"] = array();
	$fieldLabelsreport_docout["Russian"]["doc_num"] = "№ исх.";
	$fieldToolTipsreport_docout["Russian"]["doc_num"] = "";
	$placeHoldersreport_docout["Russian"]["doc_num"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_date"] = "Дата исх.";
	$fieldToolTipsreport_docout["Russian"]["doc_date"] = "";
	$placeHoldersreport_docout["Russian"]["doc_date"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_answer"] = "Ответ на:";
	$fieldToolTipsreport_docout["Russian"]["doc_answer"] = "";
	$placeHoldersreport_docout["Russian"]["doc_answer"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_name"] = "Тема";
	$fieldToolTipsreport_docout["Russian"]["doc_name"] = "";
	$placeHoldersreport_docout["Russian"]["doc_name"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_desc"] = "Описание";
	$fieldToolTipsreport_docout["Russian"]["doc_desc"] = "";
	$placeHoldersreport_docout["Russian"]["doc_desc"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_control_status"] = "На контроль";
	$fieldToolTipsreport_docout["Russian"]["doc_control_status"] = "";
	$placeHoldersreport_docout["Russian"]["doc_control_status"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_control_executiondate"] = "Дата исполнения";
	$fieldToolTipsreport_docout["Russian"]["doc_control_executiondate"] = "";
	$placeHoldersreport_docout["Russian"]["doc_control_executiondate"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_file"] = "Вложение";
	$fieldToolTipsreport_docout["Russian"]["doc_file"] = "";
	$placeHoldersreport_docout["Russian"]["doc_file"] = "";
	$fieldLabelsreport_docout["Russian"]["type_name"] = "Тип";
	$fieldToolTipsreport_docout["Russian"]["type_name"] = "";
	$placeHoldersreport_docout["Russian"]["type_name"] = "";
	$fieldLabelsreport_docout["Russian"]["partner_name"] = "Получатель";
	$fieldToolTipsreport_docout["Russian"]["partner_name"] = "";
	$placeHoldersreport_docout["Russian"]["partner_name"] = "";
	$fieldLabelsreport_docout["Russian"]["sotrudnik_fio"] = "Исполнитель";
	$fieldToolTipsreport_docout["Russian"]["sotrudnik_fio"] = "";
	$placeHoldersreport_docout["Russian"]["sotrudnik_fio"] = "";
	$fieldLabelsreport_docout["Russian"]["doc_control_date"] = "Дата контроля";
	$fieldToolTipsreport_docout["Russian"]["doc_control_date"] = "";
	$placeHoldersreport_docout["Russian"]["doc_control_date"] = "";
	if (count($fieldToolTipsreport_docout["Russian"]))
		$tdatareport_docout[".isUseToolTips"] = true;
}


	$tdatareport_docout[".NCSearch"] = true;



$tdatareport_docout[".shortTableName"] = "report_docout";
$tdatareport_docout[".nSecOptions"] = 0;

$tdatareport_docout[".mainTableOwnerID"] = "";
$tdatareport_docout[".entityType"] = 2;
$tdatareport_docout[".connId"] = "itbase3_at_192_168_1_15";


$tdatareport_docout[".strOriginalTableName"] = "public.doc_out";

	



$tdatareport_docout[".showAddInPopup"] = false;

$tdatareport_docout[".showEditInPopup"] = false;

$tdatareport_docout[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatareport_docout[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatareport_docout[".listAjax"] = false;
//	temporary
$tdatareport_docout[".listAjax"] = false;

	$tdatareport_docout[".audit"] = false;

	$tdatareport_docout[".locking"] = false;


$pages = $tdatareport_docout[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatareport_docout[".edit"] = true;
	$tdatareport_docout[".afterEditAction"] = 1;
	$tdatareport_docout[".closePopupAfterEdit"] = 1;
	$tdatareport_docout[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatareport_docout[".add"] = true;
$tdatareport_docout[".afterAddAction"] = 1;
$tdatareport_docout[".closePopupAfterAdd"] = 1;
$tdatareport_docout[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatareport_docout[".list"] = true;
}



$tdatareport_docout[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatareport_docout[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatareport_docout[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatareport_docout[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatareport_docout[".printFriendly"] = true;
}



$tdatareport_docout[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatareport_docout[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatareport_docout[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatareport_docout[".isUseAjaxSuggest"] = true;






$tdatareport_docout[".ajaxCodeSnippetAdded"] = false;

$tdatareport_docout[".buttonsAdded"] = false;

$tdatareport_docout[".addPageEvents"] = false;

// use timepicker for search panel
$tdatareport_docout[".isUseTimeForSearch"] = false;


$tdatareport_docout[".badgeColor"] = "5F9EA0";


$tdatareport_docout[".allSearchFields"] = array();
$tdatareport_docout[".filterFields"] = array();
$tdatareport_docout[".requiredSearchFields"] = array();

$tdatareport_docout[".googleLikeFields"] = array();
$tdatareport_docout[".googleLikeFields"][] = "doc_num";
$tdatareport_docout[".googleLikeFields"][] = "doc_date";
$tdatareport_docout[".googleLikeFields"][] = "type_name";
$tdatareport_docout[".googleLikeFields"][] = "partner_name";
$tdatareport_docout[".googleLikeFields"][] = "doc_name";
$tdatareport_docout[".googleLikeFields"][] = "doc_answer";
$tdatareport_docout[".googleLikeFields"][] = "doc_desc";
$tdatareport_docout[".googleLikeFields"][] = "sotrudnik_fio";
$tdatareport_docout[".googleLikeFields"][] = "doc_control_status";
$tdatareport_docout[".googleLikeFields"][] = "doc_control_date";
$tdatareport_docout[".googleLikeFields"][] = "doc_control_executiondate";
$tdatareport_docout[".googleLikeFields"][] = "doc_file";



$tdatareport_docout[".tableType"] = "report";

$tdatareport_docout[".printerPageOrientation"] = 0;
$tdatareport_docout[".nPrinterPageScale"] = 100;

$tdatareport_docout[".nPrinterSplitRecords"] = 40;

$tdatareport_docout[".geocodingEnabled"] = false;

//report settings
$tdatareport_docout[".printReportLayout"] = 2;

$tdatareport_docout[".reportPrintPartitionType"] = 1;
$tdatareport_docout[".reportPrintGroupsPerPage"] = 3;
$tdatareport_docout[".lowGroup"] = 2;



$tdatareport_docout[".reportGroupFields"] = true;
$tdatareport_docout[".pageSize"] = 5;
$reportGroupFieldsList = array();
$reportGroupFields = array();
	$reportGroupFieldsList []= "doc_control_date";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "doc_control_date";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 1;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
	$reportGroupFieldsList []= "sotrudnik_fio";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "sotrudnik_fio";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 2;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
$tdatareport_docout[".reportGroupFieldsData"] = $reportGroupFields;
$tdatareport_docout[".reportGroupFieldsList"] = $reportGroupFieldsList;






$tdatareport_docout[".repShowDet"] = true;

$tdatareport_docout[".reportLayout"] = 2;

//end of report settings










$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatareport_docout[".strOrderBy"] = $tstrOrderBy;

$tdatareport_docout[".orderindexes"] = array();


$tdatareport_docout[".sqlHead"] = "SELECT \"public\".doc_out.doc_num,  \"public\".doc_out.doc_date,  \"public\".spr_doc_type.type_name,  \"public\".spr_partners.partner_name,  \"public\".doc_out.doc_name,  \"public\".doc_out.doc_answer,  \"public\".doc_out.doc_desc,  \"public\".sotrudnik.sotrudnik_fio,  \"public\".doc_out.doc_control_status,  \"public\".doc_out.doc_control_date,  \"public\".doc_out.doc_control_executiondate,  \"public\".doc_out.doc_file";
$tdatareport_docout[".sqlFrom"] = "FROM \"public\".doc_out  INNER JOIN \"public\".sotrudnik ON \"public\".doc_out.doc_executor = \"public\".sotrudnik.sotrudnik_id  INNER JOIN \"public\".spr_partners ON \"public\".doc_out.doc_destination = \"public\".spr_partners.partner_id  INNER JOIN \"public\".spr_doc_type ON \"public\".doc_out.doc_type = \"public\".spr_doc_type.doctype_id";
$tdatareport_docout[".sqlWhereExpr"] = "(\"public\".doc_out.doc_control_status =TRUE) AND (\"public\".doc_out.doc_control_executiondate IS NULL)";
$tdatareport_docout[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatareport_docout[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatareport_docout[".arrGroupsPerPage"] = $arrGPP;

$tdatareport_docout[".highlightSearchResults"] = true;

$tableKeysreport_docout = array();
$tdatareport_docout[".Keys"] = $tableKeysreport_docout;


$tdatareport_docout[".hideMobileList"] = array();




//	doc_num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "doc_num";
	$fdata["GoodName"] = "doc_num";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_num");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_num";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_num";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_num"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_num";
//	doc_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "doc_date";
	$fdata["GoodName"] = "doc_date";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_date");
	$fdata["FieldType"] = 7;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_date"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_date";
//	type_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "type_name";
	$fdata["GoodName"] = "type_name";
	$fdata["ownerTable"] = "public.spr_doc_type";
	$fdata["Label"] = GetFieldLabel("report_docout","type_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "type_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_doc_type.type_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["type_name"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "type_name";
//	partner_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "partner_name";
	$fdata["GoodName"] = "partner_name";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("report_docout","partner_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "partner_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_partners.partner_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["partner_name"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "partner_name";
//	doc_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "doc_name";
	$fdata["GoodName"] = "doc_name";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_name"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_name";
//	doc_answer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "doc_answer";
	$fdata["GoodName"] = "doc_answer";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_answer");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_answer";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_answer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.doc_in";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "docin_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "doc_outnum || ' от ' || doc_outdate || ' : ' || doc_name    ";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_answer"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_answer";
//	doc_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "doc_desc";
	$fdata["GoodName"] = "doc_desc";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_desc");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=1024";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_desc"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_desc";
//	sotrudnik_fio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "sotrudnik_fio";
	$fdata["GoodName"] = "sotrudnik_fio";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("report_docout","sotrudnik_fio");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "sotrudnik_fio";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".sotrudnik.sotrudnik_fio";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["sotrudnik_fio"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "sotrudnik_fio";
//	doc_control_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "doc_control_status";
	$fdata["GoodName"] = "doc_control_status";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_control_status");
	$fdata["FieldType"] = 11;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_control_status";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatareport_docout["doc_control_status"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_control_status";
//	doc_control_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "doc_control_date";
	$fdata["GoodName"] = "doc_control_date";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_control_date");
	$fdata["FieldType"] = 135;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_control_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_control_date"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_control_date";
//	doc_control_executiondate
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "doc_control_executiondate";
	$fdata["GoodName"] = "doc_control_executiondate";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_control_executiondate");
	$fdata["FieldType"] = 135;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_control_executiondate";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_control_executiondate";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_control_executiondate"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_control_executiondate";
//	doc_file
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "doc_file";
	$fdata["GoodName"] = "doc_file";
	$fdata["ownerTable"] = "public.doc_out";
	$fdata["Label"] = GetFieldLabel("report_docout","doc_file");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "doc_file";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".doc_out.doc_file";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 20;

	
		$edata["maxTotalFilesSize"] = 80000;

	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_docout["doc_file"] = $fdata;
		$tdatareport_docout[".searchableFields"][] = "doc_file";


$tables_data["report_docout"]=&$tdatareport_docout;
$field_labels["report_docout"] = &$fieldLabelsreport_docout;
$fieldToolTips["report_docout"] = &$fieldToolTipsreport_docout;
$placeHolders["report_docout"] = &$placeHoldersreport_docout;
$page_titles["report_docout"] = &$pageTitlesreport_docout;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["report_docout"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["report_docout"] = array();



	
				$strOriginalDetailsTable="public.doc_in";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.doc_in";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "doc_in";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["report_docout"][0] = $masterParams;
				$masterTablesData["report_docout"][0]["masterKeys"] = array();
	$masterTablesData["report_docout"][0]["masterKeys"][]="docin_id";
				$masterTablesData["report_docout"][0]["detailKeys"] = array();
	$masterTablesData["report_docout"][0]["detailKeys"][]="doc_answer";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_report_docout()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"public\".doc_out.doc_num,  \"public\".doc_out.doc_date,  \"public\".spr_doc_type.type_name,  \"public\".spr_partners.partner_name,  \"public\".doc_out.doc_name,  \"public\".doc_out.doc_answer,  \"public\".doc_out.doc_desc,  \"public\".sotrudnik.sotrudnik_fio,  \"public\".doc_out.doc_control_status,  \"public\".doc_out.doc_control_date,  \"public\".doc_out.doc_control_executiondate,  \"public\".doc_out.doc_file";
$proto0["m_strFrom"] = "FROM \"public\".doc_out  INNER JOIN \"public\".sotrudnik ON \"public\".doc_out.doc_executor = \"public\".sotrudnik.sotrudnik_id  INNER JOIN \"public\".spr_partners ON \"public\".doc_out.doc_destination = \"public\".spr_partners.partner_id  INNER JOIN \"public\".spr_doc_type ON \"public\".doc_out.doc_type = \"public\".spr_doc_type.doctype_id";
$proto0["m_strWhere"] = "(\"public\".doc_out.doc_control_status =TRUE) AND (\"public\".doc_out.doc_control_executiondate IS NULL)";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "(\"public\".doc_out.doc_control_status =TRUE) AND (\"public\".doc_out.doc_control_executiondate IS NULL)";
$proto2["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "(\"public\".doc_out.doc_control_status =TRUE) AND (\"public\".doc_out.doc_control_executiondate IS NULL)"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
						$proto4=array();
$proto4["m_sql"] = "\"public\".doc_out.doc_control_status =TRUE";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "=TRUE";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = true;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

			$proto2["m_contained"][]=$obj;
						$proto6=array();
$proto6["m_sql"] = "\"public\".doc_out.doc_control_executiondate IS NULL";
$proto6["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto6["m_column"]=$obj;
$proto6["m_contained"] = array();
$proto6["m_strCase"] = "IS NULL";
$proto6["m_havingmode"] = false;
$proto6["m_inBrackets"] = true;
$proto6["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto6);

			$proto2["m_contained"][]=$obj;
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto8=array();
$proto8["m_sql"] = "";
$proto8["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto8["m_column"]=$obj;
$proto8["m_contained"] = array();
$proto8["m_strCase"] = "";
$proto8["m_havingmode"] = false;
$proto8["m_inBrackets"] = false;
$proto8["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto8);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_num",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto10["m_sql"] = "\"public\".doc_out.doc_num";
$proto10["m_srcTableName"] = "report_docout";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_date",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto12["m_sql"] = "\"public\".doc_out.doc_date";
$proto12["m_srcTableName"] = "report_docout";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "type_name",
	"m_strTable" => "public.spr_doc_type",
	"m_srcTableName" => "report_docout"
));

$proto14["m_sql"] = "\"public\".spr_doc_type.type_name";
$proto14["m_srcTableName"] = "report_docout";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_name",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "report_docout"
));

$proto16["m_sql"] = "\"public\".spr_partners.partner_name";
$proto16["m_srcTableName"] = "report_docout";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_name",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto18["m_sql"] = "\"public\".doc_out.doc_name";
$proto18["m_srcTableName"] = "report_docout";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_answer",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto20["m_sql"] = "\"public\".doc_out.doc_answer";
$proto20["m_srcTableName"] = "report_docout";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_desc",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto22["m_sql"] = "\"public\".doc_out.doc_desc";
$proto22["m_srcTableName"] = "report_docout";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_fio",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "report_docout"
));

$proto24["m_sql"] = "\"public\".sotrudnik.sotrudnik_fio";
$proto24["m_srcTableName"] = "report_docout";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_status",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto26["m_sql"] = "\"public\".doc_out.doc_control_status";
$proto26["m_srcTableName"] = "report_docout";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_date",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto28["m_sql"] = "\"public\".doc_out.doc_control_date";
$proto28["m_srcTableName"] = "report_docout";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_control_executiondate",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto30["m_sql"] = "\"public\".doc_out.doc_control_executiondate";
$proto30["m_srcTableName"] = "report_docout";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_file",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto32["m_sql"] = "\"public\".doc_out.doc_file";
$proto32["m_srcTableName"] = "report_docout";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "public.doc_out";
$proto35["m_srcTableName"] = "report_docout";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "docout_id";
$proto35["m_columns"][] = "doc_num";
$proto35["m_columns"][] = "doc_date";
$proto35["m_columns"][] = "doc_answer";
$proto35["m_columns"][] = "doc_type";
$proto35["m_columns"][] = "doc_destination";
$proto35["m_columns"][] = "doc_executor";
$proto35["m_columns"][] = "doc_name";
$proto35["m_columns"][] = "doc_desc";
$proto35["m_columns"][] = "doc_control_date";
$proto35["m_columns"][] = "doc_control_status";
$proto35["m_columns"][] = "doc_control_executiondate";
$proto35["m_columns"][] = "doc_file";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "\"public\".doc_out";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "report_docout";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
												$proto38=array();
$proto38["m_link"] = "SQLL_INNERJOIN";
			$proto39=array();
$proto39["m_strName"] = "public.sotrudnik";
$proto39["m_srcTableName"] = "report_docout";
$proto39["m_columns"] = array();
$proto39["m_columns"][] = "sotrudnik_id";
$proto39["m_columns"][] = "sotrudnik_fio";
$proto39["m_columns"][] = "sotrudnik_login";
$proto39["m_columns"][] = "sotrudnik_dep";
$proto39["m_columns"][] = "sotrudnik_photo";
$proto39["m_columns"][] = "sotrudnik_dismissed";
$obj = new SQLTable($proto39);

$proto38["m_table"] = $obj;
$proto38["m_sql"] = "INNER JOIN \"public\".sotrudnik ON \"public\".doc_out.doc_executor = \"public\".sotrudnik.sotrudnik_id";
$proto38["m_alias"] = "";
$proto38["m_srcTableName"] = "report_docout";
$proto40=array();
$proto40["m_sql"] = "\"public\".doc_out.doc_executor = \"public\".sotrudnik.sotrudnik_id";
$proto40["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_executor",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto40["m_column"]=$obj;
$proto40["m_contained"] = array();
$proto40["m_strCase"] = "= \"public\".sotrudnik.sotrudnik_id";
$proto40["m_havingmode"] = false;
$proto40["m_inBrackets"] = false;
$proto40["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto40);

$proto38["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto38);

$proto0["m_fromlist"][]=$obj;
												$proto42=array();
$proto42["m_link"] = "SQLL_INNERJOIN";
			$proto43=array();
$proto43["m_strName"] = "public.spr_partners";
$proto43["m_srcTableName"] = "report_docout";
$proto43["m_columns"] = array();
$proto43["m_columns"][] = "partner_id";
$proto43["m_columns"][] = "partner_name";
$proto43["m_columns"][] = "partner_adress";
$proto43["m_columns"][] = "partner_telephone";
$proto43["m_columns"][] = "partner_contact";
$obj = new SQLTable($proto43);

$proto42["m_table"] = $obj;
$proto42["m_sql"] = "INNER JOIN \"public\".spr_partners ON \"public\".doc_out.doc_destination = \"public\".spr_partners.partner_id";
$proto42["m_alias"] = "";
$proto42["m_srcTableName"] = "report_docout";
$proto44=array();
$proto44["m_sql"] = "\"public\".doc_out.doc_destination = \"public\".spr_partners.partner_id";
$proto44["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_destination",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto44["m_column"]=$obj;
$proto44["m_contained"] = array();
$proto44["m_strCase"] = "= \"public\".spr_partners.partner_id";
$proto44["m_havingmode"] = false;
$proto44["m_inBrackets"] = false;
$proto44["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto44);

$proto42["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto42);

$proto0["m_fromlist"][]=$obj;
												$proto46=array();
$proto46["m_link"] = "SQLL_INNERJOIN";
			$proto47=array();
$proto47["m_strName"] = "public.spr_doc_type";
$proto47["m_srcTableName"] = "report_docout";
$proto47["m_columns"] = array();
$proto47["m_columns"][] = "doctype_id";
$proto47["m_columns"][] = "type_name";
$obj = new SQLTable($proto47);

$proto46["m_table"] = $obj;
$proto46["m_sql"] = "INNER JOIN \"public\".spr_doc_type ON \"public\".doc_out.doc_type = \"public\".spr_doc_type.doctype_id";
$proto46["m_alias"] = "";
$proto46["m_srcTableName"] = "report_docout";
$proto48=array();
$proto48["m_sql"] = "\"public\".doc_out.doc_type = \"public\".spr_doc_type.doctype_id";
$proto48["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "doc_type",
	"m_strTable" => "public.doc_out",
	"m_srcTableName" => "report_docout"
));

$proto48["m_column"]=$obj;
$proto48["m_contained"] = array();
$proto48["m_strCase"] = "= \"public\".spr_doc_type.doctype_id";
$proto48["m_havingmode"] = false;
$proto48["m_inBrackets"] = false;
$proto48["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto48);

$proto46["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto46);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="report_docout";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_report_docout = createSqlQuery_report_docout();


	
		;

												

$tdatareport_docout[".sqlquery"] = $queryData_report_docout;



$tableEvents["report_docout"] = new eventsBase;
$tdatareport_docout[".hasEvents"] = false;

?>