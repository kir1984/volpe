<?php
$tdatareport_licenses_used = array();
$tdatareport_licenses_used[".searchableFields"] = array();
$tdatareport_licenses_used[".ShortName"] = "report_licenses_used";
$tdatareport_licenses_used[".OwnerID"] = "";
$tdatareport_licenses_used[".OriginalTable"] = "public.spr_license";


$tdatareport_licenses_used[".pagesByType"] = my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" );
$tdatareport_licenses_used[".originalPagesByType"] = $tdatareport_licenses_used[".pagesByType"];
$tdatareport_licenses_used[".pages"] = types2pages( my_json_decode( "{\"masterreport\":[\"masterreport\"],\"masterrprint\":[\"masterrprint\"],\"report\":[\"report\"],\"rprint\":[\"rprint\"],\"search\":[\"search\"]}" ) );
$tdatareport_licenses_used[".originalPages"] = $tdatareport_licenses_used[".pages"];
$tdatareport_licenses_used[".defaultPages"] = my_json_decode( "{\"masterreport\":\"masterreport\",\"masterrprint\":\"masterrprint\",\"report\":\"report\",\"rprint\":\"rprint\",\"search\":\"search\"}" );
$tdatareport_licenses_used[".originalDefaultPages"] = $tdatareport_licenses_used[".defaultPages"];

//	field labels
$fieldLabelsreport_licenses_used = array();
$fieldToolTipsreport_licenses_used = array();
$pageTitlesreport_licenses_used = array();
$placeHoldersreport_licenses_used = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsreport_licenses_used["Russian"] = array();
	$fieldToolTipsreport_licenses_used["Russian"] = array();
	$placeHoldersreport_licenses_used["Russian"] = array();
	$pageTitlesreport_licenses_used["Russian"] = array();
	$fieldLabelsreport_licenses_used["Russian"]["lic_key"] = "Ключ";
	$fieldToolTipsreport_licenses_used["Russian"]["lic_key"] = "";
	$placeHoldersreport_licenses_used["Russian"]["lic_key"] = "";
	$fieldLabelsreport_licenses_used["Russian"]["po_name"] = "Наименование ПО";
	$fieldToolTipsreport_licenses_used["Russian"]["po_name"] = "";
	$placeHoldersreport_licenses_used["Russian"]["po_name"] = "";
	$fieldLabelsreport_licenses_used["Russian"]["lic_quantity"] = "Имеющиеся лицензии";
	$fieldToolTipsreport_licenses_used["Russian"]["lic_quantity"] = "";
	$placeHoldersreport_licenses_used["Russian"]["lic_quantity"] = "";
	$fieldLabelsreport_licenses_used["Russian"]["count"] = "Используемые лицензии";
	$fieldToolTipsreport_licenses_used["Russian"]["count"] = "";
	$placeHoldersreport_licenses_used["Russian"]["count"] = "";
	if (count($fieldToolTipsreport_licenses_used["Russian"]))
		$tdatareport_licenses_used[".isUseToolTips"] = true;
}


	$tdatareport_licenses_used[".NCSearch"] = true;



$tdatareport_licenses_used[".shortTableName"] = "report_licenses_used";
$tdatareport_licenses_used[".nSecOptions"] = 0;

$tdatareport_licenses_used[".mainTableOwnerID"] = "";
$tdatareport_licenses_used[".entityType"] = 2;
$tdatareport_licenses_used[".connId"] = "itbase3_at_192_168_1_15";


$tdatareport_licenses_used[".strOriginalTableName"] = "public.spr_license";

	



$tdatareport_licenses_used[".showAddInPopup"] = false;

$tdatareport_licenses_used[".showEditInPopup"] = false;

$tdatareport_licenses_used[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatareport_licenses_used[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatareport_licenses_used[".listAjax"] = false;
//	temporary
$tdatareport_licenses_used[".listAjax"] = false;

	$tdatareport_licenses_used[".audit"] = false;

	$tdatareport_licenses_used[".locking"] = false;


$pages = $tdatareport_licenses_used[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatareport_licenses_used[".edit"] = true;
	$tdatareport_licenses_used[".afterEditAction"] = 1;
	$tdatareport_licenses_used[".closePopupAfterEdit"] = 1;
	$tdatareport_licenses_used[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatareport_licenses_used[".add"] = true;
$tdatareport_licenses_used[".afterAddAction"] = 1;
$tdatareport_licenses_used[".closePopupAfterAdd"] = 1;
$tdatareport_licenses_used[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatareport_licenses_used[".list"] = true;
}



$tdatareport_licenses_used[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatareport_licenses_used[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatareport_licenses_used[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatareport_licenses_used[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatareport_licenses_used[".printFriendly"] = true;
}



$tdatareport_licenses_used[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatareport_licenses_used[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatareport_licenses_used[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatareport_licenses_used[".isUseAjaxSuggest"] = true;






$tdatareport_licenses_used[".ajaxCodeSnippetAdded"] = false;

$tdatareport_licenses_used[".buttonsAdded"] = false;

$tdatareport_licenses_used[".addPageEvents"] = false;

// use timepicker for search panel
$tdatareport_licenses_used[".isUseTimeForSearch"] = false;


$tdatareport_licenses_used[".badgeColor"] = "E07878";


$tdatareport_licenses_used[".allSearchFields"] = array();
$tdatareport_licenses_used[".filterFields"] = array();
$tdatareport_licenses_used[".requiredSearchFields"] = array();

$tdatareport_licenses_used[".googleLikeFields"] = array();
$tdatareport_licenses_used[".googleLikeFields"][] = "po_name";
$tdatareport_licenses_used[".googleLikeFields"][] = "lic_key";
$tdatareport_licenses_used[".googleLikeFields"][] = "lic_quantity";
$tdatareport_licenses_used[".googleLikeFields"][] = "count";



$tdatareport_licenses_used[".tableType"] = "report";

$tdatareport_licenses_used[".printerPageOrientation"] = 1;
$tdatareport_licenses_used[".nPrinterPageScale"] = 100;

$tdatareport_licenses_used[".nPrinterSplitRecords"] = 40;

$tdatareport_licenses_used[".geocodingEnabled"] = false;

//report settings
$tdatareport_licenses_used[".printReportLayout"] = 1;

$tdatareport_licenses_used[".reportPrintPartitionType"] = 1;
$tdatareport_licenses_used[".reportPrintGroupsPerPage"] = 3;
$tdatareport_licenses_used[".lowGroup"] = 4;



$tdatareport_licenses_used[".reportGroupFields"] = true;
$tdatareport_licenses_used[".pageSize"] = 5;
$reportGroupFieldsList = array();
$reportGroupFields = array();
	$reportGroupFieldsList []= "po_name";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "po_name";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 1;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
	$reportGroupFieldsList []= "lic_key";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "lic_key";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 2;
	$rgroupField['showGroupSummary'] = "0";
	$rgroupField['crossTabAxis'] = "1";
	$reportGroupFields[] = $rgroupField;
	$reportGroupFieldsList []= "lic_quantity";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "lic_quantity";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 3;
	$rgroupField['showGroupSummary'] = "0";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
	$reportGroupFieldsList []= "count";

	$rgroupField = array();
	$rgroupField['strGroupField'] = "count";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 4;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
$tdatareport_licenses_used[".reportGroupFieldsData"] = $reportGroupFields;
$tdatareport_licenses_used[".reportGroupFieldsList"] = $reportGroupFieldsList;


$tdatareport_licenses_used[".isExistTotalFields"] = true;





$tdatareport_licenses_used[".reportLayout"] = 1;

//end of report settings










$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatareport_licenses_used[".strOrderBy"] = $tstrOrderBy;

$tdatareport_licenses_used[".orderindexes"] = array();


$tdatareport_licenses_used[".sqlHead"] = "SELECT (	SELECT  			po_name   		FROM  			spr_po   		WHERE  			spr_license.lic_poname=spr_po.po_id),  spr_license.lic_key,  spr_license.lic_quantity,  COUNT(arm_po.po_key)";
$tdatareport_licenses_used[".sqlFrom"] = "FROM \"public\".spr_license AS spr_license  , \"public\".arm_po AS arm_po";
$tdatareport_licenses_used[".sqlWhereExpr"] = "(spr_license.lic_id = \"arm_po\".\"po_key\")";
$tdatareport_licenses_used[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatareport_licenses_used[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatareport_licenses_used[".arrGroupsPerPage"] = $arrGPP;

$tdatareport_licenses_used[".highlightSearchResults"] = true;

$tableKeysreport_licenses_used = array();
$tdatareport_licenses_used[".Keys"] = $tableKeysreport_licenses_used;


$tdatareport_licenses_used[".hideMobileList"] = array();




//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("report_licenses_used","po_name");
	$fdata["FieldType"] = 200;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "(	SELECT  			po_name   		FROM  			spr_po   		WHERE  			spr_license.lic_poname=spr_po.po_id)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_licenses_used["po_name"] = $fdata;
		$tdatareport_licenses_used[".searchableFields"][] = "po_name";
//	lic_key
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "lic_key";
	$fdata["GoodName"] = "lic_key";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("report_licenses_used","lic_key");
	$fdata["FieldType"] = 200;

		// report field settings
		$fdata["isTotalMin"] = true;
				// end of report field settings

	
	
			

		$fdata["strField"] = "lic_key";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "spr_license.lic_key";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_licenses_used["lic_key"] = $fdata;
		$tdatareport_licenses_used[".searchableFields"][] = "lic_key";
//	lic_quantity
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "lic_quantity";
	$fdata["GoodName"] = "lic_quantity";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("report_licenses_used","lic_quantity");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "lic_quantity";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "spr_license.lic_quantity";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_licenses_used["lic_quantity"] = $fdata;
		$tdatareport_licenses_used[".searchableFields"][] = "lic_quantity";
//	count
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "count";
	$fdata["GoodName"] = "count";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("report_licenses_used","count");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			

		$fdata["strField"] = "count";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "COUNT(arm_po.po_key)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatareport_licenses_used["count"] = $fdata;
		$tdatareport_licenses_used[".searchableFields"][] = "count";


$tables_data["report_licenses_used"]=&$tdatareport_licenses_used;
$field_labels["report_licenses_used"] = &$fieldLabelsreport_licenses_used;
$fieldToolTips["report_licenses_used"] = &$fieldToolTipsreport_licenses_used;
$placeHolders["report_licenses_used"] = &$placeHoldersreport_licenses_used;
$page_titles["report_licenses_used"] = &$pageTitlesreport_licenses_used;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["report_licenses_used"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["report_licenses_used"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_report_licenses_used()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "(	SELECT  			po_name   		FROM  			spr_po   		WHERE  			spr_license.lic_poname=spr_po.po_id),  spr_license.lic_key,  spr_license.lic_quantity,  COUNT(arm_po.po_key)";
$proto0["m_strFrom"] = "FROM \"public\".spr_license AS spr_license  , \"public\".arm_po AS arm_po";
$proto0["m_strWhere"] = "(spr_license.lic_id = \"arm_po\".\"po_key\")";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "spr_license.lic_id = \"arm_po\".\"po_key\"";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "lic_id",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "= \"arm_po\".\"po_key\"";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "(	SELECT  			po_name   		FROM  			spr_po   		WHERE  			spr_license.lic_poname=spr_po.po_id)"
));

$proto6["m_sql"] = "(	SELECT  			po_name   		FROM  			spr_po   		WHERE  			spr_license.lic_poname=spr_po.po_id)";
$proto6["m_srcTableName"] = "report_licenses_used";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_key",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto8["m_sql"] = "spr_license.lic_key";
$proto8["m_srcTableName"] = "report_licenses_used";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_quantity",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto10["m_sql"] = "spr_license.lic_quantity";
$proto10["m_srcTableName"] = "report_licenses_used";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$proto13=array();
$proto13["m_functiontype"] = "SQLF_COUNT";
$proto13["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "po_key",
	"m_strTable" => "arm_po",
	"m_srcTableName" => "report_licenses_used"
));

$proto13["m_arguments"][]=$obj;
$proto13["m_strFunctionName"] = "COUNT";
$obj = new SQLFunctionCall($proto13);

$proto12["m_sql"] = "COUNT(arm_po.po_key)";
$proto12["m_srcTableName"] = "report_licenses_used";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto15=array();
$proto15["m_link"] = "SQLL_MAIN";
			$proto16=array();
$proto16["m_strName"] = "public.spr_license";
$proto16["m_srcTableName"] = "report_licenses_used";
$proto16["m_columns"] = array();
$proto16["m_columns"][] = "lic_id";
$proto16["m_columns"][] = "lic_poname";
$proto16["m_columns"][] = "lic_type";
$proto16["m_columns"][] = "lic_quantity";
$proto16["m_columns"][] = "lic_dogovor_number";
$proto16["m_columns"][] = "lic_dogovor_date";
$proto16["m_columns"][] = "lic_dogovor_contractor";
$proto16["m_columns"][] = "lic_desc";
$proto16["m_columns"][] = "lic_key";
$proto16["m_columns"][] = "lic_servicenumber";
$obj = new SQLTable($proto16);

$proto15["m_table"] = $obj;
$proto15["m_sql"] = "\"public\".spr_license AS spr_license";
$proto15["m_alias"] = "spr_license";
$proto15["m_srcTableName"] = "report_licenses_used";
$proto17=array();
$proto17["m_sql"] = "";
$proto17["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto17["m_column"]=$obj;
$proto17["m_contained"] = array();
$proto17["m_strCase"] = "";
$proto17["m_havingmode"] = false;
$proto17["m_inBrackets"] = false;
$proto17["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto17);

$proto15["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto15);

$proto0["m_fromlist"][]=$obj;
												$proto19=array();
$proto19["m_link"] = "SQLL_CROSSJOIN";
			$proto20=array();
$proto20["m_strName"] = "public.arm_po";
$proto20["m_srcTableName"] = "report_licenses_used";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "armpo_id";
$proto20["m_columns"][] = "boxid";
$proto20["m_columns"][] = "po_name";
$proto20["m_columns"][] = "po_version";
$proto20["m_columns"][] = "po_arch";
$proto20["m_columns"][] = "po_key";
$proto20["m_columns"][] = "po_desc";
$proto20["m_columns"][] = "box_id";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "\"public\".arm_po AS arm_po";
$proto19["m_alias"] = "arm_po";
$proto19["m_srcTableName"] = "report_licenses_used";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "lic_key",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto23["m_column"]=$obj;
$obj = new SQLGroupByItem($proto23);

$proto0["m_groupby"][]=$obj;
												$proto25=array();
						$obj = new SQLField(array(
	"m_strName" => "lic_quantity",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto25["m_column"]=$obj;
$obj = new SQLGroupByItem($proto25);

$proto0["m_groupby"][]=$obj;
												$proto27=array();
						$obj = new SQLField(array(
	"m_strName" => "lic_poname",
	"m_strTable" => "spr_license",
	"m_srcTableName" => "report_licenses_used"
));

$proto27["m_column"]=$obj;
$obj = new SQLGroupByItem($proto27);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="report_licenses_used";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_report_licenses_used = createSqlQuery_report_licenses_used();


	
		;

				

$tdatareport_licenses_used[".sqlquery"] = $queryData_report_licenses_used;



$tableEvents["report_licenses_used"] = new eventsBase;
$tdatareport_licenses_used[".hasEvents"] = false;

?>