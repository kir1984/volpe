<?php
$tdataservice_refilling = array();
$tdataservice_refilling[".searchableFields"] = array();
$tdataservice_refilling[".ShortName"] = "service_refilling";
$tdataservice_refilling[".OwnerID"] = "";
$tdataservice_refilling[".OriginalTable"] = "public.service_refilling";


$tdataservice_refilling[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataservice_refilling[".originalPagesByType"] = $tdataservice_refilling[".pagesByType"];
$tdataservice_refilling[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataservice_refilling[".originalPages"] = $tdataservice_refilling[".pages"];
$tdataservice_refilling[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataservice_refilling[".originalDefaultPages"] = $tdataservice_refilling[".defaultPages"];

//	field labels
$fieldLabelsservice_refilling = array();
$fieldToolTipsservice_refilling = array();
$pageTitlesservice_refilling = array();
$placeHoldersservice_refilling = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsservice_refilling["Russian"] = array();
	$fieldToolTipsservice_refilling["Russian"] = array();
	$placeHoldersservice_refilling["Russian"] = array();
	$pageTitlesservice_refilling["Russian"] = array();
	$fieldLabelsservice_refilling["Russian"]["conscart_id"] = "Картридж";
	$fieldToolTipsservice_refilling["Russian"]["conscart_id"] = "";
	$placeHoldersservice_refilling["Russian"]["conscart_id"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_partner"] = "Контрагент";
	$fieldToolTipsservice_refilling["Russian"]["ref_partner"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_partner"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_date"] = "Дата отправки";
	$fieldToolTipsservice_refilling["Russian"]["ref_date"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_date"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_datereturn"] = "Дата возвращения";
	$fieldToolTipsservice_refilling["Russian"]["ref_datereturn"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_datereturn"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_statusreturn"] = "Статус при возврате";
	$fieldToolTipsservice_refilling["Russian"]["ref_statusreturn"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_statusreturn"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_statuscurrent"] = "Статус отправки";
	$fieldToolTipsservice_refilling["Russian"]["ref_statuscurrent"] = "<p>Если картридж <strong>вернулся </strong>от контрагента - <strong>снимаем галочку</strong></p>";
	$placeHoldersservice_refilling["Russian"]["ref_statuscurrent"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_comment"] = "Комментарии";
	$fieldToolTipsservice_refilling["Russian"]["ref_comment"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_comment"] = "";
	$fieldLabelsservice_refilling["Russian"]["ref_id"] = "Ref Id";
	$fieldToolTipsservice_refilling["Russian"]["ref_id"] = "";
	$placeHoldersservice_refilling["Russian"]["ref_id"] = "";
	if (count($fieldToolTipsservice_refilling["Russian"]))
		$tdataservice_refilling[".isUseToolTips"] = true;
}


	$tdataservice_refilling[".NCSearch"] = true;



$tdataservice_refilling[".shortTableName"] = "service_refilling";
$tdataservice_refilling[".nSecOptions"] = 0;

$tdataservice_refilling[".mainTableOwnerID"] = "";
$tdataservice_refilling[".entityType"] = 0;
$tdataservice_refilling[".connId"] = "itbase3_at_192_168_1_15";


$tdataservice_refilling[".strOriginalTableName"] = "public.service_refilling";

	



$tdataservice_refilling[".showAddInPopup"] = false;

$tdataservice_refilling[".showEditInPopup"] = false;

$tdataservice_refilling[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataservice_refilling[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataservice_refilling[".listAjax"] = true;
//	temporary
$tdataservice_refilling[".listAjax"] = false;

	$tdataservice_refilling[".audit"] = false;

	$tdataservice_refilling[".locking"] = false;


$pages = $tdataservice_refilling[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataservice_refilling[".edit"] = true;
	$tdataservice_refilling[".afterEditAction"] = 1;
	$tdataservice_refilling[".closePopupAfterEdit"] = 1;
	$tdataservice_refilling[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataservice_refilling[".add"] = true;
$tdataservice_refilling[".afterAddAction"] = 1;
$tdataservice_refilling[".closePopupAfterAdd"] = 1;
$tdataservice_refilling[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataservice_refilling[".list"] = true;
}



$tdataservice_refilling[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataservice_refilling[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataservice_refilling[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataservice_refilling[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataservice_refilling[".printFriendly"] = true;
}



$tdataservice_refilling[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataservice_refilling[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataservice_refilling[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataservice_refilling[".isUseAjaxSuggest"] = true;

$tdataservice_refilling[".rowHighlite"] = true;





$tdataservice_refilling[".ajaxCodeSnippetAdded"] = false;

$tdataservice_refilling[".buttonsAdded"] = false;

$tdataservice_refilling[".addPageEvents"] = false;

// use timepicker for search panel
$tdataservice_refilling[".isUseTimeForSearch"] = false;


$tdataservice_refilling[".badgeColor"] = "BC8F8F";


$tdataservice_refilling[".allSearchFields"] = array();
$tdataservice_refilling[".filterFields"] = array();
$tdataservice_refilling[".requiredSearchFields"] = array();

$tdataservice_refilling[".googleLikeFields"] = array();
$tdataservice_refilling[".googleLikeFields"][] = "conscart_id";
$tdataservice_refilling[".googleLikeFields"][] = "ref_partner";
$tdataservice_refilling[".googleLikeFields"][] = "ref_date";
$tdataservice_refilling[".googleLikeFields"][] = "ref_datereturn";
$tdataservice_refilling[".googleLikeFields"][] = "ref_statusreturn";
$tdataservice_refilling[".googleLikeFields"][] = "ref_statuscurrent";
$tdataservice_refilling[".googleLikeFields"][] = "ref_comment";
$tdataservice_refilling[".googleLikeFields"][] = "ref_id";



$tdataservice_refilling[".tableType"] = "list";

$tdataservice_refilling[".printerPageOrientation"] = 0;
$tdataservice_refilling[".nPrinterPageScale"] = 100;

$tdataservice_refilling[".nPrinterSplitRecords"] = 40;

$tdataservice_refilling[".geocodingEnabled"] = false;




$tdataservice_refilling[".isDisplayLoading"] = true;

$tdataservice_refilling[".isResizeColumns"] = true;





$tdataservice_refilling[".pageSize"] = 20;

$tdataservice_refilling[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataservice_refilling[".strOrderBy"] = $tstrOrderBy;

$tdataservice_refilling[".orderindexes"] = array();


$tdataservice_refilling[".sqlHead"] = "SELECT conscart_id,  ref_partner,  ref_date,  ref_datereturn,  ref_statusreturn,  ref_statuscurrent,  ref_comment,  ref_id";
$tdataservice_refilling[".sqlFrom"] = "FROM \"public\".service_refilling";
$tdataservice_refilling[".sqlWhereExpr"] = "";
$tdataservice_refilling[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataservice_refilling[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataservice_refilling[".arrGroupsPerPage"] = $arrGPP;

$tdataservice_refilling[".highlightSearchResults"] = true;

$tableKeysservice_refilling = array();
$tableKeysservice_refilling[] = "ref_id";
$tdataservice_refilling[".Keys"] = $tableKeysservice_refilling;


$tdataservice_refilling[".hideMobileList"] = array();




//	conscart_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "conscart_id";
	$fdata["GoodName"] = "conscart_id";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","conscart_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "conscart_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "conscart_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.cons_cartridge";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "conscart_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "cartridge_inv || ': ' || cartridge_type || ' ' || cartridge_name ";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
		$fdata["filterTotalFields"] = "conscart_id";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["conscart_id"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "conscart_id";
//	ref_partner
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ref_partner";
	$fdata["GoodName"] = "ref_partner";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_partner");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "ref_partner";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_partner";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_partners";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "partner_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "partner_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_partner"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_partner";
//	ref_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ref_date";
	$fdata["GoodName"] = "ref_date";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_date");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "ref_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 1;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_date"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_date";
//	ref_datereturn
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ref_datereturn";
	$fdata["GoodName"] = "ref_datereturn";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_datereturn");
	$fdata["FieldType"] = 7;

	
	
	
			

		$fdata["strField"] = "ref_datereturn";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_datereturn";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 1;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_datereturn"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_datereturn";
//	ref_statusreturn
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ref_statusreturn";
	$fdata["GoodName"] = "ref_statusreturn";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_statusreturn");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ref_statusreturn";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_statusreturn";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Заправлен";
	$edata["LookupValues"][] = "Восстановлен";
	$edata["LookupValues"][] = "Неремонтопригоден";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
		$fdata["filterTotalFields"] = "conscart_id";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_statusreturn"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_statusreturn";
//	ref_statuscurrent
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ref_statuscurrent";
	$fdata["GoodName"] = "ref_statuscurrent";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_statuscurrent");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "ref_statuscurrent";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_statuscurrent";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdataservice_refilling["ref_statuscurrent"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_statuscurrent";
//	ref_comment
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ref_comment";
	$fdata["GoodName"] = "ref_comment";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_comment");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ref_comment";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_comment";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=1024";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_comment"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_comment";
//	ref_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ref_id";
	$fdata["GoodName"] = "ref_id";
	$fdata["ownerTable"] = "public.service_refilling";
	$fdata["Label"] = GetFieldLabel("public_service_refilling","ref_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "ref_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ref_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataservice_refilling["ref_id"] = $fdata;
		$tdataservice_refilling[".searchableFields"][] = "ref_id";


$tables_data["public.service_refilling"]=&$tdataservice_refilling;
$field_labels["public_service_refilling"] = &$fieldLabelsservice_refilling;
$fieldToolTips["public_service_refilling"] = &$fieldToolTipsservice_refilling;
$placeHolders["public_service_refilling"] = &$placeHoldersservice_refilling;
$page_titles["public_service_refilling"] = &$pageTitlesservice_refilling;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.service_refilling"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.service_refilling"] = array();



	
				$strOriginalDetailsTable="public.spr_partners";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_partners";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_partners";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.service_refilling"][0] = $masterParams;
				$masterTablesData["public.service_refilling"][0]["masterKeys"] = array();
	$masterTablesData["public.service_refilling"][0]["masterKeys"][]="partner_id";
				$masterTablesData["public.service_refilling"][0]["detailKeys"] = array();
	$masterTablesData["public.service_refilling"][0]["detailKeys"][]="ref_partner";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_service_refilling()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "conscart_id,  ref_partner,  ref_date,  ref_datereturn,  ref_statusreturn,  ref_statuscurrent,  ref_comment,  ref_id";
$proto0["m_strFrom"] = "FROM \"public\".service_refilling";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "conscart_id",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto6["m_sql"] = "conscart_id";
$proto6["m_srcTableName"] = "public.service_refilling";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_partner",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto8["m_sql"] = "ref_partner";
$proto8["m_srcTableName"] = "public.service_refilling";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_date",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto10["m_sql"] = "ref_date";
$proto10["m_srcTableName"] = "public.service_refilling";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_datereturn",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto12["m_sql"] = "ref_datereturn";
$proto12["m_srcTableName"] = "public.service_refilling";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_statusreturn",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto14["m_sql"] = "ref_statusreturn";
$proto14["m_srcTableName"] = "public.service_refilling";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_statuscurrent",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto16["m_sql"] = "ref_statuscurrent";
$proto16["m_srcTableName"] = "public.service_refilling";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_comment",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto18["m_sql"] = "ref_comment";
$proto18["m_srcTableName"] = "public.service_refilling";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ref_id",
	"m_strTable" => "public.service_refilling",
	"m_srcTableName" => "public.service_refilling"
));

$proto20["m_sql"] = "ref_id";
$proto20["m_srcTableName"] = "public.service_refilling";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "public.service_refilling";
$proto23["m_srcTableName"] = "public.service_refilling";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "ref_id";
$proto23["m_columns"][] = "conscart_id";
$proto23["m_columns"][] = "ref_partner";
$proto23["m_columns"][] = "ref_date";
$proto23["m_columns"][] = "ref_datereturn";
$proto23["m_columns"][] = "ref_statusreturn";
$proto23["m_columns"][] = "ref_statuscurrent";
$proto23["m_columns"][] = "ref_comment";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "\"public\".service_refilling";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "public.service_refilling";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.service_refilling";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_service_refilling = createSqlQuery_service_refilling();


	
		;

								

$tdataservice_refilling[".sqlquery"] = $queryData_service_refilling;



$tableEvents["public.service_refilling"] = new eventsBase;
$tdataservice_refilling[".hasEvents"] = false;

?>