<?php
$tdatasotrudnik = array();
$tdatasotrudnik[".searchableFields"] = array();
$tdatasotrudnik[".ShortName"] = "sotrudnik";
$tdatasotrudnik[".OwnerID"] = "";
$tdatasotrudnik[".OriginalTable"] = "public.sotrudnik";


$tdatasotrudnik[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatasotrudnik[".originalPagesByType"] = $tdatasotrudnik[".pagesByType"];
$tdatasotrudnik[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatasotrudnik[".originalPages"] = $tdatasotrudnik[".pages"];
$tdatasotrudnik[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatasotrudnik[".originalDefaultPages"] = $tdatasotrudnik[".defaultPages"];

//	field labels
$fieldLabelssotrudnik = array();
$fieldToolTipssotrudnik = array();
$pageTitlessotrudnik = array();
$placeHolderssotrudnik = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelssotrudnik["Russian"] = array();
	$fieldToolTipssotrudnik["Russian"] = array();
	$placeHolderssotrudnik["Russian"] = array();
	$pageTitlessotrudnik["Russian"] = array();
	$fieldLabelssotrudnik["Russian"]["sotrudnik_id"] = "Sotrudnik Id";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_id"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_id"] = "";
	$fieldLabelssotrudnik["Russian"]["sotrudnik_fio"] = "ФИО сотрудника";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_fio"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_fio"] = "";
	$fieldLabelssotrudnik["Russian"]["sotrudnik_login"] = "Логин";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_login"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_login"] = "";
	$fieldLabelssotrudnik["Russian"]["sotrudnik_dep"] = "Подразделение";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_dep"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_dep"] = "";
	$fieldLabelssotrudnik["Russian"]["sotrudnik_photo"] = "Фото";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_photo"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_photo"] = "";
	$fieldLabelssotrudnik["Russian"]["sotrudnik_dismissed"] = "Уволен";
	$fieldToolTipssotrudnik["Russian"]["sotrudnik_dismissed"] = "";
	$placeHolderssotrudnik["Russian"]["sotrudnik_dismissed"] = "";
	if (count($fieldToolTipssotrudnik["Russian"]))
		$tdatasotrudnik[".isUseToolTips"] = true;
}


	$tdatasotrudnik[".NCSearch"] = true;



$tdatasotrudnik[".shortTableName"] = "sotrudnik";
$tdatasotrudnik[".nSecOptions"] = 0;

$tdatasotrudnik[".mainTableOwnerID"] = "";
$tdatasotrudnik[".entityType"] = 0;
$tdatasotrudnik[".connId"] = "itbase3_at_192_168_1_15";


$tdatasotrudnik[".strOriginalTableName"] = "public.sotrudnik";

	



$tdatasotrudnik[".showAddInPopup"] = false;

$tdatasotrudnik[".showEditInPopup"] = false;

$tdatasotrudnik[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatasotrudnik[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdatasotrudnik[".listAjax"] = true;
//	temporary
$tdatasotrudnik[".listAjax"] = false;

	$tdatasotrudnik[".audit"] = true;

	$tdatasotrudnik[".locking"] = true;


$pages = $tdatasotrudnik[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatasotrudnik[".edit"] = true;
	$tdatasotrudnik[".afterEditAction"] = 1;
	$tdatasotrudnik[".closePopupAfterEdit"] = 1;
	$tdatasotrudnik[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatasotrudnik[".add"] = true;
$tdatasotrudnik[".afterAddAction"] = 1;
$tdatasotrudnik[".closePopupAfterAdd"] = 1;
$tdatasotrudnik[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatasotrudnik[".list"] = true;
}



$tdatasotrudnik[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatasotrudnik[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatasotrudnik[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatasotrudnik[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatasotrudnik[".printFriendly"] = true;
}



$tdatasotrudnik[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatasotrudnik[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatasotrudnik[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatasotrudnik[".isUseAjaxSuggest"] = true;

$tdatasotrudnik[".rowHighlite"] = true;





$tdatasotrudnik[".ajaxCodeSnippetAdded"] = false;

$tdatasotrudnik[".buttonsAdded"] = false;

$tdatasotrudnik[".addPageEvents"] = false;

// use timepicker for search panel
$tdatasotrudnik[".isUseTimeForSearch"] = false;


$tdatasotrudnik[".badgeColor"] = "bc8f8f";


$tdatasotrudnik[".allSearchFields"] = array();
$tdatasotrudnik[".filterFields"] = array();
$tdatasotrudnik[".requiredSearchFields"] = array();

$tdatasotrudnik[".googleLikeFields"] = array();
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_id";
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_fio";
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_login";
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_dep";
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_photo";
$tdatasotrudnik[".googleLikeFields"][] = "sotrudnik_dismissed";



$tdatasotrudnik[".tableType"] = "list";

$tdatasotrudnik[".printerPageOrientation"] = 0;
$tdatasotrudnik[".nPrinterPageScale"] = 100;

$tdatasotrudnik[".nPrinterSplitRecords"] = 40;

$tdatasotrudnik[".geocodingEnabled"] = false;




$tdatasotrudnik[".isDisplayLoading"] = true;

$tdatasotrudnik[".isResizeColumns"] = true;





$tdatasotrudnik[".pageSize"] = 20;

$tdatasotrudnik[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatasotrudnik[".strOrderBy"] = $tstrOrderBy;

$tdatasotrudnik[".orderindexes"] = array();


$tdatasotrudnik[".sqlHead"] = "SELECT sotrudnik_id,  	sotrudnik_fio,  	sotrudnik_login,  	sotrudnik_dep,  	sotrudnik_photo,  	sotrudnik_dismissed";
$tdatasotrudnik[".sqlFrom"] = "FROM \"public\".sotrudnik";
$tdatasotrudnik[".sqlWhereExpr"] = "";
$tdatasotrudnik[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatasotrudnik[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatasotrudnik[".arrGroupsPerPage"] = $arrGPP;

$tdatasotrudnik[".highlightSearchResults"] = true;

$tableKeyssotrudnik = array();
$tableKeyssotrudnik[] = "sotrudnik_id";
$tdatasotrudnik[".Keys"] = $tableKeyssotrudnik;


$tdatasotrudnik[".hideMobileList"] = array();




//	sotrudnik_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "sotrudnik_id";
	$fdata["GoodName"] = "sotrudnik_id";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "sotrudnik_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasotrudnik["sotrudnik_id"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_id";
//	sotrudnik_fio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "sotrudnik_fio";
	$fdata["GoodName"] = "sotrudnik_fio";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_fio");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "sotrudnik_fio";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_fio";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasotrudnik["sotrudnik_fio"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_fio";
//	sotrudnik_login
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "sotrudnik_login";
	$fdata["GoodName"] = "sotrudnik_login";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_login");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "sotrudnik_login";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_login";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasotrudnik["sotrudnik_login"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_login";
//	sotrudnik_dep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "sotrudnik_dep";
	$fdata["GoodName"] = "sotrudnik_dep";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_dep");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "sotrudnik_dep";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_dep";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_department";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "dep_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "dep_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasotrudnik["sotrudnik_dep"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_dep";
//	sotrudnik_photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "sotrudnik_photo";
	$fdata["GoodName"] = "sotrudnik_photo";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_photo");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "sotrudnik_photo";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_photo";

		$fdata["DeleteAssociatedFile"] = true;

	
				$fdata["UploadFolder"] = "photo";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "File-based Image");

	
	
				$vdata["ShowThumbnail"] = true;
	$vdata["ThumbWidth"] = 200;
	$vdata["ThumbHeight"] = 150;
	$vdata["ImageWidth"] = 50;
	$vdata["ImageHeight"] = 60;

			$vdata["multipleImgMode"] = 1;
	$vdata["maxImages"] = 0;

			$vdata["showGallery"] = true;
	$vdata["galleryMode"] = 2;
	$vdata["captionMode"] = 3;
	$vdata["captionField"] = "sotrudnik_fio";

	$vdata["imageBorder"] = 0;
	$vdata["imageFullWidth"] = 1;


	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
							$edata["acceptFileTypes"] = "bmp";
			$edata["acceptFileTypesHtml"] = ".bmp";
						$edata["acceptFileTypes"] .= "|jpg";
			$edata["acceptFileTypesHtml"] .= ",.jpg";
						$edata["acceptFileTypes"] .= "|png";
			$edata["acceptFileTypesHtml"] .= ",.png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 250;

				$edata["ResizeImage"] = true;
				$edata["NewSize"] = 600;

	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasotrudnik["sotrudnik_photo"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_photo";
//	sotrudnik_dismissed
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "sotrudnik_dismissed";
	$fdata["GoodName"] = "sotrudnik_dismissed";
	$fdata["ownerTable"] = "public.sotrudnik";
	$fdata["Label"] = GetFieldLabel("public_sotrudnik","sotrudnik_dismissed");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "sotrudnik_dismissed";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sotrudnik_dismissed";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdatasotrudnik["sotrudnik_dismissed"] = $fdata;
		$tdatasotrudnik[".searchableFields"][] = "sotrudnik_dismissed";


$tables_data["public.sotrudnik"]=&$tdatasotrudnik;
$field_labels["public_sotrudnik"] = &$fieldLabelssotrudnik;
$fieldToolTips["public_sotrudnik"] = &$fieldToolTipssotrudnik;
$placeHolders["public_sotrudnik"] = &$placeHolderssotrudnik;
$page_titles["public_sotrudnik"] = &$pageTitlessotrudnik;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.sotrudnik"] = array();
//	public.ecp
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.ecp";
		$detailsParam["dOriginalTable"] = "public.ecp";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ecp";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_ecp");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.sotrudnik"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="ecp_user";
//	public.hw_ibp
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_ibp";
		$detailsParam["dOriginalTable"] = "public.hw_ibp";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_ibp";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_ibp");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.sotrudnik"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="ibp_user";
//	public.doc_in
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_in";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_in";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_in");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.sotrudnik"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="doc_control_executor";
//	public.doc_out
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_out";
		$detailsParam["dOriginalTable"] = "public.doc_out";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_out";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_out");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.sotrudnik"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="doc_executor";
//	public.doc_int
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_int";
		$detailsParam["dOriginalTable"] = "public.doc_int";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_int";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_int");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.sotrudnik"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

	$detailsTablesData["public.sotrudnik"][$dIndex]["masterKeys"][]="sotrudnik_id";

				$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="doc_initiator";

		
	$detailsTablesData["public.sotrudnik"][$dIndex]["detailKeys"][]="doc_executor";

// tables which are master tables for current table (detail)
$masterTablesData["public.sotrudnik"] = array();



	
				$strOriginalDetailsTable="public.arm";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.arm";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "arm";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.sotrudnik"][0] = $masterParams;
				$masterTablesData["public.sotrudnik"][0]["masterKeys"] = array();
	$masterTablesData["public.sotrudnik"][0]["masterKeys"][]="sotrudnik_id";
				$masterTablesData["public.sotrudnik"][0]["detailKeys"] = array();
	$masterTablesData["public.sotrudnik"][0]["detailKeys"][]="sotrudnik_id";
		
	
				$strOriginalDetailsTable="public.spr_department";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_department";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_department";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.sotrudnik"][1] = $masterParams;
				$masterTablesData["public.sotrudnik"][1]["masterKeys"] = array();
	$masterTablesData["public.sotrudnik"][1]["masterKeys"][]="dep_id";
				$masterTablesData["public.sotrudnik"][1]["detailKeys"] = array();
	$masterTablesData["public.sotrudnik"][1]["detailKeys"][]="sotrudnik_dep";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_sotrudnik()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "sotrudnik_id,  	sotrudnik_fio,  	sotrudnik_login,  	sotrudnik_dep,  	sotrudnik_photo,  	sotrudnik_dismissed";
$proto0["m_strFrom"] = "FROM \"public\".sotrudnik";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_id",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto6["m_sql"] = "sotrudnik_id";
$proto6["m_srcTableName"] = "public.sotrudnik";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_fio",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto8["m_sql"] = "sotrudnik_fio";
$proto8["m_srcTableName"] = "public.sotrudnik";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_login",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto10["m_sql"] = "sotrudnik_login";
$proto10["m_srcTableName"] = "public.sotrudnik";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_dep",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto12["m_sql"] = "sotrudnik_dep";
$proto12["m_srcTableName"] = "public.sotrudnik";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_photo",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto14["m_sql"] = "sotrudnik_photo";
$proto14["m_srcTableName"] = "public.sotrudnik";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "sotrudnik_dismissed",
	"m_strTable" => "public.sotrudnik",
	"m_srcTableName" => "public.sotrudnik"
));

$proto16["m_sql"] = "sotrudnik_dismissed";
$proto16["m_srcTableName"] = "public.sotrudnik";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "public.sotrudnik";
$proto19["m_srcTableName"] = "public.sotrudnik";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "sotrudnik_id";
$proto19["m_columns"][] = "sotrudnik_fio";
$proto19["m_columns"][] = "sotrudnik_login";
$proto19["m_columns"][] = "sotrudnik_dep";
$proto19["m_columns"][] = "sotrudnik_photo";
$proto19["m_columns"][] = "sotrudnik_dismissed";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "\"public\".sotrudnik";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "public.sotrudnik";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.sotrudnik";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_sotrudnik = createSqlQuery_sotrudnik();


	
		;

						

$tdatasotrudnik[".sqlquery"] = $queryData_sotrudnik;



$tableEvents["public.sotrudnik"] = new eventsBase;
$tdatasotrudnik[".hasEvents"] = false;

?>