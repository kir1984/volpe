<?php
$tdataspr_department = array();
$tdataspr_department[".searchableFields"] = array();
$tdataspr_department[".ShortName"] = "spr_department";
$tdataspr_department[".OwnerID"] = "";
$tdataspr_department[".OriginalTable"] = "public.spr_department";


$tdataspr_department[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_department[".originalPagesByType"] = $tdataspr_department[".pagesByType"];
$tdataspr_department[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_department[".originalPages"] = $tdataspr_department[".pages"];
$tdataspr_department[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_department[".originalDefaultPages"] = $tdataspr_department[".defaultPages"];

//	field labels
$fieldLabelsspr_department = array();
$fieldToolTipsspr_department = array();
$pageTitlesspr_department = array();
$placeHoldersspr_department = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_department["Russian"] = array();
	$fieldToolTipsspr_department["Russian"] = array();
	$placeHoldersspr_department["Russian"] = array();
	$pageTitlesspr_department["Russian"] = array();
	$fieldLabelsspr_department["Russian"]["dep_id"] = "Dep Id";
	$fieldToolTipsspr_department["Russian"]["dep_id"] = "";
	$placeHoldersspr_department["Russian"]["dep_id"] = "";
	$fieldLabelsspr_department["Russian"]["dep_name"] = "Наименование";
	$fieldToolTipsspr_department["Russian"]["dep_name"] = "";
	$placeHoldersspr_department["Russian"]["dep_name"] = "";
	$fieldLabelsspr_department["Russian"]["dep_desc"] = "Описание";
	$fieldToolTipsspr_department["Russian"]["dep_desc"] = "";
	$placeHoldersspr_department["Russian"]["dep_desc"] = "";
	if (count($fieldToolTipsspr_department["Russian"]))
		$tdataspr_department[".isUseToolTips"] = true;
}


	$tdataspr_department[".NCSearch"] = true;



$tdataspr_department[".shortTableName"] = "spr_department";
$tdataspr_department[".nSecOptions"] = 0;

$tdataspr_department[".mainTableOwnerID"] = "";
$tdataspr_department[".entityType"] = 0;
$tdataspr_department[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_department[".strOriginalTableName"] = "public.spr_department";

	



$tdataspr_department[".showAddInPopup"] = false;

$tdataspr_department[".showEditInPopup"] = false;

$tdataspr_department[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_department[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_department[".listAjax"] = true;
//	temporary
$tdataspr_department[".listAjax"] = false;

	$tdataspr_department[".audit"] = true;

	$tdataspr_department[".locking"] = true;


$pages = $tdataspr_department[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_department[".edit"] = true;
	$tdataspr_department[".afterEditAction"] = 1;
	$tdataspr_department[".closePopupAfterEdit"] = 1;
	$tdataspr_department[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_department[".add"] = true;
$tdataspr_department[".afterAddAction"] = 1;
$tdataspr_department[".closePopupAfterAdd"] = 1;
$tdataspr_department[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_department[".list"] = true;
}



$tdataspr_department[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_department[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_department[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_department[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_department[".printFriendly"] = true;
}



$tdataspr_department[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_department[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_department[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_department[".isUseAjaxSuggest"] = true;

$tdataspr_department[".rowHighlite"] = true;





$tdataspr_department[".ajaxCodeSnippetAdded"] = false;

$tdataspr_department[".buttonsAdded"] = false;

$tdataspr_department[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_department[".isUseTimeForSearch"] = false;


$tdataspr_department[".badgeColor"] = "9ACD32";


$tdataspr_department[".allSearchFields"] = array();
$tdataspr_department[".filterFields"] = array();
$tdataspr_department[".requiredSearchFields"] = array();

$tdataspr_department[".googleLikeFields"] = array();
$tdataspr_department[".googleLikeFields"][] = "dep_id";
$tdataspr_department[".googleLikeFields"][] = "dep_name";
$tdataspr_department[".googleLikeFields"][] = "dep_desc";



$tdataspr_department[".tableType"] = "list";

$tdataspr_department[".printerPageOrientation"] = 0;
$tdataspr_department[".nPrinterPageScale"] = 100;

$tdataspr_department[".nPrinterSplitRecords"] = 40;

$tdataspr_department[".geocodingEnabled"] = false;





$tdataspr_department[".isResizeColumns"] = true;





$tdataspr_department[".pageSize"] = 20;

$tdataspr_department[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_department[".strOrderBy"] = $tstrOrderBy;

$tdataspr_department[".orderindexes"] = array();


$tdataspr_department[".sqlHead"] = "SELECT dep_id,  	dep_name,  	dep_desc";
$tdataspr_department[".sqlFrom"] = "FROM \"public\".spr_department";
$tdataspr_department[".sqlWhereExpr"] = "";
$tdataspr_department[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_department[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_department[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_department[".highlightSearchResults"] = true;

$tableKeysspr_department = array();
$tableKeysspr_department[] = "dep_id";
$tdataspr_department[".Keys"] = $tableKeysspr_department;


$tdataspr_department[".hideMobileList"] = array();




//	dep_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "dep_id";
	$fdata["GoodName"] = "dep_id";
	$fdata["ownerTable"] = "public.spr_department";
	$fdata["Label"] = GetFieldLabel("public_spr_department","dep_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "dep_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dep_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_department["dep_id"] = $fdata;
		$tdataspr_department[".searchableFields"][] = "dep_id";
//	dep_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "dep_name";
	$fdata["GoodName"] = "dep_name";
	$fdata["ownerTable"] = "public.spr_department";
	$fdata["Label"] = GetFieldLabel("public_spr_department","dep_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "dep_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dep_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_department["dep_name"] = $fdata;
		$tdataspr_department[".searchableFields"][] = "dep_name";
//	dep_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "dep_desc";
	$fdata["GoodName"] = "dep_desc";
	$fdata["ownerTable"] = "public.spr_department";
	$fdata["Label"] = GetFieldLabel("public_spr_department","dep_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "dep_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dep_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_department["dep_desc"] = $fdata;
		$tdataspr_department[".searchableFields"][] = "dep_desc";


$tables_data["public.spr_department"]=&$tdataspr_department;
$field_labels["public_spr_department"] = &$fieldLabelsspr_department;
$fieldToolTips["public_spr_department"] = &$fieldToolTipsspr_department;
$placeHolders["public_spr_department"] = &$placeHoldersspr_department;
$page_titles["public_spr_department"] = &$pageTitlesspr_department;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_department"] = array();
//	public.sotrudnik
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.sotrudnik";
		$detailsParam["dOriginalTable"] = "public.sotrudnik";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "sotrudnik";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_sotrudnik");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_department"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_department"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_department"][$dIndex]["masterKeys"][]="dep_id";

				$detailsTablesData["public.spr_department"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_department"][$dIndex]["detailKeys"][]="sotrudnik_dep";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_department"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_department()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "dep_id,  	dep_name,  	dep_desc";
$proto0["m_strFrom"] = "FROM \"public\".spr_department";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "dep_id",
	"m_strTable" => "public.spr_department",
	"m_srcTableName" => "public.spr_department"
));

$proto6["m_sql"] = "dep_id";
$proto6["m_srcTableName"] = "public.spr_department";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "dep_name",
	"m_strTable" => "public.spr_department",
	"m_srcTableName" => "public.spr_department"
));

$proto8["m_sql"] = "dep_name";
$proto8["m_srcTableName"] = "public.spr_department";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "dep_desc",
	"m_strTable" => "public.spr_department",
	"m_srcTableName" => "public.spr_department"
));

$proto10["m_sql"] = "dep_desc";
$proto10["m_srcTableName"] = "public.spr_department";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "public.spr_department";
$proto13["m_srcTableName"] = "public.spr_department";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "dep_id";
$proto13["m_columns"][] = "dep_name";
$proto13["m_columns"][] = "dep_desc";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "\"public\".spr_department";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "public.spr_department";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_department";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_department = createSqlQuery_spr_department();


	
		;

			

$tdataspr_department[".sqlquery"] = $queryData_spr_department;



$tableEvents["public.spr_department"] = new eventsBase;
$tdataspr_department[".hasEvents"] = false;

?>