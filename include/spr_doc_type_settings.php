<?php
$tdataspr_doc_type = array();
$tdataspr_doc_type[".searchableFields"] = array();
$tdataspr_doc_type[".ShortName"] = "spr_doc_type";
$tdataspr_doc_type[".OwnerID"] = "";
$tdataspr_doc_type[".OriginalTable"] = "public.spr_doc_type";


$tdataspr_doc_type[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_doc_type[".originalPagesByType"] = $tdataspr_doc_type[".pagesByType"];
$tdataspr_doc_type[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_doc_type[".originalPages"] = $tdataspr_doc_type[".pages"];
$tdataspr_doc_type[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_doc_type[".originalDefaultPages"] = $tdataspr_doc_type[".defaultPages"];

//	field labels
$fieldLabelsspr_doc_type = array();
$fieldToolTipsspr_doc_type = array();
$pageTitlesspr_doc_type = array();
$placeHoldersspr_doc_type = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_doc_type["Russian"] = array();
	$fieldToolTipsspr_doc_type["Russian"] = array();
	$placeHoldersspr_doc_type["Russian"] = array();
	$pageTitlesspr_doc_type["Russian"] = array();
	$fieldLabelsspr_doc_type["Russian"]["doctype_id"] = "Doctype Id";
	$fieldToolTipsspr_doc_type["Russian"]["doctype_id"] = "";
	$placeHoldersspr_doc_type["Russian"]["doctype_id"] = "";
	$fieldLabelsspr_doc_type["Russian"]["type_name"] = "Наименование";
	$fieldToolTipsspr_doc_type["Russian"]["type_name"] = "";
	$placeHoldersspr_doc_type["Russian"]["type_name"] = "";
	if (count($fieldToolTipsspr_doc_type["Russian"]))
		$tdataspr_doc_type[".isUseToolTips"] = true;
}


	$tdataspr_doc_type[".NCSearch"] = true;



$tdataspr_doc_type[".shortTableName"] = "spr_doc_type";
$tdataspr_doc_type[".nSecOptions"] = 0;

$tdataspr_doc_type[".mainTableOwnerID"] = "";
$tdataspr_doc_type[".entityType"] = 0;
$tdataspr_doc_type[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_doc_type[".strOriginalTableName"] = "public.spr_doc_type";

	



$tdataspr_doc_type[".showAddInPopup"] = false;

$tdataspr_doc_type[".showEditInPopup"] = false;

$tdataspr_doc_type[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_doc_type[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_doc_type[".listAjax"] = true;
//	temporary
$tdataspr_doc_type[".listAjax"] = false;

	$tdataspr_doc_type[".audit"] = true;

	$tdataspr_doc_type[".locking"] = true;


$pages = $tdataspr_doc_type[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_doc_type[".edit"] = true;
	$tdataspr_doc_type[".afterEditAction"] = 1;
	$tdataspr_doc_type[".closePopupAfterEdit"] = 1;
	$tdataspr_doc_type[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_doc_type[".add"] = true;
$tdataspr_doc_type[".afterAddAction"] = 1;
$tdataspr_doc_type[".closePopupAfterAdd"] = 1;
$tdataspr_doc_type[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_doc_type[".list"] = true;
}



$tdataspr_doc_type[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_doc_type[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_doc_type[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_doc_type[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_doc_type[".printFriendly"] = true;
}



$tdataspr_doc_type[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_doc_type[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_doc_type[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_doc_type[".isUseAjaxSuggest"] = true;

$tdataspr_doc_type[".rowHighlite"] = true;





$tdataspr_doc_type[".ajaxCodeSnippetAdded"] = false;

$tdataspr_doc_type[".buttonsAdded"] = false;

$tdataspr_doc_type[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_doc_type[".isUseTimeForSearch"] = false;


$tdataspr_doc_type[".badgeColor"] = "E8926F";


$tdataspr_doc_type[".allSearchFields"] = array();
$tdataspr_doc_type[".filterFields"] = array();
$tdataspr_doc_type[".requiredSearchFields"] = array();

$tdataspr_doc_type[".googleLikeFields"] = array();
$tdataspr_doc_type[".googleLikeFields"][] = "doctype_id";
$tdataspr_doc_type[".googleLikeFields"][] = "type_name";



$tdataspr_doc_type[".tableType"] = "list";

$tdataspr_doc_type[".printerPageOrientation"] = 0;
$tdataspr_doc_type[".nPrinterPageScale"] = 100;

$tdataspr_doc_type[".nPrinterSplitRecords"] = 40;

$tdataspr_doc_type[".geocodingEnabled"] = false;





$tdataspr_doc_type[".isResizeColumns"] = true;





$tdataspr_doc_type[".pageSize"] = 20;

$tdataspr_doc_type[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_doc_type[".strOrderBy"] = $tstrOrderBy;

$tdataspr_doc_type[".orderindexes"] = array();


$tdataspr_doc_type[".sqlHead"] = "SELECT doctype_id,  	type_name";
$tdataspr_doc_type[".sqlFrom"] = "FROM \"public\".spr_doc_type";
$tdataspr_doc_type[".sqlWhereExpr"] = "";
$tdataspr_doc_type[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_doc_type[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_doc_type[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_doc_type[".highlightSearchResults"] = true;

$tableKeysspr_doc_type = array();
$tableKeysspr_doc_type[] = "doctype_id";
$tdataspr_doc_type[".Keys"] = $tableKeysspr_doc_type;


$tdataspr_doc_type[".hideMobileList"] = array();




//	doctype_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "doctype_id";
	$fdata["GoodName"] = "doctype_id";
	$fdata["ownerTable"] = "public.spr_doc_type";
	$fdata["Label"] = GetFieldLabel("public_spr_doc_type","doctype_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "doctype_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "doctype_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_doc_type["doctype_id"] = $fdata;
		$tdataspr_doc_type[".searchableFields"][] = "doctype_id";
//	type_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "type_name";
	$fdata["GoodName"] = "type_name";
	$fdata["ownerTable"] = "public.spr_doc_type";
	$fdata["Label"] = GetFieldLabel("public_spr_doc_type","type_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "type_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "type_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_doc_type["type_name"] = $fdata;
		$tdataspr_doc_type[".searchableFields"][] = "type_name";


$tables_data["public.spr_doc_type"]=&$tdataspr_doc_type;
$field_labels["public_spr_doc_type"] = &$fieldLabelsspr_doc_type;
$fieldToolTips["public_spr_doc_type"] = &$fieldToolTipsspr_doc_type;
$placeHolders["public_spr_doc_type"] = &$placeHoldersspr_doc_type;
$page_titles["public_spr_doc_type"] = &$pageTitlesspr_doc_type;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_doc_type"] = array();
//	public.doc_in
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_in";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_in";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_in");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_doc_type"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_doc_type"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_doc_type"][$dIndex]["masterKeys"][]="doctype_id";

				$detailsTablesData["public.spr_doc_type"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_doc_type"][$dIndex]["detailKeys"][]="doc_type";
//	public.doc_out
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_out";
		$detailsParam["dOriginalTable"] = "public.doc_out";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_out";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_out");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_doc_type"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_doc_type"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_doc_type"][$dIndex]["masterKeys"][]="doctype_id";

				$detailsTablesData["public.spr_doc_type"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_doc_type"][$dIndex]["detailKeys"][]="doc_type";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_doc_type"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_doc_type()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "doctype_id,  	type_name";
$proto0["m_strFrom"] = "FROM \"public\".spr_doc_type";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "doctype_id",
	"m_strTable" => "public.spr_doc_type",
	"m_srcTableName" => "public.spr_doc_type"
));

$proto6["m_sql"] = "doctype_id";
$proto6["m_srcTableName"] = "public.spr_doc_type";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "type_name",
	"m_strTable" => "public.spr_doc_type",
	"m_srcTableName" => "public.spr_doc_type"
));

$proto8["m_sql"] = "type_name";
$proto8["m_srcTableName"] = "public.spr_doc_type";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.spr_doc_type";
$proto11["m_srcTableName"] = "public.spr_doc_type";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "doctype_id";
$proto11["m_columns"][] = "type_name";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".spr_doc_type";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.spr_doc_type";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_doc_type";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_doc_type = createSqlQuery_spr_doc_type();


	
		;

		

$tdataspr_doc_type[".sqlquery"] = $queryData_spr_doc_type;



$tableEvents["public.spr_doc_type"] = new eventsBase;
$tdataspr_doc_type[".hasEvents"] = false;

?>