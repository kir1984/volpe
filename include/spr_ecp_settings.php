<?php
$tdataspr_ecp = array();
$tdataspr_ecp[".searchableFields"] = array();
$tdataspr_ecp[".ShortName"] = "spr_ecp";
$tdataspr_ecp[".OwnerID"] = "";
$tdataspr_ecp[".OriginalTable"] = "public.spr_ecp";


$tdataspr_ecp[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_ecp[".originalPagesByType"] = $tdataspr_ecp[".pagesByType"];
$tdataspr_ecp[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_ecp[".originalPages"] = $tdataspr_ecp[".pages"];
$tdataspr_ecp[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_ecp[".originalDefaultPages"] = $tdataspr_ecp[".defaultPages"];

//	field labels
$fieldLabelsspr_ecp = array();
$fieldToolTipsspr_ecp = array();
$pageTitlesspr_ecp = array();
$placeHoldersspr_ecp = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_ecp["Russian"] = array();
	$fieldToolTipsspr_ecp["Russian"] = array();
	$placeHoldersspr_ecp["Russian"] = array();
	$pageTitlesspr_ecp["Russian"] = array();
	$fieldLabelsspr_ecp["Russian"]["sprecp_id"] = "Sprecp Id";
	$fieldToolTipsspr_ecp["Russian"]["sprecp_id"] = "";
	$placeHoldersspr_ecp["Russian"]["sprecp_id"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_number"] = "Серийный номер";
	$fieldToolTipsspr_ecp["Russian"]["ecp_number"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_number"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_owner"] = "Владелец";
	$fieldToolTipsspr_ecp["Russian"]["ecp_owner"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_owner"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_ud"] = "УЦ";
	$fieldToolTipsspr_ecp["Russian"]["ecp_ud"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_ud"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_date"] = "Дата выдачи";
	$fieldToolTipsspr_ecp["Russian"]["ecp_date"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_date"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_dateexp_cert"] = "Срок (откр.)";
	$fieldToolTipsspr_ecp["Russian"]["ecp_dateexp_cert"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_dateexp_cert"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_dateexp"] = "Срок (закр.)";
	$fieldToolTipsspr_ecp["Russian"]["ecp_dateexp"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_dateexp"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_desc"] = "Описание";
	$fieldToolTipsspr_ecp["Russian"]["ecp_desc"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_desc"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_status"] = "Статус";
	$fieldToolTipsspr_ecp["Russian"]["ecp_status"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_status"] = "";
	$fieldLabelsspr_ecp["Russian"]["ecp_daysleft"] = "Ecp Daysleft";
	$fieldToolTipsspr_ecp["Russian"]["ecp_daysleft"] = "";
	$placeHoldersspr_ecp["Russian"]["ecp_daysleft"] = "";
	if (count($fieldToolTipsspr_ecp["Russian"]))
		$tdataspr_ecp[".isUseToolTips"] = true;
}


	$tdataspr_ecp[".NCSearch"] = true;



$tdataspr_ecp[".shortTableName"] = "spr_ecp";
$tdataspr_ecp[".nSecOptions"] = 0;

$tdataspr_ecp[".mainTableOwnerID"] = "";
$tdataspr_ecp[".entityType"] = 0;
$tdataspr_ecp[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_ecp[".strOriginalTableName"] = "public.spr_ecp";

	



$tdataspr_ecp[".showAddInPopup"] = false;

$tdataspr_ecp[".showEditInPopup"] = false;

$tdataspr_ecp[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_ecp[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_ecp[".listAjax"] = true;
//	temporary
$tdataspr_ecp[".listAjax"] = false;

	$tdataspr_ecp[".audit"] = true;

	$tdataspr_ecp[".locking"] = true;


$pages = $tdataspr_ecp[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_ecp[".edit"] = true;
	$tdataspr_ecp[".afterEditAction"] = 1;
	$tdataspr_ecp[".closePopupAfterEdit"] = 1;
	$tdataspr_ecp[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_ecp[".add"] = true;
$tdataspr_ecp[".afterAddAction"] = 1;
$tdataspr_ecp[".closePopupAfterAdd"] = 1;
$tdataspr_ecp[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_ecp[".list"] = true;
}



$tdataspr_ecp[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_ecp[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_ecp[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_ecp[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_ecp[".printFriendly"] = true;
}



$tdataspr_ecp[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_ecp[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_ecp[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_ecp[".isUseAjaxSuggest"] = true;

$tdataspr_ecp[".rowHighlite"] = true;





$tdataspr_ecp[".ajaxCodeSnippetAdded"] = false;

$tdataspr_ecp[".buttonsAdded"] = false;

$tdataspr_ecp[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_ecp[".isUseTimeForSearch"] = false;


$tdataspr_ecp[".badgeColor"] = "6DA5C8";


$tdataspr_ecp[".allSearchFields"] = array();
$tdataspr_ecp[".filterFields"] = array();
$tdataspr_ecp[".requiredSearchFields"] = array();

$tdataspr_ecp[".googleLikeFields"] = array();
$tdataspr_ecp[".googleLikeFields"][] = "sprecp_id";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_number";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_owner";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_ud";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_date";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_dateexp_cert";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_dateexp";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_desc";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_daysleft";
$tdataspr_ecp[".googleLikeFields"][] = "ecp_status";



$tdataspr_ecp[".tableType"] = "list";

$tdataspr_ecp[".printerPageOrientation"] = 0;
$tdataspr_ecp[".nPrinterPageScale"] = 100;

$tdataspr_ecp[".nPrinterSplitRecords"] = 40;

$tdataspr_ecp[".geocodingEnabled"] = false;





$tdataspr_ecp[".isResizeColumns"] = true;





$tdataspr_ecp[".pageSize"] = 20;

$tdataspr_ecp[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_ecp[".strOrderBy"] = $tstrOrderBy;

$tdataspr_ecp[".orderindexes"] = array();


$tdataspr_ecp[".sqlHead"] = "SELECT sprecp_id,  	ecp_number,  	ecp_owner,  	ecp_ud,  	ecp_date,  	ecp_dateexp_cert,  	ecp_dateexp,  	ecp_desc,    (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) ecp_daysleft,  	 	CASE  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) > 15 THEN 'Действующий'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) BETWEEN 1 AND 15 THEN 'Необходима замена'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) < 0 OR (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) = 0 THEN 'Просрочен'  		END ecp_status";
$tdataspr_ecp[".sqlFrom"] = "FROM \"public\".spr_ecp";
$tdataspr_ecp[".sqlWhereExpr"] = "";
$tdataspr_ecp[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_ecp[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_ecp[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_ecp[".highlightSearchResults"] = true;

$tableKeysspr_ecp = array();
$tableKeysspr_ecp[] = "sprecp_id";
$tdataspr_ecp[".Keys"] = $tableKeysspr_ecp;


$tdataspr_ecp[".hideMobileList"] = array();




//	sprecp_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "sprecp_id";
	$fdata["GoodName"] = "sprecp_id";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","sprecp_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "sprecp_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sprecp_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["sprecp_id"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "sprecp_id";
//	ecp_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ecp_number";
	$fdata["GoodName"] = "ecp_number";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_number");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ecp_number";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_number";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_number"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_number";
//	ecp_owner
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ecp_owner";
	$fdata["GoodName"] = "ecp_owner";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_owner");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ecp_owner";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_owner";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_owner"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_owner";
//	ecp_ud
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ecp_ud";
	$fdata["GoodName"] = "ecp_ud";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_ud");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ecp_ud";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_ud";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_ud"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_ud";
//	ecp_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ecp_date";
	$fdata["GoodName"] = "ecp_date";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "ecp_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 5;
	$edata["LastYearFactor"] = 30;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_date"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_date";
//	ecp_dateexp_cert
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ecp_dateexp_cert";
	$fdata["GoodName"] = "ecp_dateexp_cert";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_dateexp_cert");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "ecp_dateexp_cert";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_dateexp_cert";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_dateexp_cert"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_dateexp_cert";
//	ecp_dateexp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ecp_dateexp";
	$fdata["GoodName"] = "ecp_dateexp";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_dateexp");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "ecp_dateexp";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_dateexp";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 5;
	$edata["LastYearFactor"] = 30;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_dateexp"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_dateexp";
//	ecp_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ecp_desc";
	$fdata["GoodName"] = "ecp_desc";
	$fdata["ownerTable"] = "public.spr_ecp";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "ecp_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ecp_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_desc"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_desc";
//	ecp_daysleft
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ecp_daysleft";
	$fdata["GoodName"] = "ecp_daysleft";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_daysleft");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "ecp_daysleft";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "(SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date))";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_daysleft"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_daysleft";
//	ecp_status
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ecp_status";
	$fdata["GoodName"] = "ecp_status";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("public_spr_ecp","ecp_status");
	$fdata["FieldType"] = 201;

	
	
	
			

		$fdata["strField"] = "ecp_status";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CASE  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) > 15 THEN 'Действующий'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) BETWEEN 1 AND 15 THEN 'Необходима замена'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) < 0 OR (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) = 0 THEN 'Просрочен'  		END";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_ecp["ecp_status"] = $fdata;
		$tdataspr_ecp[".searchableFields"][] = "ecp_status";


$tables_data["public.spr_ecp"]=&$tdataspr_ecp;
$field_labels["public_spr_ecp"] = &$fieldLabelsspr_ecp;
$fieldToolTips["public_spr_ecp"] = &$fieldToolTipsspr_ecp;
$placeHolders["public_spr_ecp"] = &$placeHoldersspr_ecp;
$page_titles["public_spr_ecp"] = &$pageTitlesspr_ecp;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_ecp"] = array();
//	public.ecp
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.ecp";
		$detailsParam["dOriginalTable"] = "public.ecp";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ecp";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_ecp");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_ecp"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_ecp"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_ecp"][$dIndex]["masterKeys"][]="sprecp_id";

				$detailsTablesData["public.spr_ecp"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_ecp"][$dIndex]["detailKeys"][]="spr_ecp";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_ecp"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_ecp()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "sprecp_id,  	ecp_number,  	ecp_owner,  	ecp_ud,  	ecp_date,  	ecp_dateexp_cert,  	ecp_dateexp,  	ecp_desc,    (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) ecp_daysleft,  	 	CASE  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) > 15 THEN 'Действующий'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) BETWEEN 1 AND 15 THEN 'Необходима замена'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) < 0 OR (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) = 0 THEN 'Просрочен'  		END ecp_status";
$proto0["m_strFrom"] = "FROM \"public\".spr_ecp";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "sprecp_id",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto6["m_sql"] = "sprecp_id";
$proto6["m_srcTableName"] = "public.spr_ecp";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_number",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto8["m_sql"] = "ecp_number";
$proto8["m_srcTableName"] = "public.spr_ecp";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_owner",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto10["m_sql"] = "ecp_owner";
$proto10["m_srcTableName"] = "public.spr_ecp";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_ud",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto12["m_sql"] = "ecp_ud";
$proto12["m_srcTableName"] = "public.spr_ecp";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_date",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto14["m_sql"] = "ecp_date";
$proto14["m_srcTableName"] = "public.spr_ecp";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_dateexp_cert",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto16["m_sql"] = "ecp_dateexp_cert";
$proto16["m_srcTableName"] = "public.spr_ecp";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_dateexp",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto18["m_sql"] = "ecp_dateexp";
$proto18["m_srcTableName"] = "public.spr_ecp";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ecp_desc",
	"m_strTable" => "public.spr_ecp",
	"m_srcTableName" => "public.spr_ecp"
));

$proto20["m_sql"] = "ecp_desc";
$proto20["m_srcTableName"] = "public.spr_ecp";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$proto23=array();
$proto23["m_strHead"] = "SELECT";
$proto23["m_strFieldList"] = "CAST(ecp_dateexp AS date) - CAST( now() AS date)";
$proto23["m_strFrom"] = "";
$proto23["m_strWhere"] = "";
$proto23["m_strOrderBy"] = "";
	
		;
			$proto23["cipherer"] = null;
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_where"] = $obj;
$proto27=array();
$proto27["m_sql"] = "";
$proto27["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto27["m_column"]=$obj;
$proto27["m_contained"] = array();
$proto27["m_strCase"] = "";
$proto27["m_havingmode"] = false;
$proto27["m_inBrackets"] = false;
$proto27["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto27);

$proto23["m_having"] = $obj;
$proto23["m_fieldlist"] = array();
						$proto29=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "CAST(ecp_dateexp AS date) - CAST( now() AS date)"
));

$proto29["m_sql"] = "CAST(ecp_dateexp AS date) - CAST( now() AS date)";
$proto29["m_srcTableName"] = "public.spr_ecp";
$proto29["m_expr"]=$obj;
$proto29["m_alias"] = "";
$obj = new SQLFieldListItem($proto29);

$proto23["m_fieldlist"][]=$obj;
$proto23["m_fromlist"] = array();
$proto23["m_groupby"] = array();
$proto23["m_orderby"] = array();
$proto23["m_srcTableName"]="public.spr_ecp";		
$obj = new SQLQuery($proto23);

$proto22["m_sql"] = "(SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date))";
$proto22["m_srcTableName"] = "public.spr_ecp";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "ecp_daysleft";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto31=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "CASE  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) > 15 THEN 'Действующий'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) BETWEEN 1 AND 15 THEN 'Необходима замена'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) < 0 OR (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) = 0 THEN 'Просрочен'  		END"
));

$proto31["m_sql"] = "CASE  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) > 15 THEN 'Действующий'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) BETWEEN 1 AND 15 THEN 'Необходима замена'  			WHEN (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) < 0 OR (SELECT CAST(ecp_dateexp AS date) - CAST( now() AS date)) = 0 THEN 'Просрочен'  		END";
$proto31["m_srcTableName"] = "public.spr_ecp";
$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "ecp_status";
$obj = new SQLFieldListItem($proto31);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto33=array();
$proto33["m_link"] = "SQLL_MAIN";
			$proto34=array();
$proto34["m_strName"] = "public.spr_ecp";
$proto34["m_srcTableName"] = "public.spr_ecp";
$proto34["m_columns"] = array();
$proto34["m_columns"][] = "sprecp_id";
$proto34["m_columns"][] = "ecp_number";
$proto34["m_columns"][] = "ecp_owner";
$proto34["m_columns"][] = "ecp_ud";
$proto34["m_columns"][] = "ecp_date";
$proto34["m_columns"][] = "ecp_dateexp_cert";
$proto34["m_columns"][] = "ecp_dateexp";
$proto34["m_columns"][] = "ecp_desc";
$obj = new SQLTable($proto34);

$proto33["m_table"] = $obj;
$proto33["m_sql"] = "\"public\".spr_ecp";
$proto33["m_alias"] = "";
$proto33["m_srcTableName"] = "public.spr_ecp";
$proto35=array();
$proto35["m_sql"] = "";
$proto35["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto35["m_column"]=$obj;
$proto35["m_contained"] = array();
$proto35["m_strCase"] = "";
$proto35["m_havingmode"] = false;
$proto35["m_inBrackets"] = false;
$proto35["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto35);

$proto33["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto33);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_ecp";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_ecp = createSqlQuery_spr_ecp();


	
		;

										

$tdataspr_ecp[".sqlquery"] = $queryData_spr_ecp;



$tableEvents["public.spr_ecp"] = new eventsBase;
$tdataspr_ecp[".hasEvents"] = false;

?>