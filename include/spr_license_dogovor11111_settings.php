<?php
$tdataspr_license_dogovor11111 = array();
$tdataspr_license_dogovor11111[".searchableFields"] = array();
$tdataspr_license_dogovor11111[".ShortName"] = "spr_license_dogovor11111";
$tdataspr_license_dogovor11111[".OwnerID"] = "";
$tdataspr_license_dogovor11111[".OriginalTable"] = "public.spr_license_dogovor";


$tdataspr_license_dogovor11111[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"]}" );
$tdataspr_license_dogovor11111[".originalPagesByType"] = $tdataspr_license_dogovor11111[".pagesByType"];
$tdataspr_license_dogovor11111[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"]}" ) );
$tdataspr_license_dogovor11111[".originalPages"] = $tdataspr_license_dogovor11111[".pages"];
$tdataspr_license_dogovor11111[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\"}" );
$tdataspr_license_dogovor11111[".originalDefaultPages"] = $tdataspr_license_dogovor11111[".defaultPages"];

//	field labels
$fieldLabelsspr_license_dogovor11111 = array();
$fieldToolTipsspr_license_dogovor11111 = array();
$pageTitlesspr_license_dogovor11111 = array();
$placeHoldersspr_license_dogovor11111 = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_license_dogovor11111["Russian"] = array();
	$fieldToolTipsspr_license_dogovor11111["Russian"] = array();
	$placeHoldersspr_license_dogovor11111["Russian"] = array();
	$pageTitlesspr_license_dogovor11111["Russian"] = array();
	$fieldLabelsspr_license_dogovor11111["Russian"]["licdog_licnumber"] = "Licdog Licnumber";
	$fieldToolTipsspr_license_dogovor11111["Russian"]["licdog_licnumber"] = "";
	$placeHoldersspr_license_dogovor11111["Russian"]["licdog_licnumber"] = "";
	$fieldLabelsspr_license_dogovor11111["Russian"]["po_name"] = "Po Name";
	$fieldToolTipsspr_license_dogovor11111["Russian"]["po_name"] = "";
	$placeHoldersspr_license_dogovor11111["Russian"]["po_name"] = "";
	if (count($fieldToolTipsspr_license_dogovor11111["Russian"]))
		$tdataspr_license_dogovor11111[".isUseToolTips"] = true;
}


	$tdataspr_license_dogovor11111[".NCSearch"] = true;



$tdataspr_license_dogovor11111[".shortTableName"] = "spr_license_dogovor11111";
$tdataspr_license_dogovor11111[".nSecOptions"] = 0;

$tdataspr_license_dogovor11111[".mainTableOwnerID"] = "";
$tdataspr_license_dogovor11111[".entityType"] = 1;
$tdataspr_license_dogovor11111[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_license_dogovor11111[".strOriginalTableName"] = "public.spr_license_dogovor";

	



$tdataspr_license_dogovor11111[".showAddInPopup"] = false;

$tdataspr_license_dogovor11111[".showEditInPopup"] = false;

$tdataspr_license_dogovor11111[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_license_dogovor11111[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataspr_license_dogovor11111[".listAjax"] = false;
//	temporary
$tdataspr_license_dogovor11111[".listAjax"] = false;

	$tdataspr_license_dogovor11111[".audit"] = false;

	$tdataspr_license_dogovor11111[".locking"] = false;


$pages = $tdataspr_license_dogovor11111[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_license_dogovor11111[".edit"] = true;
	$tdataspr_license_dogovor11111[".afterEditAction"] = 1;
	$tdataspr_license_dogovor11111[".closePopupAfterEdit"] = 1;
	$tdataspr_license_dogovor11111[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_license_dogovor11111[".add"] = true;
$tdataspr_license_dogovor11111[".afterAddAction"] = 1;
$tdataspr_license_dogovor11111[".closePopupAfterAdd"] = 1;
$tdataspr_license_dogovor11111[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_license_dogovor11111[".list"] = true;
}



$tdataspr_license_dogovor11111[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_license_dogovor11111[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_license_dogovor11111[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_license_dogovor11111[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_license_dogovor11111[".printFriendly"] = true;
}



$tdataspr_license_dogovor11111[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_license_dogovor11111[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_license_dogovor11111[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_license_dogovor11111[".isUseAjaxSuggest"] = true;

$tdataspr_license_dogovor11111[".rowHighlite"] = true;





$tdataspr_license_dogovor11111[".ajaxCodeSnippetAdded"] = false;

$tdataspr_license_dogovor11111[".buttonsAdded"] = false;

$tdataspr_license_dogovor11111[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_license_dogovor11111[".isUseTimeForSearch"] = false;


$tdataspr_license_dogovor11111[".badgeColor"] = "D2691E";


$tdataspr_license_dogovor11111[".allSearchFields"] = array();
$tdataspr_license_dogovor11111[".filterFields"] = array();
$tdataspr_license_dogovor11111[".requiredSearchFields"] = array();

$tdataspr_license_dogovor11111[".googleLikeFields"] = array();
$tdataspr_license_dogovor11111[".googleLikeFields"][] = "licdog_licnumber";
$tdataspr_license_dogovor11111[".googleLikeFields"][] = "po_name";



$tdataspr_license_dogovor11111[".tableType"] = "list";

$tdataspr_license_dogovor11111[".printerPageOrientation"] = 0;
$tdataspr_license_dogovor11111[".nPrinterPageScale"] = 100;

$tdataspr_license_dogovor11111[".nPrinterSplitRecords"] = 40;

$tdataspr_license_dogovor11111[".geocodingEnabled"] = false;










$tdataspr_license_dogovor11111[".pageSize"] = 20;

$tdataspr_license_dogovor11111[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_license_dogovor11111[".strOrderBy"] = $tstrOrderBy;

$tdataspr_license_dogovor11111[".orderindexes"] = array();


$tdataspr_license_dogovor11111[".sqlHead"] = "SELECT \"public\".spr_license_dogovor.licdog_licnumber,  \"public\".spr_po.po_name";
$tdataspr_license_dogovor11111[".sqlFrom"] = "FROM \"public\".spr_license_dogovor  INNER JOIN \"public\".spr_po ON \"public\".spr_license_dogovor.licdog_poname = \"public\".spr_po.po_id";
$tdataspr_license_dogovor11111[".sqlWhereExpr"] = "";
$tdataspr_license_dogovor11111[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_license_dogovor11111[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_license_dogovor11111[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_license_dogovor11111[".highlightSearchResults"] = true;

$tableKeysspr_license_dogovor11111 = array();
$tdataspr_license_dogovor11111[".Keys"] = $tableKeysspr_license_dogovor11111;


$tdataspr_license_dogovor11111[".hideMobileList"] = array();




//	licdog_licnumber
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "licdog_licnumber";
	$fdata["GoodName"] = "licdog_licnumber";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11111","licdog_licnumber");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_licnumber";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_license_dogovor.licdog_licnumber";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11111["licdog_licnumber"] = $fdata;
		$tdataspr_license_dogovor11111[".searchableFields"][] = "licdog_licnumber";
//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11111","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_po.po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11111["po_name"] = $fdata;
		$tdataspr_license_dogovor11111[".searchableFields"][] = "po_name";


$tables_data["public.spr_license_dogovor11111"]=&$tdataspr_license_dogovor11111;
$field_labels["public_spr_license_dogovor11111"] = &$fieldLabelsspr_license_dogovor11111;
$fieldToolTips["public_spr_license_dogovor11111"] = &$fieldToolTipsspr_license_dogovor11111;
$placeHolders["public_spr_license_dogovor11111"] = &$placeHoldersspr_license_dogovor11111;
$page_titles["public_spr_license_dogovor11111"] = &$pageTitlesspr_license_dogovor11111;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_license_dogovor11111"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_license_dogovor11111"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_license_dogovor11111()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"public\".spr_license_dogovor.licdog_licnumber,  \"public\".spr_po.po_name";
$proto0["m_strFrom"] = "FROM \"public\".spr_license_dogovor  INNER JOIN \"public\".spr_po ON \"public\".spr_license_dogovor.licdog_poname = \"public\".spr_po.po_id";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_licnumber",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11111"
));

$proto6["m_sql"] = "\"public\".spr_license_dogovor.licdog_licnumber";
$proto6["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_license_dogovor11111"
));

$proto8["m_sql"] = "\"public\".spr_po.po_name";
$proto8["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.spr_license_dogovor";
$proto11["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "licdog_id";
$proto11["m_columns"][] = "licdog_poname";
$proto11["m_columns"][] = "licdog_type";
$proto11["m_columns"][] = "licdog_quantity";
$proto11["m_columns"][] = "licdog_number";
$proto11["m_columns"][] = "licdog_date";
$proto11["m_columns"][] = "licdog_contractor";
$proto11["m_columns"][] = "licdog_desc";
$proto11["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".spr_license_dogovor";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
												$proto14=array();
$proto14["m_link"] = "SQLL_INNERJOIN";
			$proto15=array();
$proto15["m_strName"] = "public.spr_po";
$proto15["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "po_id";
$proto15["m_columns"][] = "po_name";
$proto15["m_columns"][] = "po_desc";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "INNER JOIN \"public\".spr_po ON \"public\".spr_license_dogovor.licdog_poname = \"public\".spr_po.po_id";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "public.spr_license_dogovor11111";
$proto16=array();
$proto16["m_sql"] = "\"public\".spr_license_dogovor.licdog_poname = \"public\".spr_po.po_id";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "licdog_poname",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11111"
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "= \"public\".spr_po.po_id";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_license_dogovor11111";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_license_dogovor11111 = createSqlQuery_spr_license_dogovor11111();


	
		;

		

$tdataspr_license_dogovor11111[".sqlquery"] = $queryData_spr_license_dogovor11111;



$tableEvents["public.spr_license_dogovor11111"] = new eventsBase;
$tdataspr_license_dogovor11111[".hasEvents"] = false;

?>