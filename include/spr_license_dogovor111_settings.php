<?php
$tdataspr_license_dogovor111 = array();
$tdataspr_license_dogovor111[".searchableFields"] = array();
$tdataspr_license_dogovor111[".ShortName"] = "spr_license_dogovor111";
$tdataspr_license_dogovor111[".OwnerID"] = "";
$tdataspr_license_dogovor111[".OriginalTable"] = "public.spr_license_dogovor";


$tdataspr_license_dogovor111[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_license_dogovor111[".originalPagesByType"] = $tdataspr_license_dogovor111[".pagesByType"];
$tdataspr_license_dogovor111[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_license_dogovor111[".originalPages"] = $tdataspr_license_dogovor111[".pages"];
$tdataspr_license_dogovor111[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_license_dogovor111[".originalDefaultPages"] = $tdataspr_license_dogovor111[".defaultPages"];

//	field labels
$fieldLabelsspr_license_dogovor111 = array();
$fieldToolTipsspr_license_dogovor111 = array();
$pageTitlesspr_license_dogovor111 = array();
$placeHoldersspr_license_dogovor111 = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_license_dogovor111["Russian"] = array();
	$fieldToolTipsspr_license_dogovor111["Russian"] = array();
	$placeHoldersspr_license_dogovor111["Russian"] = array();
	$pageTitlesspr_license_dogovor111["Russian"] = array();
	$fieldLabelsspr_license_dogovor111["Russian"]["licdog_id"] = "Licdog Id";
	$fieldToolTipsspr_license_dogovor111["Russian"]["licdog_id"] = "";
	$placeHoldersspr_license_dogovor111["Russian"]["licdog_id"] = "";
	$fieldLabelsspr_license_dogovor111["Russian"]["licdog_poname"] = "Licdog Poname";
	$fieldToolTipsspr_license_dogovor111["Russian"]["licdog_poname"] = "";
	$placeHoldersspr_license_dogovor111["Russian"]["licdog_poname"] = "";
	$fieldLabelsspr_license_dogovor111["Russian"]["licdog_number"] = "Licdog Number";
	$fieldToolTipsspr_license_dogovor111["Russian"]["licdog_number"] = "";
	$placeHoldersspr_license_dogovor111["Russian"]["licdog_number"] = "";
	$fieldLabelsspr_license_dogovor111["Russian"]["licdog_licnumber"] = "Licdog Licnumber";
	$fieldToolTipsspr_license_dogovor111["Russian"]["licdog_licnumber"] = "";
	$placeHoldersspr_license_dogovor111["Russian"]["licdog_licnumber"] = "";
	$fieldLabelsspr_license_dogovor111["Russian"]["po_name"] = "Po Name";
	$fieldToolTipsspr_license_dogovor111["Russian"]["po_name"] = "";
	$placeHoldersspr_license_dogovor111["Russian"]["po_name"] = "";
	if (count($fieldToolTipsspr_license_dogovor111["Russian"]))
		$tdataspr_license_dogovor111[".isUseToolTips"] = true;
}


	$tdataspr_license_dogovor111[".NCSearch"] = true;



$tdataspr_license_dogovor111[".shortTableName"] = "spr_license_dogovor111";
$tdataspr_license_dogovor111[".nSecOptions"] = 0;

$tdataspr_license_dogovor111[".mainTableOwnerID"] = "";
$tdataspr_license_dogovor111[".entityType"] = 1;
$tdataspr_license_dogovor111[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_license_dogovor111[".strOriginalTableName"] = "public.spr_license_dogovor";

	



$tdataspr_license_dogovor111[".showAddInPopup"] = false;

$tdataspr_license_dogovor111[".showEditInPopup"] = false;

$tdataspr_license_dogovor111[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_license_dogovor111[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataspr_license_dogovor111[".listAjax"] = false;
//	temporary
$tdataspr_license_dogovor111[".listAjax"] = false;

	$tdataspr_license_dogovor111[".audit"] = false;

	$tdataspr_license_dogovor111[".locking"] = false;


$pages = $tdataspr_license_dogovor111[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_license_dogovor111[".edit"] = true;
	$tdataspr_license_dogovor111[".afterEditAction"] = 1;
	$tdataspr_license_dogovor111[".closePopupAfterEdit"] = 1;
	$tdataspr_license_dogovor111[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_license_dogovor111[".add"] = true;
$tdataspr_license_dogovor111[".afterAddAction"] = 1;
$tdataspr_license_dogovor111[".closePopupAfterAdd"] = 1;
$tdataspr_license_dogovor111[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_license_dogovor111[".list"] = true;
}



$tdataspr_license_dogovor111[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_license_dogovor111[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_license_dogovor111[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_license_dogovor111[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_license_dogovor111[".printFriendly"] = true;
}



$tdataspr_license_dogovor111[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_license_dogovor111[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_license_dogovor111[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_license_dogovor111[".isUseAjaxSuggest"] = true;

$tdataspr_license_dogovor111[".rowHighlite"] = true;





$tdataspr_license_dogovor111[".ajaxCodeSnippetAdded"] = false;

$tdataspr_license_dogovor111[".buttonsAdded"] = false;

$tdataspr_license_dogovor111[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_license_dogovor111[".isUseTimeForSearch"] = false;


$tdataspr_license_dogovor111[".badgeColor"] = "4169E1";


$tdataspr_license_dogovor111[".allSearchFields"] = array();
$tdataspr_license_dogovor111[".filterFields"] = array();
$tdataspr_license_dogovor111[".requiredSearchFields"] = array();

$tdataspr_license_dogovor111[".googleLikeFields"] = array();
$tdataspr_license_dogovor111[".googleLikeFields"][] = "licdog_id";
$tdataspr_license_dogovor111[".googleLikeFields"][] = "licdog_poname";
$tdataspr_license_dogovor111[".googleLikeFields"][] = "licdog_number";
$tdataspr_license_dogovor111[".googleLikeFields"][] = "licdog_licnumber";
$tdataspr_license_dogovor111[".googleLikeFields"][] = "po_name";



$tdataspr_license_dogovor111[".tableType"] = "list";

$tdataspr_license_dogovor111[".printerPageOrientation"] = 0;
$tdataspr_license_dogovor111[".nPrinterPageScale"] = 100;

$tdataspr_license_dogovor111[".nPrinterSplitRecords"] = 40;

$tdataspr_license_dogovor111[".geocodingEnabled"] = false;










$tdataspr_license_dogovor111[".pageSize"] = 20;

$tdataspr_license_dogovor111[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_license_dogovor111[".strOrderBy"] = $tstrOrderBy;

$tdataspr_license_dogovor111[".orderindexes"] = array();


$tdataspr_license_dogovor111[".sqlHead"] = "SELECT \"public\".spr_license_dogovor.licdog_id,  \"public\".spr_license_dogovor.licdog_poname,  \"public\".spr_license_dogovor.licdog_number,  \"public\".spr_license_dogovor.licdog_licnumber,  \"public\".spr_po.po_name";
$tdataspr_license_dogovor111[".sqlFrom"] = "FROM \"public\".spr_license_dogovor  INNER JOIN \"public\".spr_po ON \"public\".spr_po.po_id = \"public\".spr_license_dogovor.licdog_poname";
$tdataspr_license_dogovor111[".sqlWhereExpr"] = "";
$tdataspr_license_dogovor111[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_license_dogovor111[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_license_dogovor111[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_license_dogovor111[".highlightSearchResults"] = true;

$tableKeysspr_license_dogovor111 = array();
$tableKeysspr_license_dogovor111[] = "licdog_id";
$tdataspr_license_dogovor111[".Keys"] = $tableKeysspr_license_dogovor111;


$tdataspr_license_dogovor111[".hideMobileList"] = array();




//	licdog_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "licdog_id";
	$fdata["GoodName"] = "licdog_id";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor111","licdog_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "licdog_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_license_dogovor.licdog_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor111["licdog_id"] = $fdata;
		$tdataspr_license_dogovor111[".searchableFields"][] = "licdog_id";
//	licdog_poname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "licdog_poname";
	$fdata["GoodName"] = "licdog_poname";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor111","licdog_poname");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licdog_poname";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_license_dogovor.licdog_poname";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_po";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "po_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "po_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor111["licdog_poname"] = $fdata;
		$tdataspr_license_dogovor111[".searchableFields"][] = "licdog_poname";
//	licdog_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "licdog_number";
	$fdata["GoodName"] = "licdog_number";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor111","licdog_number");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_number";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_license_dogovor.licdog_number";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor111["licdog_number"] = $fdata;
		$tdataspr_license_dogovor111[".searchableFields"][] = "licdog_number";
//	licdog_licnumber
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "licdog_licnumber";
	$fdata["GoodName"] = "licdog_licnumber";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor111","licdog_licnumber");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_licnumber";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_license_dogovor.licdog_licnumber";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor111["licdog_licnumber"] = $fdata;
		$tdataspr_license_dogovor111[".searchableFields"][] = "licdog_licnumber";
//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor111","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"public\".spr_po.po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor111["po_name"] = $fdata;
		$tdataspr_license_dogovor111[".searchableFields"][] = "po_name";


$tables_data["public.spr_license_dogovor111"]=&$tdataspr_license_dogovor111;
$field_labels["public_spr_license_dogovor111"] = &$fieldLabelsspr_license_dogovor111;
$fieldToolTips["public_spr_license_dogovor111"] = &$fieldToolTipsspr_license_dogovor111;
$placeHolders["public_spr_license_dogovor111"] = &$placeHoldersspr_license_dogovor111;
$page_titles["public_spr_license_dogovor111"] = &$pageTitlesspr_license_dogovor111;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_license_dogovor111"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_license_dogovor111"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_license_dogovor111()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"public\".spr_license_dogovor.licdog_id,  \"public\".spr_license_dogovor.licdog_poname,  \"public\".spr_license_dogovor.licdog_number,  \"public\".spr_license_dogovor.licdog_licnumber,  \"public\".spr_po.po_name";
$proto0["m_strFrom"] = "FROM \"public\".spr_license_dogovor  INNER JOIN \"public\".spr_po ON \"public\".spr_po.po_id = \"public\".spr_license_dogovor.licdog_poname";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_id",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto6["m_sql"] = "\"public\".spr_license_dogovor.licdog_id";
$proto6["m_srcTableName"] = "public.spr_license_dogovor111";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_poname",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto8["m_sql"] = "\"public\".spr_license_dogovor.licdog_poname";
$proto8["m_srcTableName"] = "public.spr_license_dogovor111";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_number",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto10["m_sql"] = "\"public\".spr_license_dogovor.licdog_number";
$proto10["m_srcTableName"] = "public.spr_license_dogovor111";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_licnumber",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto12["m_sql"] = "\"public\".spr_license_dogovor.licdog_licnumber";
$proto12["m_srcTableName"] = "public.spr_license_dogovor111";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto14["m_sql"] = "\"public\".spr_po.po_name";
$proto14["m_srcTableName"] = "public.spr_license_dogovor111";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "public.spr_license_dogovor";
$proto17["m_srcTableName"] = "public.spr_license_dogovor111";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "licdog_id";
$proto17["m_columns"][] = "licdog_poname";
$proto17["m_columns"][] = "licdog_type";
$proto17["m_columns"][] = "licdog_quantity";
$proto17["m_columns"][] = "licdog_number";
$proto17["m_columns"][] = "licdog_date";
$proto17["m_columns"][] = "licdog_contractor";
$proto17["m_columns"][] = "licdog_desc";
$proto17["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "\"public\".spr_license_dogovor";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "public.spr_license_dogovor111";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
												$proto20=array();
$proto20["m_link"] = "SQLL_INNERJOIN";
			$proto21=array();
$proto21["m_strName"] = "public.spr_po";
$proto21["m_srcTableName"] = "public.spr_license_dogovor111";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "po_id";
$proto21["m_columns"][] = "po_name";
$proto21["m_columns"][] = "po_desc";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "INNER JOIN \"public\".spr_po ON \"public\".spr_po.po_id = \"public\".spr_license_dogovor.licdog_poname";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.spr_license_dogovor111";
$proto22=array();
$proto22["m_sql"] = "\"public\".spr_po.po_id = \"public\".spr_license_dogovor.licdog_poname";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "po_id",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_license_dogovor111"
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "= \"public\".spr_license_dogovor.licdog_poname";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_license_dogovor111";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_license_dogovor111 = createSqlQuery_spr_license_dogovor111();


	
		;

					

$tdataspr_license_dogovor111[".sqlquery"] = $queryData_spr_license_dogovor111;



$tableEvents["public.spr_license_dogovor111"] = new eventsBase;
$tdataspr_license_dogovor111[".hasEvents"] = false;

?>