<?php
$tdataspr_license_dogovor11 = array();
$tdataspr_license_dogovor11[".searchableFields"] = array();
$tdataspr_license_dogovor11[".ShortName"] = "spr_license_dogovor11";
$tdataspr_license_dogovor11[".OwnerID"] = "";
$tdataspr_license_dogovor11[".OriginalTable"] = "public.spr_license_dogovor";


$tdataspr_license_dogovor11[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_license_dogovor11[".originalPagesByType"] = $tdataspr_license_dogovor11[".pagesByType"];
$tdataspr_license_dogovor11[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_license_dogovor11[".originalPages"] = $tdataspr_license_dogovor11[".pages"];
$tdataspr_license_dogovor11[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_license_dogovor11[".originalDefaultPages"] = $tdataspr_license_dogovor11[".defaultPages"];

//	field labels
$fieldLabelsspr_license_dogovor11 = array();
$fieldToolTipsspr_license_dogovor11 = array();
$pageTitlesspr_license_dogovor11 = array();
$placeHoldersspr_license_dogovor11 = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_license_dogovor11["Russian"] = array();
	$fieldToolTipsspr_license_dogovor11["Russian"] = array();
	$placeHoldersspr_license_dogovor11["Russian"] = array();
	$pageTitlesspr_license_dogovor11["Russian"] = array();
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_id"] = "Licdog Id";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_id"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_id"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_poname"] = "Licdog Poname";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_poname"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_poname"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_type"] = "Licdog Type";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_type"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_type"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_quantity"] = "Licdog Quantity";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_quantity"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_quantity"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_number"] = "Licdog Number";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_number"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_number"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_date"] = "Licdog Date";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_date"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_date"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_contractor"] = "Licdog Contractor";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_contractor"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_contractor"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_desc"] = "Licdog Desc";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_desc"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_desc"] = "";
	$fieldLabelsspr_license_dogovor11["Russian"]["licdog_licnumber"] = "Licdog Licnumber";
	$fieldToolTipsspr_license_dogovor11["Russian"]["licdog_licnumber"] = "";
	$placeHoldersspr_license_dogovor11["Russian"]["licdog_licnumber"] = "";
	if (count($fieldToolTipsspr_license_dogovor11["Russian"]))
		$tdataspr_license_dogovor11[".isUseToolTips"] = true;
}


	$tdataspr_license_dogovor11[".NCSearch"] = true;



$tdataspr_license_dogovor11[".shortTableName"] = "spr_license_dogovor11";
$tdataspr_license_dogovor11[".nSecOptions"] = 0;

$tdataspr_license_dogovor11[".mainTableOwnerID"] = "";
$tdataspr_license_dogovor11[".entityType"] = 1;
$tdataspr_license_dogovor11[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_license_dogovor11[".strOriginalTableName"] = "public.spr_license_dogovor";

	



$tdataspr_license_dogovor11[".showAddInPopup"] = false;

$tdataspr_license_dogovor11[".showEditInPopup"] = false;

$tdataspr_license_dogovor11[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_license_dogovor11[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataspr_license_dogovor11[".listAjax"] = false;
//	temporary
$tdataspr_license_dogovor11[".listAjax"] = false;

	$tdataspr_license_dogovor11[".audit"] = false;

	$tdataspr_license_dogovor11[".locking"] = false;


$pages = $tdataspr_license_dogovor11[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_license_dogovor11[".edit"] = true;
	$tdataspr_license_dogovor11[".afterEditAction"] = 1;
	$tdataspr_license_dogovor11[".closePopupAfterEdit"] = 1;
	$tdataspr_license_dogovor11[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_license_dogovor11[".add"] = true;
$tdataspr_license_dogovor11[".afterAddAction"] = 1;
$tdataspr_license_dogovor11[".closePopupAfterAdd"] = 1;
$tdataspr_license_dogovor11[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_license_dogovor11[".list"] = true;
}



$tdataspr_license_dogovor11[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_license_dogovor11[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_license_dogovor11[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_license_dogovor11[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_license_dogovor11[".printFriendly"] = true;
}



$tdataspr_license_dogovor11[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_license_dogovor11[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_license_dogovor11[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_license_dogovor11[".isUseAjaxSuggest"] = true;

$tdataspr_license_dogovor11[".rowHighlite"] = true;





$tdataspr_license_dogovor11[".ajaxCodeSnippetAdded"] = false;

$tdataspr_license_dogovor11[".buttonsAdded"] = false;

$tdataspr_license_dogovor11[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_license_dogovor11[".isUseTimeForSearch"] = false;


$tdataspr_license_dogovor11[".badgeColor"] = "E8926F";


$tdataspr_license_dogovor11[".allSearchFields"] = array();
$tdataspr_license_dogovor11[".filterFields"] = array();
$tdataspr_license_dogovor11[".requiredSearchFields"] = array();

$tdataspr_license_dogovor11[".googleLikeFields"] = array();
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_id";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_poname";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_type";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_quantity";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_number";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_date";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_contractor";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_desc";
$tdataspr_license_dogovor11[".googleLikeFields"][] = "licdog_licnumber";



$tdataspr_license_dogovor11[".tableType"] = "list";

$tdataspr_license_dogovor11[".printerPageOrientation"] = 0;
$tdataspr_license_dogovor11[".nPrinterPageScale"] = 100;

$tdataspr_license_dogovor11[".nPrinterSplitRecords"] = 40;

$tdataspr_license_dogovor11[".geocodingEnabled"] = false;










$tdataspr_license_dogovor11[".pageSize"] = 20;

$tdataspr_license_dogovor11[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_license_dogovor11[".strOrderBy"] = $tstrOrderBy;

$tdataspr_license_dogovor11[".orderindexes"] = array();


$tdataspr_license_dogovor11[".sqlHead"] = "SELECT licdog_id,  	licdog_poname,  	licdog_type,  	licdog_quantity,  	licdog_number,  	licdog_date,  	licdog_contractor,  	licdog_desc,  	licdog_licnumber";
$tdataspr_license_dogovor11[".sqlFrom"] = "FROM \"public\".spr_license_dogovor";
$tdataspr_license_dogovor11[".sqlWhereExpr"] = "";
$tdataspr_license_dogovor11[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_license_dogovor11[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_license_dogovor11[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_license_dogovor11[".highlightSearchResults"] = true;

$tableKeysspr_license_dogovor11 = array();
$tableKeysspr_license_dogovor11[] = "licdog_id";
$tdataspr_license_dogovor11[".Keys"] = $tableKeysspr_license_dogovor11;


$tdataspr_license_dogovor11[".hideMobileList"] = array();




//	licdog_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "licdog_id";
	$fdata["GoodName"] = "licdog_id";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "licdog_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_id"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_id";
//	licdog_poname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "licdog_poname";
	$fdata["GoodName"] = "licdog_poname";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_poname");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licdog_poname";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_poname";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_po";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "po_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "po_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_poname"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_poname";
//	licdog_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "licdog_type";
	$fdata["GoodName"] = "licdog_type";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_type");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_type"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_type";
//	licdog_quantity
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "licdog_quantity";
	$fdata["GoodName"] = "licdog_quantity";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_quantity");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licdog_quantity";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_quantity";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_quantity"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_quantity";
//	licdog_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "licdog_number";
	$fdata["GoodName"] = "licdog_number";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_number");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_number";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_number";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_number"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_number";
//	licdog_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "licdog_date";
	$fdata["GoodName"] = "licdog_date";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "licdog_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_date"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_date";
//	licdog_contractor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "licdog_contractor";
	$fdata["GoodName"] = "licdog_contractor";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_contractor");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_contractor";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_contractor";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_contractor"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_contractor";
//	licdog_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "licdog_desc";
	$fdata["GoodName"] = "licdog_desc";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_desc"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_desc";
//	licdog_licnumber
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "licdog_licnumber";
	$fdata["GoodName"] = "licdog_licnumber";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor11","licdog_licnumber");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_licnumber";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_licnumber";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor11["licdog_licnumber"] = $fdata;
		$tdataspr_license_dogovor11[".searchableFields"][] = "licdog_licnumber";


$tables_data["public.spr_license_dogovor11"]=&$tdataspr_license_dogovor11;
$field_labels["public_spr_license_dogovor11"] = &$fieldLabelsspr_license_dogovor11;
$fieldToolTips["public_spr_license_dogovor11"] = &$fieldToolTipsspr_license_dogovor11;
$placeHolders["public_spr_license_dogovor11"] = &$placeHoldersspr_license_dogovor11;
$page_titles["public_spr_license_dogovor11"] = &$pageTitlesspr_license_dogovor11;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_license_dogovor11"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_license_dogovor11"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_license_dogovor11()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "licdog_id,  	licdog_poname,  	licdog_type,  	licdog_quantity,  	licdog_number,  	licdog_date,  	licdog_contractor,  	licdog_desc,  	licdog_licnumber";
$proto0["m_strFrom"] = "FROM \"public\".spr_license_dogovor";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_id",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto6["m_sql"] = "licdog_id";
$proto6["m_srcTableName"] = "public.spr_license_dogovor11";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_poname",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto8["m_sql"] = "licdog_poname";
$proto8["m_srcTableName"] = "public.spr_license_dogovor11";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_type",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto10["m_sql"] = "licdog_type";
$proto10["m_srcTableName"] = "public.spr_license_dogovor11";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_quantity",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto12["m_sql"] = "licdog_quantity";
$proto12["m_srcTableName"] = "public.spr_license_dogovor11";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_number",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto14["m_sql"] = "licdog_number";
$proto14["m_srcTableName"] = "public.spr_license_dogovor11";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_date",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto16["m_sql"] = "licdog_date";
$proto16["m_srcTableName"] = "public.spr_license_dogovor11";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_contractor",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto18["m_sql"] = "licdog_contractor";
$proto18["m_srcTableName"] = "public.spr_license_dogovor11";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_desc",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto20["m_sql"] = "licdog_desc";
$proto20["m_srcTableName"] = "public.spr_license_dogovor11";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_licnumber",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor11"
));

$proto22["m_sql"] = "licdog_licnumber";
$proto22["m_srcTableName"] = "public.spr_license_dogovor11";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "public.spr_license_dogovor";
$proto25["m_srcTableName"] = "public.spr_license_dogovor11";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "licdog_id";
$proto25["m_columns"][] = "licdog_poname";
$proto25["m_columns"][] = "licdog_type";
$proto25["m_columns"][] = "licdog_quantity";
$proto25["m_columns"][] = "licdog_number";
$proto25["m_columns"][] = "licdog_date";
$proto25["m_columns"][] = "licdog_contractor";
$proto25["m_columns"][] = "licdog_desc";
$proto25["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "\"public\".spr_license_dogovor";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "public.spr_license_dogovor11";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_license_dogovor11";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_license_dogovor11 = createSqlQuery_spr_license_dogovor11();


	
		;

									

$tdataspr_license_dogovor11[".sqlquery"] = $queryData_spr_license_dogovor11;



$tableEvents["public.spr_license_dogovor11"] = new eventsBase;
$tdataspr_license_dogovor11[".hasEvents"] = false;

?>