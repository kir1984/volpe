<?php
$tdataspr_license_dogovor = array();
$tdataspr_license_dogovor[".searchableFields"] = array();
$tdataspr_license_dogovor[".ShortName"] = "spr_license_dogovor";
$tdataspr_license_dogovor[".OwnerID"] = "";
$tdataspr_license_dogovor[".OriginalTable"] = "public.spr_license_dogovor";


$tdataspr_license_dogovor[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_license_dogovor[".originalPagesByType"] = $tdataspr_license_dogovor[".pagesByType"];
$tdataspr_license_dogovor[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_license_dogovor[".originalPages"] = $tdataspr_license_dogovor[".pages"];
$tdataspr_license_dogovor[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_license_dogovor[".originalDefaultPages"] = $tdataspr_license_dogovor[".defaultPages"];

//	field labels
$fieldLabelsspr_license_dogovor = array();
$fieldToolTipsspr_license_dogovor = array();
$pageTitlesspr_license_dogovor = array();
$placeHoldersspr_license_dogovor = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_license_dogovor["Russian"] = array();
	$fieldToolTipsspr_license_dogovor["Russian"] = array();
	$placeHoldersspr_license_dogovor["Russian"] = array();
	$pageTitlesspr_license_dogovor["Russian"] = array();
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_id"] = "Licdog Id";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_id"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_id"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_poname"] = "Наименование";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_poname"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_poname"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_type"] = "Тип лицензии";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_type"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_type"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_quantity"] = "Количество";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_quantity"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_quantity"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_number"] = "Дог. №";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_number"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_number"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_date"] = "Дог. дата";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_date"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_date"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_contractor"] = "Поставщик";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_contractor"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_contractor"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_desc"] = "Описание";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_desc"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_desc"] = "";
	$fieldLabelsspr_license_dogovor["Russian"]["licdog_licnumber"] = "Номер лицензии";
	$fieldToolTipsspr_license_dogovor["Russian"]["licdog_licnumber"] = "";
	$placeHoldersspr_license_dogovor["Russian"]["licdog_licnumber"] = "";
	if (count($fieldToolTipsspr_license_dogovor["Russian"]))
		$tdataspr_license_dogovor[".isUseToolTips"] = true;
}


	$tdataspr_license_dogovor[".NCSearch"] = true;



$tdataspr_license_dogovor[".shortTableName"] = "spr_license_dogovor";
$tdataspr_license_dogovor[".nSecOptions"] = 0;

$tdataspr_license_dogovor[".mainTableOwnerID"] = "";
$tdataspr_license_dogovor[".entityType"] = 0;
$tdataspr_license_dogovor[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_license_dogovor[".strOriginalTableName"] = "public.spr_license_dogovor";

	



$tdataspr_license_dogovor[".showAddInPopup"] = false;

$tdataspr_license_dogovor[".showEditInPopup"] = false;

$tdataspr_license_dogovor[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_license_dogovor[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataspr_license_dogovor[".listAjax"] = false;
//	temporary
$tdataspr_license_dogovor[".listAjax"] = false;

	$tdataspr_license_dogovor[".audit"] = false;

	$tdataspr_license_dogovor[".locking"] = false;


$pages = $tdataspr_license_dogovor[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_license_dogovor[".edit"] = true;
	$tdataspr_license_dogovor[".afterEditAction"] = 1;
	$tdataspr_license_dogovor[".closePopupAfterEdit"] = 1;
	$tdataspr_license_dogovor[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_license_dogovor[".add"] = true;
$tdataspr_license_dogovor[".afterAddAction"] = 1;
$tdataspr_license_dogovor[".closePopupAfterAdd"] = 1;
$tdataspr_license_dogovor[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_license_dogovor[".list"] = true;
}



$tdataspr_license_dogovor[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_license_dogovor[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_license_dogovor[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_license_dogovor[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_license_dogovor[".printFriendly"] = true;
}



$tdataspr_license_dogovor[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_license_dogovor[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_license_dogovor[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_license_dogovor[".isUseAjaxSuggest"] = true;

$tdataspr_license_dogovor[".rowHighlite"] = true;





$tdataspr_license_dogovor[".ajaxCodeSnippetAdded"] = false;

$tdataspr_license_dogovor[".buttonsAdded"] = false;

$tdataspr_license_dogovor[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_license_dogovor[".isUseTimeForSearch"] = false;


$tdataspr_license_dogovor[".badgeColor"] = "d2af80";


$tdataspr_license_dogovor[".allSearchFields"] = array();
$tdataspr_license_dogovor[".filterFields"] = array();
$tdataspr_license_dogovor[".requiredSearchFields"] = array();

$tdataspr_license_dogovor[".googleLikeFields"] = array();
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_id";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_poname";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_type";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_quantity";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_number";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_date";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_contractor";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_desc";
$tdataspr_license_dogovor[".googleLikeFields"][] = "licdog_licnumber";



$tdataspr_license_dogovor[".tableType"] = "list";

$tdataspr_license_dogovor[".printerPageOrientation"] = 0;
$tdataspr_license_dogovor[".nPrinterPageScale"] = 100;

$tdataspr_license_dogovor[".nPrinterSplitRecords"] = 40;

$tdataspr_license_dogovor[".geocodingEnabled"] = false;










$tdataspr_license_dogovor[".pageSize"] = 20;

$tdataspr_license_dogovor[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_license_dogovor[".strOrderBy"] = $tstrOrderBy;

$tdataspr_license_dogovor[".orderindexes"] = array();


$tdataspr_license_dogovor[".sqlHead"] = "SELECT licdog_id,  	licdog_poname,  	licdog_type,  	licdog_quantity,  	licdog_number,  	licdog_date,  	licdog_contractor,  	licdog_desc,  	licdog_licnumber";
$tdataspr_license_dogovor[".sqlFrom"] = "FROM \"public\".spr_license_dogovor";
$tdataspr_license_dogovor[".sqlWhereExpr"] = "";
$tdataspr_license_dogovor[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_license_dogovor[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_license_dogovor[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_license_dogovor[".highlightSearchResults"] = true;

$tableKeysspr_license_dogovor = array();
$tableKeysspr_license_dogovor[] = "licdog_id";
$tdataspr_license_dogovor[".Keys"] = $tableKeysspr_license_dogovor;


$tdataspr_license_dogovor[".hideMobileList"] = array();




//	licdog_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "licdog_id";
	$fdata["GoodName"] = "licdog_id";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "licdog_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_id"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_id";
//	licdog_poname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "licdog_poname";
	$fdata["GoodName"] = "licdog_poname";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_poname");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licdog_poname";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_poname";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_po";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "po_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "po_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_poname"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_poname";
//	licdog_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "licdog_type";
	$fdata["GoodName"] = "licdog_type";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_type");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_type"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_type";
//	licdog_quantity
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "licdog_quantity";
	$fdata["GoodName"] = "licdog_quantity";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_quantity");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "licdog_quantity";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_quantity";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_quantity"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_quantity";
//	licdog_number
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "licdog_number";
	$fdata["GoodName"] = "licdog_number";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_number");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_number";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_number";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_number"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_number";
//	licdog_date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "licdog_date";
	$fdata["GoodName"] = "licdog_date";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_date");
	$fdata["FieldType"] = 135;

	
	
	
			

		$fdata["strField"] = "licdog_date";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_date";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_date"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_date";
//	licdog_contractor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "licdog_contractor";
	$fdata["GoodName"] = "licdog_contractor";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_contractor");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_contractor";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_contractor";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_contractor"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_contractor";
//	licdog_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "licdog_desc";
	$fdata["GoodName"] = "licdog_desc";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_desc"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_desc";
//	licdog_licnumber
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "licdog_licnumber";
	$fdata["GoodName"] = "licdog_licnumber";
	$fdata["ownerTable"] = "public.spr_license_dogovor";
	$fdata["Label"] = GetFieldLabel("public_spr_license_dogovor","licdog_licnumber");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "licdog_licnumber";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "licdog_licnumber";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license_dogovor["licdog_licnumber"] = $fdata;
		$tdataspr_license_dogovor[".searchableFields"][] = "licdog_licnumber";


$tables_data["public.spr_license_dogovor"]=&$tdataspr_license_dogovor;
$field_labels["public_spr_license_dogovor"] = &$fieldLabelsspr_license_dogovor;
$fieldToolTips["public_spr_license_dogovor"] = &$fieldToolTipsspr_license_dogovor;
$placeHolders["public_spr_license_dogovor"] = &$placeHoldersspr_license_dogovor;
$page_titles["public_spr_license_dogovor"] = &$pageTitlesspr_license_dogovor;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_license_dogovor"] = array();
//	public.spr_license
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.spr_license";
		$detailsParam["dOriginalTable"] = "public.spr_license";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "spr_license";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_spr_license");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_license_dogovor"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_license_dogovor"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_license_dogovor"][$dIndex]["masterKeys"][]="licdog_id";

				$detailsTablesData["public.spr_license_dogovor"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_license_dogovor"][$dIndex]["detailKeys"][]="lic_licdog";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_license_dogovor"] = array();



	
				$strOriginalDetailsTable="public.spr_po";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_po";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_po";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.spr_license_dogovor"][0] = $masterParams;
				$masterTablesData["public.spr_license_dogovor"][0]["masterKeys"] = array();
	$masterTablesData["public.spr_license_dogovor"][0]["masterKeys"][]="po_id";
				$masterTablesData["public.spr_license_dogovor"][0]["detailKeys"] = array();
	$masterTablesData["public.spr_license_dogovor"][0]["detailKeys"][]="licdog_poname";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_license_dogovor()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "licdog_id,  	licdog_poname,  	licdog_type,  	licdog_quantity,  	licdog_number,  	licdog_date,  	licdog_contractor,  	licdog_desc,  	licdog_licnumber";
$proto0["m_strFrom"] = "FROM \"public\".spr_license_dogovor";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_id",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto6["m_sql"] = "licdog_id";
$proto6["m_srcTableName"] = "public.spr_license_dogovor";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_poname",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto8["m_sql"] = "licdog_poname";
$proto8["m_srcTableName"] = "public.spr_license_dogovor";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_type",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto10["m_sql"] = "licdog_type";
$proto10["m_srcTableName"] = "public.spr_license_dogovor";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_quantity",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto12["m_sql"] = "licdog_quantity";
$proto12["m_srcTableName"] = "public.spr_license_dogovor";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_number",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto14["m_sql"] = "licdog_number";
$proto14["m_srcTableName"] = "public.spr_license_dogovor";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_date",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto16["m_sql"] = "licdog_date";
$proto16["m_srcTableName"] = "public.spr_license_dogovor";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_contractor",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto18["m_sql"] = "licdog_contractor";
$proto18["m_srcTableName"] = "public.spr_license_dogovor";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_desc",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto20["m_sql"] = "licdog_desc";
$proto20["m_srcTableName"] = "public.spr_license_dogovor";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "licdog_licnumber",
	"m_strTable" => "public.spr_license_dogovor",
	"m_srcTableName" => "public.spr_license_dogovor"
));

$proto22["m_sql"] = "licdog_licnumber";
$proto22["m_srcTableName"] = "public.spr_license_dogovor";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "public.spr_license_dogovor";
$proto25["m_srcTableName"] = "public.spr_license_dogovor";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "licdog_id";
$proto25["m_columns"][] = "licdog_poname";
$proto25["m_columns"][] = "licdog_type";
$proto25["m_columns"][] = "licdog_quantity";
$proto25["m_columns"][] = "licdog_number";
$proto25["m_columns"][] = "licdog_date";
$proto25["m_columns"][] = "licdog_contractor";
$proto25["m_columns"][] = "licdog_desc";
$proto25["m_columns"][] = "licdog_licnumber";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "\"public\".spr_license_dogovor";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "public.spr_license_dogovor";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_license_dogovor";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_license_dogovor = createSqlQuery_spr_license_dogovor();


	
		;

									

$tdataspr_license_dogovor[".sqlquery"] = $queryData_spr_license_dogovor;



$tableEvents["public.spr_license_dogovor"] = new eventsBase;
$tdataspr_license_dogovor[".hasEvents"] = false;

?>