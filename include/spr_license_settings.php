<?php
$tdataspr_license = array();
$tdataspr_license[".searchableFields"] = array();
$tdataspr_license[".ShortName"] = "spr_license";
$tdataspr_license[".OwnerID"] = "";
$tdataspr_license[".OriginalTable"] = "public.spr_license";


$tdataspr_license[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_license[".originalPagesByType"] = $tdataspr_license[".pagesByType"];
$tdataspr_license[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_license[".originalPages"] = $tdataspr_license[".pages"];
$tdataspr_license[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_license[".originalDefaultPages"] = $tdataspr_license[".defaultPages"];

//	field labels
$fieldLabelsspr_license = array();
$fieldToolTipsspr_license = array();
$pageTitlesspr_license = array();
$placeHoldersspr_license = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_license["Russian"] = array();
	$fieldToolTipsspr_license["Russian"] = array();
	$placeHoldersspr_license["Russian"] = array();
	$pageTitlesspr_license["Russian"] = array();
	$fieldLabelsspr_license["Russian"]["lic_id"] = "Lic Id";
	$fieldToolTipsspr_license["Russian"]["lic_id"] = "";
	$placeHoldersspr_license["Russian"]["lic_id"] = "";
	$fieldLabelsspr_license["Russian"]["lic_poname"] = "Наименование";
	$fieldToolTipsspr_license["Russian"]["lic_poname"] = "";
	$placeHoldersspr_license["Russian"]["lic_poname"] = "";
	$fieldLabelsspr_license["Russian"]["lic_type"] = "Тип";
	$fieldToolTipsspr_license["Russian"]["lic_type"] = "";
	$placeHoldersspr_license["Russian"]["lic_type"] = "";
	$fieldLabelsspr_license["Russian"]["lic_desc"] = "Описание";
	$fieldToolTipsspr_license["Russian"]["lic_desc"] = "";
	$placeHoldersspr_license["Russian"]["lic_desc"] = "";
	$fieldLabelsspr_license["Russian"]["lic_key"] = "Ключ";
	$fieldToolTipsspr_license["Russian"]["lic_key"] = "";
	$placeHoldersspr_license["Russian"]["lic_key"] = "";
	$fieldLabelsspr_license["Russian"]["lic_servicenumber"] = "Сервисный номер";
	$fieldToolTipsspr_license["Russian"]["lic_servicenumber"] = "";
	$placeHoldersspr_license["Russian"]["lic_servicenumber"] = "";
	$fieldLabelsspr_license["Russian"]["lic_licdog"] = "Лиц. договор";
	$fieldToolTipsspr_license["Russian"]["lic_licdog"] = "";
	$placeHoldersspr_license["Russian"]["lic_licdog"] = "";
	if (count($fieldToolTipsspr_license["Russian"]))
		$tdataspr_license[".isUseToolTips"] = true;
}


	$tdataspr_license[".NCSearch"] = true;



$tdataspr_license[".shortTableName"] = "spr_license";
$tdataspr_license[".nSecOptions"] = 0;

$tdataspr_license[".mainTableOwnerID"] = "";
$tdataspr_license[".entityType"] = 0;
$tdataspr_license[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_license[".strOriginalTableName"] = "public.spr_license";

	



$tdataspr_license[".showAddInPopup"] = false;

$tdataspr_license[".showEditInPopup"] = false;

$tdataspr_license[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_license[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_license[".listAjax"] = true;
//	temporary
$tdataspr_license[".listAjax"] = false;

	$tdataspr_license[".audit"] = true;

	$tdataspr_license[".locking"] = true;


$pages = $tdataspr_license[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_license[".edit"] = true;
	$tdataspr_license[".afterEditAction"] = 1;
	$tdataspr_license[".closePopupAfterEdit"] = 1;
	$tdataspr_license[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_license[".add"] = true;
$tdataspr_license[".afterAddAction"] = 1;
$tdataspr_license[".closePopupAfterAdd"] = 1;
$tdataspr_license[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_license[".list"] = true;
}



$tdataspr_license[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_license[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_license[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_license[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_license[".printFriendly"] = true;
}



$tdataspr_license[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_license[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_license[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_license[".isUseAjaxSuggest"] = true;

$tdataspr_license[".rowHighlite"] = true;





$tdataspr_license[".ajaxCodeSnippetAdded"] = false;

$tdataspr_license[".buttonsAdded"] = false;

$tdataspr_license[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_license[".isUseTimeForSearch"] = false;


$tdataspr_license[".badgeColor"] = "6da5c8";


$tdataspr_license[".allSearchFields"] = array();
$tdataspr_license[".filterFields"] = array();
$tdataspr_license[".requiredSearchFields"] = array();

$tdataspr_license[".googleLikeFields"] = array();
$tdataspr_license[".googleLikeFields"][] = "lic_id";
$tdataspr_license[".googleLikeFields"][] = "lic_poname";
$tdataspr_license[".googleLikeFields"][] = "lic_type";
$tdataspr_license[".googleLikeFields"][] = "lic_desc";
$tdataspr_license[".googleLikeFields"][] = "lic_key";
$tdataspr_license[".googleLikeFields"][] = "lic_servicenumber";
$tdataspr_license[".googleLikeFields"][] = "lic_licdog";



$tdataspr_license[".tableType"] = "list";

$tdataspr_license[".printerPageOrientation"] = 0;
$tdataspr_license[".nPrinterPageScale"] = 100;

$tdataspr_license[".nPrinterSplitRecords"] = 40;

$tdataspr_license[".geocodingEnabled"] = false;




$tdataspr_license[".isDisplayLoading"] = true;

$tdataspr_license[".isResizeColumns"] = true;





$tdataspr_license[".pageSize"] = 20;

$tdataspr_license[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_license[".strOrderBy"] = $tstrOrderBy;

$tdataspr_license[".orderindexes"] = array();


$tdataspr_license[".sqlHead"] = "SELECT lic_id,  	lic_poname,  	lic_type,  	lic_desc,  	lic_key,  	lic_servicenumber,  	lic_licdog";
$tdataspr_license[".sqlFrom"] = "FROM \"public\".spr_license";
$tdataspr_license[".sqlWhereExpr"] = "";
$tdataspr_license[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_license[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_license[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_license[".highlightSearchResults"] = true;

$tableKeysspr_license = array();
$tableKeysspr_license[] = "lic_id";
$tdataspr_license[".Keys"] = $tableKeysspr_license;


$tdataspr_license[".hideMobileList"] = array();




//	lic_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "lic_id";
	$fdata["GoodName"] = "lic_id";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "lic_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_id"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_id";
//	lic_poname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "lic_poname";
	$fdata["GoodName"] = "lic_poname";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_poname");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "lic_poname";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_poname";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_po";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "po_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "po_name";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
		$fdata["filterTotalFields"] = "lic_id";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_poname"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_poname";
//	lic_type
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "lic_type";
	$fdata["GoodName"] = "lic_type";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_type");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "lic_type";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_type";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "OEM";
	$edata["LookupValues"][] = "OLP";
	$edata["LookupValues"][] = "Устройство";
	$edata["LookupValues"][] = "Пользователь";

	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_type"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_type";
//	lic_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "lic_desc";
	$fdata["GoodName"] = "lic_desc";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "lic_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_desc"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_desc";
//	lic_key
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "lic_key";
	$fdata["GoodName"] = "lic_key";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_key");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "lic_key";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_key";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_key"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_key";
//	lic_servicenumber
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "lic_servicenumber";
	$fdata["GoodName"] = "lic_servicenumber";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_servicenumber");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "lic_servicenumber";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_servicenumber";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_servicenumber"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_servicenumber";
//	lic_licdog
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "lic_licdog";
	$fdata["GoodName"] = "lic_licdog";
	$fdata["ownerTable"] = "public.spr_license";
	$fdata["Label"] = GetFieldLabel("public_spr_license","lic_licdog");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "lic_licdog";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lic_licdog";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.spr_license_dogovor1111111";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "licdog_id";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "po_name || ' ' || licdog_licnumber  ";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_license["lic_licdog"] = $fdata;
		$tdataspr_license[".searchableFields"][] = "lic_licdog";


$tables_data["public.spr_license"]=&$tdataspr_license;
$field_labels["public_spr_license"] = &$fieldLabelsspr_license;
$fieldToolTips["public_spr_license"] = &$fieldToolTipsspr_license;
$placeHolders["public_spr_license"] = &$placeHoldersspr_license;
$page_titles["public_spr_license"] = &$pageTitlesspr_license;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_license"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_license"] = array();



	
				$strOriginalDetailsTable="public.spr_po";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_po";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_po";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.spr_license"][0] = $masterParams;
				$masterTablesData["public.spr_license"][0]["masterKeys"] = array();
	$masterTablesData["public.spr_license"][0]["masterKeys"][]="po_id";
				$masterTablesData["public.spr_license"][0]["detailKeys"] = array();
	$masterTablesData["public.spr_license"][0]["detailKeys"][]="lic_poname";
		
	
				$strOriginalDetailsTable="public.spr_license_dogovor";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.spr_license_dogovor";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "spr_license_dogovor";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.spr_license"][1] = $masterParams;
				$masterTablesData["public.spr_license"][1]["masterKeys"] = array();
	$masterTablesData["public.spr_license"][1]["masterKeys"][]="licdog_id";
				$masterTablesData["public.spr_license"][1]["detailKeys"] = array();
	$masterTablesData["public.spr_license"][1]["detailKeys"][]="lic_licdog";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_license()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "lic_id,  	lic_poname,  	lic_type,  	lic_desc,  	lic_key,  	lic_servicenumber,  	lic_licdog";
$proto0["m_strFrom"] = "FROM \"public\".spr_license";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_id",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto6["m_sql"] = "lic_id";
$proto6["m_srcTableName"] = "public.spr_license";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_poname",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto8["m_sql"] = "lic_poname";
$proto8["m_srcTableName"] = "public.spr_license";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_type",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto10["m_sql"] = "lic_type";
$proto10["m_srcTableName"] = "public.spr_license";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_desc",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto12["m_sql"] = "lic_desc";
$proto12["m_srcTableName"] = "public.spr_license";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_key",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto14["m_sql"] = "lic_key";
$proto14["m_srcTableName"] = "public.spr_license";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_servicenumber",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto16["m_sql"] = "lic_servicenumber";
$proto16["m_srcTableName"] = "public.spr_license";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "lic_licdog",
	"m_strTable" => "public.spr_license",
	"m_srcTableName" => "public.spr_license"
));

$proto18["m_sql"] = "lic_licdog";
$proto18["m_srcTableName"] = "public.spr_license";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "public.spr_license";
$proto21["m_srcTableName"] = "public.spr_license";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "lic_id";
$proto21["m_columns"][] = "lic_poname";
$proto21["m_columns"][] = "lic_type";
$proto21["m_columns"][] = "lic_desc";
$proto21["m_columns"][] = "lic_key";
$proto21["m_columns"][] = "lic_servicenumber";
$proto21["m_columns"][] = "lic_licdog";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "\"public\".spr_license";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.spr_license";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_license";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_license = createSqlQuery_spr_license();


	
		;

							

$tdataspr_license[".sqlquery"] = $queryData_spr_license;



$tableEvents["public.spr_license"] = new eventsBase;
$tdataspr_license[".hasEvents"] = false;

?>