<?php
$tdataspr_location = array();
$tdataspr_location[".searchableFields"] = array();
$tdataspr_location[".ShortName"] = "spr_location";
$tdataspr_location[".OwnerID"] = "";
$tdataspr_location[".OriginalTable"] = "public.spr_location";


$tdataspr_location[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_location[".originalPagesByType"] = $tdataspr_location[".pagesByType"];
$tdataspr_location[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_location[".originalPages"] = $tdataspr_location[".pages"];
$tdataspr_location[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_location[".originalDefaultPages"] = $tdataspr_location[".defaultPages"];

//	field labels
$fieldLabelsspr_location = array();
$fieldToolTipsspr_location = array();
$pageTitlesspr_location = array();
$placeHoldersspr_location = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_location["Russian"] = array();
	$fieldToolTipsspr_location["Russian"] = array();
	$placeHoldersspr_location["Russian"] = array();
	$pageTitlesspr_location["Russian"] = array();
	$fieldLabelsspr_location["Russian"]["location_id"] = "Location Id";
	$fieldToolTipsspr_location["Russian"]["location_id"] = "";
	$placeHoldersspr_location["Russian"]["location_id"] = "";
	$fieldLabelsspr_location["Russian"]["location_name"] = "Наименование";
	$fieldToolTipsspr_location["Russian"]["location_name"] = "";
	$placeHoldersspr_location["Russian"]["location_name"] = "";
	$fieldLabelsspr_location["Russian"]["location_description"] = "Описание";
	$fieldToolTipsspr_location["Russian"]["location_description"] = "";
	$placeHoldersspr_location["Russian"]["location_description"] = "";
	$fieldLabelsspr_location["Russian"]["location_plan"] = "План помещения";
	$fieldToolTipsspr_location["Russian"]["location_plan"] = "";
	$placeHoldersspr_location["Russian"]["location_plan"] = "";
	$fieldLabelsspr_location["Russian"]["location_storagestatus"] = "Статус склада";
	$fieldToolTipsspr_location["Russian"]["location_storagestatus"] = "";
	$placeHoldersspr_location["Russian"]["location_storagestatus"] = "";
	if (count($fieldToolTipsspr_location["Russian"]))
		$tdataspr_location[".isUseToolTips"] = true;
}


	$tdataspr_location[".NCSearch"] = true;



$tdataspr_location[".shortTableName"] = "spr_location";
$tdataspr_location[".nSecOptions"] = 0;

$tdataspr_location[".mainTableOwnerID"] = "";
$tdataspr_location[".entityType"] = 0;
$tdataspr_location[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_location[".strOriginalTableName"] = "public.spr_location";

	



$tdataspr_location[".showAddInPopup"] = false;

$tdataspr_location[".showEditInPopup"] = false;

$tdataspr_location[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_location[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_location[".listAjax"] = true;
//	temporary
$tdataspr_location[".listAjax"] = false;

	$tdataspr_location[".audit"] = true;

	$tdataspr_location[".locking"] = true;


$pages = $tdataspr_location[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_location[".edit"] = true;
	$tdataspr_location[".afterEditAction"] = 1;
	$tdataspr_location[".closePopupAfterEdit"] = 1;
	$tdataspr_location[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_location[".add"] = true;
$tdataspr_location[".afterAddAction"] = 1;
$tdataspr_location[".closePopupAfterAdd"] = 1;
$tdataspr_location[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_location[".list"] = true;
}



$tdataspr_location[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_location[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_location[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_location[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_location[".printFriendly"] = true;
}



$tdataspr_location[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_location[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_location[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_location[".isUseAjaxSuggest"] = true;

$tdataspr_location[".rowHighlite"] = true;





$tdataspr_location[".ajaxCodeSnippetAdded"] = false;

$tdataspr_location[".buttonsAdded"] = false;

$tdataspr_location[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_location[".isUseTimeForSearch"] = false;


$tdataspr_location[".badgeColor"] = "E8926F";


$tdataspr_location[".allSearchFields"] = array();
$tdataspr_location[".filterFields"] = array();
$tdataspr_location[".requiredSearchFields"] = array();

$tdataspr_location[".googleLikeFields"] = array();
$tdataspr_location[".googleLikeFields"][] = "location_id";
$tdataspr_location[".googleLikeFields"][] = "location_name";
$tdataspr_location[".googleLikeFields"][] = "location_description";
$tdataspr_location[".googleLikeFields"][] = "location_plan";
$tdataspr_location[".googleLikeFields"][] = "location_storagestatus";



$tdataspr_location[".tableType"] = "list";

$tdataspr_location[".printerPageOrientation"] = 0;
$tdataspr_location[".nPrinterPageScale"] = 100;

$tdataspr_location[".nPrinterSplitRecords"] = 40;

$tdataspr_location[".geocodingEnabled"] = false;





$tdataspr_location[".isResizeColumns"] = true;





$tdataspr_location[".pageSize"] = 20;

$tdataspr_location[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_location[".strOrderBy"] = $tstrOrderBy;

$tdataspr_location[".orderindexes"] = array();


$tdataspr_location[".sqlHead"] = "SELECT location_id,  	location_name,  	location_description,  	location_plan,  	location_storagestatus";
$tdataspr_location[".sqlFrom"] = "FROM \"public\".spr_location";
$tdataspr_location[".sqlWhereExpr"] = "";
$tdataspr_location[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_location[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_location[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_location[".highlightSearchResults"] = true;

$tableKeysspr_location = array();
$tableKeysspr_location[] = "location_id";
$tdataspr_location[".Keys"] = $tableKeysspr_location;


$tdataspr_location[".hideMobileList"] = array();




//	location_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "location_id";
	$fdata["GoodName"] = "location_id";
	$fdata["ownerTable"] = "public.spr_location";
	$fdata["Label"] = GetFieldLabel("public_spr_location","location_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "location_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_location["location_id"] = $fdata;
		$tdataspr_location[".searchableFields"][] = "location_id";
//	location_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "location_name";
	$fdata["GoodName"] = "location_name";
	$fdata["ownerTable"] = "public.spr_location";
	$fdata["Label"] = GetFieldLabel("public_spr_location","location_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "location_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_location["location_name"] = $fdata;
		$tdataspr_location[".searchableFields"][] = "location_name";
//	location_description
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "location_description";
	$fdata["GoodName"] = "location_description";
	$fdata["ownerTable"] = "public.spr_location";
	$fdata["Label"] = GetFieldLabel("public_spr_location","location_description");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "location_description";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location_description";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_location["location_description"] = $fdata;
		$tdataspr_location[".searchableFields"][] = "location_description";
//	location_plan
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "location_plan";
	$fdata["GoodName"] = "location_plan";
	$fdata["ownerTable"] = "public.spr_location";
	$fdata["Label"] = GetFieldLabel("public_spr_location","location_plan");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "location_plan";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location_plan";

	
	
				$fdata["UploadFolder"] = "plan";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "File-based Image");

	
	
				$vdata["ImageWidth"] = 160;
	$vdata["ImageHeight"] = 60;

			$vdata["multipleImgMode"] = 1;
	$vdata["maxImages"] = 0;

			$vdata["showGallery"] = true;
	$vdata["galleryMode"] = 2;
	$vdata["captionMode"] = 3;
	$vdata["captionField"] = "location_name";

	$vdata["imageBorder"] = 0;
	$vdata["imageFullWidth"] = 1;


	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
							$edata["acceptFileTypes"] = "bmp";
			$edata["acceptFileTypesHtml"] = ".bmp";
						$edata["acceptFileTypes"] .= "|gif";
			$edata["acceptFileTypesHtml"] .= ",.gif";
						$edata["acceptFileTypes"] .= "|jpg";
			$edata["acceptFileTypesHtml"] .= ",.jpg";
						$edata["acceptFileTypes"] .= "|png";
			$edata["acceptFileTypesHtml"] .= ",.png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 200;

				$edata["ResizeImage"] = true;
				$edata["NewSize"] = 1920;

	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_location["location_plan"] = $fdata;
		$tdataspr_location[".searchableFields"][] = "location_plan";
//	location_storagestatus
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "location_storagestatus";
	$fdata["GoodName"] = "location_storagestatus";
	$fdata["ownerTable"] = "public.spr_location";
	$fdata["Label"] = GetFieldLabel("public_spr_location","location_storagestatus");
	$fdata["FieldType"] = 11;

	
	
	
			

		$fdata["strField"] = "location_storagestatus";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "location_storagestatus";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
				$fdata["filterFormat"] = "Options list";
		$fdata["showCollapsed"] = false;

	
	
	
		$fdata["filterCheckedMessageType"] = "Text";
	$fdata["filterCheckedMessageText"] = "Checked";
	$fdata["filterUncheckedMessageType"] = "Text";
	$fdata["filterUncheckedMessageText"] = "Unchecked";

//end of Filters settings


	$tdataspr_location["location_storagestatus"] = $fdata;
		$tdataspr_location[".searchableFields"][] = "location_storagestatus";


$tables_data["public.spr_location"]=&$tdataspr_location;
$field_labels["public_spr_location"] = &$fieldLabelsspr_location;
$fieldToolTips["public_spr_location"] = &$fieldToolTipsspr_location;
$placeHolders["public_spr_location"] = &$placeHoldersspr_location;
$page_titles["public_spr_location"] = &$pageTitlesspr_location;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_location"] = array();
//	public.arm
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.arm";
		$detailsParam["dOriginalTable"] = "public.arm";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "arm";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_arm");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_location"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"][]="location_id";

				$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"][]="loc_id";
//	public.hw_switch
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.hw_switch";
		$detailsParam["dOriginalTable"] = "public.hw_switch";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "hw_switch";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_hw_switch");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_location"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"][]="location_id";

				$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"][]="sw_location";
//	public.cons_cartridge
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.cons_cartridge";
		$detailsParam["dOriginalTable"] = "public.cons_cartridge";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cons_cartridge";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_cons_cartridge");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_location"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["masterKeys"][]="location_id";

				$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_location"][$dIndex]["detailKeys"][]="cartridge_storage";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_location"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_location()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "location_id,  	location_name,  	location_description,  	location_plan,  	location_storagestatus";
$proto0["m_strFrom"] = "FROM \"public\".spr_location";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "location_id",
	"m_strTable" => "public.spr_location",
	"m_srcTableName" => "public.spr_location"
));

$proto6["m_sql"] = "location_id";
$proto6["m_srcTableName"] = "public.spr_location";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "location_name",
	"m_strTable" => "public.spr_location",
	"m_srcTableName" => "public.spr_location"
));

$proto8["m_sql"] = "location_name";
$proto8["m_srcTableName"] = "public.spr_location";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "location_description",
	"m_strTable" => "public.spr_location",
	"m_srcTableName" => "public.spr_location"
));

$proto10["m_sql"] = "location_description";
$proto10["m_srcTableName"] = "public.spr_location";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "location_plan",
	"m_strTable" => "public.spr_location",
	"m_srcTableName" => "public.spr_location"
));

$proto12["m_sql"] = "location_plan";
$proto12["m_srcTableName"] = "public.spr_location";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "location_storagestatus",
	"m_strTable" => "public.spr_location",
	"m_srcTableName" => "public.spr_location"
));

$proto14["m_sql"] = "location_storagestatus";
$proto14["m_srcTableName"] = "public.spr_location";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "public.spr_location";
$proto17["m_srcTableName"] = "public.spr_location";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "location_id";
$proto17["m_columns"][] = "location_name";
$proto17["m_columns"][] = "location_description";
$proto17["m_columns"][] = "location_plan";
$proto17["m_columns"][] = "location_storagestatus";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "\"public\".spr_location";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "public.spr_location";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_location";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_location = createSqlQuery_spr_location();


	
		;

					

$tdataspr_location[".sqlquery"] = $queryData_spr_location;



$tableEvents["public.spr_location"] = new eventsBase;
$tdataspr_location[".hasEvents"] = false;

?>