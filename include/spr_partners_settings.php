<?php
$tdataspr_partners = array();
$tdataspr_partners[".searchableFields"] = array();
$tdataspr_partners[".ShortName"] = "spr_partners";
$tdataspr_partners[".OwnerID"] = "";
$tdataspr_partners[".OriginalTable"] = "public.spr_partners";


$tdataspr_partners[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_partners[".originalPagesByType"] = $tdataspr_partners[".pagesByType"];
$tdataspr_partners[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_partners[".originalPages"] = $tdataspr_partners[".pages"];
$tdataspr_partners[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_partners[".originalDefaultPages"] = $tdataspr_partners[".defaultPages"];

//	field labels
$fieldLabelsspr_partners = array();
$fieldToolTipsspr_partners = array();
$pageTitlesspr_partners = array();
$placeHoldersspr_partners = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_partners["Russian"] = array();
	$fieldToolTipsspr_partners["Russian"] = array();
	$placeHoldersspr_partners["Russian"] = array();
	$pageTitlesspr_partners["Russian"] = array();
	$fieldLabelsspr_partners["Russian"]["partner_id"] = "Partner Id";
	$fieldToolTipsspr_partners["Russian"]["partner_id"] = "";
	$placeHoldersspr_partners["Russian"]["partner_id"] = "";
	$fieldLabelsspr_partners["Russian"]["partner_name"] = "Наименование";
	$fieldToolTipsspr_partners["Russian"]["partner_name"] = "";
	$placeHoldersspr_partners["Russian"]["partner_name"] = "";
	$fieldLabelsspr_partners["Russian"]["partner_adress"] = "Адрес";
	$fieldToolTipsspr_partners["Russian"]["partner_adress"] = "";
	$placeHoldersspr_partners["Russian"]["partner_adress"] = "";
	$fieldLabelsspr_partners["Russian"]["partner_telephone"] = "Телефон";
	$fieldToolTipsspr_partners["Russian"]["partner_telephone"] = "";
	$placeHoldersspr_partners["Russian"]["partner_telephone"] = "";
	$fieldLabelsspr_partners["Russian"]["partner_contact"] = "Контактное лицо";
	$fieldToolTipsspr_partners["Russian"]["partner_contact"] = "";
	$placeHoldersspr_partners["Russian"]["partner_contact"] = "";
	if (count($fieldToolTipsspr_partners["Russian"]))
		$tdataspr_partners[".isUseToolTips"] = true;
}


	$tdataspr_partners[".NCSearch"] = true;



$tdataspr_partners[".shortTableName"] = "spr_partners";
$tdataspr_partners[".nSecOptions"] = 0;

$tdataspr_partners[".mainTableOwnerID"] = "";
$tdataspr_partners[".entityType"] = 0;
$tdataspr_partners[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_partners[".strOriginalTableName"] = "public.spr_partners";

	



$tdataspr_partners[".showAddInPopup"] = false;

$tdataspr_partners[".showEditInPopup"] = false;

$tdataspr_partners[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_partners[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_partners[".listAjax"] = true;
//	temporary
$tdataspr_partners[".listAjax"] = false;

	$tdataspr_partners[".audit"] = true;

	$tdataspr_partners[".locking"] = true;


$pages = $tdataspr_partners[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_partners[".edit"] = true;
	$tdataspr_partners[".afterEditAction"] = 1;
	$tdataspr_partners[".closePopupAfterEdit"] = 1;
	$tdataspr_partners[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_partners[".add"] = true;
$tdataspr_partners[".afterAddAction"] = 1;
$tdataspr_partners[".closePopupAfterAdd"] = 1;
$tdataspr_partners[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_partners[".list"] = true;
}



$tdataspr_partners[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_partners[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_partners[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_partners[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_partners[".printFriendly"] = true;
}



$tdataspr_partners[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_partners[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_partners[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_partners[".isUseAjaxSuggest"] = true;

$tdataspr_partners[".rowHighlite"] = true;





$tdataspr_partners[".ajaxCodeSnippetAdded"] = false;

$tdataspr_partners[".buttonsAdded"] = false;

$tdataspr_partners[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_partners[".isUseTimeForSearch"] = false;


$tdataspr_partners[".badgeColor"] = "4682B4";


$tdataspr_partners[".allSearchFields"] = array();
$tdataspr_partners[".filterFields"] = array();
$tdataspr_partners[".requiredSearchFields"] = array();

$tdataspr_partners[".googleLikeFields"] = array();
$tdataspr_partners[".googleLikeFields"][] = "partner_id";
$tdataspr_partners[".googleLikeFields"][] = "partner_name";
$tdataspr_partners[".googleLikeFields"][] = "partner_adress";
$tdataspr_partners[".googleLikeFields"][] = "partner_telephone";
$tdataspr_partners[".googleLikeFields"][] = "partner_contact";



$tdataspr_partners[".tableType"] = "list";

$tdataspr_partners[".printerPageOrientation"] = 0;
$tdataspr_partners[".nPrinterPageScale"] = 100;

$tdataspr_partners[".nPrinterSplitRecords"] = 40;

$tdataspr_partners[".geocodingEnabled"] = false;





$tdataspr_partners[".isResizeColumns"] = true;





$tdataspr_partners[".pageSize"] = 20;

$tdataspr_partners[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_partners[".strOrderBy"] = $tstrOrderBy;

$tdataspr_partners[".orderindexes"] = array();


$tdataspr_partners[".sqlHead"] = "SELECT partner_id,  	partner_name,  	partner_adress,  	partner_telephone,  	partner_contact";
$tdataspr_partners[".sqlFrom"] = "FROM \"public\".spr_partners";
$tdataspr_partners[".sqlWhereExpr"] = "";
$tdataspr_partners[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_partners[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_partners[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_partners[".highlightSearchResults"] = true;

$tableKeysspr_partners = array();
$tableKeysspr_partners[] = "partner_id";
$tdataspr_partners[".Keys"] = $tableKeysspr_partners;


$tdataspr_partners[".hideMobileList"] = array();




//	partner_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "partner_id";
	$fdata["GoodName"] = "partner_id";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("public_spr_partners","partner_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "partner_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partner_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_partners["partner_id"] = $fdata;
		$tdataspr_partners[".searchableFields"][] = "partner_id";
//	partner_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "partner_name";
	$fdata["GoodName"] = "partner_name";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("public_spr_partners","partner_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "partner_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partner_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_partners["partner_name"] = $fdata;
		$tdataspr_partners[".searchableFields"][] = "partner_name";
//	partner_adress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "partner_adress";
	$fdata["GoodName"] = "partner_adress";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("public_spr_partners","partner_adress");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "partner_adress";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partner_adress";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_partners["partner_adress"] = $fdata;
		$tdataspr_partners[".searchableFields"][] = "partner_adress";
//	partner_telephone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "partner_telephone";
	$fdata["GoodName"] = "partner_telephone";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("public_spr_partners","partner_telephone");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "partner_telephone";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partner_telephone";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_partners["partner_telephone"] = $fdata;
		$tdataspr_partners[".searchableFields"][] = "partner_telephone";
//	partner_contact
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "partner_contact";
	$fdata["GoodName"] = "partner_contact";
	$fdata["ownerTable"] = "public.spr_partners";
	$fdata["Label"] = GetFieldLabel("public_spr_partners","partner_contact");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "partner_contact";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partner_contact";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_partners["partner_contact"] = $fdata;
		$tdataspr_partners[".searchableFields"][] = "partner_contact";


$tables_data["public.spr_partners"]=&$tdataspr_partners;
$field_labels["public_spr_partners"] = &$fieldLabelsspr_partners;
$fieldToolTips["public_spr_partners"] = &$fieldToolTipsspr_partners;
$placeHolders["public_spr_partners"] = &$placeHoldersspr_partners;
$page_titles["public_spr_partners"] = &$pageTitlesspr_partners;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_partners"] = array();
//	public.doc_in
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_in";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_in";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_in");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_partners"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

				$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="doc_source";

		
	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="doc_destination";
//	public.doc_int
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_int";
		$detailsParam["dOriginalTable"] = "public.doc_int";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_int";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_int");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_partners"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

				$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="doc_destination";
//	public.doc_out
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.doc_out";
		$detailsParam["dOriginalTable"] = "public.doc_out";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "doc_out";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_doc_out");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_partners"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

				$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="doc_destination";
//	report_docin
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="report_docin";
		$detailsParam["dOriginalTable"] = "public.doc_in";



		$detailsParam["dType"]=PAGE_REPORT;
	
		$detailsParam["dShortTable"] = "report_docin";
	$detailsParam["dCaptionTable"] = GetTableCaption("report_docin");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_partners"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

				$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="doc_source";
//	public.service_refilling
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.service_refilling";
		$detailsParam["dOriginalTable"] = "public.service_refilling";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "service_refilling";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_service_refilling");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_partners"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["masterKeys"][]="partner_id";

				$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_partners"][$dIndex]["detailKeys"][]="ref_partner";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_partners"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_partners()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "partner_id,  	partner_name,  	partner_adress,  	partner_telephone,  	partner_contact";
$proto0["m_strFrom"] = "FROM \"public\".spr_partners";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_id",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "public.spr_partners"
));

$proto6["m_sql"] = "partner_id";
$proto6["m_srcTableName"] = "public.spr_partners";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_name",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "public.spr_partners"
));

$proto8["m_sql"] = "partner_name";
$proto8["m_srcTableName"] = "public.spr_partners";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_adress",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "public.spr_partners"
));

$proto10["m_sql"] = "partner_adress";
$proto10["m_srcTableName"] = "public.spr_partners";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_telephone",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "public.spr_partners"
));

$proto12["m_sql"] = "partner_telephone";
$proto12["m_srcTableName"] = "public.spr_partners";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "partner_contact",
	"m_strTable" => "public.spr_partners",
	"m_srcTableName" => "public.spr_partners"
));

$proto14["m_sql"] = "partner_contact";
$proto14["m_srcTableName"] = "public.spr_partners";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "public.spr_partners";
$proto17["m_srcTableName"] = "public.spr_partners";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "partner_id";
$proto17["m_columns"][] = "partner_name";
$proto17["m_columns"][] = "partner_adress";
$proto17["m_columns"][] = "partner_telephone";
$proto17["m_columns"][] = "partner_contact";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "\"public\".spr_partners";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "public.spr_partners";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_partners";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_partners = createSqlQuery_spr_partners();


	
		;

					

$tdataspr_partners[".sqlquery"] = $queryData_spr_partners;



$tableEvents["public.spr_partners"] = new eventsBase;
$tdataspr_partners[".hasEvents"] = false;

?>