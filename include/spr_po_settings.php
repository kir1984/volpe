<?php
$tdataspr_po = array();
$tdataspr_po[".searchableFields"] = array();
$tdataspr_po[".ShortName"] = "spr_po";
$tdataspr_po[".OwnerID"] = "";
$tdataspr_po[".OriginalTable"] = "public.spr_po";


$tdataspr_po[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_po[".originalPagesByType"] = $tdataspr_po[".pagesByType"];
$tdataspr_po[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_po[".originalPages"] = $tdataspr_po[".pages"];
$tdataspr_po[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_po[".originalDefaultPages"] = $tdataspr_po[".defaultPages"];

//	field labels
$fieldLabelsspr_po = array();
$fieldToolTipsspr_po = array();
$pageTitlesspr_po = array();
$placeHoldersspr_po = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_po["Russian"] = array();
	$fieldToolTipsspr_po["Russian"] = array();
	$placeHoldersspr_po["Russian"] = array();
	$pageTitlesspr_po["Russian"] = array();
	$fieldLabelsspr_po["Russian"]["po_id"] = "Po Id";
	$fieldToolTipsspr_po["Russian"]["po_id"] = "";
	$placeHoldersspr_po["Russian"]["po_id"] = "";
	$fieldLabelsspr_po["Russian"]["po_name"] = "Наименование";
	$fieldToolTipsspr_po["Russian"]["po_name"] = "";
	$placeHoldersspr_po["Russian"]["po_name"] = "";
	$fieldLabelsspr_po["Russian"]["po_desc"] = "Описание";
	$fieldToolTipsspr_po["Russian"]["po_desc"] = "";
	$placeHoldersspr_po["Russian"]["po_desc"] = "";
	if (count($fieldToolTipsspr_po["Russian"]))
		$tdataspr_po[".isUseToolTips"] = true;
}


	$tdataspr_po[".NCSearch"] = true;



$tdataspr_po[".shortTableName"] = "spr_po";
$tdataspr_po[".nSecOptions"] = 0;

$tdataspr_po[".mainTableOwnerID"] = "";
$tdataspr_po[".entityType"] = 0;
$tdataspr_po[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_po[".strOriginalTableName"] = "public.spr_po";

	



$tdataspr_po[".showAddInPopup"] = false;

$tdataspr_po[".showEditInPopup"] = false;

$tdataspr_po[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_po[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_po[".listAjax"] = true;
//	temporary
$tdataspr_po[".listAjax"] = false;

	$tdataspr_po[".audit"] = true;

	$tdataspr_po[".locking"] = true;


$pages = $tdataspr_po[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_po[".edit"] = true;
	$tdataspr_po[".afterEditAction"] = 1;
	$tdataspr_po[".closePopupAfterEdit"] = 1;
	$tdataspr_po[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_po[".add"] = true;
$tdataspr_po[".afterAddAction"] = 1;
$tdataspr_po[".closePopupAfterAdd"] = 1;
$tdataspr_po[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_po[".list"] = true;
}



$tdataspr_po[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_po[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_po[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_po[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_po[".printFriendly"] = true;
}



$tdataspr_po[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_po[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_po[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_po[".isUseAjaxSuggest"] = true;

$tdataspr_po[".rowHighlite"] = true;





$tdataspr_po[".ajaxCodeSnippetAdded"] = false;

$tdataspr_po[".buttonsAdded"] = false;

$tdataspr_po[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_po[".isUseTimeForSearch"] = false;


$tdataspr_po[".badgeColor"] = "9ACD32";


$tdataspr_po[".allSearchFields"] = array();
$tdataspr_po[".filterFields"] = array();
$tdataspr_po[".requiredSearchFields"] = array();

$tdataspr_po[".googleLikeFields"] = array();
$tdataspr_po[".googleLikeFields"][] = "po_id";
$tdataspr_po[".googleLikeFields"][] = "po_name";
$tdataspr_po[".googleLikeFields"][] = "po_desc";



$tdataspr_po[".tableType"] = "list";

$tdataspr_po[".printerPageOrientation"] = 0;
$tdataspr_po[".nPrinterPageScale"] = 100;

$tdataspr_po[".nPrinterSplitRecords"] = 40;

$tdataspr_po[".geocodingEnabled"] = false;





$tdataspr_po[".isResizeColumns"] = true;





$tdataspr_po[".pageSize"] = 20;

$tdataspr_po[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_po[".strOrderBy"] = $tstrOrderBy;

$tdataspr_po[".orderindexes"] = array();


$tdataspr_po[".sqlHead"] = "SELECT po_id,  	po_name,  	po_desc";
$tdataspr_po[".sqlFrom"] = "FROM \"public\".spr_po";
$tdataspr_po[".sqlWhereExpr"] = "";
$tdataspr_po[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_po[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_po[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_po[".highlightSearchResults"] = true;

$tableKeysspr_po = array();
$tableKeysspr_po[] = "po_id";
$tdataspr_po[".Keys"] = $tableKeysspr_po;


$tdataspr_po[".hideMobileList"] = array();




//	po_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "po_id";
	$fdata["GoodName"] = "po_id";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("public_spr_po","po_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "po_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_po["po_id"] = $fdata;
		$tdataspr_po[".searchableFields"][] = "po_id";
//	po_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "po_name";
	$fdata["GoodName"] = "po_name";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("public_spr_po","po_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_po["po_name"] = $fdata;
		$tdataspr_po[".searchableFields"][] = "po_name";
//	po_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "po_desc";
	$fdata["GoodName"] = "po_desc";
	$fdata["ownerTable"] = "public.spr_po";
	$fdata["Label"] = GetFieldLabel("public_spr_po","po_desc");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "po_desc";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "po_desc";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_po["po_desc"] = $fdata;
		$tdataspr_po[".searchableFields"][] = "po_desc";


$tables_data["public.spr_po"]=&$tdataspr_po;
$field_labels["public_spr_po"] = &$fieldLabelsspr_po;
$fieldToolTips["public_spr_po"] = &$fieldToolTipsspr_po;
$placeHolders["public_spr_po"] = &$placeHoldersspr_po;
$page_titles["public_spr_po"] = &$pageTitlesspr_po;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_po"] = array();
//	public.arm_po
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.arm_po";
		$detailsParam["dOriginalTable"] = "public.arm_po";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "arm_po";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_arm_po");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_po"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"][]="po_id";

				$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"][]="po_name";
//	public.spr_license
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.spr_license";
		$detailsParam["dOriginalTable"] = "public.spr_license";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "spr_license";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_spr_license");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_po"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"][]="po_id";

				$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"][]="lic_poname";
//	public.spr_license_dogovor
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.spr_license_dogovor";
		$detailsParam["dOriginalTable"] = "public.spr_license_dogovor";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "spr_license_dogovor";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_spr_license_dogovor");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_po"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["masterKeys"][]="po_id";

				$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_po"][$dIndex]["detailKeys"][]="licdog_poname";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_po"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_po()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "po_id,  	po_name,  	po_desc";
$proto0["m_strFrom"] = "FROM \"public\".spr_po";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "po_id",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_po"
));

$proto6["m_sql"] = "po_id";
$proto6["m_srcTableName"] = "public.spr_po";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "po_name",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_po"
));

$proto8["m_sql"] = "po_name";
$proto8["m_srcTableName"] = "public.spr_po";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "po_desc",
	"m_strTable" => "public.spr_po",
	"m_srcTableName" => "public.spr_po"
));

$proto10["m_sql"] = "po_desc";
$proto10["m_srcTableName"] = "public.spr_po";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "public.spr_po";
$proto13["m_srcTableName"] = "public.spr_po";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "po_id";
$proto13["m_columns"][] = "po_name";
$proto13["m_columns"][] = "po_desc";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "\"public\".spr_po";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "public.spr_po";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_po";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_po = createSqlQuery_spr_po();


	
		;

			

$tdataspr_po[".sqlquery"] = $queryData_spr_po;



$tableEvents["public.spr_po"] = new eventsBase;
$tdataspr_po[".hasEvents"] = false;

?>