<?php
$tdataspr_socket = array();
$tdataspr_socket[".searchableFields"] = array();
$tdataspr_socket[".ShortName"] = "spr_socket";
$tdataspr_socket[".OwnerID"] = "";
$tdataspr_socket[".OriginalTable"] = "public.spr_socket";


$tdataspr_socket[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataspr_socket[".originalPagesByType"] = $tdataspr_socket[".pagesByType"];
$tdataspr_socket[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"masterlist\":[\"masterlist\"],\"masterprint\":[\"masterprint\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataspr_socket[".originalPages"] = $tdataspr_socket[".pages"];
$tdataspr_socket[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"masterlist\":\"masterlist\",\"masterprint\":\"masterprint\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataspr_socket[".originalDefaultPages"] = $tdataspr_socket[".defaultPages"];

//	field labels
$fieldLabelsspr_socket = array();
$fieldToolTipsspr_socket = array();
$pageTitlesspr_socket = array();
$placeHoldersspr_socket = array();

if(mlang_getcurrentlang()=="Russian")
{
	$fieldLabelsspr_socket["Russian"] = array();
	$fieldToolTipsspr_socket["Russian"] = array();
	$placeHoldersspr_socket["Russian"] = array();
	$pageTitlesspr_socket["Russian"] = array();
	$fieldLabelsspr_socket["Russian"]["socket_id"] = "Socket Id";
	$fieldToolTipsspr_socket["Russian"]["socket_id"] = "";
	$placeHoldersspr_socket["Russian"]["socket_id"] = "";
	$fieldLabelsspr_socket["Russian"]["socket_name"] = "Розетка";
	$fieldToolTipsspr_socket["Russian"]["socket_name"] = "";
	$placeHoldersspr_socket["Russian"]["socket_name"] = "";
	$fieldLabelsspr_socket["Russian"]["patchpanel_level"] = "Этаж патчпанели";
	$fieldToolTipsspr_socket["Russian"]["patchpanel_level"] = "";
	$placeHoldersspr_socket["Russian"]["patchpanel_level"] = "";
	$fieldLabelsspr_socket["Russian"]["patchpanel_name"] = "Имя патчпанели";
	$fieldToolTipsspr_socket["Russian"]["patchpanel_name"] = "";
	$placeHoldersspr_socket["Russian"]["patchpanel_name"] = "";
	$fieldLabelsspr_socket["Russian"]["patchpanel_port"] = "Парт на патчпанели";
	$fieldToolTipsspr_socket["Russian"]["patchpanel_port"] = "";
	$placeHoldersspr_socket["Russian"]["patchpanel_port"] = "";
	$fieldLabelsspr_socket["Russian"]["switch_port"] = "Порт на коммутаторе";
	$fieldToolTipsspr_socket["Russian"]["switch_port"] = "";
	$placeHoldersspr_socket["Russian"]["switch_port"] = "";
	$fieldLabelsspr_socket["Russian"]["switch_id"] = "Коммутатор";
	$fieldToolTipsspr_socket["Russian"]["switch_id"] = "";
	$placeHoldersspr_socket["Russian"]["switch_id"] = "";
	if (count($fieldToolTipsspr_socket["Russian"]))
		$tdataspr_socket[".isUseToolTips"] = true;
}


	$tdataspr_socket[".NCSearch"] = true;



$tdataspr_socket[".shortTableName"] = "spr_socket";
$tdataspr_socket[".nSecOptions"] = 0;

$tdataspr_socket[".mainTableOwnerID"] = "";
$tdataspr_socket[".entityType"] = 0;
$tdataspr_socket[".connId"] = "itbase3_at_192_168_1_15";


$tdataspr_socket[".strOriginalTableName"] = "public.spr_socket";

	



$tdataspr_socket[".showAddInPopup"] = false;

$tdataspr_socket[".showEditInPopup"] = false;

$tdataspr_socket[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataspr_socket[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


	$tdataspr_socket[".listAjax"] = true;
//	temporary
$tdataspr_socket[".listAjax"] = false;

	$tdataspr_socket[".audit"] = true;

	$tdataspr_socket[".locking"] = true;


$pages = $tdataspr_socket[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataspr_socket[".edit"] = true;
	$tdataspr_socket[".afterEditAction"] = 1;
	$tdataspr_socket[".closePopupAfterEdit"] = 1;
	$tdataspr_socket[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataspr_socket[".add"] = true;
$tdataspr_socket[".afterAddAction"] = 1;
$tdataspr_socket[".closePopupAfterAdd"] = 1;
$tdataspr_socket[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataspr_socket[".list"] = true;
}



$tdataspr_socket[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataspr_socket[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataspr_socket[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataspr_socket[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataspr_socket[".printFriendly"] = true;
}



$tdataspr_socket[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataspr_socket[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataspr_socket[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataspr_socket[".isUseAjaxSuggest"] = true;

$tdataspr_socket[".rowHighlite"] = true;





$tdataspr_socket[".ajaxCodeSnippetAdded"] = false;

$tdataspr_socket[".buttonsAdded"] = false;

$tdataspr_socket[".addPageEvents"] = false;

// use timepicker for search panel
$tdataspr_socket[".isUseTimeForSearch"] = false;


$tdataspr_socket[".badgeColor"] = "778899";


$tdataspr_socket[".allSearchFields"] = array();
$tdataspr_socket[".filterFields"] = array();
$tdataspr_socket[".requiredSearchFields"] = array();

$tdataspr_socket[".googleLikeFields"] = array();
$tdataspr_socket[".googleLikeFields"][] = "socket_id";
$tdataspr_socket[".googleLikeFields"][] = "socket_name";
$tdataspr_socket[".googleLikeFields"][] = "patchpanel_level";
$tdataspr_socket[".googleLikeFields"][] = "patchpanel_name";
$tdataspr_socket[".googleLikeFields"][] = "patchpanel_port";
$tdataspr_socket[".googleLikeFields"][] = "switch_port";
$tdataspr_socket[".googleLikeFields"][] = "switch_id";



$tdataspr_socket[".tableType"] = "list";

$tdataspr_socket[".printerPageOrientation"] = 0;
$tdataspr_socket[".nPrinterPageScale"] = 100;

$tdataspr_socket[".nPrinterSplitRecords"] = 40;

$tdataspr_socket[".geocodingEnabled"] = false;





$tdataspr_socket[".isResizeColumns"] = true;





$tdataspr_socket[".pageSize"] = 20;

$tdataspr_socket[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataspr_socket[".strOrderBy"] = $tstrOrderBy;

$tdataspr_socket[".orderindexes"] = array();


$tdataspr_socket[".sqlHead"] = "SELECT socket_id,  	socket_name,  	patchpanel_level,  	patchpanel_name,  	patchpanel_port,  	switch_port,  	switch_id";
$tdataspr_socket[".sqlFrom"] = "FROM \"public\".spr_socket";
$tdataspr_socket[".sqlWhereExpr"] = "";
$tdataspr_socket[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataspr_socket[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataspr_socket[".arrGroupsPerPage"] = $arrGPP;

$tdataspr_socket[".highlightSearchResults"] = true;

$tableKeysspr_socket = array();
$tableKeysspr_socket[] = "socket_id";
$tdataspr_socket[".Keys"] = $tableKeysspr_socket;


$tdataspr_socket[".hideMobileList"] = array();




//	socket_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "socket_id";
	$fdata["GoodName"] = "socket_id";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","socket_id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			

		$fdata["strField"] = "socket_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "socket_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["socket_id"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "socket_id";
//	socket_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "socket_name";
	$fdata["GoodName"] = "socket_name";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","socket_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "socket_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "socket_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["socket_name"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "socket_name";
//	patchpanel_level
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "patchpanel_level";
	$fdata["GoodName"] = "patchpanel_level";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","patchpanel_level");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "patchpanel_level";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "patchpanel_level";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["patchpanel_level"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "patchpanel_level";
//	patchpanel_name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "patchpanel_name";
	$fdata["GoodName"] = "patchpanel_name";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","patchpanel_name");
	$fdata["FieldType"] = 200;

	
	
	
			

		$fdata["strField"] = "patchpanel_name";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "patchpanel_name";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["patchpanel_name"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "patchpanel_name";
//	patchpanel_port
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "patchpanel_port";
	$fdata["GoodName"] = "patchpanel_port";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","patchpanel_port");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "patchpanel_port";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "patchpanel_port";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["patchpanel_port"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "patchpanel_port";
//	switch_port
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "switch_port";
	$fdata["GoodName"] = "switch_port";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","switch_port");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "switch_port";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "switch_port";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["switch_port"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "switch_port";
//	switch_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "switch_id";
	$fdata["GoodName"] = "switch_id";
	$fdata["ownerTable"] = "public.spr_socket";
	$fdata["Label"] = GetFieldLabel("public_spr_socket","switch_id");
	$fdata["FieldType"] = 3;

	
	
	
			

		$fdata["strField"] = "switch_id";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "switch_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.hw_switch";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "sw_id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "sw_name || ' ' || sw_ip";

	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataspr_socket["switch_id"] = $fdata;
		$tdataspr_socket[".searchableFields"][] = "switch_id";


$tables_data["public.spr_socket"]=&$tdataspr_socket;
$field_labels["public_spr_socket"] = &$fieldLabelsspr_socket;
$fieldToolTips["public_spr_socket"] = &$fieldToolTipsspr_socket;
$placeHolders["public_spr_socket"] = &$placeHoldersspr_socket;
$page_titles["public_spr_socket"] = &$pageTitlesspr_socket;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.spr_socket"] = array();
//	public.arm
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="public.arm";
		$detailsParam["dOriginalTable"] = "public.arm";



		
		$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "arm";
	$detailsParam["dCaptionTable"] = GetTableCaption("public_arm");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();


		
	$detailsTablesData["public.spr_socket"][$dIndex] = $detailsParam;

	
		$detailsTablesData["public.spr_socket"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["public.spr_socket"][$dIndex]["masterKeys"][]="socket_id";

				$detailsTablesData["public.spr_socket"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["public.spr_socket"][$dIndex]["detailKeys"][]="socket_id";

// tables which are master tables for current table (detail)
$masterTablesData["public.spr_socket"] = array();



	
				$strOriginalDetailsTable="public.hw_switch";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="public.hw_switch";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "hw_switch";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["public.spr_socket"][0] = $masterParams;
				$masterTablesData["public.spr_socket"][0]["masterKeys"] = array();
	$masterTablesData["public.spr_socket"][0]["masterKeys"][]="sw_id";
				$masterTablesData["public.spr_socket"][0]["detailKeys"] = array();
	$masterTablesData["public.spr_socket"][0]["detailKeys"][]="switch_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_spr_socket()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "socket_id,  	socket_name,  	patchpanel_level,  	patchpanel_name,  	patchpanel_port,  	switch_port,  	switch_id";
$proto0["m_strFrom"] = "FROM \"public\".spr_socket";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "socket_id",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto6["m_sql"] = "socket_id";
$proto6["m_srcTableName"] = "public.spr_socket";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "socket_name",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto8["m_sql"] = "socket_name";
$proto8["m_srcTableName"] = "public.spr_socket";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "patchpanel_level",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto10["m_sql"] = "patchpanel_level";
$proto10["m_srcTableName"] = "public.spr_socket";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "patchpanel_name",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto12["m_sql"] = "patchpanel_name";
$proto12["m_srcTableName"] = "public.spr_socket";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "patchpanel_port",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto14["m_sql"] = "patchpanel_port";
$proto14["m_srcTableName"] = "public.spr_socket";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "switch_port",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto16["m_sql"] = "switch_port";
$proto16["m_srcTableName"] = "public.spr_socket";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "switch_id",
	"m_strTable" => "public.spr_socket",
	"m_srcTableName" => "public.spr_socket"
));

$proto18["m_sql"] = "switch_id";
$proto18["m_srcTableName"] = "public.spr_socket";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "public.spr_socket";
$proto21["m_srcTableName"] = "public.spr_socket";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "socket_id";
$proto21["m_columns"][] = "socket_name";
$proto21["m_columns"][] = "patchpanel_level";
$proto21["m_columns"][] = "patchpanel_name";
$proto21["m_columns"][] = "patchpanel_port";
$proto21["m_columns"][] = "switch_port";
$proto21["m_columns"][] = "switch_id";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "\"public\".spr_socket";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.spr_socket";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.spr_socket";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_spr_socket = createSqlQuery_spr_socket();


	
		;

							

$tdataspr_socket[".sqlquery"] = $queryData_spr_socket;



$tableEvents["public.spr_socket"] = new eventsBase;
$tdataspr_socket[".hasEvents"] = false;

?>